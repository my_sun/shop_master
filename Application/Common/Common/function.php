<?php
/**
向客户端发送数据包
 */
function Push_data($arr=array()){
	$resarr = array();
	$resarr["message"] = $arr["message"] ? $arr["message"] : "ok";
	$resarr["code"] = $arr["code"] ? $arr["code"] : ERRORCODE_200;
	$resarr["data"] = $arr["data"] ? $arr["data"] : array();
	//$resarr["test"]["post"] = $_POST;
	//$resarr["test"]["j"] = Den_data($_POST["j"]);
	//$resarr["test"]["file"] = json_encode($_FILES);
    writelogs($resarr);
	if($_REQUEST["test"]==1||$_REQUEST["ptest"]==1||MODULE_NAME=="Adms"){
		Dump($resarr);
	}else{
	    echo En_data($resarr);
	}
	exit;
}
/*
 * 接收来自客户端的数据包
 */
function Recive_data($post){
    if($_REQUEST["test"]!=1){
        $post = Den_data($post["j"]);
    }
    if($post){
        if(!is_array($post)){
            $post = jsonToArr($post);
            return $post;
        }elseif(is_object($post)){
            $post = object_array($post);
            return $post;
        }elseif(is_array($post)){
            foreach($post as $k => $v){
                if(is_json($v)){
                    $post[$k] = strpos($v,'{') ? json_decode($v):$v;
                    if(is_object($post[$k]))
                        $post[$k] = object_array($post[$k]);
                }
            }

            return $post;
        }else{
            Push_data(array('message'=>'post data error','code'=>'201'));
        }
    }else{
        //Push_data(array('msg'=>'no post data','error'=>'1'));
    }
}
function jsonToArr($post){

    $post = object_array(json_decode($post));
    foreach($post as $k => &$v){
        //var_dump($k.'-------'.!empty(strpos($v,'}')));
        if(!empty(strpos($v,'}'))){
            $post[$k] = json_decode($v,true);

            if(is_object($post[$k]))
                $post[$k] = object_array($post[$k]);
        }
        $post[$k] = $v;

    }
    return $post;
}
/*
 * 递归数组
 */
function utf8_encode_arr($a) {
    foreach ($a as $k=>$val) {
        if (is_array($val)) { //如果键值是数组，则进行函数递归调用
            $a[$k]=utf8_encode_arr($val);
        } else { // 如果键值是数值，则进行输出
            //echo "$val<br />";
            $a[$k]=mb_convert_encoding($val,'utf-8',mb_detect_encoding($val));//utf8_encode($val);
        } //end if
        
    } //end foreach
    return $a;
}
/**
 加密
 */
function En_data($resarr=array()){
   
    $str = json_encode(utf8_encode_arr($resarr));
    $str = base64_encode($str);
    if($_REQUEST["ctest"]==1){
        echo "<br />";
        echo $str;
        echo "<br />"; 
        echo json_encode(utf8_encode_arr($resarr));
        Dump(json_last_error());
        Dump(json_encode($resarr));
        Dump($resarr);
    }
    $strLenth = strlen($str);
    if($strLenth>9){
        $max = 9;
    }else{
        $max = $strLenth-1;
    }
    $sing = rand(1,$max);
    
    $strsing = md5(gmdate('Ym').$sing);
    $str = str_insert($str,$sing,$strsing);
    $str = base64_encode($str.$sing);
    
    return $str;
}
/**
 解密
 */
function Den_data($str){
    $str = base64_decode($str);
    $sing = substr($str, -1); // 返回 "ef"
    $str = substr($str, 0, -1); 
    $strsing = md5(gmdate('Ym').$sing);
    $strsing2 = md5(date('Ym').$sing);
    $str = str_replace($strsing,"",$str);
    $str = str_replace($strsing2,"",$str);
    $str = base64_decode($str);
    
    return $str;
}
function str_insert($str, $i, $substr)
{
    for($j=0; $j<$i; $j++){
        $startstr .= $str[$j];
    }
    for ($j=$i; $j<strlen($str); $j++){
        $laststr .= $str[$j];
    }
    $str = ($startstr . $substr . $laststr);
    return $str;
}
/**
 google FCMPush
 */
function FCMPush($Title,$Content,$Token,$product) {
    //hotelB2B
    $API_ACCESS_KEY=C("FCM_API_KEY")[$product];
    
    $msg = array(
        'body'  =>  $Content,
        'title' => $Title,
        'icon'  => 'myicon',/*Default Icon*/
        'sound' => 'mySound'/*Default sound*/
    );
    
    $fields = array(
        'to' => $Token,
        'notification' => $msg);
    
    $headers = array(
        'Authorization: key=' . $API_ACCESS_KEY,
        'Content-Type: application/json'
    );
    
    $ch = curl_init();
    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
    curl_setopt( $ch,CURLOPT_POST, true );
    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
    $result = curl_exec($ch );
    curl_close( $ch );
}
/**
查询google订单状态
 */
function PayStatus($packageName,$productId,$token,$product,$isauto=0){

		require_once WR.'/lib/vendor/autoload.php';

		$credentials_file=WR."/lib/googlekey/".$product.".json";
		$orderjson=M('order_switch')->where(array('product'=>$product))->getField('orderjson');
	try{  
		$client = new Google_Client(); 
		$client->setAuthConfig($orderjson);
		$client->setApplicationName($packageName);
		$client->setScopes(array('https://www.googleapis.com/auth/androidpublisher'));
		$service = new Google_Service_AndroidPublisher($client);
		
		$optps=array();
		if($isauto==1){
			$resp = $service->purchases_subscriptions->get( $packageName, $productId, $token, $optps );
		}else{
			$resp = $service->purchases_products->get( $packageName, $productId, $token, $optps );
		}
		$resp = (array)$resp;
		return $resp;
	}catch(Exception $e){  
	    if($_GET["test"]==2){
	        echo $credentials_file;
	        echo file_get_contents($credentials_file);
	        Dump($e->getMessage());exit;
	    }
	    //echo $e->getCode(),'|',$e->getMessage();  
		return $e->getCode();
		//echo $e->getCode(),'|',$e->getMessage();  
	} 
}
/**
 * 获取指定行内容
 *
 * @param $file 文件路径
 * @param $line 行数
 * @param $length 指定行返回内容长度
 */
function getLine($file, $line, $length = 4096){
    $returnTxt = null; // 初始化返回
    $i = 1; // 行数
 
    $handle = @fopen($file, "r");
    if ($handle) {
        while (!feof($handle)) {
            $buffer = fgets($handle, $length);
            if($line == $i) $returnTxt = $buffer;
            $i++;
        }
        fclose($handle);
    }
    return $returnTxt;
}
function LM_sendmsg($sendmsg){
    $address = C("MSGSERVER");
    
    $socket = @stream_socket_client("tcp://" . $address, $errno, $errstr, 60, STREAM_CLIENT_CONNECT);
    if (!$socket) {
        return false;
    }
   
    stream_set_timeout($socket, 10);
    stream_set_blocking($socket, 0);
    $i = 0;
    $array["u"]=$sendmsg["msgbody"]["user"]["uid"];
    $array["p"]="123456";
    $array["m"]="20210";
    $buffer = "";
    $buffer ="LM".base64_encode(json_encode($array));
    $i  =  strlen($buffer);
    $head = "   ";
    $head{0} = chr(0x01);
    $head{1} = chr($i%256);
    $head{2} = chr($i/256%256);
    $headstrlen = strlen($head);
    
    fwrite($socket, $head.$buffer);
    $string = LM_read(3,$socket);
    $sing = LM_chrTohex($string{0});
    
    if($sing == 0x02){
        //Dump($sendmsg);
        $i = 0;
        $buffer = "";
        $buffer = "LM".base64_encode(json_encode($sendmsg));
        $i  =  strlen($buffer);
        $head = "   ";
        $head{0} = chr(0x03);
        $head{1} = chr($i%256);
        $head{2} = chr($i/256%256);
        fwrite($socket, $head.$buffer);
        fclose($socket);
        return true;
    }else{
        return false;
    }
    return false;
}
function LM_chrTohex($string){
    $sing = ord($string);
    if($sing<16){
        $hex = "0x0".dechex($sing);
    }else{
        $hex = "0x".dechex($sing);
    }
    return $hex;
}
/* read: reads in so many bytes */
function LM_read($int = 8192,$socket, $nb = false){
    
    //	print_r(socket_get_status($this->socket));
    
    $string="";
    $togo = $int;
    
    if($nb){
        return fread($socket, $togo);
    }
    
    while (!feof($socket) && $togo>0) {
        $fread = fread($socket, $togo);
        $string .= $fread;
        $togo = $int - strlen($string);
    }
    return $string;
}

//获取汇率
function convertCurrency($from, $to, $amount=1){
  $data = file_get_contents("http://www.baidu.com/s?wd={$from}%20{$to}&rsv_spt={$amount}");
  preg_match("/<div>1\D*=(\d*\.\d*)\D*<\/div>/",$data, $converted);
  $converted = preg_replace("/[^0-9.]/", "", $converted[1]);

  return number_format(round($converted, 4), 4);
}

//获取星期方法
    function   get_week($date){
        //强制转换日期格式
        $date_str=date('Y-m-d',strtotime($date));
   
        //封装成数组
        $arr=explode("-", $date_str);
        
        //参数赋值
        //年
        $year=$arr[0];
        
        //月，输出2位整型，不够2位右对齐
        $month=sprintf('%02d',$arr[1]);
        
        //日，输出2位整型，不够2位右对齐
        $day=sprintf('%02d',$arr[2]);
        
        //时分秒默认赋值为0；
        $hour = $minute = $second = 0;   
        
        //转换成时间戳
        $strap = mktime($hour,$minute,$second,$month,$day,$year);
        
        //获取数字型星期几
        $number_wk=date("w",$strap);
        
        //自定义星期数组
        $weekArr=array("星期日","星期一","星期二","星期三","星期四","星期五","星期六");
        
        //获取数字对应的星期
        return $weekArr[$number_wk];
    }
/*
 * 向Oss存放文件
 */
function PutOss($filePath){
	if(!defined('ALIYUNOSS')){
		require_once WR.'/lib/aliyun-oss-php-sdk-master/OssClient.php';

	}
	$oss = new MyOssClient();
	$oss->put($filePath);
	//存放完文件之后删除文件
    _unlink(WR.$filePath[0]);

}
/*
 * 删除Oss中的文件
 */
function DelOss($filePath){
	if(!defined('ALIYUNOSS')){
		require_once WR.'/lib/aliyun-oss-php-sdk-master/OssClient.php';

	}
	$oss = new MyOssClient();
	$oss->del($filePath);
}
/**
 向客户端发送数据包
 */
function Client_Push($to,$from,$body,$argc=array()){
		$push_api_url = "http://".C("MSGSERVER")."/plugins/online/Sendmsg";
		
		$post_data = array(
			"domain" => $argc["domain"]?$argc["domain"]:"jiaoyou",
			"from" => $from,
			"body" => $body,
			"to" => $to,
			"subject" => $argc["subject"]?$argc["subject"]:"subject",
			);
		$push_api_url=$push_api_url."?".http_build_query($post_data);
		//pclose(popen(WR."/cli_http.php -s $push_api_url &", 'r'));
		return file_get_contents($push_api_url);
}

/**
获取在线用户id列表
 */
function Client_Online(){
		$push_api_url = "http://".C("MSGSERVER")."/plugins/online/status";
		
		$push_api_url=$push_api_url;
		//pclose(popen(WR."/cli_http.php -s $push_api_url &", 'r'));
		$str = file_get_contents($push_api_url);
		return explode(",",$str);
}

/**
对象和数组互转
 */
function object_array($array) {
	if(is_object($array)) {
		$array = (array)$array;
	} if(is_array($array)) {
		foreach($array as $key=>$value) {
			$array[$key] = object_array($value);
		}
	}
	return $array;
}

function format_time($str){
	if(!is_int($str))
		$str = strtotime($str);
	return date("Y-m-d H:i:s",$str);
}

/**
 * 简单对称加密算法之加密
 * @param String $string 需要加密的字串
 * @param String $skey 加密EKY
 * @author Anyon Zou <zoujingli@qq.com>
 * @date 2013-08-13 19:30
 * @update 2014-10-10 10:10
 * @return String
 */
function encode($string = '', $skey = 'friend') {
	$strArr = str_split(base64_encode($string));
	$strCount = count($strArr);
	foreach (str_split($skey) as $key => $value)
		$key <=$strCount && $strArr[$key].=$value;
	return str_replace(array('=', '+', '/'), array('O0O0O', 'o000o', 'oo00o'), join('', $strArr));
}
/**
 * 简单对称加密算法之解密
 * @param String $string 需要解密的字串
 * @param String $skey 解密KEY
 * @author Anyon Zou <zoujingli@qq.com>
 * @date 2013-08-13 19:30
 * @update 2014-10-10 10:10
 * @return String
 */
function decode($string = '', $skey = 'friend') {
	$strArr = str_split(str_replace(array('O0O0O', 'o000o', 'oo00o'), array('=', '+', '/'), $string), 2);
	$strCount = count($strArr);
	foreach (str_split($skey) as $key => $value)
		$key <= $strCount  && isset($strArr[$key]) && $strArr[$key][1] === $value && $strArr[$key] = $strArr[$key][0];
	return base64_decode(join('', $strArr));
}

function TxtDB_insert($var,$tb_name=''){
	import("Api.lib.Behavior.txtDB");
	$txtDB = new txtDB();
	$txtDB->opendb($tb_name);
	$txtDB->insertline($var);
	//int mktime(时, 分, 秒, 月, 日, 年)
	touch(dirname($tb_name),mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y")));
}
function TxtDB_readall($tb_name=''){
	import("Api.lib.Behavior.txtDB");
	$txtDB = new txtDB();
	return $txtDB->readall($tb_name);
}
/*
 * 判断数据是合法的json数据
 */
function is_json($string) {
	json_decode($string);
	return (json_last_error() == JSON_ERROR_NONE);
}

/**
 * 求两个日期之间相差的天数
 * (针对1970年1月1日之后，求之前可以采用泰勒公式)
 * @param string $day1
 * @param string $day2
 * @return number
 */
function diffBetweenTwoDays($day1, $day2)
{
	$second1 = strtotime($day1);
	$second2 = strtotime($day2);

	if ($second1 < $second2) {
		$tmp = $second2;
		$second2 = $second1;
		$second1 = $tmp;
	}
	return ($second1 - $second2) / 86400;
}
function mkdirs($path , $mode = 0777 ){
	if(!is_dir($path)){
		mkdirs(dirname($path),$mode);
		mkdir($path,$mode);
	}
	return true;
}
//删除空格和回车
function trimall($str){
	$qian=array(" ","　","\t","\n","\r");
	return str_replace($qian, '', $str);
}
function searchDir($path,&$data){
	if(is_dir($path)){
		$dp=dir($path);
		while($file=$dp->read()){
			if($file!='.'&& $file!='..'){
				searchDir($path.'/'.$file,$data);
			}
		}
		$dp->close();
	}
	if(is_file($path)){
		$data[]=$path;
	}
}
function getDir($dir){
	$data=array();
	searchDir($dir,$data);
	return   $data;
}
/***
 * 写入日志文件
 */
function write_logs($data='',$files='a.txt'){
	$post = Recive_data($_POST);
	$uid = $post['token'] ? decode($post['token']) : 0;
	
	$data=$data.'-'.CONTROLLER_NAME.'/'.ACTION_NAME.'--'.json_encode($post).'--'.format_time(time());
	$fp=fopen(WR.'/userdata/Log/'.$files,"a");
	fputs($fp,"$data\n");
	fclose($fp);
}

function writelogs($post){
    $data='INFO:-'.CONTROLLER_NAME.'/'.ACTION_NAME.'--'.json_encode($post).'--'.format_time(time());
    $fp=fopen(WR.'/userdata/Log/pushdata.log',"a");
    fputs($fp,"$data\n");
    fclose($fp);
}



/**
 * 二维数组打乱
 * @param unknown $list
 * @return unknown|multitype:unknown
 */
function shuffle_assoc($list) {
	if (!is_array($list)) return $list;
	$keys = array_keys($list);
	shuffle($keys);
	$random = array();
	foreach ($keys as $key=>$val){
		$random[$key] = $list[$val];
	}
	return $random;
}
/**
 * 删除文件
 * @param unknown $list
 * @return unknown|multitype:unknown
 */
function _unlink($file) {
	if(file_exists($file)){
		unlink($file);
	}
	return true;
}
/**
 * 删除文件夹及文件
 * @param unknown $dir
 * @return boolean
 */
function deldir($dir) {
	//先删除目录下的文件：
	$dh=opendir($dir);
	while ($file=readdir($dh)) {
		if($file!="." && $file!="..") {
			$fullpath=$dir."/".$file;
			if(!is_dir($fullpath)) {
				unlink($fullpath);
			} else {
				deldir($fullpath);
			}
		}
	}

	closedir($dh);
	//删除当前文件夹：
	if(rmdir($dir)) {
		return true;
	} else {
		return false;
	}
}
/**
 建立文件夹
 *
 @param  string $aimUrl
 @return  viod
 */
function createDir($aimUrl, $mode = 0777) {
    $aimUrl = str_replace('', '/', $aimUrl);
    $aimDir = '';
    $arr = explode('/', $aimUrl);
    foreach ($arr as $str) {
        $aimDir .= $str . '/';
        if (!file_exists($aimDir)) {
            mkdir($aimDir, $mode);
        }
    }
} 
/**
 * 生成随机字符串字母数字组合
 * @param unknown $length
 */
function createRandomStr($length){
	//$str = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';//62个字符
	$str = '_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';//62个字符
	$strlen = 62;
	while($length > $strlen){
		$str .= $str;
		$strlen += 62;
	}
	
	$str = str_shuffle($str);
	
	return substr($str,0,$length);
}
/************按时间顺序输出文件夹中的文件******************/
function dir_time($dir) {
	$dh = @opendir ( $dir ); // 打开目录，返回一个目录流
	$return = array ();
	$i = 0;
	while ( $file = @readdir ( $dh ) ) { // 循环读取目录下的文件
		if ($file != '.' and $file != '..') {
			$path = $dir . '/' . $file; // 设置目录，用于含有子目录的情况
			if (is_dir ( $path )) {
			} elseif (is_file ( $path )) {
				$filetime [] = date ( "Y-m-d H:i:s", filemtime ( $path ) ); // 获取文件最近修改日期
				$return [] = $file;
			}
		}
	}
	@closedir ( $dh ); // 关闭目录流
	array_multisort($filetime,SORT_ASC,SORT_STRING, $return);//按时间排序
	return $return; // 返回文件
}
function list_dir_time($dir) {
	$dh = @opendir ( $dir ); // 打开目录，返回一个目录流
	$return = array ();
	$i = 0;
	while ( $file = @readdir ( $dh ) ) { // 循环读取目录下的文件
		if ($file != '.' and $file != '..') {
			$path = $dir . '/' . $file; // 设置目录，用于含有子目录的情况
			if (is_dir ( $path )) {
				$filetime [] = date ( "Y-m-d H:i:s", filemtime ( $path ) ); // 获取文件最近修改日期
				$return [] = $file;
			} 
		}
	}
	@closedir ( $dh ); // 关闭目录流
	array_multisort($filetime,SORT_DESC,SORT_STRING, $return);//按时间排序
	return $return; // 返回文件
}
/** 
*自动判断把gbk或gb2312编码的字符串转为utf8 
*能自动判断输入字符串的编码类，如果本身是utf-8就不用转换，否则就转换为utf-8的字符串 
*支持的字符编码类型是：utf-8,gbk,gb2312 
*@$str:string 字符串 
*/ 
function yang_gbk2utf8($str){ 
    $charset = mb_detect_encoding($str,array('UTF-8','GBK','GB2312')); 
    $charset = strtolower($charset); 
    if('cp936' == $charset){ 
        $charset='GBK'; 
    } 
    if("utf-8" != $charset){ 
        $str = iconv($charset,"UTF-8//IGNORE",$str); 
    } 
    return $str; 
}
function ischinese($str){
	if (preg_match("/[\x7f-\xff]/", $str)) { 
		return 1;
	}else{ 
		return 0;
	}           
}
// 过滤掉emoji表情
function filterEmoji($str)
{
    $str = preg_replace_callback(
        '/./u',
        function (array $match) {
            return strlen($match[0]) >= 4 ? '' : $match[0];
        },
        $str);
    
    return $str;
}
function Dump($str){
	echo "<pre>";      
	print_r($str);
	echo "</pre>";    
}
/**
 * 向Rest服务器发送请求
 * @param string $http_type http类型,比如https
 * @param string $method 请求方式，比如POST
 * @param string $url 请求的url
 * @return string $data 请求的数据
 */
function http_req($http_type, $method, $url, $data)
{
    $ch = curl_init();
    if (strstr($http_type, 'https'))
    {
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    }
    
    if ($method == 'post')
    {
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    } else
    {
        $url = $url . '?' . $data;
    }
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT,100000);//超时时间
    
    try
    {
        $ret=curl_exec($ch);
    }catch(Exception $e)
    {
        curl_close($ch);
        return json_encode(array('ret'=>0,'msg'=>'failure'));
    }
    curl_close($ch);
    return $ret;
}
function tencent_sendmsg($sendmsg,$sdkappid){
    $uid = $sendmsg["msgbody"]["user"]["uid"];
    $touid = $sendmsg["touid"];
    $msg_content = array();
    //创建array 所需元素
    $msg_content_elem = array(
        'MsgType' => 'TIMCustomElem',       //文本类型
        'MsgContent' => array(
            'Data' => "LM".base64_encode(json_encode($sendmsg)),                //hello 为文本信息
        )
    );
    //将创建的元素$msg_content_elem, 加入array $msg_content
    array_push($msg_content, $msg_content_elem);
    
    #构造新消息
    $msg = array(
        'From_Account' => (string)$uid,
        'To_Account' => (string)$touid,
        'MsgSeq' => rand(1, 65535),
        'MsgRandom' => rand(1, 65535),
        'MsgTimeStamp' => time(),
        'MsgBody' => $msg_content,
        
    );
    #将消息序列化为json串
    $req_data = json_encode($msg);
   
    $service_name = "openim";
    $cmd_name = "sendmsg";
    
    $ret = json_decode($ret, true);
    //$req_tmp用来做格式化输出
    $req_tmp = json_decode($req_data, true);
    # 构建HTTP请求参数，具体格式请参考 REST API接口文档 (http://avc.qcloud.com/wiki/im/)(即时通信云-数据管理REST接口)
    #app基本信息
    $identifier=10000;//管理员
    $usersig = tencent_signature($identifier,$sdkappid);
    
    
    #开放IM https接口参数, 一般不需要修改
     $http_type = 'https://';
     $method = 'post';
     $im_yun_url = 'console.tim.qq.com';
     $version = 'v4';
     $contenttype = 'json';
     $apn = '0';
    $parameter =  "usersig=" . $usersig
    . "&identifier=" . $identifier
    . "&sdkappid=" . $sdkappid
    . "&contenttype=" . $contenttype;
    $url = $http_type . $im_yun_url . '/' . $version . '/' . $service_name . '/' .$cmd_name . '?' . $parameter;
    //Dump($url);
    //Dump($req_data);
   
    $ret = http_req('https', 'post', $url, $req_data);
    $ret = json_decode($ret,true);
    if($ret["ErrorCode"]=="20003"){
        $service_name1 = "im_open_login_svc";
        $cmd_name1 = "account_import";
        //用户信息没有导入腾讯云
        $url1 = $http_type . $im_yun_url . '/' . $version . '/' . $service_name1 . '/' .$cmd_name1 . '?' . $parameter;
        #构造新消息
        $msg1 = array(
            "Identifier"=>$uid,
            "Nick"=>$uid,
            "FaceUrl"=>"http://www.qq.com",
            "Type"=>0
        );
        #将消息序列化为json串
        $req_data1 = json_encode($msg1);
        $ret1 = http_req('https', 'post', $url1, $req_data1);
        //导入完成继续发信
        $ret = http_req('https', 'post', $url, $req_data);
        $ret = json_decode($ret,true);
    }
    return $ret;
}
function tencent_onlinestate($data,$sdkappid){
    
    
    #构造新消息
    $msg = array(
        'To_Account' => $data
        
    );
    #将消息序列化为json串
    $req_data = json_encode($msg);
    
    $service_name = "openim";
    $cmd_name = "querystate";//获取在线用户状态
    # 构建HTTP请求参数，具体格式请参考 REST API接口文档 (http://avc.qcloud.com/wiki/im/)(即时通信云-数据管理REST接口)
    #app基本信息
    $identifier=10000;//管理员
    $usersig = tencent_signature($identifier,$sdkappid);
    
    
    #开放IM https接口参数, 一般不需要修改
    $http_type = 'https://';
    $method = 'post';
    $im_yun_url = 'console.tim.qq.com';
    $version = 'v4';
    $contenttype = 'json';
    $apn = '0';
    $parameter =  "usersig=" . $usersig
    . "&identifier=" . $identifier
    . "&sdkappid=" . $sdkappid
    . "&contenttype=" . $contenttype;
    $url = $http_type . $im_yun_url . '/' . $version . '/' . $service_name . '/' .$cmd_name . '?' . $parameter;
    //Dump($url);

    //Dump($req_data);
    /*
     * {
    "ActionStatus": "OK",
    "ErrorInfo": "",
    "ErrorCode": 0,
    "QueryResult": [
        {
            "To_Account": "id1",
            "State": "Offline"
        },
        {
            "To_Account": "id2",
            "State": "Online"
        },
        {
            "To_Account": "id3",
            "State": "PushOnline"
        }
    ]
}
     */
    $ret = http_req('https', 'post', $url, $req_data);
    $ret = json_decode($ret,true);
    if ($_REQUEST['ptest'] == 1) {
        dump($usersig);
        dump($req_data);
        Dump($url);
        dump($ret);
    }
    return $ret["QueryResult"];
}
function tencent_signature($uid,$sdkappid)
{
    $private_key_path=WR."/Public/key/private_key_".$sdkappid;
    //echo $private_key_path;
    # 这里需要写绝对路径，开发者根据自己的路径进行调整
    $command = '/var/www/signature'
        . ' ' . escapeshellarg($private_key_path)
        . ' ' . escapeshellarg($sdkappid)
        . ' ' . escapeshellarg($uid);
        //echo $command;
        $ret = exec($command, $out, $status);
        if ($status == -1)
        {
            return null;
        }
        //Dump($out);
        return $out[0];
}
//快速排序

function quick_sort($arr) {
    //先判断是否需要继续进行
    $length = count($arr);
    if($length <= 1) {
        return $arr;
    }
    //如果没有返回，说明数组内的元素个数 多余1个，需要排序
    //选择一个标尺
    //选择第一个元素
    $base_num = $arr[0];
    //遍历 除了标尺外的所有元素，按照大小关系放入两个数组内
    //初始化两个数组
    $left_array = array();//小于标尺的
    $right_array = array();//大于标尺的
    for($i=1; $i<$length; $i++) {
        if($base_num > $arr[$i]) {
            //放入左边数组
            $left_array[] = $arr[$i];
        } else {
            //放入右边
            $right_array[] = $arr[$i];
        }
    }
    //再分别对 左边 和 右边的数组进行相同的排序处理方式
    //递归调用这个函数,并记录结果
    $left_array = quick_sort($left_array);
    $right_array = quick_sort($right_array);
    //合并左边 标尺 右边
    return array_merge($right_array, array($base_num), $left_array);
}


//生成唯一订单
 function create_order($prefix){
    /* 选择一个随机的方案 */
    mt_srand((double) microtime() * 1000000);
    //根据当前系统时间生成微妙为单位的唯一id
    /* PHPALLY + 年月日 + 6位随机数 */
    return $prefix . date('Ymdhis') . str_pad(mt_rand(1, 999999), 6, '0', STR_PAD_LEFT);
}



/**
 * 记录自定义日志
 * @author gyj  <375023402@qq.com>
 * @createtime 2018-08-24 14:12:01
 * @param $msg 错误信息
 * @param $type 写入类型 wechat aliyun
 * @return [type] [description]
 */
if(!function_exists('log_result')){
    function log_result($msg='',$type='normal')
    {
        $dir = LOG_PATH."/".$type."/";
        if(!is_dir($dir)){
            mkdir($dir,0777);
        }
        $dir .= date('Ym')."/";
        $file = $dir.date('d').".log";
        if(!is_dir($dir)){
            mkdir($dir,0777);
        }
        file_put_contents($file,date('Y-m-d H:i:s')."\r\n".$msg."\r\n---------------------------------------------------------------\r\n", FILE_APPEND);
    }

    /*
     *content: 根据数组某个字段进行排序
     * $arr    需要排序的数组
     * $field  数组里的某个字段
     * sort    1为正序排序  2为倒序排序
     * time :  2016年12月21日19:02:33
     */
     function f_order($arr,$field,$sort){
        $order = array();
        foreach($arr as $kay => $value){
            $order[] = $value[$field];
        }
        if($sort==1){
            array_multisort($order,SORT_ASC,$arr);
        }else{
            array_multisort($order,SORT_DESC,$arr);
        }
        return $arr;
    }

    /**
     * 二维数组去重
     * @param $arr 数组
     * @return $key 去重字段
     */
    function assoc_unique($arr, $key){
        $tmp_arr = array();
        foreach($arr as $k => $v){
            //搜索$v[$key]是否在$tmp_arr数组中存在，若存在返回true
            if(in_array($v[$key],$tmp_arr)){
                unset($arr[$k]);
            }else{
                $tmp_arr[] = $v[$key];
            }
        }
        //sort($arr); //sort函数对数组进行排序
        return $arr;
    }



}


        /**
         * 发送邮件
         * @param  string $address 需要发送的邮箱地址 发送给多个地址需要写成数组形式
         * @param  string $subject 标题
         * @param  string $content 内容
         * @return boolean       是否成功
         */
function send_email($address, $subject, $content)
{
    $email_smtp = C('EMAIL_SMTP');
    $email_username = C('EMAIL_USERNAME');
    $email_password = C('EMAIL_PASSWORD');
    $email_from_name = C('EMAIL_FROM_NAME');
    if (empty($email_smtp) || empty($email_username) || empty($email_password) || empty($email_from_name)) {
        return array("error" => 1, "message" => '邮箱配置不完整');
    }
    require WR.'/ThinkPHP/Library/Org/Nx/class.phpmailer.php';
    require WR.'/ThinkPHP/Library/Org/Nx/class.smtp.php';
    $phpmailer = new \Phpmailer();
    // 设置PHPMailer使用SMTP服务器发送Email
    $phpmailer->IsSMTP();
    // 设置为html格式
    $phpmailer->IsHTML(true);
    // 设置邮件的字符编码'
    $phpmailer->CharSet = 'UTF-8';
    // 设置SMTP服务器。
    $phpmailer->Host = $email_smtp;
    // 设置为"需要验证"
    $phpmailer->SMTPAuth = true;
    // 设置用户名
    $phpmailer->Username = $email_username;
    // 设置密码
    $phpmailer->Password = $email_password;
    // 设置邮件头的From字段。
    $phpmailer->From = $email_username;
    // 设置发件人名字
    $phpmailer->FromName = $email_from_name;
    // 添加收件人地址，可以多次使用来添加多个收件人
    if (is_array($address)) {
        foreach ($address as $addressv) {
            $phpmailer->AddAddress($addressv);
        }
    } else {
        $phpmailer->AddAddress($address);
    }
    // 设置邮件标题
    $phpmailer->Subject = $subject;
    // 设置邮件正文
    $phpmailer->Body = $content;
    // 发送邮件。
    if (!$phpmailer->Send()) {
        $phpmailererror = $phpmailer->ErrorInfo;
        return array("error" => 1, "message" => $phpmailererror);
    } else {
        return array("error" => 0);
    }
}
//概率可用以抽奖
function get_rand($proArr)
{
    $result = '';
    //概率数组的总概率精度
    $proSum = array_sum($proArr);
    //概率数组循环
    foreach ($proArr as $key => $proCur) {
        $randNum = mt_rand(1, $proSum);
        if ($randNum <= $proCur) {
            $result = $key;
            break;
        } else {
            $proSum -= $proCur;
        }
    }
    unset ($proArr);
    return $result;
}

function create_xls($data,$filename='simple.xls'){
    ini_set('max_execution_time', '0');
    Vendor('PHPExcel.PHPExcel');
    $filename=str_replace('.xls', '', $filename).'.xls';
    $phpexcel = new PHPExcel();
    $phpexcel->getProperties()
        ->setCreator("Maarten Balliauw")
        ->setLastModifiedBy("Maarten Balliauw")
        ->setTitle("Office 2007 XLSX Test Document")
        ->setSubject("Office 2007 XLSX Test Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("Test result file");
    $phpexcel->getActiveSheet()->fromArray($data);
    $phpexcel->getActiveSheet()->setTitle('Sheet1');
    $phpexcel->setActiveSheetIndex(0);
    header('Content-Type: application/vnd.ms-excel');
    header("Content-Disposition: attachment;filename=$filename");
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0
    $objwriter = PHPExcel_IOFactory::createWriter($phpexcel, 'Excel5');
    $objwriter->save('php://output');
    exit;
}
?>
