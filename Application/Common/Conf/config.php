<?php
return array(
    //'配置项'=>'配置值'
    'LOAD_EXT_CONFIG' => 'cache.config',
    'URL_MODEL'=>2,
    'SESSION_TIMEOUT'=>60*60*24*2,
    'LANG_PATH'=>WR.'/userdata/lang/',
    'MODULE_ALLOW_LIST'    =>  array('Api','Adms','Home'),
    'DEFAULT_MODULE'       =>  'Api',
    'DEFAULT_CONTROLLER' => 'index', // 默认控制器名称
    'MSGSERVER'=>"47.91.215.236:2355",//消息服务器地址
    'IMAGEURL'=>"http://limida.oss-cn-hongkong.aliyuncs.com/data",//图片服务器地址
    /*Redis服务器配置*/
    'REDIS_HOST'         => '127.0.0.1',
    'REDIS_PORT'         => '6379',
    'REDIS_PASSWORD'     => '',
    'REDIS_RW'           => true,
    /*数据库服务器配置*/
    'DB_TYPE'            => 'mysql',
    //'DB_HOST'=>'47.75.189.126',
    'DB_HOST'            => '127.0.0.1',
    // 'DB_HOST'            => 'localhost',
    'DB_NAME'            => 'shop',
    'DB_USER'            => 'root',
    //'DB_PWD'=>'j6tyylsdkd',
    'DB_PWD'             => 'OUTfox@123',
    'DB_PREFIX'          => 't_',
    'TMPL_CACHE_ON'   => true,  // 默认开启模板编译缓存 false 的话每次都重新编译模板
    'ACTION_CACHE_ON'  => true,  // 默认关闭Action 缓存
    'HTML_CACHE_ON'   => true,   // 默认关闭静态缓存
    'DATA_CACHE_SUBDIR'=>true,
    'DATA_PATH_LEVEL'=>5,
    //接下来配置主从数据库
    'DB_DEPLOY_TYPE'=>1,//开启分布式数据库
    'DB_RW_SEPARATE'=>true,//读写分离，默认第一台服务器为写入服务器，其它的只读取不写入
    'NO_LOGIN'=>array(

    ),

);
