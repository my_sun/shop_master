<?php
use Think\Controller;
class IndexController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}

	//加载商城首页
	public function Index(){
		//banner信息
		$BannerM = new BannerModel();
		$where = array();
		$where['is_up'] = 1;
		$BannerInfo = $BannerM->getList($where);
		//商品分类信息
		$GoodsTypeM = new GoodsTypeModel();
		$where2 = array();
		$where2['is_del'] = 1;
		$GoodsTypeInfo = $GoodsTypeM->getList($where2);
		$GoodsTypeInfo = array_chunk($GoodsTypeInfo, 8);
		$specialInfo = M('special')->select();
		$robis_up =$specialInfo[0]['is_up'];
		$integralis_up =$specialInfo[1]['is_up'];
		$newsis_up =$specialInfo[2]['is_up'];
		//拼团抢购信息 1
		$GoodsM = new GoodsModel();
		$where3 = array();
		$where3['is_up'] = 1;
		$where3['special_id'] = 1;
		$RobInfo = $GoodsM->getList($where3);
		$gid = [];
		foreach($RobInfo as $k=>$v){
			$RobInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
			$gid[] = $v['id'];
		}
		//规格信息
//		$stock = 0;
//		$normswhere = array();
//		$normswhere['goods_id'] = array('in',$gid);
//		$robnormsInfo = M('norms')->where($normswhere)->select();
//		foreach ($robnormsInfo as $k=>$v){
//			$robnormsInfo[$v['goods_id']][] = $v['stock'];
//		}
//		foreach ($RobInfo as $k=>$v){
//			foreach ($robnormsInfo as $k1=>$v1){
//				if($v['id'] == $k1){
//					$RobInfo[$k]['stocks'] = $v1;
//				}
//			}
//		}
//		foreach($RobInfo as $k=>$v){
//			foreach ($v['stocks'] as $k1=>$v1){
//				$RobInfo[$k]['stocks'] = $v['stocks'][$k1] + $v['stocks'][($k1+1)];
//			}
//		}

		//关联抢购表
		$timeInfo = M('rob')->select();
		$endtime = $timeInfo[0]['endtime']; //结束时间
		$time = time(); //当前时间
		if($time < $endtime){
			$timecha = $endtime-$time;
		}else{
			$timecha = 0;
		}
		//计算天数
		$days = intval($timecha/(24*60*60)); //天
		$hours =  intval($timecha/3600); //小时
		$hcount = strlen($hours); //小时个数
		$timecha = $timecha % 3600;
		$mins = intval($timecha / 60); //分钟
		//计算秒数
		$secs = $timecha % 60; //秒
		//积分专区信息 2
		$where4 = array();
		$where4['is_up'] = 1;
		$where4['special_id'] = 2;
		$IntegralInfo = $GoodsM->getList($where4);
		foreach($IntegralInfo as $k=>$v){
			$IntegralInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
		}
		//新品推荐信息 3
		$where5 = array();
		$where5['is_up'] = 1;
		$where5['special_id'] = 3;
		$NewInfo = $GoodsM->getList($where5);
		foreach($NewInfo as $k=>$v){
			$NewInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
		}
		$new_count = count($NewInfo);
		//返回数据
		$this->BannerInfo = $BannerInfo;
		$this->new_count = $new_count;
		$this->GoodsTypeInfo = $GoodsTypeInfo;
		$this->RobInfo = $RobInfo;
		$this->IntegralInfo = $IntegralInfo;
		$this->NewInfo = $NewInfo;
		$this->robis_up = $robis_up;
		$this->integralis_up = $integralis_up;
		$this->newsis_up = $newsis_up;
		$this->hours = $hours;
		$this->mins = $mins;
		$this->secs = $secs;
		$this->msgurl=C ("IMAGEURL");
		$this->display("index");
	}

	//加载拼团抢购查看更多页面 1
	public function RushMore(){
		$GoodsM = new GoodsModel();
		$where = array();
		$where['is_up'] = 1;
		$where['special_id'] = 1;
		$RobInfo = $GoodsM->getList($where);
		foreach($RobInfo as $k=>$v){
			$RobInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
		}
		//关联抢购表
		$timeInfo = M('rob')->select();
		$endtime = $timeInfo[0]['endtime']; //结束时间
		$time = time(); //当前时间
		if($time < $endtime){
			$timecha = $endtime-$time;
		}else{
			$timecha = 0;
		}
		//天数
		$days = intval($timecha/(24*60*60)); //天
		//小时
		$hours =  intval($timecha/3600); //小时
		$hcount = strlen($hours); //小时个数
		//分钟
		$timecha = $timecha % 3600;
		$mins = intval($timecha / 60); //分钟
		//秒数
		$secs = $timecha % 60; //秒
		$this->RobInfo = $RobInfo;
		$this->hours = $hours;
		$this->mins = $mins;
		$this->secs = $secs;
		$this->msgurl=C ("IMAGEURL");
		$this->display("rush");
	}
	//加载积分专区查看更多页面 2
	public function IntegralMore(){
		$GoodsM = new GoodsModel();
		$where = array();
		$where['is_up'] = 1;
		$where['special_id'] = 2;
		$IntegralInfo = $GoodsM->getList($where);
		foreach($IntegralInfo as $k=>$v){
			$IntegralInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
		}
		$this->IntegralInfo = $IntegralInfo;
		$this->msgurl=C ("IMAGEURL");
		$this->display("store");
	}

	//ajax拼团抢购 下拉更多 1
	public function RushMore2(){
		if(IS_POST){
			$data = I('request.');
			$page = $data['page']-1; //1
			$pageSize = 6; //每页条数
			$where = array();
			$where['is_up'] = 1;
			$where['special_id'] = 1;
			$RobInfo = M("Goods")->where($where)->limit($page*$pageSize,$pageSize)->select();
			foreach($RobInfo as $k=>$v){
				$RobInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
				$RobInfo[$k]['msgurl'] = C ("IMAGEURL");
			}
			$this->ajaxReturn($RobInfo);
		}
	}
	//ajax积分专区 下拉更多 2
	public function IntegralMore2(){
		if(IS_POST){
			$data = I('request.');
			$page = $data['page']-1; //1
			$pageSize = 6; //每页条数
			$where = array();
			$where['is_up'] = 1;
			$where['special_id'] = 2;
			$IntegralInfo = M("Goods")->where($where)->limit($page*$pageSize,$pageSize)->select();
			foreach($IntegralInfo as $k=>$v){
				$IntegralInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
				$IntegralInfo[$k]['msgurl'] = C ("IMAGEURL");
			}
			$this->ajaxReturn($IntegralInfo);
		}
	}

	//ajax加载新品推荐 下拉更多 3
	public function NewMore2(){
		if(IS_POST){
			$data = I('request.');
			$page = $data['page']-1; //1
			$pageSize = 4; //每页条数
			$where = array();
			$where['is_up'] = 1;
			$where['special_id'] = 3;
			$NewInfo = M("Goods")->where($where)->limit($page*$pageSize,$pageSize)->select();
			foreach($NewInfo as $k=>$v){
				$NewInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
				$NewInfo[$k]['msgurl'] = C ("IMAGEURL");
			}
			$this->ajaxReturn($NewInfo);
		}
	}



}

?>
