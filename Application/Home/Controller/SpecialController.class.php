<?php

class SpecialController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}

	//专题列表
	public function lists(){
		$this->name = '专题列表'; // 进行模板变量赋值
		$Page = $_GET['Page'] ? $_GET['Page'] : 1;
		$PageSize = 20;

		$WhereArr = array();
		if(isset($_GET['keyword'])&&$_GET['keyword']!=""){
			$keyword=$_GET['keyword'];
			$WhereArr['name']=array('like',"%".$keyword."%");
		}
        $SpecialM = new SpecialModel();

		$SpecialInfo = $SpecialM->getList($WhereArr,$Page,$PageSize);
        // dump($SpecialInfo);die;
		$all_page = ceil($SpecialInfo['totalCount']/$PageSize);
		$UserInfo['all_page'] = $all_page;
		$this->SpecialInfo = $SpecialInfo;
		$this->Pages = $this->GetPages($UserInfo);
		$this->get = $_GET;
		$this->action = "/Adms/Special/lists.html";
		$this->display("lists");
	}

	//添加专题
	public function add(){
		$this->name = '添加专题';
		if($_POST){
                $SpecialM = new SpecialModel();
				$map = array();
				$map["name"] = $_POST["name"];
				$map["is_up"] = 1;
				$map["addtime"] = time();
                $SpecialM->addOne($map);
				header('Location:lists.html');
			}
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//修改专题
	public function edit(){
		$this->name = '修改专题';  // 进行模板变量赋值

		//获取id
		$id = $_GET['id'];
		if ($id){
            $SpecialM = new SpecialModel();
			$where['id'] = $id;
			//查数据
			$SpecialInfo = $SpecialM->getOne($where);
		}
		$this->SpecialInfo = $SpecialInfo;
		$this->id = $id;
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//处理修改
	public function update(){
		$this->name = '修改专题';  // 进行模板变量赋值

		$id = $_POST['id'];
		$where = array();
        $SpecialM = new SpecialModel();
		if ($id){
			$data['name'] = $_POST['name'];
			$where["id"] = $_POST['id'];
			$res = $SpecialM->updateOne($where,$data);
			if ($res){
				$this->tip = "修改成功";
			}
			header('Location:lists.html');
		}
	}

	//软删除用户
	public function del(){
		$this->name = '删除专题';
        $SpecialM = new SpecialModel();
		$where = array();
		$where['id'] = $_GET['id'];
		if($_GET["id"]){
			$data['is_del'] = 0;
			$ret = $SpecialM->updateOne($where,$data);
			header("Location:/Adms/Special/lists");
		}
	}

	//上架专题
	public function upzhuanti(){
		$id = $_GET['id'];
        $SpecialM = new SpecialModel();
		$where = array();
		$where['id'] = $id;
		if ($id){
			$data['is_up'] = 1;
            $SpecialM->updateOne($where,$data);
			$this->success('修改为上架成功');
		}
	}

	//专题下架
	public function downzhuanti(){
		$id = $_GET['id'];
        $SpecialM = new SpecialModel();
		$where = array();
		$where['id'] = $id;
		if ($id){
			$data['is_up'] = 0;
            $SpecialM->updateOne($where,$data);
			$this->success('修改为下架成功');
		}
	}
}

?>
