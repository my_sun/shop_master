<?php
use Think\Controller;
class GoodsDetailController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//加载商品详情页
	public function Index(){
		$gid = $_GET['gid'];
		$GoodsM = new GoodsModel();
		$where = array();
		$where['is_up'] = 1;
		$where['id'] = $gid;
		$GoodsDetailInfo = $GoodsM->getList($where);
		$normsArr = array();
		$detailsArr = array();
		foreach($GoodsDetailInfo as $k=>$v){
			//图片
			$GoodsDetailInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
			//规格
			$norms = json_decode($v['norms']);//转为数组
			foreach($norms as $k1=>$v1){
				$normskey = explode("：",$v1);//用：分割
				list($key,$val) = $normskey;
				$strsub = strpos($val,"|")?strpos($val,"|"):strlen($val);
					$normsGroup = substr($val,0,$strsub);
					$shoponeo = substr($val,$strsub+1,-1);
					$shoponeoArr = explode("|",$shoponeo);
				$normArr = array();
				foreach($shoponeoArr as $k=>$item){
					$norstoc = explode(",",$item);
					$normArr[$k]['norms'] = $norstoc[0];
					$normArr[$k]['stoc'] = $norstoc[1];
					$normArr[$k]['price'] = $norstoc[2];
					$normArr[$k]['discount_price'] = $norstoc[3];
				}
				$normsArr[$key][$normsGroup][] = $normArr;
			}
			$GoodsDetailInfo[0]['norms'] = $normsArr;
			//详情
			$details = explode(",",$v['details']);
			foreach($details as $k2=>$v2){
				$detailskey = explode("：",$v2);//用：分割
				list($key,$val) = $detailskey;
				$detailsArr[$key] = $val;
			}
			$GoodsDetailInfo[0]['details'] = $detailsArr;
		}
//		dump($GoodsDetailInfo);die;
		$userInfo = $_SESSION['userInfo'];
		$this->GoodsDetailInfo = $GoodsDetailInfo;
		$this->userInfo = $userInfo;
		$this->msgurl=C ("IMAGEURL");
		$this->display("productdetail");
	}

}

?>
