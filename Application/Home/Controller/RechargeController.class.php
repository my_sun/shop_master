<?php

class RechargeController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//加载充值页面
	public function recharge(){
		$rechargeInfo = M("recharge")->where(["is_del"=>1])->select();
		$this->rechargeInfo = $rechargeInfo;
		$this->display("recharge");
	}

}

?>
