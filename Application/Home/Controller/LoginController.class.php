<?php

class LoginController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}

	//加载登录页
	public function index(){
		$this->display("login");
	}

	//处理登录
	public function login(){
		$data = I('');
		//判断手机号
		$phone = $data['phone'];
		if(empty($phone)){
			$this->ajaxReturn(["err"=>500,"msg"=>"请输入手机号！"]);exit;
		}
		$myreg="/^1(3|4|5|6|7|8|9)\d{9}$/";
		if (!preg_match($myreg,$phone)) {
			$this->ajaxReturn(["err"=>500,"msg"=>"请输入正确的手机号！"]);exit;
		}
		$userInfo = M('user')->where(["phone"=>$phone])->find();
		//判断验证码是否为空
		$verification_code = $data['verification_code'];
		if(empty($verification_code)){
			$this->ajaxReturn(["err"=>500,"msg"=>"请输入验证码！"]);exit;
		}
		//redis获取验证码
		$redis = new RedisModel();
		$getLoginCode = $redis->get("login_code_".$phone);
//		if ($getLoginCode !== $verification_code){
//			$this->ajaxReturn(["err"=>500,"msg"=>"验证码错误！"]);exit;
//		}
		//判断用户是否被拉黑
		if ($userInfo['is_del'] == 0){
			$this->ajaxReturn(["err"=>500,"msg"=>"您已被拉入黑名单！"]);
		}
		//手机号不存在 添加
		if (empty($userInfo['phone'])){
			$map['phone'] = $phone;
			$map['addtime'] = time();
			$map['ufile'] = ""; //默认头像
			$map['nickname'] = "小米哒";//默认昵称
			$UserM = new UserModel();
			$res = $UserM->addOne($map);
			$uid = $UserM->max('id');
			$where = array();
			$where['uid'] = $uid;
			$userInfo = $UserM->getList($where);
			if ($res){
				session("userInfo",$userInfo);
				$this->ajaxReturn(["err"=>200,"msg"=>"注册成功！"]);
			}else{
				$this->ajaxReturn(["err"=>500,"msg"=>"注册失败！"]);
			}
		}else{
			//手机号存在登录
			session("userInfo",$userInfo);
			cookie("userLoginInfo",$userInfo);
			$this->ajaxReturn(["err"=>200,"msg"=>"登录成功！"]);
		}
	}

	//qq第三方登录获取用户信息
	public function getUserInfo(){
		//接收QQ第三方数据
//		dump(I(''));
		//没绑定过或者解除了QQ关联 跳转到绑定页面
		$this->display('loginbind');
		//绑定过 直接跳转到首页
		header('Location:/Home/Index/index.html');
	}
	//qq处理绑定
	public function qqloginbind(){
		//接收QQ第三方数据

		//处理信息 添加数据库
		$data = I('');
		//判断手机号
		$phone = $data['phone'];
		if(empty($phone)){
			$this->ajaxReturn(["err"=>500,"msg"=>"请输入手机号！"]);exit;
		}
		$myreg="/^1(3|4|5|6|7|8|9)\d{9}$/";
		if (!preg_match($myreg,$phone)) {
			$this->ajaxReturn(["err"=>500,"msg"=>"请输入正确的手机号！"]);exit;
		}
		$userInfo = M('user')->where(["phone"=>$phone])->find();
		//判断验证码
		$verification_code = $data['verification_code'];
		if(empty($verification_code)){
			$this->ajaxReturn(["err"=>500,"msg"=>"请输入验证码！"]);exit;
		}
		//redis获取验证码
		$redis = new RedisModel();
		$getqqLoginCode = $redis->get("login_code_".$phone);
//		if ($getqqLoginCode !== $verification_code){
//			$this->ajaxReturn(["err"=>500,"msg"=>"验证码错误！"]);exit;
//		}
		//绑定手机号不存在 添加
		if (empty($userInfo['phone'])){
			$map['phone'] = $phone;
			$map['addtime'] = time();
			$map['ufile'] = ""; //头像
			$map['nickname'] = ""; //昵称
			$map['is_qqbind'] = 1; //是否 1 关联qq
			$UserM = new UserModel();
			$res = $UserM->addOne($map);
			$uid = $UserM->max('id');
			$where = array();
			$where['uid'] = $uid;
			$userInfo = $UserM->getList($where);
			if ($res){
				session("userInfo",$userInfo);
				$this->ajaxReturn(["err"=>200,"msg"=>"绑定成功！"]);
			}else{
				$this->ajaxReturn(["err"=>500,"msg"=>"绑定失败！"]);
			}
		}
//		else{
//			//绑定手机号存在登录
//			session("userInfo",$userInfo);
//			$this->ajaxReturn(["err"=>200,"msg"=>"绑定成功！"]);
//		}
	}

	//微信第三方登录获取用户信息
	public function getwxUserInfo(){
		//接收微信第三方数据
//		dump(I(''));
		//没绑定过或者解除了微信关联 跳转到绑定页面
		$this->display('loginbind');
		//绑定过 直接跳转到首页
		header('Location:/Home/Index/index.html');
	}
	//微信处理绑定
	public function wxloginbind(){
		//接收微信第三方数据

		//处理信息 添加数据库
		$data = I('');
		//判断手机号
		$phone = $data['phone'];
		if(empty($phone)){
			$this->ajaxReturn(["err"=>500,"msg"=>"请输入手机号！"]);exit;
		}
		$myreg="/^1(3|4|5|6|7|8|9)\d{9}$/";
		if (!preg_match($myreg,$phone)) {
			$this->ajaxReturn(["err"=>500,"msg"=>"请输入正确的手机号！"]);exit;
		}
		$userInfo = M('user')->where(["phone"=>$phone])->find();
		//判断验证码
		$verification_code = $data['verification_code'];
		if(empty($verification_code)){
			$this->ajaxReturn(["err"=>500,"msg"=>"请输入验证码！"]);exit;
		}
		//redis获取验证码
		$redis = new RedisModel();
		$getqqLoginCode = $redis->get("login_code_".$phone);
		if ($getqqLoginCode !== $verification_code){
			$this->ajaxReturn(["err"=>500,"msg"=>"验证码错误！"]);exit;
		}
		//绑定手机号不存在 添加
		if (empty($userInfo['phone'])){
			$map['phone'] = $phone;
			$map['addtime'] = time();
			$map['ufile'] = ""; //头像
			$map['nickname'] = ""; //昵称
			$map['is_wxbind'] = 1; //是否 1 关联微信
			$UserM = new UserModel();
			$res = $UserM->addOne($map);
			$uid = $UserM->max('id');
			$where = array();
			$where['uid'] = $uid;
			$userInfo = $UserM->getList($where);
			if ($res){
				session("userInfo",$userInfo);
				$this->ajaxReturn(["err"=>200,"msg"=>"绑定成功！"]);
			}else{
				$this->ajaxReturn(["err"=>500,"msg"=>"绑定失败！"]);
			}
		}
	}

	//获取验证码
	public function get_verification_code(){
		$data = I('');
		$phone=$data['phone'];
		//生成验证码
		$code=str_shuffle(mt_rand(1000,9999));
		$redis = new RedisModel();
		$getCodeNum = $redis->get("login_code_num_".$phone);
		if($getCodeNum > 60 ){
			$this->ajaxReturn(["err"=>500,"msg"=>"发送太频繁！"]);
		}else{
			$redis->set("login_code_num_".$phone,$getCodeNum+1,0,0,60 * 60 * 8);
			$res = $this->sendSms($phone,$code);
			if ($res){
				//验证码存Redis
				$login_code = $redis->set("login_code_".$phone,$code,0,0,60 * 2);
				if (!$login_code){
					return false;
				}
				$this->ajaxReturn(["err"=>200,"msg"=>"发送成功！"]);
			}else{
				$this->ajaxReturn(["err"=>500,"msg"=>"120秒内仅能获取一次短信验证码,请稍后重试！"]);
			}
		}
	}

	//发送验证码
	public function sendSms($phone,$code) {
		$params = array ();
		// fixme 必填：是否启用https
		$security = false;

		$accessKeyId = "LTAI4Fnipr52yvJxeeBk3ouJ";
		$accessKeySecret = "7uetEZWWfMRn3yjLPRHx7mX3cjxeHF";

		// fixme 必填: 短信接收号码
		$params["PhoneNumbers"] = $phone;

		// fixme 必填: 短信签名，应严格按"签名名称"填写，请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/sign
		$params["SignName"] = "话蜀";

		// fixme 必填: 短信模板Code，应严格按"模板CODE"填写, 请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/template
		$params["TemplateCode"] = "SMS_180340412";

		// fixme 可选: 设置模板参数, 假如模板中存在变量需要替换则为必填项
		$params['TemplateParam'] = Array (
			"code" => $code,
		);
		// fixme 可选: 设置发送短信流水号
//		$params['OutId'] = "12345";
		// fixme 可选: 上行短信扩展码, 扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段
//		$params['SmsUpExtendCode'] = "1234567";

		// *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
		if(!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
			$params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
		}

		// 初始化SignatureHelper实例用于设置参数，签名以及发送请求
		$helper = new SignatureHelperController();

		// 此处可能会抛出异常，注意catch
		$content = $helper->request(
			$accessKeyId,
			$accessKeySecret,
			"dysmsapi.aliyuncs.com",
			array_merge($params, array(
				"RegionId" => "cn-hangzhou",
				"Action" => "SendSms",
				"Version" => "2017-05-25",
			)),
			$security
		);

		return $content;
	}

	//退出登录
	public function logout(){
		session("userInfo",null);
		cookie("userLoginInfo",null);
		session("addressInfo",null);
		$this->ajaxReturn(["err"=>200,"msg"=>"退出成功"]);
	}


}

?>
