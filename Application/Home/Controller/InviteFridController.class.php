<?php

class InviteFridController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//加载 邀请好友页面
	public function invitefrid(){
		$uid = $_SESSION['userInfo']['uid'];
		$code=str_shuffle(mt_rand(100000,999999));
		$codename = M('user')->where(["uid"=>$uid])->getField('code');
		if ($codename == 0){
			$code = $code;
			$data['code'] = $code;
			M('user')->where(["uid"=>$uid])->save($data);
		}else{
			$userInfo =  M('user')->where(["uid"=>$uid])->find();
			$code = $userInfo['code'];
		}
		$this->code = $code;
		$this->display("invitefrid");
	}

	//加载 二维码页面
	public function sendewm(){
		$userInfo =  M('user')->where(["uid"=>$_SESSION['userInfo']['uid']])->find();
		$this->code = $userInfo['code'];
		$this->display("sendewm");
	}

	//我的邀请人
	public function myyqr(){
		//接收自己填写的上级邀请码
		$data = I('');
		//通过接收的上级邀请码，查询出上级信息
		$myupInfo =  M('user')->where(["code"=>$data['up_code']])->find();
		$msgurl=C ("IMAGEURL");
		if ($myupInfo['ufile']){
			$ufile = "http://limida.oss-cn-hongkong.aliyuncs.com/data".$myupInfo['ufile'];
		}else{
			$ufile = "__PUBLIC__/home/images/headimg.png";
		}

		//判断上级是否为平台推广人员
		if ($myupInfo){
			//上级为普通会员 0
			if ($myupInfo['is_extension'] == 0){
			//---绑定上级邀请码（把自己的上级邀请码添加到表里）---
				M('user')->where(["uid"=>$_SESSION['userInfo']['uid']])->save($data);
				//我的上级的上级邀请码（我的上上级）
				$data2['up_up_code'] = $myupInfo['up_code'];
			//---绑定上上级邀请码（把自己的上上级邀请码添加到表里）---
				M('user')->where(["uid"=>$_SESSION['userInfo']['uid']])->save($data2);
			//---返给上级积分---
				$integral = $myupInfo['integral']; //上级的原有积分
				//返积分信息
				$returnIntegralInfo = M('returnIntegral')->select();
				//上级的原有积分 + 所返积分
				$map['integral'] = $returnIntegralInfo[0]['return_up_integral'] + $integral;
				//修改上级的积分
				M('user')->where(["code"=>$data['up_code']])->save($map);
			}else{
			//上级为推广人员（注册成功不返积分） 1
				//---绑定上级邀请码（把自己的上级邀请码添加到表里）---
				M('user')->where(["uid"=>$_SESSION['userInfo']['uid']])->save($data);
			}
		}
		$this->ajaxReturn(["err"=>200,"nickname"=>$myupInfo['nickname'],"ufile"=>$ufile]);
	}

	//邀请明细
	public function invitefridmx(){
		//查我自己的信息 拿出上级邀请码
		$myInfo = M('user')->where(["uid"=>$_SESSION['userInfo']['uid'],'is_del'=>1])->find();
		//通过我的上级邀请码 查上级信息
		$upInfo = M('user')->where(["code"=>$myInfo['up_code'],'is_del'=>1])->find();
		//我邀请的人信息(上级邀请码为我的邀请码)
		$myyqInfo =  M('user')->where(["up_code"=>$myInfo['code'],'is_del'=>1])->select();
		//我邀请的人的数量
		$count = count($myyqInfo);
		$this->msgurl=C ("IMAGEURL");
		$this->myyqInfo = $myyqInfo;
		$this->myInfo = $myInfo;
		$this->upInfo = $upInfo;
		$this->count = $count;
		$this->display("invitefridmx");
	}


}

?>
