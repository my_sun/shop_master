<?php
use Think\Db;

class SearchFindController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//加载搜索发现列表
	public function searchfind(){
		$data = I('');
		//你可能喜欢信息
		$SearchFindM = new SearchFindModel();
		$SearchFindInfo = $SearchFindM->getAll();
		//搜索历史信息
		$SearchHistoryInfo = M('search_history')->where(['is_del'=>1])->order('id desc')->limit(0,10)->select();
		$count = "";
		foreach ($SearchHistoryInfo as $k=>$v){
			$count += strlen($v['name']);
		}
		$hcount = $count/3; //统计个数
		$this->SearchFindInfo = $SearchFindInfo;
		$this->SearchHistoryInfo = $SearchHistoryInfo;
		$this->hcount = $hcount;
		$this->keywords = $data['keywords'];
		$this->display("searchhistory");
	}

	//清空搜索历史
	public function deletehis(){
		$id = I('post.');
		$where = array();
		$where['id'] = array('in',$id['hids']);
		$res = M('search_history')->where($where)->delete();
		if ($res){
			$this->ajaxReturn(['err'=>200,'msg'=>'清空成功']);
		}
	}

	//查询搜索列表数据
	public function searchdata(){
		$keyword = I('post.');
		$key = $keyword['keywords'];

		//拼接条件
		//1.status
		$where = "is_up=1";
		//2.模糊查询商品名 name
		if ($key) {
			$where .= " AND name like '%{$key}%' ";
		}

		//sql语句
		$sql = "SELECT * FROM t_goods WHERE $where LIMIT 0,6";  //条数
		//查询数据
		if ($key){
			$goodsInfo = D()->query($sql);
			foreach ($goodsInfo as $k=>$v){
				$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
			}
		}else{
			$goodsInfo = "";
		}
		//保存到搜索历史表
		$data['name'] = $keyword['keywords'];
		$data['addtime'] = time();
		$SearchHistoryInfo = M('search_history')->where(["name"=>$data['name']])->find();
		if ($SearchHistoryInfo['name'] != $data['name']) {
			$res = M('search_history')->add($data);
		}
		session("goodsInfo",$goodsInfo);
		session("keywords",$key);
//		if ($keyword){
			$this->ajaxReturn(['err'=>200,'msg'=>'成功']);
//		}
	}

	//搜索发现点击跳转 搜索列表(传商品数据)
	public function searchlist(){
		if(I('')){
			$keyword = I('');
			$key = trim($keyword['keyword'],"''");

			//拼接条件
			//1.status
			$where = "is_up=1";
			//2.模糊查询商品名 name
			if ($key) {
				$where .= " AND name like '%{$key}%' ";
			}
			//sql语句
			$sql = "SELECT * FROM t_goods WHERE $where LIMIT 0,6"; //条数
			//查询数据
			$goodsInfo = D()->query($sql);
			foreach ($goodsInfo as $k=>$v){
				$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
			}
			$this->goodsInfo = $goodsInfo;
			$this->keywords = $key;
		}else{
			$this->goodsInfo = $_SESSION['goodsInfo'];
			$this->keywords = $_SESSION['keywords'];
			session("goodsInfo",null);
		}
//		dump($goodsInfo);die;
		$this->msgurl=C("IMAGEURL");
		$this->display('searchend');
	}

	//搜索列表 下拉加载更多
	public function SearchMore(){
		if(IS_POST){
			$data = I('post.');
			$key = $data['keywords'];

			//拼接条件
			//1.status
			$where = "is_up=1";
			//2.模糊查询商品名 name
			if ($key) {
				$where .= " AND name like '%{$key}%' ";
			}

			$page = $data['page']-1; //1
			$pageSize = 6; //每页条数
			$start = $page*$pageSize;
			//sql语句
			$sql = "SELECT * FROM t_goods WHERE $where LIMIT $start,$pageSize";
			//查询数据
			if ($key){
				$goodsInfo = D()->query($sql);
				foreach ($goodsInfo as $k=>$v){
					$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
					$goodsInfo[$k]['msgurl'] = C ("IMAGEURL");
				}
			}else{
				$goodsInfo = "";
			}
			$this->ajaxReturn($goodsInfo);

		}
	}


}

?>
