<?php

class AccountBindController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//加载 账号关联页面
	public function accountbind(){
		$userInfo = M('user')->where(['uid'=>$_SESSION['userInfo']['uid'],'is_del'=>1])->find();
		$this->userInfo = $userInfo;
		$this->display("accountbind");
	}

	//加载 微信关联页面
	public function wxblind(){
		$this->display("wxblind");
	}

	//加载 QQ关联页面
	public function qqblind(){
		$this->display("qqblind");
	}

	//qq 解除关联
	public function relieveqqbind(){
		$map['is_qqbind'] = 0;
		$res = M('user')->where(['uid'=>$_SESSION['userInfo']['uid']])->save($map);
		if ($res){
			$this->ajaxReturn(['err'=>200,"msg"=>"解除qq关联成功"]);
		}else{
			$this->ajaxReturn(['err'=>500,"msg"=>"解除qq关联失败"]);
		}
	}

	//微信 解除关联
	public function relievewxbind(){
		$data['is_wxbind'] = 0;
		$res = M('user')->where(['uid'=>$_SESSION['userInfo']['uid']])->save($data);
		if ($res){
			$this->ajaxReturn(['err'=>200,"msg"=>"解除微信关联成功"]);
		}else{
			$this->ajaxReturn(['err'=>500,"msg"=>"解除微信关联失败"]);
		}
	}



}

?>
