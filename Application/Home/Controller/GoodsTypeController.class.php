<?php

class GoodsTypeController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}

	//商品分类列表
	public function lists(){
		$this->name = '商品分类列表'; // 进行模板变量赋值

		$GoodsTypeM = new GoodsTypeModel();
		//当前页数
		$Page = $_GET['Page'] ? $_GET['Page'] : 1;
		$PageSize = 20;
		//搜索数据
		$WhereArr = array();
		$WhereArr['is_del']='1';
		if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
			$kerword=$_GET['kerword'];
			$WhereArr['name']=array('like',"%".$kerword."%");
		}
		$GoodsTypeInfo = $GoodsTypeM->getListPage($WhereArr,$Page,$PageSize);
		$all_page = ceil($GoodsTypeInfo['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;

		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->GoodsTypeInfo = $GoodsTypeInfo['list'];
		$this->action = "/Adms/GoodsType/lists.html";
		$this->display("lists");
	}

	//添加商品分类
	public function add(){
		$this->name = '添加商品分类';
		$GoodsTypeM = new GoodsTypeModel();
		if($_POST){
			$map = array();
			$map["name"] = $_POST["name"];
			$map["addtime"] = time();
			$GoodsTypeM->addOne($map);
			header('Location:lists.html');
		}
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//修改商品分类
	public function edit(){
		$this->name = '修改商品分类';  // 进行模板变量赋值
		$GoodsTypeM = new GoodsTypeModel();
		//获取id
		$id = $_GET['id'];
		if ($id){
			$where['id'] = $id;
			//查数据
			$GoodsTypeInfo = $GoodsTypeM->getOne($where);
		}
		$this->GoodsTypeInfo = $GoodsTypeInfo;
		$this->id = $id;
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//处理修改
	public function update(){
		$this->name = '修改商品分类';  // 进行模板变量赋值

		$id = $_POST['id'];
		$where = array();
		$GoodsTypeM = new GoodsTypeModel();
		if ($id){
			$data['name'] = $_POST['name'];
			$data['updatetime'] = time();
			$where["id"] = $_POST['id'];
			$res = $GoodsTypeM->updateOne($where,$data);
			if ($res){
				$this->tip = "修改成功";
			}
			header('Location:lists.html');
		}
	}

	//软删除商品分类
	public function del(){
		$this->name = '删除商品分类';
		$GoodsTypeM = new GoodsTypeModel();
		$where = array();
		if($_GET["id"]){
			$where['id'] = $_GET['id'];
			// $data = $GoodsTypeM->getOne($_GET["id"]);
			$datas['is_del'] = 0;
			$ret = $GoodsTypeM->updateOne($where,$datas);
			header("Location:/Adms/GoodsType/lists");
		}
	}
}

?>
