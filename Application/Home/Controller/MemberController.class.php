<?php

class MemberController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//加载 立即开通会员页面
	public function member(){
		$MemberInfo = M('member')->select();
		$this->MemberInfo = $MemberInfo[0];
		$this->display("member");
	}

	//加载 立即加入页面
	public function memrecharge(){
		$MemberInfo = M('member')->select();
		$this->MemberInfo = $MemberInfo[0];
		$this->display("memrecharge");
	}

}

?>
