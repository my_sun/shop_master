<?php

class UserController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/head/';
		$this->msgfileurl = '/userdata/headimg/'.date('Ymd',time())."/";
	}

	//加载 我的页面
	public function mine(){
		$userInfo = M("user")->where(["uid"=>$_SESSION['userInfo']['uid'],'is_del'=>1])->find();
		$this->userInfo = $userInfo;
		$this->msgurl=C ("IMAGEURL");
		$this->display("mine");
	}

	//加载 我的不是会员页面
	public function minenomember(){
		$userInfo = M("user")->where(["uid"=>$_SESSION['userInfo']['uid'],'is_del'=>1])->find();
		$this->userInfo = $userInfo;
		$this->msgurl=C ("IMAGEURL");
		$this->display("minenomember");
	}

	//加载 设置中心页面
	public function setting(){
		$userInfo = M("user")->where(["uid"=>$_SESSION['userInfo']['uid'],'is_del'=>1])->find();
		$this->userInfo = $userInfo;
		$this->msgurl=C ("IMAGEURL");
		$this->display("setting");
	}

	//修改昵称
	public function editNickname(){
		$data['nickname'] = I('post.nickname');
		$UserM = new UserModel();
		$userInfo = $UserM->where(['nickname'=>$data['nickname']])->find();
		if ($userInfo){
			$this->ajaxReturn(["err"=>500,"msg"=>"该昵称已存在，请重新输入"]);exit;
		}else{
			$where = array();
			$where['uid'] = $_SESSION['userInfo']['uid'];
			$res = $UserM->updateOne($where,$data);
			if ($res){
				$this->ajaxReturn(["err"=>200,"msg"=>"昵称修改成功"]);
			}else{
				$this->ajaxReturn(["err"=>500,"msg"=>"昵称修改失败"]);
			}
		}

	}

	//修改头像
	public function editufile(){
		if($_POST){
			$data['ufile'] = $_POST['ufile'];
			$res = M('user')->where(['uid'=>$_SESSION['userInfo']['uid']])->save($data);
			if($res){
				$this->ajaxReturn(["err"=>200,"msg"=>"头像修改成功"]);
			}else{
				$this->ajaxReturn(["err"=>500,"msg"=>"头像修改失败"]);
			}
		}
	}

	/**
	 *
	 */
	public function upload_img(){
		$PutOssarr=$this->_imgupload();
		echo $PutOssarr;
	}

	//加载 账户余额页面
	public function money(){
		$moneyInfo = M("user")->where(["uid"=>$_SESSION['userInfo']['uid'],'is_del'=>1])->select();
		$this->moneyInfo = $moneyInfo[0];
		$this->display("myaccount");
	}

	//加载 我的积分页面
	public function Integral(){
		$userInfo = M('user')->where(["uid"=>$_SESSION['userInfo']['uid'],'is_del'=>1])->find();
		$this->userInfo = $userInfo;
		$this->display("myintegral");
	}

	//加载 积分明细页面
	public function IntegralDetail(){
		$IntegralInfo = M("integral")->where(["uid"=>$_SESSION['userInfo']['uid']])->select();
		$this->IntegralInfo = $IntegralInfo;
		$this->display("integraldetail");
	}

	//加载 联系我们页面
	public function contact(){
		$contactInfo = M('contact')->where("is_del",1)->select();
		$this->contactInfo = $contactInfo;
		$this->display("contactus");
	}

	//加载 是否推送页面
	public function push(){
		$uid = $_SESSION['userInfo']['uid'];
		$userInfo = M('user')->where(["uid"=>$uid])->find();
		$this->uid = $uid;
		$this->userInfo = $userInfo;
		$this->display("msgpush");
	}
	//处理推送
	public function editpush(){
		$uid = $_SESSION['userInfo']['uid'];
		$userInfo = M('user')->where(["uid"=>$uid])->find();
		if ($userInfo['is_push'] == 0){
			$res = M('user')->where(["uid"=>$uid])->save(["is_push"=>1]);
		}else{
			$res = M('user')->where(["uid"=>$uid])->save(["is_push"=>0]);
		}
		if ($res){
			$this->ajaxReturn(["err"=>200,"msg"=>"推送设置成功"]);
		}else{
			$this->ajaxReturn(["err"=>500,"msg"=>"推送设置失败"]);
		}
	}

	//图片上传方法
	protected function _upload($savePath) {
		import("Api.lib.Behavior.fileDirUtil");
		$fileutil = new fileDirUtil();
		$fileutil->createDir($savePath);
		import("Api.lib.Behavior.UploadFile");
		//导入上传类
		$upload = new UploadFile();
		//设置上传文件大小
		$upload->maxSize =10485760;
		//设置上传文件类型
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
		//设置附件上传目录
		$upload->savePath = $savePath;
		//设置需要生成缩略图，仅对图像文件有效
		$upload->thumb = false;
		// 设置引用图片类库包路径
		$upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
		//设置需要生成缩略图的文件后缀
		$upload->thumbPrefix = 'm_';  //生产2张缩略图
		//设置缩略图最大宽度
		$upload->thumbMaxWidth = '400';
		//设置缩略图最大高度
		$upload->thumbMaxHeight = '400';
		//设置上传文件规则
		$upload->saveRule = "md5content";
		//删除原图
		$upload->thumbRemoveOrigin = false;
		if (!$upload->upload()) {
			$return = array();
			$return['code'] = ERRORCODE_501;
			$return['message'] = $upload->getErrorMsg();
			if($_REQUEST['ptesst']==1){
				dump($return);
			}
			//Push_data($return);
			//捕获上传异常
			//$this->error($upload->getErrorMsg());
		} else {
			//取得成功上传的文件信息
			$uploadList = $upload->getUploadFileInfo();
			//$uploadList = $uploadList[0];
			//$uploadList['imageUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . $upload->saveRule;
			//$uploadList['thumbnailUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . 'm_' . $upload->saveRule;

			return $uploadList;
			//import("Extend.Library.ORG.Util.Image");
			//给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
			//Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '/logo2.png');
			//$_POST['image'] = $uploadList[0]['savename'];
		}

	}
	//图片上传封装
	public function _imgupload(){
		$filetype = "images";
		$saveUrl = $this->msgfileurl . $filetype . "/";
		$savePath = WR . $saveUrl;
		//echo $savePath;exit;
		$uploadList = $this->_upload ($savePath);
		if (!empty($uploadList)) {
			// $UserPhotoM = new PhotoModel();
			$PutOssarr = array();
			$returnurlarr = array();
			foreach ($uploadList as $k => $v) {
				$PutOssarr[] = $saveUrl . $v['savename'];
				$returnurlarr[] = C ("IMAGEURL") . $saveUrl . $v['savename'];
			}
			PutOss ($PutOssarr);
			// print_r ($PutOssarr);exit;
			$return = array();
			$return["data"]["url"] = $PutOssarr[0];
			return $PutOssarr[0];
		}
	}

}

?>
