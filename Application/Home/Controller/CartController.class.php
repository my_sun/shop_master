<?php
use Think\Controller;
class CartController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}

	//添加购物车
	public function addcart(){
		$normskw = I('post.normskw');
		$normskwval = I('post.normskwval');
		$normsggval = I('post.normsggval');
		$normsgg = "规格";
		$data['normskw'] = $normskw.':'.$normskwval;
		$data['normsgg'] = $normsgg.':'.$normsggval;
		$data['goods_id'] = I('post.goods_id');
		$data['uid'] = $_SESSION['userInfo']['uid'];
		$data['number'] = 1;
		$data['addtime'] = time();
		//查询商品规格总库存
		$stockInfo = M('norms')->where(['goods_id'=>$data['goods_id'],'is_up'=>1,'normskwval'=>$data['normskw'],'normsggval'=>$data['normsgg']])->find();
		if ($stockInfo['stock'] == 0){
			$this->ajaxReturn(["err"=>500,"msg"=>"库存不足"]);
		}
		//查询是否已经添加过
		$where = array();
		$where['goods_id'] = $data['goods_id'];
		$where['uid'] = $data['uid'];
		$where['normskw'] = $data['normskw'];
		$where['normsgg'] = $data['normsgg'];
		$cartInfo = M('cart')->where(['uid'=>$data['uid'],'goods_id'=>$data['goods_id'],'is_up'=>1,'normskw'=>$data['normskw'],'normsgg'=>$data['normsgg']])->find();
		if ($cartInfo){
			$map['number'] = $cartInfo['number']+1;
			$res = M('cart')->where($where)->save($map);
			if ($res){
				$this->ajaxReturn(["err"=>200,"msg"=>"购物车添加成功"]);
			}
		}else{
			//如果没添加过该类型商品
			$res2 = M('cart')->add($data);
			if ($res2){
				$this->ajaxReturn(["err"=>200,"msg"=>"购物车添加成功"]);
			}
		}
	}

	//加载购物车页面
	public function Index(){
		//购物车信息
		$cartInfo = M('cart')->where(["uid"=>$_SESSION['userInfo']['uid'],'is_del'=>1])->select();
		$gid = array();
		$normskw = array();
		$normsgg = array();
		if ($cartInfo){
			foreach ($cartInfo as $k=>$v){
				$gid[] = $v['goods_id']; //商品id
				$normskw[] = $v['normskw'];//口味
				$normsgg[] = $v['normsgg'];//规格
			}
			$gid = array_unique($gid);
			//商品条件
			$where = array();
			$where['id'] = array('in',$gid);
//			$where['is_up'] = 1;
			//规格条件
			$where2 = array();
			$where2['goods_id'] = array('in',$gid);
			$where2['normskwval'] = array('in',$normskw);
			$where2['normsggval'] = array('in',$normsgg);
//			$where2['is_up'] = 1;
			//抢购条件
			$where3 = array();
			$where3['goods_id'] = array('in',$gid);
//			$where3['is_up'] = 1;
			//商品信息
			$goodsInfo = M('goods')->where($where)->select();
			//规格信息
			$normsInfo = M('norms')->where($where2)->select();
			//抢购信息
			$robInfo = M('rob')->where($where3)->select();
			foreach ($goodsInfo as $k=>$v){
				$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
			}
			//商品信息添加到规格信息里
			foreach ($normsInfo as $k=>$v){
				foreach ($goodsInfo as $k1=>$v1){
					if ($v['goods_id'] == $v1['id']){
						$normsInfo[$k]['goods'] = $v1;
					}
				}
			}
			//抢购信息添加到规格信息里
			foreach ($normsInfo as $k=>$v){
				foreach ($robInfo as $k1=>$v1){
					if ($v['goods_id'] == $v1['goods_id']){
						$normsInfo[$k]['rob'] = $v1;
					}
				}
			}
		}
		$this->msgurl=C("IMAGEURL");
		$this->cartInfo = $cartInfo;
		$this->normsInfo = $normsInfo;
		$this->display("cart");
	}

	//删除购物车单个商品
	public function delOneGoods(){
		$data['id'] = $_POST['id'];
		$res = M('cart')->where(["id"=>$data['id']])->delete();
		if ($res){
			$this->ajaxReturn(["err"=>200,"msg"=>"移除成功"]);
		}else{
			$this->ajaxReturn(["err"=>500,"msg"=>"移除失败"]);
		}
	}
	//删除多个购物车商品
	public function delAllGoods(){
		$data['ids'] = $_POST['ids'];
		$where = array();
		$where['id'] = array('in',$data['ids']);
		$where['uid'] = $_SESSION['userInfo']['uid'];
		$res = M('cart')->where($where)->delete();
		if ($res){
			$this->ajaxReturn(["err"=>200,"msg"=>"移除成功"]);
		}else{
			$this->ajaxReturn(["err"=>500,"msg"=>"移除失败"]);
		}
	}

	//更新购物车数量
	public function UpdateCartNum(){
		$id = I('post.id');
		$data['number'] = I('post.num');
		$res = M('cart')->where(["id"=>$id])->save($data);
		if ($res){
			$this->ajaxReturn(["err"=>200,"msg"=>"购物车更新成功"]);
		}
	}

	//购物车结算 往确认订单页面传数据(存session)
	public function cartSettlement(){
		$data = I('post.');
		//用户信息（用户积分）
		$userInfo = M('user')->where(['uid'=>$_SESSION['userInfo']['uid'],'is_del'=>1])->find();
		//接收购物车id
		if ($data['ids']){
			$goods_integrals = 0;//总积分
			$goods_price = 0;//原价
			$goods_discount_price = 0;//积分不够结算价
			$user_integral = $userInfo['integral'];//用户积分
			foreach ($data['ids'] as $val){
				$cart = M('cart')->where(array("id"=>$val))->find();
				$norms = M('norms')->where(array("goods_id"=>$cart['goods_id'],"normskwval"=>$cart['normskw'],"normsggval"=>$cart['normsgg']))->find();
				if($norms['special_id'] == 2){
					//用户积分减去第一个商品的抵扣积分
					$user_integral -= $norms['integral']* $cart['number'];
					if($user_integral >= 0 ){
						//大于0 当前商品走的积分+折扣价
						$goods_discount_price += $norms['discount_price']* $cart['number'];
						$goods_integrals += $norms['integral']* $cart['number'];
					}else{
						//当前商品走的原价
						$goods_price +=  $norms['price']* $cart['number'];
					}
				}elseif ($norms['special_id'] == 1){
					$goods_discount_price += $norms['discount_price']* $cart['number'];
				}else{
					$goods_price += $norms['price']* $cart['number'];
				}
			}
			//最终结算价格
			$totalprice = $goods_price + $goods_discount_price;
			//最终结算积分
			if($goods_integrals != 0 ){
				//用到了多少积分  需要扣除 $goods_integrals
				$totalintegral = $goods_integrals;
			}else{
				$totalintegral = 0;
			}
		}

		$where = array();
		$goodswhere = array();
		$normswhere = array();
		$gid = array();
		//接收购物车id
		if ($data['ids']){
			foreach ($data['ids'] as $val){
				$where[] = $val;
			}
			$cartwhere["id"] = array('in',$where);
			//购物车信息
			$cartInfo = M('cart')->where($cartwhere)->select();
			if ($cartInfo){
				foreach ($cartInfo as $k=>$v){
					$gid[] = $v['goods_id'];
					$normskwval[] = $v['normskw'];
					$normsggval[] = $v['normsgg'];
				}
				$goodswhere["id"] = array('in',$gid);
				$normswhere["goods_id"] = array('in',$gid);
				$normswhere["normskwval"] = array('in',$normskwval);
				$normswhere["normsggval"] = array('in',$normsggval);
				//商品信息
				$goodsInfo = M('goods')->where($goodswhere)->select();
				//规格信息
				$normsInfo = M('norms')->where($normswhere)->select();
			}
		}

		session("cartInfo",$cartInfo);
		session("goodsInfo",$goodsInfo);
		session("normsInfo",$normsInfo);
		session("userInfo",$userInfo);
		session("totalprice",$totalprice);
		session("totalintegral",$totalintegral);
//		session("totalintegrals",$totalintegrals);
//		if ($totalintegral != 0 &&  $totalintegral > 0){
			$this->ajaxReturn(["err"=>200,"msg"=>"成功"]);
//		}
	}
}

?>
