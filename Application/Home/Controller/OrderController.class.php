<?php

class OrderController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//接收购物车的结算数据 加载 确认下单页面
	public function confirmorder(){
		//用户地址信息
		$addressInfo = M('address')->where(['uid'=>$_SESSION['userInfo']['uid']])->select();
		$address = array();
		if(count($addressInfo) == 1){
			$address = $addressInfo;
		}else if (count($addressInfo) > 1){
			foreach ($addressInfo as $k=>$v){
				if ($v['is_default'] == 1){
					$address = $v;
				}
			}
		}
		//商品信息
		$goodsInfo = $_SESSION['goodsInfo'];
		foreach ($goodsInfo as $k=>$v){
			$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
		}
		$this->addressInfo = $address;
		$this->cartInfo = $_SESSION['cartInfo'];
		$this->goodsInfo = $goodsInfo;
		$this->normsInfo = $_SESSION['normsInfo'];
		$this->userInfo = $_SESSION['userInfo'];
		$this->totalprice = $_SESSION['totalprice'];//总价
		$this->totalintegral = $_SESSION['totalintegral'];//总积分
		session("totalintegral",null);
		$this->msgurl=C ("IMAGEURL");
		$this->display('confirmorder');
	}

	//接收确认订单页面传来的 提交订单数据
	public function orderdfkshuju(){
		$data = I('');
		//用户收货地址信息
		$addressInfo = M('address')->where(['id'=>$data['aid'],'is_del'=>1])->find();
		//规格信息
		$normswhere = array();
		$normswhere['id'] = array('in',$data['nids']);
		$normswhere['is_up'] = 1;
		$normsInfo = M('norms')->where($normswhere)->select();
		//商品信息  商品查询条件
		$goodswhere = array();
		$goodswhere['id'] = array('in',$data['gids']);
		$goodswhere['is_up'] = 1;
		$goodsInfo = M('goods')->where($goodswhere)->select();
		foreach ($goodsInfo as $k=>$v){
			$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
			$goodsInfo[$k]['normskw'] = $data['normskw'][$k];
			$goodsInfo[$k]['normsgg'] = $data['normsgg'][$k];
			$goodsInfo[$k]['num'] = $data['OrderNums'][$k];
			$goodsInfo[$k]['allprice'] = $data['allprice'][$k];
			$goodsInfo[$k]['allintegral'] = $data['allintegral'][$k];
			$goodsInfo[$k]['remarks'] = $data['remarks'][$k];
			foreach ($normsInfo as $k2=>$v2){
				if($v2['goods_id'] == $v['id']){
					$goodsInfo[$k]['norms'] = $v2;
				}
			}
		}
		//添加原价
		foreach($data['goods'] as $k=>$v){
			foreach($normsInfo as $k2=>$v2){
				if ($v2['goods_id'] == $v['gid'])
				$data['goods'][$k]['price'] = $v2['price'];
			}
		}
		$maps['totalprice'] = 0;
		$maps['integral'] = 0;
		foreach($data['goods'] as $k=>$v){
			//积分商品
			if ($data['totalintegral'] > 0 && $v['sid'] == 2){
				if ($data['userjf'] < $data['totalintegral']){ //用户积分小于抵扣积分
					//积分不够走原价
					$maps['totalprice'] += $v['price']*$v['num']; //原价*数量
					$maps['integral'] += 0;
				}else{
					$maps['totalprice'] += $v['discount_price']*$v['num']; //折扣价*数量
					$maps['integral'] += $v['allintegral'];
				}
			} elseif($v['sid'] == 1){
				//拼团抢购
				$maps['totalprice'] += $v['discount_price']*$v['num']; //折扣价*数量
				$maps['integral'] += 0;
			}else{
				//新品推荐
				$maps['totalprice'] += $v['price']*$v['num']; //原价*数量
				$maps['integral'] += 0;
			}
		}
		//数据 保存到订单表
		$cart_order = $this->make_no();//订单号
		foreach($data['goods'] as $k=>$v){
			$map['uid'] = $_SESSION['userInfo']['uid'];
			$map['goods_id'] = $v['gid'];
			$map['goodsno'] = $this->make_goodsno($v['gid']); //商品号
			$map['order'] = $cart_order;//订单号
			//积分商品
//			if ($data['totalintegral'] > 0 && $v['sid'] == 2){
//				if ($data['userjf'] < $data['totalintegral']){ //用户积分小于抵扣积分
//					//积分不够走原价
//					$map['totalprice'] = $v['price']*$v['num']; //原价*数量
//					$map['integral'] = 0;
//					//$this->ajaxReturn(['err'=>500,'msg'=>'积分不足，按照商品原价结算']);
//				}else{
//					$map['totalprice'] = $v['discount_price']*$v['num']; //折扣价*数量
//					$map['integral'] = $data['totalintegral'];
//				}
//			} elseif($v['sid'] == 1){
//				//拼团抢购
//				$map['totalprice'] = $v['discount_price']*$v['num']; //折扣价*数量
//				$map['integral'] = 0;
//			}else{
//				//新品推荐
//				$map['totalprice'] = $v['price']*$v['num']; //原价*数量
//				$map['integral'] = 0;
//			}
			$map['num'] = $v['num'];
			$map['remarks'] = $v['remarks'];
			$map['name'] = $addressInfo['name'];
			$map['phone'] = $addressInfo['phone'];
			$map['address_info'] = $addressInfo['address_info'];
			$map['details_address'] = $addressInfo['details_address'];
			$map['price'] = $v['price']; //原价
			$map['normskwval'] = $v['normskw'];
			$map['normsggval'] = $v['normsgg'];
			$map['discount_price'] = $v['discount_price']; //折扣价
			$map['allprice'] = $v['allprice']; //右边总价
			$map['allintegral'] = $v['allintegral']; //右边总积分
			$map['totalprice'] = $maps['totalprice']; //结算总价
			$map['integral'] = $maps['integral']; //结算总积分
			$map['status'] = 0;
			$map['addtime'] = time();
			$res = M('order')->add($map);
		}
		//取最新添加的订单id
		$orderM = new OrderModel();
		$oid = $orderM->max('id');
		$orderInfo = M('order')->where(['id'=>$oid,'is_del'=>1])->find();
//		session('goodsInfo',$goodsInfo);
//		session('addressInfo',$addressInfo);
//		session('oid',$oid);
		if ($res){
			$this->ajaxReturn(['err'=>200,'msg'=>'订单提交成功','totalprice'=>$orderInfo['totalprice'],'totalintegral'=>$orderInfo['integral'],'oid'=>$oid,'order'=>$orderInfo['order']]);
		}
	}

	//购物车 提交订单页面（多余方法）
	public function orderdfk(){
		$id = $_SESSION['oid'];
		$orderInfo = M('order')->where(['id'=>$id,'is_del'=>1])->find();
		$this->addressInfo = $_SESSION['addressInfo'];
		$this->goodsInfo = $_SESSION['goodsInfo'];
		$this->orderInfo = $orderInfo;
		$this->msgurl = C('IMAGEURL');
		$this->display('orderdetail1');
	}
//-------------------------------------------------------------
	//加载 待付款订单详情页面
	public function orderdfkdetail(){
		$data['id'] = $_GET['id'];
		//订单信息
		$orderInfo = M('order')->where(["uid"=>$_SESSION['userInfo']['uid'],"id"=>$data['id'],"is_del"=>1])->find();
		$orderInfo2 = M('order')->where(['order'=>$orderInfo['order'],'is_del'=>1])->select();
		if ($orderInfo2){
			$gid = array();
			//规格信息
			$normswhere = array();
			foreach ($orderInfo2 as $k=>$v){
				$gid[] = $v['goods_id'];
				$normswhere[$k]['goods_id'] = $v['goods_id'];
				$normswhere[$k]['normskwval'] = $v['normskwval'];
				$normswhere[$k]['normsggval'] = $v['normsggval'];
			}
			$normsInfo = array();
			foreach($normswhere as $k=>$v){
				$normsInfo[] = M('norms')->where(['goods_id'=>$v['goods_id'],"normskwval"=>$v['normskwval'],'normsggval'=>$v['normsggval'],'is_up'=>1])->select();
			}
			//商品信息
			$where['id'] = array('in',$gid);
			$where['is_up'] = 1;
			$goodsInfo = M('goods')->where($where)->select();
			foreach ($goodsInfo as $k=>$v){
				$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
			}
		}
		//把商品信息追加到订单数组里
		foreach ($orderInfo2 as $k=>$v){
			foreach ($goodsInfo as $k1=>$v1){
				if($v['goods_id'] == $v1['id']){
					$orderInfo2[$k]['goods'] = $v1;
				}
			}
		}
		//把规格信息追加到订单数组里
		foreach ($orderInfo2 as $k=>$v){
			foreach ($normsInfo as $k1=>$v1){
				if($v['goods_id'] == $v1[0]['goods_id']){
					$orderInfo2[$k]['norms'] = $v1;
				}
			}
		}
		$orderData = array();
		foreach ($orderInfo2 as $k=>$v){
			$orderData[$v['order']]['list'][] = $v;
		}
		$this->orderData = $orderData;
		$this->orderInfo = $orderInfo;
		$this->msgurl=C("IMAGEURL");
		$this->display("orderdfkdetail");
	}
	//加载 待收货订单详情页面
	public function orderdshdetail(){
		$data['id'] = $_GET['id'];
		//订单信息
		$orderInfo = M('order')->where(["uid"=>$_SESSION['userInfo']['uid'],"id"=>$data['id'],"is_del"=>1])->find();
		$orderInfo2 = M('order')->where(['order'=>$orderInfo['order'],'is_del'=>1])->select();
		if ($orderInfo2){
			$gid = array();
			//规格信息
			$normswhere = array();
			foreach ($orderInfo2 as $k=>$v){
				$gid[] = $v['goods_id'];
				$normswhere[$k]['goods_id'] = $v['goods_id'];
				$normswhere[$k]['normskwval'] = $v['normskwval'];
				$normswhere[$k]['normsggval'] = $v['normsggval'];
			}
			$normsInfo = array();
			foreach($normswhere as $k=>$v){
				$normsInfo[] = M('norms')->where(['goods_id'=>$v['goods_id'],"normskwval"=>$v['normskwval'],'normsggval'=>$v['normsggval'],'is_up'=>1])->select();
			}
			//商品信息
			$where['id'] = array('in',$gid);
			$where['is_up'] = 1;
			$goodsInfo = M('goods')->where($where)->select();
			foreach ($goodsInfo as $k=>$v){
				$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
			}
		}
		//把商品信息追加到订单数组里
		foreach ($orderInfo2 as $k=>$v){
			foreach ($goodsInfo as $k1=>$v1){
				if($v['goods_id'] == $v1['id']){
					$orderInfo2[$k]['goods'] = $v1;
				}
			}
		}
		//把规格信息追加到订单数组里
		foreach ($orderInfo2 as $k=>$v){
			foreach ($normsInfo as $k1=>$v1){
				if($v['goods_id'] == $v1[0]['goods_id']){
					$orderInfo2[$k]['norms'] = $v1;
				}
			}
		}
		$orderData = array();
		foreach ($orderInfo2 as $k=>$v){
			$orderData[$v['order']]['list'][] = $v;
		}
		$this->orderData = $orderData;
		$this->orderInfo = $orderInfo;
		$this->msgurl=C("IMAGEURL");
		$this->display("orderdshdetail");
	}
	//加载 已完成订单详情页面
	public function orderywcdetail(){
		$data['id'] = $_GET['id'];
		//订单信息
		$orderInfo = M('order')->where(["uid"=>$_SESSION['userInfo']['uid'],"id"=>$data['id'],"is_del"=>1])->find();
		$orderInfo2 = M('order')->where(['order'=>$orderInfo['order'],'is_del'=>1])->select();
		if ($orderInfo2){
			$gid = array();
			//规格信息
			$normswhere = array();
			foreach ($orderInfo2 as $k=>$v){
				$gid[] = $v['goods_id'];
				$normswhere[$k]['goods_id'] = $v['goods_id'];
				$normswhere[$k]['normskwval'] = $v['normskwval'];
				$normswhere[$k]['normsggval'] = $v['normsggval'];
			}
			$normsInfo = array();
			foreach($normswhere as $k=>$v){
				$normsInfo[] = M('norms')->where(['goods_id'=>$v['goods_id'],"normskwval"=>$v['normskwval'],'normsggval'=>$v['normsggval'],'is_up'=>1])->select();
			}
			//商品信息
			$where['id'] = array('in',$gid);
			$where['is_up'] = 1;
			$goodsInfo = M('goods')->where($where)->select();
			foreach ($goodsInfo as $k=>$v){
				$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
			}
		}
		//把商品信息追加到订单数组里
		foreach ($orderInfo2 as $k=>$v){
			foreach ($goodsInfo as $k1=>$v1){
				if($v['goods_id'] == $v1['id']){
					$orderInfo2[$k]['goods'] = $v1;
				}
			}
		}
		//把规格信息追加到订单数组里
		foreach ($orderInfo2 as $k=>$v){
			foreach ($normsInfo as $k1=>$v1){
				if($v['goods_id'] == $v1[0]['goods_id']){
					$orderInfo2[$k]['norms'] = $v1;
				}
			}
		}
		$orderData = array();
		foreach ($orderInfo2 as $k=>$v){
			$orderData[$v['order']]['list'][] = $v;
		}
		$this->orderData = $orderData;
		$this->orderInfo = $orderInfo;
		$this->msgurl=C("IMAGEURL");
		$this->display("orderywcdetail");
	}
//-------------------------------------------------------------
	//加载 全部订单页面
	public function allorder(){
		//订单信息
		$orderInfo = M('order')->where(["uid"=>$_SESSION['userInfo']['uid'],"is_del"=>1])->select();
		if ($orderInfo){
			//规格信息
			$gid = array();
			$normswhere = array();
			foreach ($orderInfo as $k=>$v){
				$gid[] = $v['goods_id'];
				$normswhere[$k]['goods_id'] = $v['goods_id'];
				$normswhere[$k]['normskwval'] = $v['normskwval'];
				$normswhere[$k]['normsggval'] = $v['normsggval'];
			}
			$normsInfo = array();
			foreach($normswhere as $k=>$v){
				$normsInfo[] = M('norms')->where(['goods_id'=>$v['goods_id'],"normskwval"=>$v['normskwval'],'normsggval'=>$v['normsggval'],'is_up'=>1])->select();
			}
			//商品信息
			$goodswhere['id'] = array('in',$gid);
			$goodswhere['is_up'] = 1;
			$goodsInfo = M('goods')->where($goodswhere)->select();
			foreach ($goodsInfo as $k=>$v){
				$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
			}
			//规格信息追加到商品数组里
//			foreach ($normsInfo as $k=>$v){
//				foreach ($goodsInfo as $k2=>$v2){
//					if ($v[0]['goods_id'] == $v2['id']){
//						$goodsInfo[$k]['norms'] = $v[0];
//					}
//				}
//			}
		}
		//把商品信息追加到订单数组里
		foreach ($orderInfo as $k=>$v){
			foreach ($goodsInfo as $k1=>$v1){
				if($v['goods_id'] == $v1['id']){
					$orderInfo[$k]['goods'] = $v1;
				}
			}
		}
		//把规格信息追加到订单数组里
		foreach ($orderInfo as $k=>$v){
			foreach ($normsInfo as $k1=>$v1){
				if($v['goods_id'] == $v1[0]['goods_id']){
					$orderInfo[$k]['norms'] = $v1;
				}
			}
		}
		$orderData = array();
		foreach ($orderInfo as $k=>$v){
			$orderData[$v['order']]['list'][] = $v;
		}
		$dfknum = M('order')->where(["uid"=>$_SESSION['userInfo']['uid'],"is_del"=>1,"status"=>0])->count();
		$dshnum = M('order')->where(["uid"=>$_SESSION['userInfo']['uid'],"is_del"=>1,"status"=>1])->count();
		$ywcnum = M('order')->where(["uid"=>$_SESSION['userInfo']['uid'],"is_del"=>1,"status"=>2])->count();
		$this->orderInfo = $orderInfo;
		$this->orderData = $orderData;
		$this->goodsInfo = $goodsInfo;
		$this->dfknum = $dfknum;
		$this->dshnum = $dshnum;
		$this->ywcnum = $ywcnum;
		$this->msgurl=C("IMAGEURL");
		$this->display("allorder");
	}
	//加载 待付款订单页面
	public function pendaorder(){
		//订单信息
		$orderInfo = M('order')->where(["uid"=>$_SESSION['userInfo']['uid'],"status"=>0,"is_del"=>1])->select();
		if ($orderInfo){
			$gid = array();
			$normswhere = array();
			foreach ($orderInfo as $k=>$v){
				$gid[] = $v['goods_id'];
				$normswhere[$k]['goods_id'] = $v['goods_id'];
				$normswhere[$k]['normskwval'] = $v['normskwval'];
				$normswhere[$k]['normsggval'] = $v['normsggval'];
			}
			//规格信息
			$normsInfo = array();
			foreach($normswhere as $k=>$v){
				$normsInfo[] = M('norms')->where(['goods_id'=>$v['goods_id'],"normskwval"=>$v['normskwval'],'normsggval'=>$v['normsggval'],'is_up'=>1])->select();
			}
			//商品信息
			$where['id'] = array('in',$gid);
			$where['is_up'] = 1;
			$goodsInfo = M('goods')->where($where)->select();
			foreach ($goodsInfo as $k=>$v){
				$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
			}
		}
		//把商品信息追加到订单数组里
		foreach ($orderInfo as $k=>$v){
			foreach ($goodsInfo as $k1=>$v1){
				if($v['goods_id'] == $v1['id']){
					$orderInfo[$k]['goods'] = $v1;
				}
			}
		}
		//把规格信息追加到订单数组里
		foreach ($orderInfo as $k=>$v){
			foreach ($normsInfo as $k1=>$v1){
				if($v['goods_id'] == $v1[0]['goods_id']){
					$orderInfo[$k]['norms'] = $v1;
				}
			}
		}
		$orderData = array();
		foreach ($orderInfo as $k=>$v){
			$orderData[$v['order']]['list'][] = $v;
		}
		$this->orderData = $orderData;
//		$this->goodsInfo = $goodsInfo;
		$this->msgurl=C("IMAGEURL");
		$this->display("pendaorder");
	}
	//加载 待收货订单页面
	public function receiveorder(){
		//订单信息
		$orderInfo = M('order')->where(["uid"=>$_SESSION['userInfo']['uid'],"status"=>1,"is_del"=>1])->select();
		if ($orderInfo){
			$gid = array();
			$normswhere = array();
			foreach ($orderInfo as $k=>$v){
				$gid[] = $v['goods_id'];
				$normswhere[$k]['goods_id'] = $v['goods_id'];
				$normswhere[$k]['normskwval'] = $v['normskwval'];
				$normswhere[$k]['normsggval'] = $v['normsggval'];
			}
			//规格信息
			$normsInfo = array();
			foreach($normswhere as $k=>$v){
				$normsInfo[] = M('norms')->where(['goods_id'=>$v['goods_id'],"normskwval"=>$v['normskwval'],'normsggval'=>$v['normsggval'],'is_up'=>1])->select();
			}
			//商品信息
			$where['id'] = array('in',$gid);
			$where['is_up'] = 1;
			$goodsInfo = M('goods')->where($where)->select();
			foreach ($goodsInfo as $k=>$v){
				$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
			}
		}
		//把商品信息追加到订单数组里
		foreach ($orderInfo as $k=>$v){
			foreach ($goodsInfo as $k1=>$v1){
				if($v['goods_id'] == $v1['id']){
					$orderInfo[$k]['goods'] = $v1;
				}
			}
		}
		//把规格信息追加到订单数组里
		foreach ($orderInfo as $k=>$v){
			foreach ($normsInfo as $k1=>$v1){
				if($v['goods_id'] == $v1[0]['goods_id']){
					$orderInfo[$k]['norms'] = $v1;
				}
			}
		}
		$orderData = array();
		foreach ($orderInfo as $k=>$v){
			$orderData[$v['order']]['list'][] = $v;
		}
		$this->orderData = $orderData;
		$this->msgurl=C("IMAGEURL");
		$this->display("receiveorder");
	}
	//加载 已完成订单页面
	public function completedorder(){
		//订单信息
		$orderInfo = M('order')->where(["uid"=>$_SESSION['userInfo']['uid'],"status"=>2,"is_del"=>1])->select();
		if ($orderInfo){
			$gid = array();
			$normswhere = array();
			foreach ($orderInfo as $k=>$v){
				$gid[] = $v['goods_id'];
				$normswhere[$k]['goods_id'] = $v['goods_id'];
				$normswhere[$k]['normskwval'] = $v['normskwval'];
				$normswhere[$k]['normsggval'] = $v['normsggval'];
			}
			//规格信息
			$normsInfo = array();
			foreach($normswhere as $k=>$v){
				$normsInfo[] = M('norms')->where(['goods_id'=>$v['goods_id'],"normskwval"=>$v['normskwval'],'normsggval'=>$v['normsggval'],'is_up'=>1])->select();
			}
			//商品信息
			$where['id'] = array('in',$gid);
			$where['is_up'] = 1;
			$goodsInfo = M('goods')->where($where)->select();
			foreach ($goodsInfo as $k=>$v){
				$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
			}
		}
		//把商品信息追加到订单数组里
		foreach ($orderInfo as $k=>$v){
			foreach ($goodsInfo as $k1=>$v1){
				if($v['goods_id'] == $v1['id']){
					$orderInfo[$k]['goods'] = $v1;
				}
			}
		}
		//把规格信息追加到订单数组里
		foreach ($orderInfo as $k=>$v){
			foreach ($normsInfo as $k1=>$v1){
				if($v['goods_id'] == $v1[0]['goods_id']){
					$orderInfo[$k]['norms'] = $v1;
				}
			}
		}
		$orderData = array();
		foreach ($orderInfo as $k=>$v){
			$orderData[$v['order']]['list'][] = $v;
		}
		$this->orderData = $orderData;
		$this->msgurl=C("IMAGEURL");
		$this->display("completedorder");
	}
//-------------------------------------------------------------
	//软取消订单
	public function qxorder(){
		$data = I('');
		$where = array();
		$where['is_del'] = 0;
		$res = M('order')->where(["order"=>$data['order']])->save($where);
//		$res = M('order')->where(["order"=>$data['order']])->delete(); //彻底删除
		if ($res){
			$this->ajaxReturn(["err"=>200,"msg"=>"订单取消成功"]);
		}else{
			$this->ajaxReturn(["err"=>500,"msg"=>"订单取消失败"]);
		}
	}
	//软删除订单
	public function scorder(){
		$data = I('');
		$where = array();
		$where['is_del'] = 0;
		$res = M('order')->where(["order"=>$data['order']])->save($where);
		if ($res){
			$this->ajaxReturn(["err"=>200,"msg"=>"订单删除成功"]);
		}else{
			$this->ajaxReturn(["err"=>500,"msg"=>"订单删除失败"]);
		}
	}
	//查看物流页面
	public function logistics(){
		$data['id'] = $_GET['id'];
		$orderInfo = M('order')->where(["id"=>$data['id'],'is_del'=>1])->find();
		$goodsInfo = M('goods')->where(["id"=>$orderInfo['goods_id'],'is_up'=>1])->select();
		foreach ($goodsInfo as $k=>$v){
			$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
		}
		$this->msgurl=C("IMAGEURL");
		$this->orderInfo = $orderInfo;
		$this->goodsInfo = $goodsInfo;
		$this->display("logistics");
	}

	/**
	 * 生成订单号
	 * @return string
	 */
	function make_no()
	{
		return strval(date('YmdHis') . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 4));
	}

	/**
	 * 生成商品号
	 * @return string
	 */
	function make_goodsno($gid)
	{
		return mt_rand(1000,9999).'_' .$gid;
	}


}

?>
