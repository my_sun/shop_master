<?php

class PayController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}

	//加载 立即付款 支付页面
	public function payment(){
		$data = I('');
		$userInfo = M('user')->where(['uid'=>$_SESSION['userInfo']['uid'],'is_del'=>1])->find();
		$this->totalprice = $data['totalprice'];
		$this->totalintegral = $data['totalintegral'];
		$this->oid = $data['oid'];
		$this->order = $data['order'];
		$this->userInfo = $userInfo;
		$this->display("payment");
	}

	//处理支付
	public function pay(){
		$data = I('');
		//用户信息
		$userInfo = M('user')->where(['uid'=>$_SESSION['userInfo']['uid'],'is_del'=>1])->find();
		//判断支付密码是否为空

		//判断支付密码是否正确
		if (md5($data['pwd']) != $userInfo['pay_pwd']){
			$this->ajaxReturn(['err'=>500,'msg'=>'支付密码错误，请重新输入']);
		}
		if ($data['paytype'] == 1){
		// 一.余额支付-----------
			//余额不足 支付失败
			if ($userInfo['balance'] < $data['totalprice']){
				$this->ajaxReturn(['err'=>500,'msg'=>'余额不足']);
			}
			//1.积分+现金支付
			if ($data['totalprice'] > 0 && $data['totalintegral'] > 0){
				$map['balance'] = $userInfo['balance'] - $data['totalprice'];
				$map['integral'] = $userInfo['integral'] - $data['totalintegral'];
			} else if($data['totalprice'] == 0 && $data['totalintegral'] > 0){
			//2.纯积分支付
				$map['integral'] = $userInfo['integral'] - $data['totalintegral'];
			}else{
			//3.积分不够按金额支付
				$map['balance'] = $userInfo['balance'] - $data['totalprice'];
			}
			//修改用户的积分
			$res = M('user')->where(["uid"=>$_SESSION['userInfo']['uid'],'is_del'=>1])->save($map);
			//查询修改后的用户信息
			$newuserInfo = M('user')->where(['uid'=>$_SESSION['userInfo']['uid'],'is_del'=>1])->find();
			//支付成功 修改订单状态为待收货
			$statusres = M('order')->where(['order'=>$data['order'],'is_del'=>1])->save(['status'=>1]);
		}elseif($data['paytype'] == 2){
		//二.微信支付--------------
		}else{
		//三.支付宝支付------------
		}

		//---支付成功奖励用户本人,上级及上上级积分---
		if($res){
			//订单信息
			$orderInfo = M('order')->where(['order'=>$data['order'],'is_del'=>1])->select();
			//商品信息
			$gid = array();
			foreach($orderInfo as $k=>$v){
				$gid[] = $v['goods_id'];
			}
			$goodswhere = array();
			$goodswhere['id'] = array('in',$gid);
			$goodswhere['is_up'] = 1;
			$goodsInfo = M('goods')->where($goodswhere)->select();
			//上级用户信息
			$upInfo = M('user')->where(['code'=>$userInfo['up_code'],'is_del'=>1])->find();
			//上上级用户信息
			$upupInfo = M('user')->where(['code'=>$upInfo['up_code'],'is_del'=>1])->find();
			//判断上级用户是否为推广人员 若上级为推广人员则不赠送积分并不查找上上级赠送积分
			$integraltui = 0;
			if($upInfo['is_extension'] == 1){
				foreach($goodsInfo as $k=>$v){
					//奖励用户本人的积分
					$integraltui += $v['reward_integral'];
				}
				//用户原有积分+奖励积分
				$integralstui['integral'] = $newuserInfo['integral'] + $integraltui;
				//修改用户本人积分
				$resuser = M('user')->where(['uid'=>$_SESSION['userInfo']['uid'],'is_del'=>1])->save($integralstui);
			}
			//若上级不为推广人员 奖励上级及上上级积分
			$integral = 0;
			$up_integral = 0;
			$up_up_integral = 0;
			if ($upInfo['is_extension'] != 1){
				foreach($goodsInfo as $k=>$v){
					//奖励用户本人的积分
					$integral += $v['reward_integral'];
					//奖励上级的积分
					$up_integral += $v['reward_up_integral'];
					//奖励上上级的积分
					$up_up_integral += $v['reward_up_up_integral'];
				}
				//用户原有积分+奖励积分
				$integrals['integral'] = $newuserInfo['integral'] + $integral;
				//上级原有积分+奖励积分
				$upintegral['integral'] = $upInfo['integral'] + $up_integral;
				//上上级原有积分+奖励积分
				$upupintegral['integral'] = $upupInfo['integral'] + $up_up_integral;
				//修改用户本人,上级,上上级用户积分
				$resuser = M('user')->where(['uid'=>$_SESSION['userInfo']['uid'],'is_del'=>1])->save($integrals);
				$resup = M('user')->where(['code'=>$userInfo['up_code'],'is_del'=>1])->save($upintegral);
				$resupup = M('user')->where(['code'=>$upInfo['up_code'],'is_del'=>1])->save($upupintegral);
			}
			//---添加积分明细表---
			//兑换积分
			if ($data['totalintegral'] != 0 && $data['totalintegral'] > 0){
				$jifendata['uid'] = $_SESSION['userInfo']['uid'];
				$jifendata['integral'] = $data['totalintegral'];
				$jifendata['sign'] = '减';
				$jifendata['order'] = $data['order'];
				$jifendata['info'] = '兑换商品';
				$jifendata['addtime'] = time();
				$jifenres = M('integral')->add($jifendata);
			}
			//下单奖励积分
			$integral2 = 0;
			$jifendata2['uid'] = $_SESSION['userInfo']['uid'];
			foreach($goodsInfo as $k=>$v){
				$integral2 += $v['reward_integral'];//奖励用户本人的积分
			}
			$jifendata2['integral'] = $integral2;
			$jifendata2['sign'] = '加';
			$jifendata2['order'] = $data['order'];
			$jifendata2['info'] = '下单奖励';
			$jifendata2['addtime'] = time();
			$jifenres2 = M('integral')->add($jifendata2);
			session('reward_integral',$integral);
			$this->ajaxReturn(['err'=>200,'msg'=>'支付成功']);
		}


	}

	//支付成功页面
	public function paymentsuc(){
		$this->reward_integral = $_SESSION['reward_integral'];
		$this->display('paymentsuc');
	}
	//支付失败页面
	public function paymentfail(){
		$this->display('paymentfail');
	}

	//处理确认收货 修改订单状态为已完成
	public function editreceive(){
		$data = I('');
		//用户信息
		$userInfo = M('user')->where(['uid'=>$_SESSION['userInfo']['uid'],'is_del'=>1])->find();
		//判断支付密码是否正确
		if (md5($data['pwd']) != $userInfo['pay_pwd']){
			$this->ajaxReturn(['err'=>500,'msg'=>'密码错误，请重新输入']);
		}
		//修改订单状态为已完成
		$res = M('order')->where(['order'=>$data['order'],'is_del'=>1])->save(['status'=>2]);
		if ($res){
			$this->ajaxReturn(['err'=>200,'msg'=>'确认收货成功']);
		}
	}


}

?>
