<?php
use Think\Controller;
class ClassController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}

	//加载分类页面
	public function Index(){
		$data = I('');
		//获取商品分类
		$GoodsTypeM = new GoodsTypeModel();
		$where = array();
		$where['is_del'] = 1;
		$GoodsTypeInfo = $GoodsTypeM->getList($where);
		//获取推荐的商品
		$GoodsM = new GoodsModel();
		$where2 = array();
		$where2['is_up'] = 1;
		$where2['special_id'] = 3;
		$NewInfo = $GoodsM->getList($where2);
		foreach($NewInfo as $k=>$v){
			$NewInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
		}
		//获取所有商品
		$where3 = array();
		$where3['is_up'] = 1;
//		$where3['stock'] != 0;
		$GoodsInfo = $GoodsM->getList($where3);
		foreach($GoodsInfo as $k=>$v){
			$GoodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
		}
		foreach($GoodsTypeInfo as &$val){
			foreach ($GoodsInfo as $k=>$v2){
				if ($v2['goods_type_id'] == $val['id']){
						$val['list'][] = $v2;
				}
			}
		}
//		dump($GoodsInfo);die;
		//返回数据
		$this->GoodsTypeInfo = $GoodsTypeInfo;
		$this->NewInfo = $NewInfo;
		$this->GoodsInfo = $GoodsInfo;
		$this->id = $data['id'];
		$this->msgurl=C ("IMAGEURL");
		$this->display("classify");
	}

	//ajax加载分类 下拉更多页面
	public function GoodsMore(){
		if(IS_POST){
			$data = I('request.');
			$page = $data['page']-1; //1
			$pageSize = 12; //每页条数
			$where = array();
			$where['is_up'] = 1;
			$where['goods_type_id'] = $data['goods_type_id'];
			$GoodsInfo = M("Goods")->where($where)->limit($page*$pageSize,$pageSize)->select();
			foreach($GoodsInfo as $k=>$v){
				$GoodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
				$GoodsInfo[$k]['msgurl'] = C ("IMAGEURL");
			}
			$this->ajaxReturn($GoodsInfo);
		}
	}

}

?>
