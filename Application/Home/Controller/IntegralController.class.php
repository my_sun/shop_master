<?php

class IntegralController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}

	//积分列表
	public function lists(){
		$this->name = '积分列表'; // 进行模板变量赋值
		$Page = $_GET['Page'] ? $_GET['Page'] : 1;
		$PageSize = 20;

		$WhereArr = array();
		$WhereArr['is_del'] = 1;
		if(isset($_GET['keyword'])&&$_GET['keyword']!=""){
			$keyword=$_GET['keyword'];
			$WhereArr['order']=array('like',"%".$keyword."%");
		}
        $IntegralM = new IntegralModel();
		$IntegralInfo['list'] = $IntegralM->getList($WhereArr,$Page,$PageSize);
		$all_page = ceil($IntegralInfo['totalCount']/$PageSize);
		$IntegralInfo['all_page'] = $all_page;
		$this->IntegralInfo = 	$IntegralInfo['list'];
		$this->Pages = $this->GetPages($IntegralInfo);
		$this->get = $_GET;
		$this->action = "/Adms/Integral/lists.html";
		$this->display("lists");
	}

	//添加积分
	public function add(){
		$this->name = '添加积分';
		if($_POST){
				$IntegralM = new IntegralModel();
				$map = array();
				$map["integral"] = $_POST["name"];
				$map["sign"] = $_POST["name"];
				$map["order"] = $_POST["name"];
				$map["info"] = $_POST["name"];
				$IntegralM->addOne($map);
				header('Location:lists.html');
			}
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//修改积分
	public function edit(){
		$this->name = '修改积分';  // 进行模板变量赋值

		//获取id
		$id = $_GET['id'];
		if ($id){
			$IntegralM = new IntegralModel();
			$where['id'] = $id;
			//查数据
			$IntegralInfo = $IntegralM->getOne($where);
		}
		$this->IntegralInfo = $IntegralInfo;
		$this->id = $id;
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//处理修改
	public function update(){
		$this->name = '修改积分';  // 进行模板变量赋值

		$id = $_POST['id'];
		$where = array();
		$IntegralM = new IntegralModel();
		if ($id){
			$data['name'] = $_POST['name'];
			$where["id"] = $_POST['id'];
			$res = $IntegralM->updateOne($where,$data);
			if ($res){
				$this->tip = "修改成功";
			}
			header('Location:lists.html');
		}
	}

	//软删除积分
	public function del(){
		$this->name = '删除积分';
		$IntegralM = new IntegralModel();
		$where = array();
		$where['id'] = $_GET['id'];
		if($_GET["id"]){
			$data['is_del'] = 0;
			$ret = $IntegralM->updateOne($where,$data);
			header("Location:/Adms/integral/lists");
		}
	}
}

?>
