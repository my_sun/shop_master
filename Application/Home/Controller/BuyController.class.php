<?php

class BuyController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//点击立即购买 接收数据
	public function buy(){
		$goods_id = I('post.goods_id');
		$normskw = I('post.normskw');
		$normskwval = I('post.normskwval');
		$normsggval = I('post.normsggval');
		$normsgg = "规格";
		$data['normskwval'] = $normskw.':'.$normskwval;
		$data['normsggval'] = $normsgg.':'.$normsggval;
		//商品信息
		$goodsInfo = M('goods')->where(['id'=>$goods_id,'is_up'=>1])->select();
		foreach ($goodsInfo as $k=>$v){
			$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
		}
		//规格信息
		$normsInfo = M('norms')->where(['normskwval'=>$data['normskwval'],'normsggval'=>$data['normsggval'],'goods_id'=>$goods_id,'is_up'=>1])->find();
		session("goodsInfo",$goodsInfo);
		session("normsInfo",$normsInfo);
		if ($normsInfo['stock'] == 0){
			$this->ajaxReturn(['err'=>500,'msg'=>"库存不足"]);
		}
		if ($goods_id && $normsInfo['stock'] != 0){
			$this->ajaxReturn(['err'=>200,'msg'=>"成功"]);
		}
	}

	//加载 确认下单页面
	public function confirmorderbuy(){
		//用户信息
		$userInfo = M('user')->where(['uid'=>$_SESSION['userInfo']['uid'],'is_del'=>1])->find();
		//地址信息
		$addressInfo =  M('address')->where(['uid'=>$_SESSION['userInfo']['uid'],'is_del'=>1])->select();
		$address = array();
		if(count($addressInfo) == 1){
			$address = $addressInfo;
		}else if (count($addressInfo) > 1){
			foreach ($addressInfo as $k=>$v){
				if ($v['is_default'] == 1){
					$address = $v;
				}
			}
		}
		$this->addressInfo = $addressInfo;
		$this->goodsInfo = $_SESSION['goodsInfo'];
		$this->normsInfo = $_SESSION['normsInfo'];
		$this->userInfo = $userInfo;
		$this->address = $address;
		$this->msgurl=C("IMAGEURL");
		$this->display('confirmorderbuy');
	}

	//提交订单所需数据
	public function orderdfkshuju(){
		$data = I('');
		//商品信息
		$goodsInfo = M('goods')->where(['id'=>$data['gid'],'is_up'=>1])->select();
		foreach ($goodsInfo as $k=>$v){
			$goodsInfo[$k]['goodsimg'] = json_decode($v['goodsimg']);
		}
		//地址信息
		$addressInfo = M('address')->where(['id'=>$data['aid'],'is_del'=>1])->find();
		//规格信息
		$normsInfo = M('norms')->where(['goods_id'=>$data['gid'],'is_up'=>1,'normskwval'=>$data['normskwval'],'normsggval'=>$data['normsggval']])->find();
		if ($normsInfo['stock'] == 0){
			$this->ajaxReturn(['err'=>500,'msg'=>'库存不足']);
		}
		//添加订单表
		$map['uid'] = $_SESSION['userInfo']['uid'];
		$map['goods_id'] = $data['gid'];
		$map['goodsno'] = $this->make_goodsno($data['gid']); //商品号
		$map['order'] = $this->make_no();
		//积分商品
		if ($data['totalintegral'] > 0 && $data['sid']==2){
			if ($data['userjf'] < $data['totalintegral']){
				//积分不够走原价
				$map['totalprice'] = $normsInfo['price']*$data['num']; //原价*数量
				$map['integral'] = 0;
			}else{
				$map['totalprice'] = $data['totalprice'];
				$map['integral'] = $data['totalintegral'];
			}
		}elseif($data['sid'] == 1){
			//拼团抢购
			$map['totalprice'] = $data['totalprice'];
			$map['integral'] = 0;
		}else{
			//新品推荐及其他
			$map['totalprice'] = $data['totalprice'];
			$map['integral'] = 0;
		}
		$map['num'] = $data['num'];
		$map['remarks'] = $data['remarks'];
		$map['name'] = $addressInfo['name'];
		$map['phone'] = $addressInfo['phone'];
		$map['address_info'] = $addressInfo['address_info'];
		$map['details_address'] = $addressInfo['details_address'];
		$map['price'] = $normsInfo['price'];//原价
		$map['discount_price'] = $normsInfo['discount_price'];//折扣价
		$map['allprice'] = $data['totalprice'];//右边总价
		$map['allintegral'] = $data['totalintegral'];//右边总积分
		$map['normskwval'] = $data['normskwval'];
		$map['normsggval'] = $data['normsggval'];
		$map['status'] = 0;
		$map['addtime'] = time();
		$res = M('order')->add($map);
		//取最新添加的订单id
		$orderM = new OrderModel();
		$oid = $orderM->max('id');
		$orderInfo = M('order')->where(['id'=>$oid,'is_del'=>1])->find();
//		session('goodsInfo',$goodsInfo);
//		session('addressInfo',$addressInfo);
//		session('normsInfo',$normsInfo);
//		session('oid',$oid);
		if ($res){
			$this->ajaxReturn(['err'=>200,'msg'=>'订单提交成功','totalprice'=>$orderInfo['totalprice'],'totalintegral'=>$orderInfo['integral'],'oid'=>$oid,'order'=>$orderInfo['order']]);
		}
	}

	//加载提交订单 待付款页面 （多余方法）
	public function orderdfk(){
		$id = $_SESSION['oid'];
		$orderInfo = M('order')->where(['id'=>$id])->find();
		$this->addressInfo = $_SESSION['addressInfo'];
		$this->goodsInfo = $_SESSION['goodsInfo'];
		$this->normsInfo = $_SESSION['normsInfo'];
		$this->orderInfo = $orderInfo;
		$this->msgurl = C('IMAGEURL');
		$this->display('orderdetail1');
	}

	/**
	 * 生成订单号
	 * @return string
	 */
	function make_no()
	{
		return strval(date('YmdHis') . substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 4));
	}

	/**
	 * 生成商品号
	 * @return string
	 */
	function make_goodsno($gid)
	{
		return mt_rand(1000,9999).'_' .$gid;
	}




}

?>
