<?php

class AddressController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//加载 地址列表页面
	public function addressmsg(){
		$addressInfo = M("address")->where(["uid"=>$_SESSION['userInfo']['uid'],"is_del"=>1])->select();
		$this->addressInfo = $addressInfo;
		$this->display("addressmsg");
	}

	//加载 添加地址页面
	public function addaddress(){
		$this->display("addadress");
	}

	//处理添加
	public function addressadd(){
		$data['name'] = I('post.name');
		$data['phone'] = I('post.phone');
		$data['address_info'] = I('post.address_info');
		$data['details_address'] = I('post.details_address');
		$data['uid'] = $_SESSION['userInfo']['uid'];
		$AddressM = new AddressModel();
		$addressInfo = M('address')->where(['uid'=>$_SESSION['userInfo']['uid']])->select();
		if (count($addressInfo) == 0){
			$data['is_default'] = 1;
		}
		$res = $AddressM->addOne($data);
		if ($res){
			$this->ajaxReturn(["err"=>200,"msg"=>"保存成功"]);
		}else{
			$this->ajaxReturn(["err"=>500,"msg"=>"保存失败"]);
		}
	}

	//加载 修改地址页面
	public function editaddress(){
		$id = $_GET['id'];
		if ($id){
			$addressInfo = M('address')->where(["id"=>$id])->find();
		}
		$this->addressInfo = $addressInfo;
		$this->display("editadress");
	}

	//处理修改
	public  function updateaddress(){
		$id = $_POST['id'];
		$data['name'] = $_POST['name'];
		$data['phone'] = $_POST['phone'];
		$data['address_info'] = $_POST['address_info'];
		$data['details_address'] = $_POST['details_address'];
		$res = M('address')->where(['id'=>$id])->save($data);
		if ($res){
			$this->ajaxReturn(["err"=>200,"msg"=>"编辑成功"]);
		}else{
			$this->ajaxReturn(["err"=>500,"msg"=>"编辑失败"]);
		}
	}

	//软删除地址
	public function deladdress(){
		$id = $_POST['id'];
		$data['is_del'] = 0;
		$res = M('address')->where(["id"=>$id])->save($data);
		if ($res){
			$this->ajaxReturn(["err"=>200,"msg"=>"删除成功"]);
		}else{
			$this->ajaxReturn(["err"=>500,"msg"=>"删除失败"]);
		}
	}

	//设为默认地址
	public function IsDefault(){
		$id = $_POST['id'];
		if($id){
			M('address')->where(["uid"=>$_SESSION['userInfo']['uid']])->save(["is_default"=>0]);
			$res = M('address')->where(["id"=>$id])->save(["is_default"=>1]);
			if ($res){
				$this->ajaxReturn(["err"=>200,"msg"=>"设为默认成功"]);
			}else{
				$this->ajaxReturn(["err"=>500,"msg"=>"设为默认失败"]);
			}
		}
	}

	//


}

?>
