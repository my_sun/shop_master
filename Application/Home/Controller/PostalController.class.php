<?php

class PostalController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//加载提现页面
	public function postal(){

		$this->display("postal");
	}

	//加载提现成功页面
	public function postalsuc(){
		$this->display("postalsuc");
	}

	//加载提现失败页面
	public function postalfail(){
		$this->display("postalfail");
	}

	//加载 账单明细页面
	public function accountdetail(){
		$accountInfo = M("account")->where(["uid"=>$_SESSION['userInfo']['uid'],"is_del"=>1])->select();
		$this->accountInfo = $accountInfo;
		$this->display("accountdetail");
	}

}

?>
