<?php

class PaypwdController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//加载 支付设置页面
	public function payset(){
		$this->display("payset");
	}

	//加载 设置支付密码页面
	public function setpaypwd(){
		$this->display("setpaypwd");
	}

	//处理 添加支付密码
	public function addpaypwd(){
		$data['pay_pwd'] = I('post.pwd');
		$data['pay_pwd'] = md5($data['pay_pwd']);
		$res = M('user')->where(["uid"=>$_SESSION['userInfo']['uid']])->save($data);
		if ($res){
			$this->ajaxReturn(["err"=>200,"msg"=>"密码设置成功！"]);exit;
		}else{
			$this->ajaxReturn(["err"=>500,"msg"=>"密码设置失败！"]);exit;
		}
	}

	//加载 手机验证页面
	public function phonelist(){
		$this->display("phoneyz");
	}

	//处理 手机验证
	public function yzphone(){
		$data = I('');
		//判断手机号是否输入
		$phone = $data['phone'];
		if(empty($phone)){
			$this->ajaxReturn(["err"=>500,"msg"=>"请输入手机号！"]);exit;
		}
		//判断手机号格式
		$myreg="/^1(3|4|5|6|7|8|9)\d{9}$/";
		if (!preg_match($myreg,$phone)) {
			$this->ajaxReturn(["err"=>500,"msg"=>"请输入正确的手机号！"]);exit;
		}
		//用户信息
		$userInfo = M('user')->where(["phone"=>$phone])->find();
		if (!$userInfo['phone']){
			$this->ajaxReturn(["err"=>500,"msg"=>"该手机号不存在！"]);exit;
		}
		//判断验证码是否输入
		$verification_code = $data['verification_code'];
		if(empty($verification_code)){
			$this->ajaxReturn(["err"=>500,"msg"=>"请输入验证码！"]);
		}
		//redis获取验证码
		$redis = new RedisModel();
		$getLoginCode = $redis->get("login_code_".$userInfo['uid']);
//		dump($verification_code);
//		dump($getLoginCode);die;
		if ($getLoginCode != $verification_code){
			$this->ajaxReturn(["err"=>500,"msg"=>"验证码错误！"]);
		}else{
			$this->ajaxReturn(["err"=>200,"msg"=>"成功！"]);
		}
	}

	//获取验证码
	public function get_verification_code()
	{
		$data = I('');
		$phone = $data['phone'];
		//用户信息
		$userInfo = M('user')->where(["phone" => $phone])->find();
		if (!$userInfo['phone']) {
			$this->ajaxReturn(["err" => 500, "msg" => "该手机号不存在！"]);
			exit;
		}
		//生成验证码
		$code = str_shuffle(mt_rand(1000, 9999));
		$redis = new RedisModel();
		//连续发送次数
		$getCodeNum = $redis->get("login_code_num_".$userInfo['uid']);
		//8小时内超过6次禁止
		if ($getCodeNum > 6) {
			$this->ajaxReturn(["err" => 500, "msg" => "发送太频繁！"]);
		} else {
			//发送次数存8小时
			$redis->set("login_code_num_".$userInfo['uid'],$getCodeNum + 1,0,0,60 * 60 * 8);
			$res = $this->sendSms($phone,$code);
			if ($res) {
				//验证码存Redis
				$login_code = $redis->set("login_code_".$userInfo['uid'],$code,0,0,60 * 2);
				if (!$login_code) {
					return false;
				}
				$this->ajaxReturn(["err" => 200, "msg" => "发送成功！"]);
			} else {
				$this->ajaxReturn(["err" => 500, "msg" => "120秒内仅能获取一次短信验证码,请稍后重试！"]);
			}
		}
	}


	//发送验证码
	public function sendSms($phone,$code) {
		$params = array ();
		// fixme 必填：是否启用https
		$security = false;

		$accessKeyId = "LTAI4Fnipr52yvJxeeBk3ouJ";
		$accessKeySecret = "7uetEZWWfMRn3yjLPRHx7mX3cjxeHF";

		// fixme 必填: 短信接收号码
		$params["PhoneNumbers"] = $phone;

		// fixme 必填: 短信签名，应严格按"签名名称"填写，请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/sign
		$params["SignName"] = "话蜀";

		// fixme 必填: 短信模板Code，应严格按"模板CODE"填写, 请参考: https://dysms.console.aliyun.com/dysms.htm#/develop/template
		$params["TemplateCode"] = "SMS_180340412";

		// fixme 可选: 设置模板参数, 假如模板中存在变量需要替换则为必填项
		$params['TemplateParam'] = Array (
			"code" => $code,
		);
		// fixme 可选: 设置发送短信流水号
//		$params['OutId'] = "12345";
		// fixme 可选: 上行短信扩展码, 扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段
//		$params['SmsUpExtendCode'] = "1234567";

		// *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
		if(!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
			$params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
		}

		// 初始化SignatureHelper实例用于设置参数，签名以及发送请求
		$helper = new SignatureHelperController();

		// 此处可能会抛出异常，注意catch
		$content = $helper->request(
			$accessKeyId,
			$accessKeySecret,
			"dysmsapi.aliyuncs.com",
			array_merge($params, array(
				"RegionId" => "cn-hangzhou",
				"Action" => "SendSms",
				"Version" => "2017-05-25",
			)),
			$security
		);

		return $content;
	}

	//修改支付密码页面
	public function editpaypwd(){
		$this->display("editpaypwd");
	}

	//处理 修改支付密码
	public function updatepaypwd(){
		$data['pay_pwd'] = I('post.pwd');
		$data['pay_pwd'] = md5($data['pay_pwd']);
		$res = M('user')->where(["uid"=>$_SESSION['userInfo']['uid']])->save($data);
		if ($res){
			$this->ajaxReturn(["err"=>200,"msg"=>"密码修改成功！"]);exit;
		}else{
			$this->ajaxReturn(["err"=>500,"msg"=>"密码修改失败！"]);exit;
		}
	}

}

?>
