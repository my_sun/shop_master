<?php
class MoneyController extends BaseController {
	public function __construct(){
		//parent::__construct();
		
	}
	/*
	 * 充值会员
	 */
	public function rechargevip($rechargeInfo,$payid){
	    
	    $uid = $rechargeInfo["uid"];
	    $days = $rechargeInfo["days"];
	    $translateid = $rechargeInfo["translateid"];
	    $paytime = $rechargeInfo["paytime"];
	    $type = $rechargeInfo["type"];
	    $to_uid = $rechargeInfo["to_uid"];
	    $money = $rechargeInfo["money"];
	    $product = $rechargeInfo["product"];
	    $data["days"] = $days;
	    $data["translateid"] = $translateid;
	    $data["paytime"] = $paytime;
	    $data["payid"] = $payid;
	    $data["type"] = $type;
	    
	    $data["product"] = $product;
        $puids=M('admin_three_user')->getField('id',true);
        $PMoneyM = new PMoneyModel();
	    if($type==1){
	        //$puid = $this->get_moneykey($uid);

            $userinfo=$this->get_user($uid);
            //如果充值用户为女生的话则加入到优质女用户列表中
            //判断与语言是否是中文简体
            if($userinfo['gender']==2&$userinfo['language']!='zh-cn'){
                //将该用户添加到优质用户列表中
                $betteruserlistkey='superusers';
                $redis = $this->redisconn();
                $redis->listRemove($betteruserlistkey, $uid, 0);
                $redis->listPush($betteruserlistkey, $uid);
                $superusers = M('superusers');
                $superusers->data(array('uid'=>$uid))->add();
            }


            //如果用户为被推广用户的话，则给推广用户部分魅力值
            if(!empty($userinfo['beshareuid'])){
                $datatg["to_uid"] = $uid;
                $datatg["uid"] = $userinfo['beshareuid'];
                $datatg["type"] =13;
                $datatg["paytime"] =$paytime;

                $tguserinfo=$this->get_user($userinfo['beshareuid']);
                    if($tguserinfo['user_type']==5){
                        $datatg["money"] =$money*0.65*0.05;
                        $PMoneyM->addOne($data);
                        $this->add_m_money($userinfo['beshareuid'],$datatg["money"]);
                    }elseif($tguserinfo['user_type']==9||$tguserinfo['user_type']==10||$tguserinfo['user_type']==11){
                        $datatg["money"] =$money*0.65*0.03;
                        $PMoneyM->addOne($data);
                        $this->add_m_money($userinfo['beshareuid'],$datatg["money"]);
                    }




            }
            //end
            $moneypuids=$this->get_moneykey_num($uid);
            $charcount=0;
            foreach($moneypuids as $k=>$v){
                $charcount=$charcount+$v['num'];
            }
            $eachmoney=$money/$charcount;

            foreach($moneypuids as $k1=>$v1){
                    //自己充值

                    $data["money"] = $v1['num']*$eachmoney*0.65*0.1;
                    $data["to_uid"] = $uid;
                    $data["uid"] = $v1['touid'];


                    //用户自己充值提成10%
                $pinfo = $this->get_user($v1['touid']);
                $h = intval(date("H"));
                if($pinfo['user_type']==3){
                    if(in_array($pinfo["puid"],$puids)){
                        $puidtype=M('admin_three_user')->where(array('id'=>$pinfo["puid"]))->getField('type');
                        if($puidtype==1){
                            if($h >=23 || $h <5){
                                $money = $money*1.5;
                            }
                        }elseif($puidtype==2){
                            if($h >=18 & $h <24){
                                $money = $money*1.5;
                            }
                        }elseif ($puidtype==3){
                            if($h >=0 & $h <5){
                                $money = $money*2;
                            }
                        }elseif ($puidtype==4){
                            if($h >=5 & $h <8){
                                $money = $money*1.5;
                            }elseif($h >=8 & $h <18){
                                $money = $money*1.2;
                            }
                        }

                    }else{
                        if($h >=23 || $h <5){
                            $data["money"] = $data["money"]*1.5;
                        }
                    }


                }elseif($pinfo['user_type']==9||$pinfo['user_type']==10){
                    if($h >=20 || $h <4){
                        $data["money"] = $data["money"]*1.2;
                    }
                }

                    if($pinfo["puid"]&&$pinfo["puid"]!="10000"){
                        $upmoney = $data["money"]*0.1;
                        $data["money"] = $data["money"]-$upmoney;
                        $updata = array();
                        $updata1 = array();
                        //如果该用户属于第三方推荐用户，则将设置的比例值储存在充值记录中
                        if(in_array($pinfo["puid"],$puids)){
                            //echo 111;
                            $bili=M('admin_three_user')->where(array('id'=>$pinfo["puid"]))->getField('bili');
                            $updata1["bili"] =$bili;
                            $upmoney1=$data["money"]*$bili/100;
                            $data["money"] =$data["money"]-$upmoney1;
                            $updata1["money"] =$upmoney1;
                            $updata1["days"] = $days;
                            $updata1["translateid"] = $translateid;
                            $updata1["paytime"] = $paytime;
                            $updata1["payid"] = $translateid;
                            $updata1["type"] =9;
                            $updata1["product"] = $product;
                            $updata1["to_uid"] = $uid;
                            $updata1["account"] = $v1['touid'];//存入发送给用户的uid
                            $updata1["account_type"]=1;//记录为2：用户帮用户充值
                            $updata["account"] =$v1['touid'] ;//存入发送给用户的uid
                            $updata["account_type"]=1;//记录为2：用户帮用户充值
                            $updata1["uid"] = $pinfo["puid"];
                            $updata1["pid"] = $pinfo["puid"];
                            $updata1["is_tixian"]=4;
                            $PMoneyM->addOne($updata1);
                            $this->add_m_money($pinfo["puid"],$upmoney1);
                            $updata["is_tixian"]=4;
                            $updata["pid"]=$pinfo["puid"];
                            $data['pid']=$pinfo["puid"];
                            $data["bili"] = $bili;
                        }
                        $updata["days"] = $days;
                        $updata["translateid"] = $translateid;
                        $updata["paytime"] = $paytime;
                        $updata["payid"] = $translateid;
                        $updata["type"] =8;
                        $updata["product"] = $product;
                        $updata["money"] = $upmoney;
                        $updata["to_uid"] = $v1['touid'];
                        $updata["uid"] = $pinfo["puid"];
                        $PMoneyM->addOne($updata);
                        $this->add_m_money($pinfo["puid"],$upmoney);


                    }
                    $PMoneyM->addOne($data);
                    $this->add_m_money($v1['touid'],$data["money"]);
            }


	      /*  if($puid){
    	        //自己充值
    	        $PMoneyM = new PMoneyModel();
    	        $data["money"] = $money*0.65*0.1;
    	        $data["to_uid"] = $uid;
    	        $data["uid"] = $puid;
    	        
    	        //用户自己充值提成10%
    	        $h = intval(date("H"));
    	        if ($h == 23 || $h ==0){
    	            //$data["money"] = $data["money"]*1.2;
                    $data["money"] = $data["money"]*1.5;
    	        }elseif($h >= 1 && $h <5){
    	            $data["money"] = $data["money"]*1.5;
    	        }elseif($h >= 10 && $h <12){
    	            $data["money"] = $data["money"]*1;
    	        }elseif($h >= 14 && $h <17){
    	            $data["money"] = $data["money"]*1;
    	        }
    	        
    	        $pinfo = $this->get_user($puid);
    	        if($pinfo["puid"]&&$pinfo["puid"]!="10000"){
    	            $upmoney = $data["money"]*0.1;
    	            $data["money"] = $data["money"]-$upmoney;
    	            $updata = array();
                    $updata1 = array();
                    //如果该用户属于第三方推荐用户，则将设置的比例值储存在充值记录中
                    if(in_array($pinfo["puid"],$puids)){
                        //echo 111;
                        $bili=M('admin_three_user')->where(array('id'=>$pinfo["puid"]))->getField('bili');
                        $updata1["bili"] =$bili;
                        $upmoney1=$data["money"]*$bili/100;
                        $data["money"] =$data["money"]-$upmoney1;
                        $updata1["money"] =$upmoney1;
                        $updata1["days"] = $days;
                        $updata1["translateid"] = $translateid;
                        $updata1["paytime"] = $paytime;
                        $updata1["payid"] = $translateid;
                        $updata1["type"] =9;
                        $updata1["product"] = $product;
                        $updata1["to_uid"] = $uid;
                        $updata1["account"] = $puid;//存入发送给用户的uid
                        $updata1["account_type"]=1;//记录为2：用户帮用户充值
                        $updata["account"] =$puid ;//存入发送给用户的uid
                        $updata["account_type"]=1;//记录为2：用户帮用户充值
                        $updata1["uid"] = $pinfo["puid"];
                        $updata1["pid"] = $pinfo["puid"];
                        $updata1["is_tixian"]=4;
                        $PMoneyM->addOne($updata1);
                        $this->add_m_money($pinfo["puid"],$upmoney1);
                        $updata["is_tixian"]=4;
                        $updata["pid"]=$pinfo["puid"];
                        $data['pid']=$pinfo["puid"];
                        $data["bili"] = $bili;
                    }
    	            $updata["days"] = $days;
    	            $updata["translateid"] = $translateid;
    	            $updata["paytime"] = $paytime;
    	            $updata["payid"] = $translateid;
    	            $updata["type"] =8;
    	            $updata["product"] = $product;
    	            $updata["money"] = $upmoney;
    	            $updata["to_uid"] = $puid;
    	            $updata["uid"] = $pinfo["puid"];
    	            $PMoneyM->addOne($updata);
    	            $this->add_m_money($pinfo["puid"],$upmoney);


    	        }
    	        $PMoneyM->addOne($data);
    	        $this->add_m_money($puid,$data["money"]);
	        }*/
	    }elseif($type==2){
	        //帮别人充值
	        $PMoneyM = new PMoneyModel();
	        $data["to_uid"] = $uid;
	        $data["uid"] = $to_uid;
	        $data["money"] = $money*0.65*0.25;
	        
	        $pinfo = $this->get_user($to_uid);


	        if($pinfo["puid"]&&$pinfo["puid"]!="10000"){
	            $upmoney = $data["money"]*0.1;
	            $data["money"] = $data["money"]-$upmoney;
	            $updata = array();
                $updata1 = array();
                //如果该用户属于第三方推荐用户，则将设置的比例值储存在充值记录中
                if(in_array($pinfo["puid"],$puids)){
                    //echo 111;
                    $bili=M('admin_three_user')->where(array('id'=>$pinfo["puid"]))->getField('bili');
                    $updata1["bili"] =$bili;
                    $upmoney1=$data["money"]*$bili/100;
                    $data["money"] =$data["money"]-$upmoney1;
                    $updata1["money"] =$upmoney1;
                    $updata1["days"] = $days;
                    $updata1["translateid"] = $translateid;
                    $updata1["paytime"] = $paytime;
                    $updata1["payid"] = $translateid;
                    $updata1["type"] =9;
                    $updata1["product"] = $product;
                    $updata1["money"] = $upmoney1;
                    $updata1["to_uid"] = $uid;
                    $updata1["account"] = $to_uid;//存入发送给用户的uid
                    $updata1["account_type"]=2;//记录为2：用户帮用户充值
                    $updata["account"] =$to_uid ;//存入发送给用户的uid
                    $updata["account_type"]=2;//记录为2：用户帮用户充值
                    $updata1["uid"] = $pinfo["puid"];
                    $updata1["is_tixian"]=4;
                    $updata1["pid"]=$pinfo["puid"];
                    $PMoneyM->addOne($updata1);
                    $this->add_m_money($pinfo["puid"],$upmoney1);
                    $updata["is_tixian"]=4;
                    $updata["pid"]=$pinfo["puid"];
                    $data['pid']=$pinfo["puid"];
                    $data["bili"] = $bili;
                }
	            $updata["days"] = $days;
	            $updata["translateid"] = $translateid;
	            $updata["paytime"] = $paytime;
	            $updata["payid"] = $translateid;
	            $updata["type"] =8;
	            $updata["product"] = $product;
	            $updata["money"] = $upmoney;
	            $updata["to_uid"] = $to_uid;
	            $updata["uid"] = $pinfo["puid"];
	            $PMoneyM->addOne($updata);
	            $this->add_m_money($pinfo["puid"],$upmoney);

	        }
	        
	        $PMoneyM->addOne($data);
	        $this->add_m_money($to_uid,$data["money"]);
	    }
	    return 1;
	}
	/*
	 *发红包
	 */
	public function sendhongbao($price,$UserInfo,$touid,$time){
	    
	    $uid = $UserInfo["uid"];
	    $translateid = 0;
	    $paytime =$time?$time:time();
	    $type =11;//发送红包转化为魅力值记录
	    $money = $price;

	    $product = $UserInfo["product"];
	    $data["days"] = 1;
	    $data["translateid"] = $translateid;
	    $data["paytime"] = $paytime;
	    $data["payid"] = $translateid;
	    $data["type"] = $type;
	    
	    $data["product"] = $product;
	    $pinfo = $this->get_user($touid);
        $puids=M('admin_three_user')->getField('id',true);
	    if($type==11){
	        $PMoneyM = new PMoneyModel();
	        /* $h = intval(date("H"));
	         if ($h > 23 || $h <= 4){
	         $money = $money*0.65*0.3;
	         }else{
	         $money = $money*0.65*0.2;
	         } */
	        $money = $money*0.65*0.2/30*100;
	        if($pinfo["puid"]&&$pinfo["puid"]!="10000"){
	            $upmoney = $money*0.1;
	            $money = $money-$upmoney;
	            $updata = array();
                $updata1 = array();
                //如果该用户属于第三方推荐用户，则将设置的比例值储存在充值记录中
                if(in_array($pinfo["puid"],$puids)){
                    //echo 111;
                    $bili=M('admin_three_user')->where(array('id'=>$pinfo["puid"]))->getField('bili');
                    $updata1["bili"] =$bili;
                    $upmoney1=$money*$bili/100;
                    $money = $money-$upmoney1;
                    $updata1["money"] =$upmoney1;
                    $updata1["days"] =1;
                    $updata1["translateid"] = $translateid;
                    $updata1["paytime"] = $paytime;
                    $updata1["payid"] = $translateid;
                    $updata1["type"] =9;
                    $updata1["product"] = $product;
                    $updata1["money"] = $upmoney1;
                    $updata1["to_uid"] = $uid;
                    $updata1["account"] = $touid;//存入发送给用户的uid
                    $updata1["account_type"]=11;//记录为发送红包类型
                    $updata["account"] = $touid;//存入发送给用户的uid
                    $updata["account_type"]=11;//记录为发送红包类型
                    $updata1["uid"] = $pinfo["puid"];
                    $updata1["is_tixian"]=4;
                    $updata1["pid"]=$pinfo["puid"];
                    $PMoneyM->addOne($updata1);
                    $this->add_m_money($pinfo["puid"],$upmoney1);
                    $updata["is_tixian"]=4;
                    $updata["pid"]=$pinfo["puid"];
                    $data['pid']=$pinfo["puid"];
                    $data["bili"] = $bili;
                }

	            $updata["days"] = 1;
	            $updata["translateid"] = $translateid;
	            $updata["paytime"] = $paytime;
	            $updata["payid"] = $translateid;
	            $updata["type"] =8;
	            $updata["product"] = $product;
	            $updata["money"] = $upmoney;
	            $updata["to_uid"] = $uid;
	            $updata["uid"] = $pinfo["puid"];
	            $PMoneyM->addOne($updata);
	            $this->add_m_money($pinfo["puid"],$upmoney);

	        }
	        
	        $data["money"] = $money;
	        $data["to_uid"] = $uid;
	        $data["uid"] = $touid;
	        $PMoneyM->addOne($data);
	        //增加魅力值的同时需删除redis中的缓存
	        $redis=$this->redisconn();
	        $redis->delete('withdrawal_'.$touid);
	        $redis->delete('withdrawal_order'.$touid);
	        //向魅力值记录表插入更新魅力值等数据开始。
	        $this->add_m_money($touid,$money);
	        /* $mmoney=M('m_money');
	        $mmon=$mmoney->where(array('uid'=>$touid))->find();
	        $dmon=array();
	        if($mmon){
	            $dmon['totalmoney']=$mmon['totalmoney']+$money;
	            $dmon['leftmoney']=$mmon['leftmoney']+$money;
	            $dmon['lasttime']=time();
	            $mmoney->where(array('uid'=>$touid))->save($dmon);
	        }else{
	            $dmon['uid']=$touid;
	            $dmon['totalmoney']=$money;
	            $dmon['leftmoney']=$money;
	            $dmon['lasttime']=time();
	            $mmoney->add($dmon);
	        } */
	        //向魅力值记录表插入更新魅力值等数据结束。
	    }
	    return 1;
	}
	public function add_m_money($uid,$money){
	    //向魅力值记录表插入更新魅力值等数据开始。
        $userinfo=$this->get_user($uid);
	    $mmoney=M('m_money');
	    $mmon=$mmoney->where(array('uid'=>$uid))->find();
	    $dmon=array();
	    if($mmon){
	        $dmon['totalmoney']=$mmon['totalmoney']+$money;
	        $dmon['leftmoney']=$mmon['leftmoney']+$money;
	        //需求：用户类型3兑换钻石后魅力值依然可以提现
            if($userinfo['user_type']==3){
                $dmon['leftgoldmoney']=$mmon['leftgoldmoney']+$money;
            }
	        $dmon['lasttime']=time();
	        $mmoney->where(array('uid'=>$uid))->save($dmon);
	    }else{
	        $dmon['uid']=$uid;
	        $dmon['totalmoney']=$money;
	        $dmon['leftmoney']=$money;
            if($userinfo['user_type']==3){
                $dmon['leftgoldmoney']=$mmon['leftgoldmoney']+$money;
            }
	        $dmon['lasttime']=time();
	        $mmoney->add($dmon);
	    }
	    //向魅力值记录表插入更新魅力值等数据结束。
	    return true;
	}
	/*
	 *送礼物
	 */
	public function sendgift($giftinfo,$UserInfo,$touid,$contentint,$time){
	    
	    $uid = $UserInfo["uid"];
	    $translateid = $giftinfo["id"];
	    $paytime = $time?$time:time();
	    $type = 3;
	    $money = $giftinfo["price"]*$contentint;
	    $url=$giftinfo['url'];
	    $product = $UserInfo["product"];
	    $data["days"] = $contentint;
	    $data["translateid"] = $translateid;
	    $data["paytime"] = $paytime;
	    $data["payid"] = $translateid;
	    $data["type"] = $type;
	    $data["product"] = $product;
        $PMoneyM = new PMoneyModel();
        //查看双方是否开通爱情岛
        if($uid>$touid){
            $boxuid=$touid.$uid;
        }else{
            $boxuid=$uid.$touid;
        }
        //$aiqingdao=M('exclusive')->where(array('boxuid'=>$boxuid))->find();
        $aiqingdao=$this->get_exclusive_message($uid,$touid);
        if($aiqingdao){
            $aiqingdaodata=array();
            if($uid==$aiqingdao['biguid']){
                $mygiftids=explode('|',$aiqingdao['biggiftids']);
                array_push($mygiftids, $giftinfo["id"]);
                $aiqingdaodata['biggiftids']=implode("|",$mygiftids);
                $aiqingdaodata['bigcharmmoney']=$aiqingdao['bigcharmmoney']+$money*0.65/30*100;
                $aiqingdaodata['allcharmmoney']=$aiqingdao['allcharmmoney']+$money*0.65/30*100;
                M('exclusive')->where(array('boxuid'=>$boxuid))->save($aiqingdaodata);
                $this->get_exclusive_message($uid,$touid,1);
            }else{
                if($aiqingdao['smallgiftids']==''){
                    $aiqingdaodata['smallgiftids']=$giftinfo["id"];
                }else{
                    $mygiftids=explode('|',$aiqingdao['smallgiftids']);
                    array_push($mygiftids, $giftinfo["id"]);
                    $aiqingdaodata['smallgiftids']=implode("|",$mygiftids);
                }

                $aiqingdaodata['smallcharmmoney']=$aiqingdao['smallcharmmoney']+$money*0.65/30*100;
                $aiqingdaodata['allcharmmoney']=$aiqingdao['allcharmmoney']+$money*0.65/30*100;
                M('exclusive')->where(array('boxuid'=>$boxuid))->save($aiqingdaodata);
                $this->get_exclusive_message($uid,$touid,1);
            }

        }






        //如果用户为被推广用户的话，则给推广用户部分魅力值
        if(!empty($UserInfo['beshareuid'])){
            $tguserinfo=$this->get_user($UserInfo['beshareuid']);
            $datatg["to_uid"] = $uid;
            $datatg["uid"] = $UserInfo['beshareuid'];
            $datatg["type"] =13;
            $datatg["paytime"] =$paytime;

            if($tguserinfo['user_type']==5){
                $datatg["money"] =$money*0.65*0.05/30*100;
                $PMoneyM->addOne($datatg);
                $this->add_m_money($UserInfo['beshareuid'],$datatg["money"]);
            }elseif($tguserinfo['user_type']==9||$tguserinfo['user_type']==10||$tguserinfo['user_type']==11){
                $datatg["money"] =$money*0.65*0.03/30*100;
                $PMoneyM->addOne($datatg);
                $this->add_m_money($UserInfo['beshareuid'],$datatg["money"]);
            }


        }
        //end




	    $pinfo = $this->get_user($touid);
        $puids=M('admin_three_user')->getField('id',true);
	    if($type==3){

	        /* $h = intval(date("H"));
    	         if ($h > 23 || $h <= 4){
    	            $money = $money*0.65*0.3;
    	        }else{
    	            $money = $money*0.65*0.2;
    	        } */

	            if($pinfo['user_type']==3 ||$pinfo['user_type']==9){
                    $money = $money*0.65*0.2/30*100;
                }elseif($pinfo['user_type']==10||$pinfo['user_type']==11){
                    $money = $money*0.65*0.3/30*100;
                }else{
                    $money = $money*0.65*0.2/30*100;
                }

    	        //用户自己充值提成10%
            $h = intval(date("H"));
            if($pinfo['user_type']==3){
                if(in_array($pinfo["puid"],$puids)){
                    $puidtype=M('admin_three_user')->where(array('id'=>$pinfo["puid"]))->getField('type');
                    if($puidtype==1){
                        if($h >=23 || $h <5){
                            $money = $money*1.5;
                        }
                    }elseif($puidtype==2){
                        if($h >=18 & $h <24){
                            $money = $money*1.5;
                        }
                    }elseif ($puidtype==3){
                        if($h >=0 & $h <5){
                            $money = $money*2;
                        }
                    }elseif ($puidtype==4){
                        if($h >=5 & $h <8){
                            $money = $money*1.5;
                        }elseif($h >=8 & $h <18){
                            $money = $money*1.2;
                        }
                    }

                }else{
                    if($h >=23 || $h <5){
                        $money = $money*1.5;
                    }
                }

            }elseif($pinfo['user_type']==9||$pinfo['user_type']==10){
                if($h >=20 || $h <4){
                    $money = $money*1.2;
                }
            }
    	       /* $h = intval(date("H"));
    	        if ($h == 23 || $h == 0){
    	            $money = $money*1.5;
    	        }elseif($h >= 1 && $h <5){
    	            $money = $money*1.5;
    	        }elseif($h >= 10 && $h <12){
    	            $money = $money*1;
    	        }elseif($h >= 14 && $h <17){
    	            $money = $money*1;
    	        }*/
    	        
    	        if($pinfo["puid"]&&$pinfo["puid"]!="10000"){
                    $upmoney = $money*0.1;
                    $money = $money-$upmoney;
                    $updata = array();
                    $updata1 = array();
                    //如果该用户属于第三方推荐用户，则将设置的比例值储存在充值记录中
                    if(in_array($pinfo["puid"],$puids)){
                        $bili=M('admin_three_user')->where(array('id'=>$pinfo["puid"]))->getField('bili');
                        if(!$bili){
                            $bili=0;
                        }
                        $updata1["bili"] =$bili;
                        $upmoney1=$money*$bili/100;
                        $money = $money-$upmoney1;
                        $updata1["money"] =$upmoney1;
                        $updata1["days"] =1;
                        $updata1["translateid"] = $translateid;
                        $updata1["paytime"] = $paytime;
                        $updata1["payid"] = $translateid;
                        $updata1["type"] =9;
                        $updata1["product"] = $product;
                        $updata1["money"] = $upmoney1;
                        $updata1["to_uid"] = $uid;
                        $updata1["account"] = $touid;//存入发送给用户的uid
                        $updata1["account_type"]=3;//记录为发送礼物类型
                        $updata["account"] = $touid;//存入发送给用户的uid
                        $updata["account_type"]=3;//记录为发送礼物类型
                        $updata1["uid"] = $pinfo["puid"];
                        $updata1["is_tixian"]=4;
                        $updata1["pid"]=$pinfo["puid"];
                        $PMoneyM->addOne($updata1);
                        $this->add_m_money($pinfo["puid"],$upmoney1);
                        $updata["is_tixian"]=4;
                        $updata["pid"]=$pinfo["puid"];
                        $data['pid']=$pinfo["puid"];
                        $data["bili"] = $bili;
                    }
    	            $updata["days"] = $contentint;
    	            $updata["translateid"] = $translateid;
    	            $updata["paytime"] = $paytime;
    	            $updata["payid"] = $translateid;
    	            $updata["type"] =8;
    	            $updata["product"] = $product;
    	            $updata["money"] = $upmoney;
    	            $updata["to_uid"] = $uid;
    	            $updata["uid"] = $pinfo["puid"];
    	            $PMoneyM->addOne($updata);
    	            $this->add_m_money($pinfo["puid"],$upmoney);
    	        }
	           
	            $data["money"] = $money;
	            $data["to_uid"] = $uid;
	            $data["uid"] = $touid;
	            //礼物图片
                $data["account"] =$url;
	            $PMoneyM->addOne($data);
	            //增加魅力值的同时需删除redis中的缓存
            $redis=$this->redisconn();
            $redis->delete('withdrawal_'.$touid);
            $redis->delete('withdrawal_order'.$touid);
	            //向魅力值记录表插入更新魅力值等数据开始。
            $this->add_m_money($touid,$money);
            /*   $mmoney=M('m_money');
            $mmon=$mmoney->where(array('uid'=>$touid))->find();
            $dmon=array();
              if($mmon){
                  $dmon['totalmoney']=$mmon['totalmoney']+$money;
                  $dmon['leftmoney']=$mmon['leftmoney']+$money;
                  $dmon['lasttime']=time();
                  $mmoney->where(array('uid'=>$touid))->save($dmon);
              }else{
                  $dmon['uid']=$touid;
                  $dmon['totalmoney']=$money;
                  $dmon['leftmoney']=$money;
                  $dmon['lasttime']=time();
                  $mmoney->add($dmon);
              } */
            //向魅力值记录表插入更新魅力值等数据结束。
	    }
	    return 1;
	}


    /*
         *拨打视频
         */
    public function sendvideo($time,$price,$touid,$uid){
        $PMoneyM = new PMoneyModel();
        $money=$time*$price*0.4*0.65*100;
        $product=$this->platforminfo['product'];
        $paytime=time();
        $updata["days"] =$time;
        $updata["paytime"] = $paytime;
        $updata["type"] =15;
        $updata["product"] = $product;
        $updata["money"] = $money;
        $updata["to_uid"] = $touid;
        $updata["uid"] = $uid;
        $PMoneyM->addOne($updata);
        $this->add_m_money($uid,$money);
    }
	/*
	 *部分3用户聊天按字符增加魅力值
	 */
	public function add_charmoney($datapuserinfo, $ToUserInfo,$content,$redis,$timecha){
	    $datapuid = $datapuserinfo["uid"];
	    $touid = $ToUserInfo["uid"];
	    //$redisStr = "addmoneymsg_".$touid."_".$datapuid;
	   // $charcount = 1;
	   /* if($redis->exists($redisStr)){
	        $charcount = $redis->get($redisStr)+1;
	        $redis->set($redisStr,$charcount,0,0,60*60*72);
	    }else{
	        $redis->set($redisStr,1,0,0,60*60*72);
	    }*/
	    //聊天少于三句的增加魅力值
	   // if($charcount<4){
	        $uid = $touid;
	        //字符数量
        $contenta=$newStr = str_replace(' ', '', $content);
        $contentint = mb_strlen($contenta, 'UTF-8');
	        $money = $contentint*0.2;
	        $money = $money>2?2:$money;//每条最多计费3魅力值，超过3魅力值的按照3魅力值计算
            if($timecha<60*30){
                $money=$money*1;
            }elseif($timecha>60*30 && $timecha<60*60){
                $money=$money*0.8;
            }elseif($timecha>60*60*1 && $timecha<60*60*3){
                $money=$money*0.5;
            }elseif($timecha>60*60*3 && $timecha<60*60*8){
                $money=$money*0.3;
            }elseif($timecha>60*60*8 && $timecha<60*60*12){
                $money=$money*0.1;
            }else{
                $money=$money*0.05;
            }
	        $paytime =time();
	        $type = 12;
	        $product = $ToUserInfo["product"];
	        $data["days"] = $contentint;
	        $data["paytime"] = $paytime;
	        $data["type"] = $type;
	        
	        $data["product"] = $product;
	       
	        $data["money"] = $money;
	        $data["to_uid"] = $touid;
	        $data["uid"] = $datapuid;
	        $PMoneyM = new PMoneyModel();
	        $PMoneyM->addOne($data);
	        //增加魅力值的同时需删除redis中的缓存
	        $redis->delete('withdrawal_'.$datapuid);
	        $redis->delete('withdrawal_order'.$datapuid);
	        //向魅力值记录表插入更新魅力值等数据开始。
	        $this->add_m_money($datapuid,$money);
	        
	        //向魅力值记录表插入更新魅力值等数据结束。
	    //}
	    return true;
	}
	public function set_moneykey($uid,$touid){
	    $moneykey = "moneykey_".$touid;

	    //echo $moneykey;
	    $redis = $this->redisconn();
	    $redis->listRemove($moneykey, $uid,1);
	    $redis->listPush($moneykey, $uid);
	    return $redis->setListKeyExpire($moneykey, 60*60*72);
	}
	public function get_moneykey($uid){
	    $moneykey = "moneykey_".$uid;
	    //echo $moneykey;
	    $redis = $this->redisconn();
	    return $redis->listGet($moneykey);
	}
	public function set_moneykey_num($uid,$touid){//$uid为vip<1用户，$touid为陪聊用户
        $redis = $this->redisconn();
        $moneytwokey = "moneytwokey_".$uid;
        $redis->listRemove($moneytwokey, $touid,1);
        $redis->listPush($moneytwokey, $touid);
        $redis->setListKeyExpire($moneytwokey, 60*60*72);
        $moneynumkey="moneynumkey_".$uid."_".$touid;
        if($redis->exists($moneynumkey)){
            $num=$redis->get($moneynumkey);
            $redis->set($moneynumkey,$num+1);
        }else{
            $redis->set($moneynumkey,1);
        }
        return $redis->setListKeyExpire($moneynumkey, 60*60*72);
    }
    public function get_moneykey_num($uid){
        $redis = $this->redisconn();
        $moneytwokey = "moneytwokey_".$uid;
        $res=$redis->listGet($moneytwokey,0,-1);
        $result=array();
        foreach($res as $k=>$v){
            $moneynumkey="moneynumkey_".$uid."_".$v;
            $result[$k]['touid']=$v;
            $result[$k]['num']=$redis->get($moneynumkey);
        }
        return $result;
    }








	public function get_Pinfo($uid,$reset=0){
	    $path =CHCHEPATH_PINFO;
	    $cache_name = 'pinfo_'.$uid;
	    if($reset==1){
	        return F($cache_name,NULL,$path);
	    }
	    if(F($cache_name,'',$path) && $reset == 0){
	        $res = F($cache_name,'',$path);
	    }else{
	        $pM = new PUserModel();
	        $res = $pM->getOne(array("uid"=>$uid));
	        F($cache_name,$res,$path);
	    }
	    return $res;
	}

	//推广用户提成
    public function vipRecharge($data,$UserInfo){
        $Indata = array();
        $Indata["days"] = $data["days"];
        $Indata["translateid"] = $data["translateid"];
        $Indata["paytime"] = $data["paytime"];
        $Indata["type"] = $data["type"];
        $Indata["to_uid"] = $data["uid"];
        $Indata["uid"] = $UserInfo["tg_uid"];
        $ticheng = $data['money']*0.65*0.03;
        $Indata["money"] = $ticheng;
        $PMoneyM = new PMoneyModel();
        $PMoneyM->addOne($Indata);
        return  $this->add_m_money($UserInfo["tg_uid"],$ticheng);

    }

}

?>