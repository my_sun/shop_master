﻿<?php
use Think\Controller;
header('Content-type: text/html; charset=utf-8');
class MailController extends Controller {
	/**
	 * 已兼容php7
	 * 注：本邮件类都是经过我测试成功了的，如果大家发送邮件的时候遇到了失败的问题，请从以下几点排查：
	 * 1. 用户名和密码是否正确；
	 * 2. 检查邮箱设置是否启用了smtp服务；
	 * 3. 是否是php环境的问题导致；
	 * 4. 将26行的$smtp->debug = false改为true，可以显示错误信息，然后可以复制报错信息到网上搜一下错误的原因；
	 * 5. 如果还是不能解决，可以访问：http://www.daixiaorui.com/read/16.html#viewpl 
	 *    下面的评论中，可能有你要找的答案。
	 *
	 *
	 * Last update time:2017/06
	 * UPDATE:
	 * 1、替换了高版本不支持的写法，如ereg、ereg_replace.
	 * 2、将 var 改为 public/private等.
	 * 3、使其兼容php7.
	 * 
	 */



	/**
	 * 邮件找回密码
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string toemail 用户的邮箱
	 * @request string title  邮件的主题
	 * @request string product  版本号
	 * @return data
	 */
	public function mail()
	{

		$this->Api_recive_date = Recive_data($_POST);


		$data = $this->Api_recive_date;



		//引入smtp类
		require_once "Smtp.class.php";
		//******************** 配置信息 ********************************
		$smtpserver = "ssl://smtp.163.com";//SMTP服务器
		$smtpserverport =465;//SMTP服务器端口
		$smtpusermail = "17335711659@163.com";//SMTP服务器的用户邮箱
		$smtpemailto = $data['toemail'];//发送给谁
		$smtpuser = "17335711659@163.com";//SMTP服务器的用户帐号，注：部分邮箱只需@前面的用户名
		$smtppass = "191118zhang";//SMTP服务器的用户密码
		$mailtitle = $data['title'];//邮件主题
		$mailtype = "HTML";//邮件格式（HTML/TXT）,TXT为文本邮件
		//************************ 配置信息 ****************************
		$product = $data['platforminfo']['product'];
		// dump($data['platforminfo']['product']);die;
		if(empty($smtpemailto)){
			$return = [
				'code' => 4001,
				'message'  => "请输入邮箱"
			];
			Push_data($return);
		}
		if(empty($mailtitle)){
			$return = [
				'code' => 4001,
				'message'  => "请输入邮件主题"
			];
			Push_data($return);
		}

		if(empty($data['platforminfo'])){
			$return = [
				'code' => 4001,
				'message'  => "platforminfo不能为空"
			];
			Push_data($return);
		}
		$res = preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $smtpemailto);
		if($res == false){
			$return = [
				'code' => 4002,
				'message'  => "请输入正确的邮箱"
			];
			Push_data($return);
		}
		//校验账号是否存在
		$sql = "select * from t_user_base where email = '".$smtpemailto."' and product = ".$product." limit 0,1";
		$res = M()->query($sql);
		// dump($res[0]['uid']);die;
		if($res ==false){
			$return = [
				'code' => 4005,
				'message'  => "用户不存在"
			];
			Push_data($return);
		}

		//连接redis
		$redis = new RedisModel();
		//设置key
		$key = "EMAIL_".$smtpemailto;

		$values = $redis->get($key);

		$keys = "EMAILIP_".$smtpemailto;

		$iskey = $redis->exists($keys);

		if($iskey != 0){
			$return = [
				'code' => 4005,
				'message'  => "你被禁了"
			];
			Push_data($return);
		}

		if($values >= 3){
			$redis->setex($keys,1,86400);
			$return = [
				'code' => 4005,
				'message'  => "操作太频繁"
			];
			Push_data($return);
		}

		// 设置认证码
		$ran = rand('100000','999999');
		$rand = $ran;
		unset($ran);
		$mailcontent = "<h1>"."【快约会】"."您的认证码是：".$rand."</h1>";//邮件内容
		$smtp = new Smtp($smtpserver,$smtpserverport,true,$smtpuser,$smtppass);//这里面的一个true是表示使用身份验证,否则不使用身份验证.
		$smtp->debug = false;//是否显示发送的调试信息
		$state = $smtp->sendmail($smtpemailto, $smtpusermail, $mailtitle, $mailcontent, $mailtype);
		if($state==""){

			$return = [
				'code' => 4003,
				'message'  => "请检查正确的邮件"
			];
			Push_data($return);
		}
		$redis->setex($key,$time = 600);
		$kk =$redis->deinc($key);
		//定义成功返回的格式
		$return = [
			'code' => 200,
			'message'  => "发送邮件成功",
			'data'  => array('codes'=>$rand,'uid'=>$res[0]['uid'])
		];
		Push_data($return);
	}

	/**
	 * 邮件认证码认证
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string code  认证码
	 * @request string password  新密码
	 * @request string product  版本号
	 * @return data
	 */
	public function verifi()
	{
		$this->Api_recive_date = Recive_data($_POST);


		$data = $this->Api_recive_date;
		$smtpemailto = $data['platforminfo'];//客户端信息对象
		$uid = $data['uid'];//uid

		$password = $data['password'];//新密码
		

		if( $password == "" || $uid == ""){

			$return = [
				'code' => 4001,
				'message'  => "参数不全"
			];
			Push_data($return);
		}

	    //修改密码
	    $sql ="update t_user_base set password = '".$password."' where uid = ".$uid;

	    $res = M()->execute($sql);

	    if(!$res){
	    	$sql ="update t_user_base_bak set password = '".$password."' where uid = ".$uid;

	    	$res = M()->execute($sql);
	    }
	    $redis = new RedisModel();

        $redis->del('getuser-'.$uid);
                
        

	    if($res !== false){

	    	$return = [
				'code' => 200,
				'message'  => "修改密码成功"
			];
			Push_data($return);
	    }

	}
	
}
?>