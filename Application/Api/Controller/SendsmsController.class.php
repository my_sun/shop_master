<?php 
use Think\Controller;

header('Content-type: text/html; charset=utf-8');
class SendsmsController extends Controller {


	/**
     * 发送短信
     * @param $phone 手机号多个手机号之间英文半角逗号隔开
     * @param  json platforminfo 客户端信息对象
     */
	public function phoneapi()
	{
		$this->Api_recive_date = Recive_data($_POST);


		$data = $this->Api_recive_date;

		//用户名 AND 密码
		$userName = "cs_v97c0y";
		$pwd = "D5WRcCFQ";

		$phone = $data['phone'];

		if(empty($phone)){

			$return = [
				'code' => 4001,
				'message'  => "请输入手机号"
			];
			Push_data($return);
		}

		//生成随机数
		$ran = rand('100000','999999');
		$rand = $ran;
		unset($ran);

		//生成时间
		$time = date("YmdHis");
		$times = $time;
		unset($time);

		//连接redis
		$redis = new RedisModel();
		//设置key
		$key = "SENDSMS_".$phone;

		$values = $redis->get($key);

		$keys = "SENDSMSIP_".$phone;

		$iskey = $redis->exists($keys);

		if($iskey != 0){
			$return = [
				'code' => 4005,
				'message'  => "你被禁了"
			];
			Push_data($return);
		}

		if($values >= 3){
			$redis->setex($keys,1,86400);
			$return = [
				'code' => 4005,
				'message'  => "操作太频繁"
			];
			Push_data($return);
		}

		//生成sign签名
		$string = $userName.$pwd.$times;
		$sign = md5($string); 
		
		$url = "http://sms.skylinelabs.cc:20003/sendsmsV2";
		//参数
		$params = array(
			'account' => $userName, //用户名
		    'sign' => $sign, //sign签名
		    'datetime' => $times, //时间戳
		    // 'numbers' => $phone, //手机号 
		    "content" => "【快约会】您的校验码是：".$rand
		);
		// dump($params);die;
		$paramstring = http_build_query($params);
		$paramstring = $paramstring.'&numbers='.$phone;

		//get 请求
		$content = $this->juheCurl($url, $paramstring);
		$result = json_decode($content, true);
		$redis->setex($key,$time = 600);
		$kk =$redis->deinc($key);
		if ($result['success']) {
			$return = [
				'code' => 200,
				'message'  => "发送短信成功",
				'data'  => array('codes'=>$rand,'phone'=>$phone)
			];
			Push_data($return);
			// var_dump($result);
		} else {
		    $return = [
				'code' => 200,
				'message'  => "请输入正确的手机号",
			];
			Push_data($return);
			// var_dump($result);
		}
	}


	/**
	 * 手机忘记密码 校验认证码
	 * @param  json platforminfo 客户端信息对象
	 * @param  string phone 手机号
	 * @param  string code 认证码
	 * @param  string password 新密码
	 * @return  data
	 */
	public function smsCode()
	{

		$this->Api_recive_date = Recive_data($_POST);


		$data = $this->Api_recive_date;

		$platforminfo = $data['platforminfo'];
		$phone = $data['phone'];
		$password = $data['password'];


		if(!isset($phone) || empty($phone)){
			$return = [
				'code' => 4002,
				'message'  => "手机号不能为空",
			];
			Push_data($return);
		}
		

		if(!isset($password) || empty($password)){
			$return = [
				'code' => 4005,
				'message'  => "新密码不能为空",
			];
			Push_data($return);
		}

		//查询手机号
	    $phone1 = " select * from t_user_extend where phonenumber = ".$phone;
	    $res = M()->query($phone1);

	    if(empty($res)){
	    	$phone2 = " select * from t_user_extend_bak where phonenumber = ".$phone;
	    	$res = M()->query($phone2);
	    	if(empty($res)){
	    		if($res !== false){
			    	$return = [
						'code' => 4006,
						'message'  => "用户手机号没认证"
					];
					Push_data($return);
	    		}
	    	}
	    }

	    $uid = $res[0]['uid'];

	    //修改密码
	    $sql ="update t_user_base set password = '".$password."' where uid = ".$uid;
	    $res = M()->execute($sql);

	    $redis = new RedisModel();

        $redis->del('getuser-'.$uid);
	    if($res !== false){

	    	$return = [
				'code' => 200,
				'message'  => "修改密码成功"
			];
			Push_data($return);
	    }else{
	    	$return = [
				'code' => 4007,
				'message'  => "修改密码失败"
			];
			Push_data($return);
	    }	
	}


	/**
	 * 请求接口返回内容
	 * @param  string $url [请求的URL地址]
	 * @param  string $params [请求的参数]
	 * @param  int $ipost [是否采用POST形式]
	 * @return  string
	 */
	function juheCurl($url, $params = false, $ispost = 0)
	{
	    $httpInfo = array();
	    $ch = curl_init();

	    curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
	    curl_setopt($ch, CURLOPT_USERAGENT, 'JuheData');
	    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
	    if ($ispost) {
	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	        curl_setopt($ch, CURLOPT_URL, $url);
	    } else {
	        if ($params) {
	            curl_setopt($ch, CURLOPT_URL, $url . '?' . $params);
	        } else {
	            curl_setopt($ch, CURLOPT_URL, $url);
	        }
	    }
	    $response = curl_exec($ch);
	    if ($response === FALSE) {
	        //echo "cURL Error: " . curl_error($ch);
	        return false;
	    }
	    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	    $httpInfo = array_merge($httpInfo, curl_getinfo($ch));
	    curl_close($ch);
	    return $response;

	}


	 //全世界手机号国码
    public function countrycode()
    {
        
    	$platforminfo = $_POST['platforminfo'];

    	// dump($this->parameterinit($platforminfo));die;

		$isenglish = $_POST['isenglish'] ? $_POST['isenglish'] : 0;

		$cn = array("中国",'美国');

		$where['countrys_cn'] = array("in",$cn);

		// $data = M('phone_info')->where($where)->field('codes,countrys_cn')->select();

		$kk = new  SysController();
		
		if($isenglish == 0){

			$data = M('phone_info')->where($where)->field('codes,countrys_cn')->select();
			$return = [
				'code' => 200,
				'message'  => "成功",
				'data' => $data
			];
			Push_data($return);
		}

		$data = M('phone_info')->where($where)->field('codes,countrys_cn,countrys_en')->select();
		$return = [
			'code' => 200,
			'message'  => "成功",
			'data' => $data
		];
		Push_data($return);


    }



}

 ?>