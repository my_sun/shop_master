<?php
/*
*收藏
*/
class CollectionController extends BaseController
{

    public function __construct(){
        parent::__construct();
        //$this->is_login();//检查用户是否登陆
        $this->fileurl = '/userdata/renzheng/'.date('Ymd',time())."/";
    }

    //添加收藏
    public function add()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $touid = $data['touid'];
        $return = array();
        $return['code'] = ERRORCODE_201;
        $return['message'] = $this->L("CANSHUYICHANG");
        if(empty($touid) || !isset($touid)){
            $return = [
                'code' => 4001,
                'message'  => "视频id不能为空"
            ];
            Push_data($return);
        }

        $Collection = new CollectionModel();

        $arr = [
            'uid' => $uid,
            'touid' => $touid,
            'time' => time()
        ];

        $res = $Collection->addOne($arr);

        if($res){
            $return = array();
            $return = [
                'code' => 200,
                'message'  => "收藏成功"
            ];
            $redis = new RedisModel();
            $key = 'my_collection'.$uid;
            // dump($key);die;
            $redis->del($key);
        }else{
            $return = [
                'code' => 4002,
                'message'  => "收藏失败"
            ];
        }
        Push_data($return);
    }

    public function del()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $return = array();
        $return['code'] = ERRORCODE_201;
        $return['message'] = $this->L("CANSHUYICHANG");
        $Collection = new CollectionModel();

        $id = $data['id'];
        if(empty($id) || !isset($id)){
            $return = [
                'code' => 4001,
                'message'  => "id不能为空"
            ];
            Push_data($return);
        }

        $Collection = new CollectionModel();

        $res = $Collection->delOne(array('id'=>$id));

        if($res){
            $return = array();
            $return = [
                'code' => 200,
                'message'  => "删除成功"
            ];
            $redis = new RedisModel();
            $key = 'my_collection'.$uid;
            // dump($key);die;
            $redis->del($key);

        }else{
            $return = [
                'code' => 4002,
                'message'  => "删除失败"
            ];
        }
        Push_data($return);
    }

    //我的收藏
    public function mycollection()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;

        $return = array();
        $return['code'] = ERRORCODE_201;
        $return['message'] = $this->L("CANSHUYICHANG");
        $redis = new RedisModel();
        $key = 'my_collection'.$uid;
        $values = $redis->exists($key);
        // dump($values);die;
        if($values){
            $vss = $redis->get($key);
            $data = json_decode($vss,true);
        }else{
            $data = M('collection as a')
                    ->join('t_video as b on b.id = a.touid')
                    ->field("b.*")
                    ->select();
            if($data){
                foreach ($data as $key => $value) {
                
                    $userinfo = $this->get_diy_user_field($value['uid']);
                    $data[$key]['userinfo'] = $userinfo;
                }
                $rets = json_encode($data);
                $redis->setex($key,$rets,$time = 3600);
            } 
        }
        $return['code'] = 200;
        $return['message'] = "获取我收藏的视频列表成功";
        $return['data'] = $data;

        Push_data($return);
    }

}

