<?php
/**
 * 视频聊天
 */

class VideochartController extends BaseController
{
    public function __construct()
    {
        parent::__construct();

    }

    //视频用户初始化信息接口

    public function parametermsg()
    {


        $cachename = "get_bili";
        $cache = S($cachename);
        if (!$cache) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
            $num = $this->randomFloat(6, 10);
            $newNum = sprintf("%.1f", $num);
            $cache = $newNum;
            S($cachename, $cache, 60 * 2);//设置缓存的生存时间
        }
        $newNum = $cache;

        $productmsg = "金币支持语音通话、视频通话、文字交流和礼物交流；视频通话每分钟消耗20金币，语音通话每分钟消耗10金币";
        //$editmsg="修改昵称可获得1分钟语音通话所对应的金币,上传图像并审核通过可获得1分钟视频通话所对应的金币";
        $return_data['num'] = $newNum;
        $return_data['productmsg'] = $productmsg;
        //$return_data['editmsg']=$editmsg;
        //返回每分钟视频聊天消耗的金币数
        $return_data['videochartgold'] = 20;
        $return_data['audiochartgold'] = 10;
        $return_data['gradeone'] = 10;
        $return_data['gradetwo'] = 20;
        $return_data['gradethree'] = 30;
        $return_data['gradefour'] = 40;
        $return_data['gradefive'] = 50;
        $return_data['gradefix'] = 60;
        $return_data['gradeseven'] = 70;
        $return_data['gradeeight'] = 80;
        $return_data['gradenine'] = 90;
        $return_data['gradeten'] = 100;
        $res['data'] = $return_data;
        Push_data($res);
    }


    /**
     * 男用户开始匹配，将个人信息储存在列表当中
     * @request string token 用户token
     * @request string status 1:用户正在匹配的时候点击取消
     * @return data
     */
    public function match()
    {
        $data = $this->Api_recive_date;
        $status = $data['status'];//1:用户正在匹配的时候点击取消
        $uid = $this->uid;
        //定时器最长等待时间
        $timer = 6;
        $cishu = 3;
        $UserInfo = $this->UserInfo;
        $gender = $UserInfo['gender'];
        $redis = $this->redisconn();

        //第一种情况：用户在匹配的过程当中点击取消
        if ($gender == 1) {
            if ($status == 1) {
                $redis->listRemove('Videochar_match_boy', $uid);
                $result['massage'] = $this->L("CHENGGONG");
                $result['data']['timer'] = $timer;
                $res = $result;
                Push_data($res);
            } else {
                //删除列表中存在的uid之后再最后边添加
                $redis->listRemove('Videochar_match_boy', $uid);
                $ret = $redis->listPush('Videochar_match_boy', $uid, 1);
                if ($ret) {
                    $result['massage'] = $this->L("CHENGGONG");
                    $result['data']['timer'] = $timer;
                    $res = $result;
                    Push_data($res);
                } else {
                    $result['code'] = ERRORCODE_501;
                    $result['massage'] = $this->L("redis内部错误");
                    $res = $result;
                    Push_data($res);
                }
            }
        } else {

            $result['massage'] = $this->L("CHENGGONG");
            $result['data']['timer'] = $timer;
            $result['data']['cishu'] = $cishu;
            $res = $result;
            Push_data($res);
        }


    }

    /**
     * 给女用户返回正在匹配男用户
     *
     * @request string token 用户token
     * @request int isxn 是否返回xn用户
     * @return data
     */
    public function matchlist()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $isxn = $data['isxn'];
        $UserInfo = $this->UserInfo;
        $gender = $UserInfo['gender'];
        $redis = $this->redisconn();
        $type = $data['type'];
        $intent = $data['intent'] ? $data['intent'] : '';
        //最后时间检测是否有真实用户存在，有则返回，没有则返回为空
        if ($type == 1) {
            $lang = $redis->listSize('Videochar_match_boy');
            if ($lang == 0) {//如果匹配列表中当前没有男性用户
                $res['massage'] = "列表为空";
                Push_data($res);
            } else {
                $ret = $redis->listLrange('Videochar_match_boy', 0, 0);
                $result['list'] = $this->get_user($ret[0]);
                $this->sendmsg($uid, $ret[0], 'successsed send message!', 'videomatch');
                $redis->listRemove('Videochar_match_boy', $ret[0]);
                $res['data'] = $result;
                Push_data($res);
            }
        }
        //女性用户情况：
        if ($gender == 2) {
            //2：12秒结束后未返回正式用户则返回xn用户
            if ($isxn == 1) {
                //女会员且意向为女生情况
                if ($UserInfo['vipgrade'] > 0 & $intent == 2) {
                    $uidlist = D('user_base')->where(array('user_type' => 2, 'gender' => 2))->order("RAND()")->limit(1)->getField('uid', true);
                    foreach ($uidlist as $k => $v) {
                        $result['xnlist'][$k] = $this->get_user($v);
                    }
                    //$redis->listRemove('Videochar_match_girl', $uid);
                    $res['data'] = $result;
                    Push_data($res);
                }
                //普通女用户情况
                $uidlist = D('user_base')->where(array('user_type' => 2, 'gender' => 1))->order("RAND()")->limit(3)->getField('uid', true);
                foreach ($uidlist as $k => $v) {
                    $result['xnlist'][$k] = $this->get_user($v);
                }
                //$redis->listRemove('Videochar_match_girl', $uid);
                $res['data'] = $result;
                Push_data($res);
            }
            if ($UserInfo['vipgrade'] > 0 & $intent == 2) {
                $girllang = $redis->listSize('Videochar_match_girl');
                if ($girllang == 0) {
                    $ret = $redis->listPush('Videochar_match_girl', $uid, 1);
                    $res['data'] = 2;//返回前端使其不在匹配，等待接听
                    Push_data($res);
                } else {
                    $ret = $redis->listLrange('Videochar_match_girl', 0, 0);
                    foreach ($ret as $k1 => $v1) {
                        $result['list'][$k1] = $this->get_user($v1);
                        $this->sendmsg($uid, $v1, 'successsed send message!', 'videomatch');
                        $redis->listRemove('Videochar_match_girl', $v1);
                    }
                    $res['data'] = $result;
                    Push_data($res);

                }

            }

            //3：从列表中获取正在匹配的男性用户
            if ($UserInfo['vipgrade'] == 0 & $intent != 1) {
                $girllang = $redis->listSize('Videochar_match_girl');
                if ($girllang != 0) {
                    $ret = $redis->listLrange('Videochar_match_girl', 0, 0);
                    foreach ($ret as $k1 => $v1) {
                        $result['list'][$k1] = $this->get_user($v1);
                        $this->sendmsg($uid, $v1, 'successsed send message!', 'videomatch');
                        $redis->listRemove('Videochar_match_girl', $v1);
                    }
                    $res['data'] = $result;
                    Push_data($res);
                }
            }

            $lang = $redis->listSize('Videochar_match_boy');
            if ($lang == 0) {//如果匹配列表中当前没有男性用户
                $res['massage'] = $this->L("CHENGGONG");
                $res['data'] = 1;
                Push_data($res);
            } else {
                $ret = $redis->listLrange('Videochar_match_boy', 0, 2);
                foreach ($ret as $k1 => $v1) {
                    $result['list'][$k1] = $this->get_user($v1);
                    $this->sendmsg($uid, $v1, 'successsed send message!', 'videomatch');
                }
                $uidlist = D('user_base')->where(array('user_type' => 2, 'gender' => 1))->order("RAND()")->limit(3)->getField('uid', true);
                foreach ($uidlist as $k => $v) {
                    $result['xnlist'][$k] = $this->get_user($v);
                }
            }
            foreach ($ret as $k3 => $v3) {
                $redis->listRemove('Videochar_match_boy', $v3);
            }
            $res['data'] = $result;
            Push_data($res);


        } else {//男用户情况
            if ($UserInfo['vipgrade'] > 0) {
                if ($intent == 1) {
                    $redis->listRemove('Videochar_match_boy', $uid);
                    $lang = $redis->listSize('Videochar_match_boy');
                    if ($lang != 0) {
                        $ret = $redis->listLrange('Videochar_match_boy', 0, 0);
                        $result['list'] = $this->get_user($ret[0]);
                        $this->sendmsg($uid, $ret[0], 'successsed send message!', 'videomatch');
                        $redis->listRemove('Videochar_match_boy', $ret[0]);
                        $res['data'] = $result;
                        Push_data($res);
                    } else {
                        $uidlist = D('user_base')->where(array('user_type' => 2, 'gender' => 1))->order("RAND()")->limit(3)->getField('uid');
                        $result['xnlist'][0] = $this->get_user($uidlist);
                        $res['data'] = $result;
                        Push_data($res);
                    }
                }
            }
            //12秒结束后未接受到女用户发送的消息则首先返回男用户然后返回xn用户
            if ($isxn == 1) {
                //$uidlist=D('user_base')->where(array('user_type'=>2,'gender'=>2))->order("RAND()")->limit(1)->getField('uid');
                //$result['xnlist'][0]=$this->get_user($uidlist);
                $redis->listRemove('Videochar_match_boy', $uid);
                for ($i = 1; $i < 5; $i++) {
                    $toboylang = $redis->listSize('Videochar_match_boy_toboy');
                    if ($toboylang == 0) {//如果匹配列表中当前没有男性用户
                        $ret = $redis->listPush('Videochar_match_boy_toboy', $uid, 1);
                        $boylang = $redis->listSize('Videochar_match_boy');
                        if ($boylang == 0) {//如果匹配列表中当前没有男性用户
                            $redis->listRemove('Videochar_match_boy_toboy', $uid);
                            $uidlist = D('user_base')->where(array('user_type' => 2, 'gender' => 2))->order("RAND()")->limit(1)->getField('uid');
                            $result['xnlist'][0] = $this->get_user($uidlist);
                            $res['data'] = $result;
                            Push_data($res);
                        } else {
                            $ret = $redis->listLrange('Videochar_match_boy', 0, 0);
                            $result['list'] = $this->get_user($ret[0]);
                            $this->sendmsg($uid, $ret[0], 'successsed send message!', 'videomatch');
                            $redis->listRemove('Videochar_match_boy', $ret[0]);
                            $redis->listRemove('Videochar_match_boy_toboy', $uid);
                            $res['data'] = $result;
                            Push_data($res);
                        }
                    } else {
                        if ($i == 4) {
                            $uidlist = D('user_base')->where(array('user_type' => 2, 'gender' => 2))->order("RAND()")->limit(1)->getField('uid');
                            $result['xnlist'][0] = $this->get_user($uidlist);
                            $res['data'] = $result;
                            Push_data($res);
                        } else {
                            sleep(2);
                            continue;
                        }
                    }
                }
                $res['data'] = $result;
                Push_data($res);
            }
        }

    }


    //发消息接口

    public function sendmsg($uid, $touid, $content, $msgtype = 'videomatch')
    {
        $to = $touid;
        $from = $uid;
        $sendmsg = array();
        $sendmsg["touid"] = $to;
        $sendmsg["msgid"] = substr(md5($this->getMillisecond()), 8, 16);//substr(md5(time()), 8, 16)
        $body = array();
        $body["id"] = $sendmsg["msgid"];
        $body["msgType"] = $msgtype;
        $messageBody = array(
            "content" => $content,
            "sendTime" => time(),
        );
        $body["messageBody"] = $messageBody;
        $body["user"] = $this->get_diy_user_field($from, "uid|gender|age|nickname|area|head");
        $sendmsg["msgbody"] = $body;

        $touserinfo = $this->get_diy_user_field($to, "uid|product");
        $txkey=M('txmy')->where(array('product'=>$touserinfo["product"]))->getField('miyao');
        if (isset($txkey)) {
            $ret = tencent_sendmsg($sendmsg,$txkey);
            return $ret;
        } else {
            return LM_sendmsg($sendmsg);
        }
    }

    //发送消息记录接口
    public function videomsgrecord()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $touid = $data['touid'];
        $videotime = $data['videotime'];
        $time = time();
        $type = $data['type'];
        $res = array(
            'uid' => $uid,
            'touid' => $touid,
            'videotime' => $videotime,
            'type' => $type,
            'time' => $time
        );
        $isrecord = M('videomsg_record')->where(array('uid' => $uid, 'touid' => $touid))->setInc('degree'); // 用户的通话次数加1
        if ($isrecord) {
            $success = M('videomsg_record')->where(array('uid' => $uid, 'touid' => $touid))->setField('time', time());
        } else {
            $success = M('videomsg_record')->add($res);
        }
        if ($success) {
            $result['massage'] = $this->L("CHENGGONG");
            $res = $result;
            Push_data($res);
        }
    }

    //获取通话记录接口
    public function msgrecordlist()
    {
        import("Extend.Library.ORG.Util.Page");       //导入分页类
        $data = $this->Api_recive_date;
        $page = $data["page"] ? $data["page"] : 1;//
        $pagesize = $data["pagesize"] ? $data["pagesize"] : 15;//
        $uid = $this->uid;
        $count = M('videomsg_record')->where(array('uid' => $uid))->count();    //计算总数
        $Page = new Page($count, $pagesize, $page);
        $data = M('videomsg_record')->where(array('uid' => $uid))->limit($Page->firstRow . ',' . $Page->listRows)->select();
        $msgrecordlist = array();
        foreach ($data as $k => $v) {
            $msgrecordlist[$k]['time'] = $v['time'];
            $msgrecordlist[$k]['videotime'] = $v['videotime'];
            $msgrecordlist[$k]['degree'] = $v['degree'];
            $msgrecordlist[$k]['user'] = $this->get_diy_user_field($v['touid'], "uid|gender|age|nickname|head");
        }

        $return_data["msgrecordlist"] = $msgrecordlist;
        $return_data["page"] = array(
            "pagesize" => $pagesize,
            "page" => $page,
            "totalcount" => $count
        );
        $res['data'] = $return_data;
        Push_data($res);


    }

    //删除通话记录接口
    public function delmsgrecord()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $touid = $data['touid'];
        $return = array();
        $return['code'] = ERRORCODE_201;
        $return['message'] = $this->L("CANSHUYICHANG");
        if ($touid) {
            $return = array();
            $res['uid'] = $uid;
            $res['touid'] = $touid;
            $result = M('videomsg_record')->where($res)->delete();
        }
        if ($result == 0) {
            $return['message'] = '没有删除成功！';
        }
        Push_data($return);
    }

    //产生随机小数的函数
    function randomFloat($min, $max)
    {
        return $min + mt_rand() / mt_getrandmax() * ($max - $min);
    }

    public function getMillisecond()
    {
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($t1) + floatval($t2)) * 1000);
    }


    //语音或视频通话收益
    public function  getvideomoney(){
        $data = $this->Api_recive_date;
        $userinfo=$this->UserInfo;
        $uid=$this->uid;
        if($userinfo['gender']==2){
            $timea=$data['time'];
            $type=$data['type'];
            $touid=$data['touid'];
            //type=1为语音聊天 为2为视频聊天
            if($type==1){
                $price=$userinfo['audioprice'];
                if($price=='' || $price=='0'){
                    $price=10;
                }
            }else{
                $price=$userinfo['videoprice'];
                if($price==''){
                    $price=20;
                }
            }
            $MoneyC = new MoneyController();
            $MoneyC->sendvideo($timea,$price,$touid,$uid);
            //增加通话时长
            $UserBaseM = new UserBaseModel();
            $videotime=$UserBaseM->where('uid='.$uid)->getField('videotime');
            if($videotime==null){
                $UserBaseM->where('uid='.$uid)->save(array('videotime'=>$timea));
            }else{
                $UserBaseM->where('uid='.$uid)->setInc('videotime',$timea);
            }

            $this->set_user_field($uid,"videotime", $this->UserInfo["videotime"]+$timea);
        }
        $return=array();
        Push_data($return);
    }




}
