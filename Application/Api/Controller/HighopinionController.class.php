<?php
/*
 * 用户动态
 */
class HighopinionController extends BaseController {
	public function __construct(){
		parent::__construct();
		//$this->is_login();//检查用户是否登陆
		//echo date("Y-m-d",time());exit;
		$this->msgfileurl = '/userdata/highoponionfile/'.date('Ymd',time())."/";
		$this->cachepath = WR.'/userdata/cache/highopinion/';
		
	}
	public function index(){
		echo 0;exit;
	}	
	 /**
     * 上传送会员认证
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request int type 送会员类型:1: 文字2:图片3：小视频
     * @request file file 如果类型为图片最多可以上传9张，视频只能上传一个，文件必须经客户端压缩处理后再上传，文件大小不能超过5M
     * @request string content 动态文字内容
     * @request int ltime 上传小视频需要传这个参数，视频时长
     * @request file imageurl 上传小视频需要传这个参数，视频第一帧图片文件
     * @return data
     */
	 public function add(){
         $data = $this->Api_recive_date;
         $return=self::add_opinion($data);
         Push_data($return);
     }

	public  function add_opinion($data){
		$uid = $this->uid;
		$userinfo=$this->UserInfo;
		$type = $data["type"] ? $data["type"] : 1;
        switch ($type) {
            case "1":
                $dataoppinion="ishighopinion";
                break;
            case "2":
                $dataoppinion="ishighopinionvideo";
                break;
            case "3":
                $dataoppinion="ishighopinionaudio";
                break;
            case "4":
                $dataoppinion="ishighopiniondata";
                break;
            case "5":
                $dataoppinion="ishighopinionphoto";
                break;
            case "6":
                $dataoppinion="ishighopiniondynamic";
                break;
            case "7":
                $dataoppinion="ishighopinionshare";
                break;
            default:
                $dataoppinion="ishighopinion";
        }
		$content = $data["content"] ? $data["content"] : "";
		$filetype = "image";
		/*if($type==3){
			$filetype = "video";
		}*/
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("SHANGCHUANSHIBAI");
        $highopinionM = M('highopinion');
        $highopinion = array();
        //用户在类型为0和类型为3的时候可以在此上传，1为通过2为待通过
        if($userinfo[$dataoppinion]==0||$userinfo[$dataoppinion]==3) {
            if ($type == 1||$type==7) {//好评,好友分享，需上传截图
                if (!empty($_FILES)) {
                    $addArr = array();
                    $addArr["content"] = $content;
                    $addArr["uid"] = $uid;
                    $addArr["time"] = time();
                    $addArr["type"] = $type;
                    $highopinionid = $highopinionM->data($addArr)->add();
                    $saveUrl = $this->msgfileurl . $filetype . "/";
                    $savePath = WR . $saveUrl;
                    $uploadList = $this->_upload($savePath);
                    if (!empty($uploadList)) {
                        $obj = array();
                        $PutOssarr = array();
                        $returnurlarr = array();
                        $ids = 0;
                        if ($type == 1||$type==7) {
                            $ids = array();
                            $PhotohighopinionM = M('photo_highopinion');
                            foreach ($uploadList as $k => $v) {
                                $Indata = array();
                                $Indata['highopinionid'] = $highopinionid;
                                $Indata['uid'] = $uid;
                                $Indata['type'] = $type;//1好评7好友分享
                                $Indata['url'] = $saveUrl . $v['savename'];
                                $ret = $PhotohighopinionM->data($Indata)->add();
                                $ids[] = $ret;
                                $PutOssarr[] = $Indata['url'];
                                $obj[] = array(
                                    "id" => $ret,
                                    "url" => C("IMAGEURL") . $Indata['url'],
                                    "thumbnaillarge" => C("IMAGEURL") . $Indata['url'],
                                    "thumbnailsmall" => C("IMAGEURL") . $Indata['url'],
                                    "status" => 1,
                                    "seetype" => 1
                                );
                            }
                            $ids = implode("|", $ids);

                            $highopinionM->where(array("id" => $highopinionid))->setField("url", $ids);
                            PutOss($PutOssarr);
                            //将该用户的状态更改为正在审核中
                            M('user_extend')->where(array('uid'=>$uid))->setField($dataoppinion, 2);
                            //更改缓存
                            $this->set_user_field($uid,$dataoppinion,2);
                            //Dump($PutOssarr);exit;
                            $return = array();
                            $highopinion = array(
                                "id" => $highopinionid,
                                "content" => $content,
                                "type" => $type,
                                "time" => $addArr["time"],
                                "obj" => $obj,
                                "comment" => array()
                            );
                        } else {
                            $return = array();
                            $return['code'] = ERRORCODE_201;
                            $return['message'] = $this->L("SHANGCHUANDEWENJIANBUNENGWEIKONG");
                        }
                    }
                }
            } else {
                $addArr = array();
                $addArr["content"] = $content;
                $addArr["uid"] = $uid;
                $addArr["time"] = time();
                $addArr["type"] = $type;
                $highopinionid = $highopinionM->data($addArr)->add();
                $return = array();
                $highopinion = array(
                    "id" => $highopinionid,
                    "content" => $content,
                    "type" => $type,
                    "time" => $addArr["time"],
                    "obj" => '',
                    "comment" => array()
                );
                //将该用户的状态更改为正在审核中
                M('user_extend')->where(array('uid'=>$uid))->setField($dataoppinion, 2);
                //更改缓存
                $this->set_user_field($uid,$dataoppinion,2);
            }
        }else{
            $return['message'] = '已经申请过一次了或者正在审核中';
        }
		$return['data']["highopinion"]=$highopinion;
        return $return;

	}
	
	// 文件上传
	protected function _upload($savePath) {
		import("Api.lib.Behavior.fileDirUtil");
		$fileutil = new fileDirUtil();
		$fileutil->createDir($savePath);
		import("Api.lib.Behavior.UploadFile");
		//导入上传类
		$upload = new UploadFile();
		//设置上传文件大小
		$upload->maxSize = 5242880;
        //$upload->maxSize = 6291456;
		//设置上传文件类型
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
		//设置附件上传目录
		$upload->savePath = $savePath;
		//设置需要生成缩略图，仅对图像文件有效
		$upload->thumb = false;
		// 设置引用图片类库包路径
		$upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
		//设置需要生成缩略图的文件后缀
		$upload->thumbPrefix = 'm_';  //生产2张缩略图
		//设置缩略图最大宽度
		$upload->thumbMaxWidth = '400';
		//设置缩略图最大高度
		$upload->thumbMaxHeight = '400';
		//设置上传文件规则
		$upload->saveRule = "md5content";
		//删除原图
		$upload->thumbRemoveOrigin = false;
		if (!$upload->upload()) {
			$return = array();
			$return['code'] = ERRORCODE_501;
			$return['message'] = $upload->getErrorMsg();
			Push_data($return);
			//捕获上传异常
			//$this->error($upload->getErrorMsg());
		} else {
			//取得成功上传的文件信息
			$uploadList = $upload->getUploadFileInfo();
			return $uploadList;;
		}

	}
	
	/**
	 * 删除动态
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int id 动态id
	 * @return data
	 */
	public function delete(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		$id = $data["id"] ? $data["id"] : 0;
		$return = array();
		$return['code'] = ERRORCODE_201;
		$return['message'] = $this->L("CANSHUYICHANG");
		if($id){
			$return = array();
			$dynamicM = new DynamicModel();
			$where = array("id"=>$id);
			$DyInfo = $dynamicM->getOne($where);
			
			if($DyInfo["type"]==2){
				//删除照片
				$PhotoDynamicM = new PhotoDynamicModel();
				$ids = explode("|", $DyInfo["ids"]);
				foreach ($ids as $v){
					$PhotoInfo = $PhotoDynamicM->getOne(array("id"=>$v));
					_unlink(WR.$PhotoInfo["url"]);
					$this->get_photodynamic($v,1);
					DelOss(array($PhotoInfo["url"]));
				}
				$PhotoDynamicM->delOne(array("danamicid"=>$id));
				
			}elseif($DyInfo["type"]==3){
				//删除视频
				$VideoDynamicM = new VideoDynamicModel();
				$VideoInfo = $VideoDynamicM->getOne(array("id"=>$DyInfo["ids"]));
				_unlink(WR.$VideoInfo["imageurl"]);
				_unlink(WR.$VideoInfo["url"]);
				$this->get_videodynamic($DyInfo["ids"],1);
				DelOss(array($VideoInfo["imageurl"],$VideoInfo["url"]));
				$VideoDynamicM->delOne(array("danamicid"=>$id));
				//Dump($VideoInfo);exit;
			}
			//删除评论
			$DynamicCommentM = new DynamicCommentModel();
			$DynamicCommentM->delOne(array("danamicid"=>$id));
			//删除动态
			$dynamicM->delOne($where);
			$this->get_dynamiclist(0,0,0,1);//清空动态缓存
            //删除已经通过的动态，也需要将存在redis中的缓存删除，然后重新读取数据库
            $redis=$this->redisconn();
            $redis->del('dynamic_boy');
            $redis->del('dynamic_girl');
		}
		Push_data($return);
	}
}
?>