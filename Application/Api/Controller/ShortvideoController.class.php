<?php
require_once WR.'/lib/ali_voduploadsdk/aliyun-php-sdk-core/Config.php';   // 假定您的源码文件和aliyun-php-sdk处于同一目录
use vod\Request\V20170321 as vod;
date_default_timezone_set('PRC');
class ShortvideoController extends BaseController {
    const AccessKeyId='LTAI4FpbAAKxm6ppD1babNhg';
    const AccessKeySecret='siF61p3HZ8HdTOcqBnPrxIckfoWXCK';
    public function __construct(){
        parent::__construct();
        //$this->is_login();//检查用户是否登陆

    }

    function initVodClient($accessKeyId=self::AccessKeyId, $accessKeySecret=self::AccessKeySecret) {
        $regionId = 'ap-southeast-1'; // 点播服务接入区域
        $profile = DefaultProfile::getProfile($regionId, $accessKeyId, $accessKeySecret);
        return new DefaultAcsClient($profile);
    }

    /**
     * 删除视频
     */
    public function deleteVideo(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        if(empty($uid)){
            $return = [
                'code' => 4002,
                'message'  => "用户未登录"
            ];
            Push_data($return);
        }
        if(!isset($data['videoid']) or $data['videoid'] == ''){
            $return = [
                'code' => 4002,
                'message'  => "videoid不允许为空"
            ];
            Push_data($return);
        }
        $client = $this->initVodClient();
        $request = new vod\DeleteVideoRequest();
        $request->setVideoIds($data['videoid']);  // 支持批量删除视频；videoIds为传入的视频ID列表，多个用逗号分隔
        $request->setAcceptFormat('JSON');
        $ali_res = object_array($client->getAcsResponse($request));
        if(empty($ali_res['RequestId'])){
            $return = [
                'code' => 4002,
                'message'  => "删除失败"
            ];
        }

        $vodInfo = new VodInfoModel();
        $res = $vodInfo->updateOne(['videoid'=>$data['videoid'],'uid'=>$uid],['is_del'=>2]);
        if($res){
            Push_data(array(data=>array('is_del'=>2)));
        }else{
            write_logs(json_encode(['videoid'=>$data['videoid']]),"ShortvideoController_error.txt");
            $return = [
                'code' => 4002,
                'message'  => "数据删除失败"
            ];
            Push_data($return);
        }

    }

    /**
     * 获取自己的短视频
     * @throws ClientException
     * @throws ServerException
     */
    public function getMyVod(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $pageno = empty($data['page'])?1:$data['page'];
        $pagesize = empty($data['pagesize'])?20:$data['pagesize'];
        if(!empty($uid)){
            $vodInfo = new VodInfoModel();
            $where = array('uid'=>$uid,'is_del'=>1);

            //$res = $vodInfo->getList($where);
            $res = $vodInfo->getListPage($where,$pageno,$pagesize);
            if(!empty($res['list'])){
                $data =array();
                $res['list'] = array_reverse($res['list']);
                foreach ($res['list'] as $k=>$v){
                    //获取视频信息 查看状态是否正常
                    try {
                        $request = new vod\GetVideoInfoRequest();
                        $request->setVideoId($v['videoid']);
                        $request->setAcceptFormat('JSON');
                        $client = $this->initVodClient();
                        $payinfo = $client->getAcsResponse($request);
                    }catch (Exception $e){
                        continue;
                    }
                    if($payinfo->Video->Status != "Normal"){
                        $data['data'][$k]['coverurl'] = $payinfo->Video->CoverURL;
                        $data['data'][$k]['videoid'] = $payinfo->Video->VideoId;
                        $data['data'][$k]['title'] = $payinfo->Video->Title;
                        $data['data'][$k]['fab_count_num'] = 0;
                        $data['data'][$k]['vod_play'] = "";
                        $data['data'][$k]['status'] = $payinfo->Video->Status == "Blocked"?3:2;

                        continue;
                    }

                    //获取视频播放地址
                    $VideoBase = $this->GetPlayInfo($v['videoid']);

                    $data['data'][$k]['coverurl'] = isset($VideoBase['VideoBase']['CoverURL'])?$VideoBase['VideoBase']['CoverURL']:'';
                    $data['data'][$k]['videoid'] = isset($VideoBase['VideoBase']['VideoId'])?$VideoBase['VideoBase']['VideoId']:'';
                    $data['data'][$k]['title'] = isset($VideoBase['VideoBase']['Title'])?$VideoBase['VideoBase']['Title']:'';
                    $data['data'][$k]['vod_play'] = isset($VideoBase['PlayInfoList']['PlayInfo'][0]['PlayURL'])?$VideoBase['PlayInfoList']['PlayInfo'][0]['PlayURL']:'';
                    $data['data'][$k]['status'] = 1;
                    //获取点赞总条数
                    //从redis读取当前视频点赞总数
                    $redis = $this->redisconn();
                    $fab_count_num =  $redis->get('vod_fabulous_count_num_'.$v['videoid']);
                    $VodInfo = new VodInfoModel();
                    $video = $VodInfo->getOne(array('videoid'=>$v['videoid']));
                    $data['data'][$k]['fab_count_num'] = $fab_count_num ?$video['fab'] :0;

                }
            }
            $da['data'] = array_values($data['data']);//重新初始化方便安卓解析
            //获取点赞总条数
            Push_data($da);
        }else{
            $return = [
                'code' => 4002,
                'message'  => "用户未登录"
            ];
            Push_data($return);
        }


    }


    public function GetPlayInfo($VideoId){
        try {
            $client_rs = $this->initVodClient();
            $pay_request = new vod\GetPlayInfoRequest();
            $pay_request->setVideoId($VideoId);
            $pay_request->setAuthTimeout(3600 * 24);
            $pay_request->setAcceptFormat('JSON');
            $vod_info = object_array($client_rs->getAcsResponse($pay_request));
            return $vod_info;
        }catch (Exception $e) {
            Push_data();

        }
    }

    /**
     * 短视频点赞
     */
    public function fabulous(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $videoid = isset($data['videoid']) ?$data['videoid'] :'' ;
        if (empty($videoid)){
            $return = [
                'code' => 4002,
                'message'  => "视频ID不允许为空"
            ];
            Push_data($return);
        }
        //根据video 获取视频所属人id
        $vodInfo = new VodInfoModel();
        $where = array('videoid'=>$videoid,'is_del'=>1,'is_success'=>1);
        $res = $vodInfo->getOne($where);
        $addArr = array(
            "videoid"=>$videoid,
            "time"=>time(),
            "uid"=>$uid,
            "otheruid"=>$res['uid']?$res['uid']:0
        );
        $VodInfo = new VodInfoModel();
        $video = $VodInfo->getOne(array('videoid'=>$videoid));

        $m = M("vod_support");
        $temp = $m->where(array("uid"=>$uid,"videoid"=>$videoid))->find();
        $redis = $this->redisconn();
        if(!$temp){
            $m->add($addArr);
            //存入redis具体用户点赞
            $redis->set('vod_fabulous_'.$uid."_".$videoid, 1);

        }else{
            $return = [
                'code' => 4002,
                'message'  => "重复点赞"
            ];
            Push_data($return);
        }
        // 添加点赞数量
        $VodInfo->updateOne(array("videoid"=>$videoid),array("fab"=>$video['fab']+1));
        //redis 自增
        $redis->deinc('vod_fabulous_count_num_'.$videoid);
        if($video['uid'] != $uid ){
            // ------------------------------------------------------
            $User_baseM = new UserBaseModel();
            $wherebase['uid'] = $video['uid'];
            $retu = $User_baseM->getOne($wherebase);

            //消息推送
            $res = $User_baseM->updateOne($wherebase,array('unread_vod_fab'=>$retu['unread_vod_fab']+1));
            $retu = $User_baseM->getOne($wherebase);
            $contents = [
                'push_type' => "unread_vod_fab",
                'data' =>  $retu['unread_vod_fab']
            ];
            $content=json_encode($contents);
            $argv1 = array();
            $argv1['url'] = "http://127.0.0.1:81/userauth/sendtzmsg/?uid=" . $retu['uid'] . "&content=" . $content ."&msgType=text&from=10005";
            $argv1['time'] =1;
            $argv1 = base64_encode(json_encode($argv1));
            $command = "/usr/bin/php " . WR . "/openurl.php " . $argv1 . " > /dev/null &";
            system($command);
            // ------------------------------------------------------
        }
        Push_data();
    }

    /**
     * 取消点赞
     */
    public function unFabulous(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $videoid = isset($data['videoid']) ?$data['videoid'] :'' ;
        if (empty($videoid)){
            $return = [
                'code' => 4002,
                'message'  => "视频ID不允许为空"
            ];
            Push_data($return);
        }

        //删除redis数据
        $redis = $this->redisconn();
        $redis->del('vod_fabulous_'.$uid."_".$videoid);
        //自减
        $redis->deinc('vod_fabulous_count_num_'.$videoid,0);
        //数据库减去点赞
        $VodInfo = new VodInfoModel();
        $video = $VodInfo->getOne(array('videoid'=>$videoid));
        $VodInfo->updateOne(array("videoid"=>$videoid),array("fab"=>$video['fab']-1));
        //删除点赞库里记录
        $m = M("vod_support");
        $temp = $m->where(array("uid"=>$uid,"videoid"=>$videoid))->delete();
        if($temp == false){
            $return = [
                'code' => 4002,
                'message'  => "取消点赞失败"
            ];
            Push_data($return);
        }
        Push_data();
    }

    /**
     * 我收藏（点赞）的视频
     */
    public function collectVod(){
        $data = $this->Api_recive_date;
        $uid = 493172;//$this->uid;
        $pageno = empty($data['page'])?1:$data['page'];
        $pagesize = empty($data['pagesize'])?20:$data['pagesize'];
        if(empty($uid)){
            $return = [
                'code' => 4002,
                'message'  => "用户未登录"
            ];
            Push_data($return);
        }

        //读取数据库我点赞的视频id
        $m = new VodSupportModel();
        $res = $m->getListPage(array("uid"=>$uid),$pageno,$pagesize);
        if(!empty($res['list'])){
            $data = array();
            foreach ($res['list'] as $k=>$v){
                //获取视频播放地址
                //$VideoBase = $this->GetPlayInfo($v['videoid']);
                try {
                    $client_rs = $this->initVodClient();
                    $pay_request = new vod\GetPlayInfoRequest();
                    $pay_request->setVideoId($v['videoid']);
                    $pay_request->setAuthTimeout(3600 * 24);
                    $pay_request->setAcceptFormat('JSON');
                    $VideoBase = object_array($client_rs->getAcsResponse($pay_request));

                }catch (Exception $e) {
                    continue;

                }
                $data['data'][$k]['coverurl'] = isset($VideoBase['VideoBase']['CoverURL'])?$VideoBase['VideoBase']['CoverURL']:'';
                $data['data'][$k]['videoid'] = isset($VideoBase['VideoBase']['VideoId'])?$VideoBase['VideoBase']['VideoId']:'';
                $data['data'][$k]['title'] = isset($VideoBase['VideoBase']['Title'])?$VideoBase['VideoBase']['Title']:'';
                $data['data'][$k]['vod_play'] = isset($VideoBase['PlayInfoList']['PlayInfo'][0]['PlayURL'])?$VideoBase['PlayInfoList']['PlayInfo'][0]['PlayURL']:'';
                //获取点赞总条数
                //从redis读取当前视频点赞总数
                $redis = $this->redisconn();
                $fab_count_num =  $redis->get('vod_fabulous_count_num_'.$v['videoid']);
                $VodInfo = new VodInfoModel();
                $video = $VodInfo->getOne(array('videoid'=>$v['videoid']));
                $data['data'][$k]['fab_count_num'] = $fab_count_num ?$video['fab'] :0;

                //获取用户信息
                $User_baseM = new UserBaseModel();
                $baseInfo = $User_baseM->getOne(['uid'=>$v['otheruid']]);
                $data['data'][$k]['uid'] = $v['otheruid'];//发布者uid
                $data['data'][$k]['nickname'] = isset($baseInfo['nickname'])?$baseInfo['nickname']:'';
                $data['data'][$k]['is_fabulous'] = 1;
                $data['data'][$k]['isfollow'] = $this->isFollow($video['uid'],$uid);
                //获取是否在线 随机 并保持一致存入redis
                $redis = $this->redisconn();
                $keyexitone=$redis->exists('onlinestatus_25100_'.$video['uid']);
                if(!empty($keyexitone)){
                    $data['data'][$k]['isonline_vod'] = $redis->get('onlinestatus_25100_'.$video['uid']);
                }else{
                    $ran = mt_rand(1,3);
                    $data['data'][$k]['isonline_vod'] = $ran;
                    $redis->set('onlinestatus_25100_'.$video['uid'],$ran,0,0,60*60*24);
                }

                //获取用户头像
                $photo = new PhotoModel();
                $photores = $photo->getOne(['uid'=>$v['otheruid']]);
                $data['data'][$k]['imgurl'] = $photores['url'] ?C("IMAGEURL").$photores['url'] :'';

            }

            $da['data'] = array_reverse($data['data']);
            Push_data($da);
        }
        Push_data();

    }

    /*
*是否关注 isFollow
*/
    public function isFollow($touid,$uid=0,$reset=0){
        $uid = $uid ? $uid : $this->uid;
        if($touid&&$uid){
            //$path = $this->get_userpath($uid, 'cache/follow/');
            $cache_name = 'follow_'.$uid.$touid;
            if($reset==1){
                return S($cache_name,null);
            }

            if(S($cache_name)){
                $cacheValue = S($cache_name);
            }else{
                $UserFollowM = new UserFollowModel();
                $return = $UserFollowM->getOne(array('touid'=>$touid,'uid'=>$uid));

                if($return){
                    $cacheValue = 1;
                }else{
                    $cacheValue = 0;
                }
                S($cache_name,$cacheValue,60*60*24);
            }

            return $cacheValue;
        }else{
            return 0;
        }
    }


    /**
     * @throws ClientException
     * @throws ServerException
     * 我获得的赞
     */
    public function harvestFabulous(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;

        $m = new VodSupportModel();
        $page = $data['page'] ? $data['page'] :1;
        $pagesize = $data['pagesize'] ? $data['pagesize'] :20;
        $res = $m->getListPage(array("otheruid"=>$uid),$page,$pagesize);
        $data = array();
        if(!empty($res['list'])){
            $photo = new PhotoModel();
            $User_baseM = new UserBaseModel();

            foreach ($res['list'] as $k=>$v){
                $where['uid'] = $v['uid'];//点赞人id
                $where['type'] = 2;
                $retu = $photo->getOne($where);
                $name = $User_baseM->getOne(['uid'=>$v['uid']]);

                //获取短视频信息
                try {
                    $client = $this->initVodClient();
                    $request = new vod\GetPlayInfoRequest();
                    $request->setVideoId($v['videoid']);
                    $request->setAuthTimeout(3600 * 24);
                    $request->setAcceptFormat('JSON');
                    $vod_info = object_array($client->getAcsResponse($request));
                }catch (Exception $e){
                    continue;
                }
                $data['data'][$k]['time'] = date("Y-m-d",$v['time']);
                $data['data'][$k]['imgurl'] = $retu['url'] ?C("IMAGEURL").$retu['url'] :'';
                $data['data'][$k]['uid'] = $v['uid'];
                $data['data'][$k]['name'] = $name['nickname'];
                $data['data'][$k]['coverurl'] = $vod_info['VideoBase']['CoverURL'];
                $data['data'][$k]['playurl'] = $vod_info['PlayInfoList']['PlayInfo'][0]['PlayURL'];
            }
            $User_baseM->updateOne(array("uid"=>$uid),array("unread_vod_fab"=>0));
        }
        $da['data'] = array_reverse($data['data']);
        Push_data($da);

    }


    //获取直播金币数量
    public function getGoldSet(){
        $uid = $this->uid;
        //检查是否后台添加主播
        $compereinfo = new CompereInfoModel();
        $gold_conf = $compereinfo->getOne(array("uid"=>$uid));
        $TouserInfo['videoprice'] = $gold_conf['gold_conf']?$gold_conf['gold_conf']*1:10;
        $TouserInfo['audioprice'] = $gold_conf['gold_conf']?$gold_conf['gold_conf']/2:5;
        //获得通话时长和当前可设置等级
        $time_grade = $gold_conf['talk_time']+$gold_conf['video_time'];
        if($time_grade == 0){
            $grade = 0;
        }elseif ($time_grade >0 && $time_grade<=30){
            $grade = 1;
        }elseif ($time_grade >30 && $time_grade<=90){
            $grade = 2;
        }elseif ($time_grade >90 && $time_grade<=180){
            $grade = 3;
        }elseif ($time_grade >180 && $time_grade<=270){
            $grade = 4;
        }elseif ($time_grade >270 && $time_grade<=360){
            $grade = 5;
        }elseif ($time_grade >360 && $time_grade<=450){
            $grade = 6;
        }elseif ($time_grade >450 && $time_grade<=540){
            $grade = 7;
        }elseif ($time_grade >540 && $time_grade<=630){
            $grade = 8;
        }elseif ($time_grade >630 && $time_grade<=720){
            $grade = 9;
        }elseif($time_grade>720){
            $grade = 10;
        }
        $TouserInfo['time_grade'] = $grade;
        Push_data(array("data"=>$TouserInfo));
    }

    //设置通话金币消耗
    public function setGoldConf(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $gold = $data['gold'];
        $compereinfo = new CompereInfoModel();
        if(!empty($gold)){

            $res = $compereinfo->updateOne(array('uid'=>$uid),array("gold_conf"=>$gold));

                Push_data();

        }

    }

    /*
     * 通话（视频）结束后
     */
    public function callTimeAsglod(){
        $data = $this->Api_recive_date;
        $girl_uid = $data['girl_uid'];//女生uid
        $boy_uid = $data['boy_uid'];//男生uid
        $type = $data['type'];//1视频  2通话
        $time = $data['time'];//时长 （分钟）
        //查询女生设置的通话金币设置
        $com = new CompereInfoModel();
        $com_info = $com->getOne(array("uid"=>$girl_uid));
        if(!empty($com_info)){
            $gold_conf = $com_info['gold_conf'];
            if($type == 2){
                $gold_conf = $gold_conf/2;
            }
            //计算应扣男生金币数
            $ungold = $time*$gold_conf;
            //
            $User_baseM = new UserBaseModel();
            $boy_info = $User_baseM->getOne(array("uid"=>$boy_uid));
            //if($boy_info['gold']>=$ungold){//扣除男生金币
                $gold = $boy_info['gold']- $ungold;

                $gold_diff = $User_baseM->updateOne(array("uid"=>$boy_uid),array("gold"=>$gold));
                if($gold_diff){//给女生增加金币

                    //平台抽水
                    $talk_gold_pump = $com_info['talk_gold_pump'];//抽水比例
                    $girl_gold = $ungold - ($ungold*($talk_gold_pump/100));

                    $mmoney=M('m_money');
                    $mmon = $mmoney->where(array('uid'=>$girl_uid))->find();
                    $inse = array();

                    $totalmoney= $mmon['totalmoney']+$girl_gold;
                    $leftmoney= $mmon['leftmoney']+$girl_gold;
                    $m_success = M('m_money')->where(array('uid'=>$girl_uid))->save(array("totalmoney"=>$totalmoney,"leftmoney"=>$leftmoney));


                    if($type == 1){//1视频时长  2语yin时长
                        $inse['video_time'] = $time+ $com_info['video_time'];
                    }else{
                        $inse['talk_time'] = $time+ $com_info['talk_time'];
                    }
                    $inse['income'] = $com_info['income']+$girl_gold;//累计收益
                    $res_girl = $com->updateOne(array("uid"=>$girl_uid),$inse);//更新女生魅力值和通话时长

                    if($res_girl && $m_success){
                        //判断是否有上级公司
                        if($com_info['platform_id'] != 0){
                            //更新上级公司总收益
                            $vodadmin = new VodAdminModel();
                            $admininfo = $vodadmin->getOne(array("id"=>$com_info['platform_id']));
                            $count_money = $admininfo['count_money']+$girl_gold;//添加女孩收益入账上级公司
                            $vodadmin->updateOne(array("id"=>$com_info['platform_id']),array("count_money"=>$count_money));
                            //查看是否还有上级公司收益 更新上上级
                            if($admininfo['company_id']!=0){
                                $admin_company = $vodadmin->getOne(array("id"=>$admininfo['company_id']));
                                $company_count_money = $admin_company['count_money'] +$girl_gold;
                                $vodadmin->updateOne(array("id"=>$admininfo['company_id']),array("count_money"=>$company_count_money));
                            }

                        }
                    }
                //更新缓存
                    $redis = new RedisModel();

                    $redis->del('getuser-'.$boy_uid);
                    $redis->del('getuser-'.$girl_uid);
                    $redis->delete('withdrawal_' . $girl_uid);

                Push_data(array("data"=>array("gold"=>$gold)));
                }else{
                    $return = [
                        'code' => 4002,
                        'message'  => "扣除男生金币失败"
                    ];
                    //write_logs();
                    Push_data($return);
                }

           // }
        }

    }


    /**
     * 通话结束记录
     */
    public function callTalkrecord(){
        $data = $this->Api_recive_date;
        $girl_uid = $data['girl_uid'];//女生uid
        $boy_uid = $data['boy_uid'];//男生uid
        $type = $data['type'];//1视频  2通话
        $time = $data['time'];//时长 （分钟）
        //查询女生设置的通话金币设置
        $com = new CompereInfoModel();
        $com_info = $com->getOne(array("uid"=>$girl_uid));
        if(!empty($com_info)){
            $gold_conf = $com_info['gold_conf'];
            if($type == 2){
                $gold_conf = $gold_conf/2;
            }
            //计算应扣男生金币数
            $ungold = $time*$gold_conf;
            //平台抽水
            $talk_gold_pump = $com_info['talk_gold_pump'];//抽水比例
            //女生应得
            $girl_gold = $ungold - ($ungold*($talk_gold_pump/100));

            $dataInfo['girl_uid'] = $girl_uid;
            $dataInfo['boy_uid'] = $boy_uid;
            $dataInfo['boy_un_gold'] = $ungold;
            $dataInfo['girl_in_gold'] = $girl_gold;
            $dataInfo['gold_pump'] = $talk_gold_pump;
            $dataInfo['gold_conf'] = $gold_conf;
            $dataInfo['type'] = $type;
            $dataInfo['time'] = $time;
            $dataInfo['offtime'] = time();
            $dataInfo['ontime'] = time()-$time;
            $res = M("talk_record")->data($dataInfo)->add();
            if($res){
                $redis=$this->redisconn();
                $redis->delete('withdrawal_' . $girl_uid);
            }

        }

    }



}



?>