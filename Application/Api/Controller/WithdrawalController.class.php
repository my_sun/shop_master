<?php
/**
 * 魅力值管理
 */

class WithdrawalController extends BaseController
{
    public function __construct ()
    {
        parent::__construct ();
        
    }
    /**
     * 魅力值信息
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @return data
     */
    public function index(){
        $uid = $this->uid;
        $UserInfo = $this->UserInfo;
        //$redis=$this->redisconn();
        //if($redis->exists('withdrawal_'.$uid)){
           // $red=$redis->get('withdrawal_'.$uid);
          //  $return['data'] =json_decode($red,true);
      //  }else {
            $res = M('m_money')->where(array('uid' => $uid))->find();
            if ($res) {
                $ret = array();
                $ret['totalmoney'] = intval($res['totalmoney']);
                $ret['awardmoney']=intval($res['awardmoney']);
                if($UserInfo['user_type']==3){
                    $ret['leftgoldmoney'] = intval($res['leftgoldmoney']);
                }
                $ret['leftmoney'] = intval($res['leftmoney']);

                $ret['realmoney'] = sprintf("%.2f", ($res['leftmoney']/100*6.5));

                $ret['username']=$res['commonpay'];
                $ret['phonenum']=$res['otherpay'];
                $ret['zhifubao']=$res['zhifubao'];
                $ret['weixin']=$res['weixin'];
                $ret['paypal']=$res['paypal'];
            } else {
                $ret = array();
                if($UserInfo['user_type']==3){
                    $ret['leftgoldmoney'] =0;
                }
                $ret['totalmoney'] = 0;
                $ret['leftmoney'] = 0;
                $ret['realmoney'] = 0;
            }
            //界面提示信息
            $ret['message']='提示：每月仅可提现一次，魅力值≥500才可进行提现。';
            //界面魅力值转换解释信息
            //$ret['msgexplan']='魅力值获取方式为：\n上传头像\n完善个人信息95%以上\n接收礼物等';
            $ret['msgexplan']='';//先隐藏
            //界面魅力值大小限制信息
            $ret['count_mes']=500;
            //转化为金额的比例
        $ret['convermoney']=0.01*6.5;
        //添加需求：用户类型1提现在原来基础上*0.05
        if($UserInfo["user_type"]==1){
            $ret['realmoney'] = sprintf("%.2f", ($res['leftmoney']/100*6.5));
            $ret['convermoney']=0.01*6.5;
        }
        if($UserInfo["user_type"]==3){
            $ret['convergold']=1.7;
        }else{
            $ret['convergold']=0.01*25*2;
        }
       
            $redisStr = "withdrawal_" . $uid;
            $withd = json_encode($ret);
            //$redis->set($redisStr, $withd, 0, 0, C("SESSION_TIMEOUT"));
            $return['message'] = $this->L("CHENGGONG");
            $return['data'] = $ret;
        //}
            Push_data($return);
        }





        //25100项目魅力值提现
        public function moneyAscash(){
            $data = $this->Api_recive_date;
            $uid = $this->uid;
            $UserInfo = $this->UserInfo;
            $money = $data['money'];
            //获取提现抽水比例 （主播用户获取单独的抽水比例）
            $CompereInfo = new CompereInfoModel();
            //查看当前用户是否是后台添加的主播
            $is_signing = $CompereInfo->getOne(array("uid"=>$uid));
            //播放总时长小于100小时不允许提现
            $alltime = $is_signing['talk_time']+$is_signing['video_time'];
            if($alltime<=10){
                $return = [
                    'code' => 4002,
                    'message'  => "视频（语音）时间小于100分钟，不予提现申请"
                ];
                Push_data($return);
            }
            if($is_signing['is_signing'] == 1){
                $country = "CN";
            }else{
                $country = $UserInfo['country'];
            }

            //取出配置表
            $PrivateSet = new VodPrivateSetModel();
            //获取转换比例
            switch($country){
                case "CN":
                    $to_cash = $PrivateSet->getOne("to_rmb");
                    $to_cash = $to_cash['to_rmb'];
                    $unit = "¥";
                    break;
                case "HK":
                    $to_cash = $PrivateSet->getOne("to_hkd");
                    $to_cash = $to_cash['to_hkd'];
                    $unit = "HK$";
                    break;
                case "TW":
                    $to_cash = $PrivateSet->getOne("to_twd");
                    $to_cash = $to_cash['to_twd'];
                    $unit = "NT$";
                    break;
                default :
                    $to_cash = $PrivateSet->getOne("to_usd");
                    $to_cash = $to_cash['to_usd'];
                    $unit = "$";
                    break;

            }

            $money_as_cash = 0;
            if($is_signing['is_signing'] == 0){//普通用户提现
                $money_pump = $PrivateSet->getOne("money_as_cash");
                $money_as_cash =  $money_pump['money_as_cash'];
            }elseif($is_signing['is_signing'] == 1){//后台添加主播
                $money_as_cash = $is_signing['gold_pump'];
            }

            $money_as_cash = $money_as_cash == 0 || empty($money_as_cash)? 50 :$money_as_cash;//如果并未设置 则默认50%
            //平台进行抽水
            $platform_money = $money - ($money*($money_as_cash/100));
            //提现比例转化
            $draw_money = ceil($platform_money/$to_cash);

            Push_data(["data"=>array("draw_money"=>$draw_money,"unit"=>$unit)]);
        }



        //25100魅力值提现
        public function vod_draw(){
            $data = $this->Api_recive_date;
            $uid = $this->uid;
            $UserInfo = $this->UserInfo;
            $money = $data['money'];
            $username = $data['username'];
            $account = $data['account'];
            $currency_type = $data['currency_type'];
            $country = $UserInfo['country'];
            //扣除魅力值
            $mmoney = M('m_money');
            $mmon = $mmoney->where(array('uid' => $uid))->find();
            $resm = array();
            if($mmon['totalmoney'] < $money || $mmon['leftmoney'] < $money){
                $return = [
                    'code' => 4002,
                    'message'  => "没有那么多魅力值，无法兑换"
                ];
                Push_data($return);
            }
            $resm['totalmoney'] = $mmon['totalmoney'] - $money;
            $resm['leftmoney'] = $mmon['leftmoney'] - $money;
            $moneyRes = $mmoney->where(array('uid' => $uid))->save($resm);
            if($moneyRes){
                $redis=$this->redisconn();
                $redis->delete('withdrawal_' . $uid);
            }
            //取出配置表
            $PrivateSet = new VodPrivateSetModel();
            //获取转换比例
            switch($country){
                case "CN":
                    $to_cash = $PrivateSet->getOne("to_rmb");
                    $to_cash = $to_cash['to_rmb'];
                    $unit = "RMB";
                    break;
                case "HK":
                    $to_cash = $PrivateSet->getOne("to_hkd");
                    $to_cash = $to_cash['to_hkd'];
                    $unit = "HKD";
                    break;
                case "TW":
                    $to_cash = $PrivateSet->getOne("to_twd");
                    $to_cash = $to_cash['to_twd'];
                    $unit = "TWD";
                    break;
                default :
                    $to_cash = $PrivateSet->getOne("to_usd");
                    $to_cash = $to_cash['to_usd'];
                    $unit = "USD";
                    break;

            }
            //获取提现抽水比例 （主播用户获取单独的抽水比例）
            $CompereInfo = new CompereInfoModel();
            //查看当前用户是否是后台添加的主播
            $is_signing = $CompereInfo->getOne(array("uid"=>$uid));
            $money_as_cash = 0;
            $vod_draw = array();
            if($is_signing['is_signing'] == 0){//普通用户提现
                $money_pump = $PrivateSet->getOne("money_as_cash");
                $money_as_cash =  $money_pump['money_as_cash'];
            }elseif($is_signing['is_signing'] == 1){//后台添加主播
                $money_as_cash = $is_signing['gold_pump'];
                $vod_draw['superiorid'] = $is_signing['platform_id'];
            }

            $money_as_cash = $money_as_cash ==0 ? 50 :$money_as_cash;//如果并未设置 则默认50%
            //平台进行抽水
            $platform_money = $money - ($money*($money_as_cash/100));
            //提现比例转化
            $draw_money = intval(ceil($platform_money/$to_cash));

            //提现表增加一条数据

            $vod_draw['money'] = $data['money'];//提现的魅力值
            $vod_draw['draw_cash'] = $draw_money;//提现所得现金
            $vod_draw['currency'] = $unit;//币种
            $vod_draw['uid'] = $uid;
            $vod_draw['user_type'] = $is_signing['is_signing'];
            $vod_draw['pump'] = $money_as_cash;//抽水百分比
            $vod_draw['user_name'] = $username;//填写的用户名
            $vod_draw['currency_type'] = $currency_type;//1支付宝 2微信 3paypal
            $vod_draw['account'] = $account;//提现账户
            $vod_draw['time'] = time();//提现账户
            M('vod_draw')->add($vod_draw);
            Push_data();

        }

        /**
         * 魅力值提现
         *
         * @request json platforminfo 客户端信息对象
         * @request string token 用户token
         * @request string account_type 账户类型
         * @request string account 账户
         * @request string username 账户名
         * @request string draw_money 提取魅力值金额
         * @request string phonenum 电话
         * @return data
         */
    public function draw(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $pinfo = $this->get_user($uid);
        $puids=M('admin_three_user')->getField('id',true);
        $ret=array();
        $ret['uid']=$uid;
        $ret['type']=4;
        $ret['is_tixian']=1;
        if($data['account_type']==4) {
            $ret['type']=5;//转换为金币记录
            $ret['is_tixian']=2;
        }else{
            if(in_array($pinfo["puid"],$puids)){
                $ret['type']=10;//
                $ret['pid']=$pinfo["puid"];//
                $ret['is_tixian']=4;//第三方用户提交申请
            }
        }


        $ret['account_type'] = $data["account_type"]?$data["account_type"]:1;//账户类型
        if($ret['account_type']!=4) {
            if ($data["account"]) {
                $ret['account'] = $data["account"];//账户编号
            } else {
                $return['code'] = ERRORCODE_201;
                $return['message'] = $this->L("需要传入账户信息！！");
            }
            if (!$data["username"]) {
                $return['code'] = ERRORCODE_201;
                $return['message'] = $this->L("需要传入真实姓名信息！！");
            }
        }
        $ret['money']= $data["draw_money"];//提取魅力值
        $ret['paytime']= time();
        $beginThismonth=mktime(0,0,0,date('m'),1,date('Y'));
        $mmoney=M('m_money')->where(array('uid'=>$uid))->find();
        //查看截止到当前时间订单列表中是否有提现记录
        $mmo=array();
        $mmo['paytime']=array('between',array($beginThismonth,$ret['paytime']));
        $mmo['uid']=$uid;
        //$mmo['type']=4;
        //$mmo['_string']='type=4 OR type>5';
        //$mmo['_query'] = 'type=4&type=5&_logic=or';


        $mmo['_string'] = 'type=4 OR type=5';
        if(in_array($pinfo["puid"],$puids)){
            $mmo['_string'] = 'type=10';
        }

        //需求：如果用户类型为3，且转换为钻石则不限制次数
        if($pinfo["user_type"]==3){
            $mmo['_string'] = 'type=4';
        }
        $mmo['is_tixian']=array('neq',3);
        $mmo['account_type']=array('neq',4);
        $mmoneylist=M('p_money')->where($mmo)->find();
        //首先判断该用户是否有权限提现：1，账户魅力值大于500 2：用户每月只能提现一次
        if($ret['money']>=500){
            if($data['account_type']!=4){
                if($mmoneylist){
                    $return['code'] = ERRORCODE_201;
                    $return['message'] = $this->L ("对不起,本月已经提现一次");
                    Push_data($return);
                }
            }
            if($data['account_type']==4&&$pinfo['user_type'] ==3) {
                if ($mmoney['leftgoldmoney'] < $ret['money']) {
                    $return['code'] = ERRORCODE_201;
                    $return['message'] = $this->L("对不起,提取的魅力值大于剩下的魅力值");
                    Push_data($return);
                }
                } else {
                    if ($mmoney['leftmoney'] < $ret['money']) {
                        $return['code'] = ERRORCODE_201;
                        $return['message'] = $this->L("对不起,提取的魅力值大于剩下的魅力值");
                        Push_data($return);
                    }
                }
                    //添加订单的同时需删除订单缓存和魅力值信息缓存
                    $redis = $this->redisconn();
                    $redis->del('withdrawal_order' . $uid);
                    $redis->del('withdrawal_' . $uid);
                    //获取奖励魅力值
                    $ret['awardmoney']=$mmoney['awardmoney'];
                    //向记录中添加
                    M('p_money')->add($ret);
                    //向魅力值信息表插入更新账户信息以及减少对应的魅力值
                    $countm=array();
                    $countm['commonpay']=$data["username"];
                    $countm['otherpay']=$data['phonenum'];

            if($data['account_type']==4&&$pinfo['user_type'] ==3) {
                $countm['leftgoldmoney']=$mmoney['leftgoldmoney']-$ret['money'];
            }else{
                $countm['leftmoney']=$mmoney['leftmoney']-$ret['money'];
                $countm['awardmoney']=$mmoney['awardmoney']-$ret['awardmoney'];
            }

                    if($ret['account_type']==1){
                        $countm['weixin']=$ret['account'];
                    }elseif($ret['account_type']==2){
                        $countm['zhifubao']=$ret['account'];
                    }elseif($ret['account_type']==3){
                        $countm['paypal']=$ret['account'];
                    }
                    M('m_money')->where(array('uid'=>$uid))->save($countm);
                    //如果为转化为金币，则不许要审核
                if($ret['type']=5){
                    $lgold=M('user_base')->where(array('uid'=>$uid))->getField('gold');
                    if($pinfo["user_type"]==3){
                        $rgold=round($lgold+$ret['money']*1.7);
                    }else{
                        $rgold=round($lgold+$ret['money']*0.2);
                    }
                    $msgs=M('user_base')->where(array('uid'=>$uid))->save(array('gold'=>$rgold));
                    if($msgs){
                        //$this->get_user($uid,1);
                        $this->set_user_field($uid,"gold",$rgold);//更新用户金币字段
                    }

                }

                    $return['message'] = $this->L ("订单已提交");
                    Push_data($return);


        }else{
            $return['code'] = ERRORCODE_201;
            $return['message'] = $this->L ("对不起,提取的账户魅力值不足500");
            Push_data($return);
        }
    }
    /**
     * 魅力值记录
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @return data
     */
    public function order(){
        $UserInfo = $this->UserInfo;
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $pinfo = $this->get_user($uid);
        $puids=M('admin_three_user')->getField('id',true);
        if(in_array($pinfo["puid"],$puids)){
            $type=10;//从数据库中读取的提现类型
        };
        $pageNum = $data['page'] ? $data['page'] : 1;
        $pageSize = $data['pagesize'] ? $data['pagesize'] : 10;

        //$redis = $this->redisconn();
        //设置list缓存过期时间为一天
       // $redis->setListKeyExpire('withdrawal_order'.$uid,C("SESSION_TIMEOUT"));
        //缓存中储存总数据
       // $lang=$redis->listSize('withdrawal_order'.$uid);
       /* if ($lang == 0) {*/
            if (1) {
                $pmoneyM=new PMoneyModel();
                $Wdata =array();
                $Wdata['uid']=$uid;
                $Wdata['type']=array('in',array(3,4,5,10,15));
                $ret=$pmoneyM->getListPage($Wdata,$pageNum,$pageSize);

                $res=$ret['list'];
            //历史订单查询根据时间排序后限定查询六条。
            //$res = M('p_money')->where(array('uid' => $uid))->order('paytime desc')->limit(16)->select();
            $result=array();
            if ($res) {
                foreach($res as $k=>$v){
                    $result[$k]['time']=$v['paytime'];
                 if($v['type']==3){
                        $userinfo=$this->get_diy_user_field($v['to_uid']);
                        $result[$k]['type']=3;//类型为礼物
                        $result[$k]['nickname']=$userinfo['nickname'];
                        //礼物数量
                        $result[$k]['count']=$v['days'];
                        //增加的魅力值
                        $result[$k]['add_money']=$v['money'];
                        //礼物图片
                        $result[$k]['gift_img']=C ("IMAGEURL").$v['account'];
                    }elseif($v['type']==4){
                        $result[$k]['type']=4;//类型为提现记录
                        $result[$k]['account_type']=$v['account_type'];
                        //提现账户
                        $result[$k]['account']=$v['account'];
                        //提取的魅力值
                        $result[$k]['draw_money']=$v['money'];
                        //提现状态
                     if($v['is_tixian']==4){
                         $v['is_tixian']=1;
                     }
                        $result[$k]['status']=$v['is_tixian'];
                        //提取金额
                        $result[$k]['realmoney']=intval($v['money']/100*6.5);
                    }elseif($v['type']==10){
                     $result[$k]['type']=4;//类型为提现记录
                     $result[$k]['account_type']=$v['account_type'];
                     //提现账户
                     $result[$k]['account']=$v['account'];
                     //提取的魅力值
                     $result[$k]['draw_money']=$v['money'];
                     //提现状态
                     if($v['is_tixian']==4){
                         $v['is_tixian']=1;
                     }
                     $result[$k]['status']=$v['is_tixian'];
                     //提取金额
                     $result[$k]['realmoney']=intval($v['money']/100*6.5);
                 }elseif($v['type']==5){
                        $result[$k]['type']=6;//类型为转化为金币
                        //提取的魅力值
                        $result[$k]['draw_money']=$v['money'];
                        //提现状态
                        $result[$k]['status']=$v['is_tixian'];
                        //提取金币
                        if($UserInfo["user_type"]==3){
                            $result[$k]['realmoney']=intval($v['money']/100*4);
                        }else{
                            $result[$k]['realmoney']=intval($v['money']/100*2);
                        }
                        
                    }elseif($v['type']==15){
                     $result[$k]['type']=7;//类型为语音视频聊天
                     //分钟数
                     $result[$k]['count']=$v['days'];
                     //增加的魅力值
                     $result[$k]['add_money']=$v['money'];


                 }/*else{
                        $userinfo=$this->get_diy_user_field($v['to_uid']);
                        $result[$k]['type']=$v['type'];//类型为礼物
                        $result[$k]['nickname']=$userinfo['nickname'];
                        //礼物数量
                        $result[$k]['count']=$v['days'];
                        //增加的魅力值
                        $result[$k]['add_money']=$v['money'];
                    }*/
                }
                if($pageNum>ceil($ret["totalCount"]/$pageSize)){
                    $return['data']['list']=array();
                }else{
                $return['data']['list'] = $result;}
                $page["pagesize"] = $pageSize;
                $page["page"] = $pageNum;
                $page["totalcount"] = $ret["totalCount"];
                $return['data']["page"] = $page;
                $return['message'] = $this->L("CHENGGONG");
            } else {
                $return['data'] ='';
                $return['code'] = ERRORCODE_201;
                $return['message'] = $this->L("没有历史提现或礼物信息");
            }
               // $value = json_encode($return, true);
               // $ret[]=$redis->listPush('withdrawal_order'.$uid,$value);
        }
        //else{
            //$result=$redis->listLrange('withdrawal_order'.$uid,0,-1);
           /* if ($result) {
                    $result1= json_decode($result[0], true);
                $return['data']=$result1['data'];
                $return['message']=$result1['message'];
                }*/
      //  }
        Push_data($return);

    }

    //魅力值排行
    public function rankinglist($reset=0){
        $data = $this->Api_recive_date;
        $type=$data['type'];//1:排行榜2：土豪榜
        $res=array();
        if($type==2){
            $return['data']['list'] = $res;
            $return['message'] = $this->L("CHENGGONG");
            Push_data($return);
        }

        $redis = $this->redisconn();
        $reidsname='getranklist-'.$type;
        if($reset==1){
            $redis->del($reidsname);
            return true;
        }
        //首先判断redis中是否存在，不存在则在数据库中取
        $keyexit=$redis->exists($reidsname);
        if($keyexit) {
            $result = $redis->get($reidsname);
            $res = json_decode($result, true);
        }else {
            $PMoneyM = new PMoneyModel();
            $Wdata = array();
           // $Wdata['t_p_money.product'] = $product;
            $Wdata['type'] = array('in', array(1,2,3,12));
            $Wdata['money'] = array('gt', 0);
            $yueArr = $this->getShiJianChuo();
            $Wdata["paytime"] = array(array('egt', $yueArr["begin"]), array('elt', $yueArr["end"]));
            $res=array();
            if ($type == 1) {
                $ret = $PMoneyM->getListGroupOrder($Wdata);
                foreach ($ret as $k => $v) {
                    $res[$k]["user"] = $this->get_user($v["uid"]);
                    $res[$k]["uid"] = $v["uid"];
                    $res[$k]["allmoney"]=round($v["allmoney"]);
                }
                $highmoneyone=$res[0]["allmoney"];
                $highmoneythree=$res[2]["allmoney"];
                $highmoneyfive=$res[4]["allmoney"];
                $users=array('378994','378992','378988');
                $resb=array();
                foreach($users as $k1=>$v1){
                    $resb[$k1]["user"] = $this->get_user($v1);
                    $resb[$k1]["uid"] = $v1;
                    $redis_fakeuid='fakeuid-'.$v1;
                    if($k1==0){
                        if($redis->exists($redis_fakeuid)){
                            $fakemoney=$redis->get($redis_fakeuid);
                            $gap_money=$highmoneyone-$fakemoney;
                            if($gap_money>0){
                                if($gap_money>1000){
                                    $resb[$k1]["allmoney"] =rand($highmoneyone-1000,$highmoneyone+1000);
                                }else{
                                    $resb[$k1]["allmoney"] =rand($fakemoney,$fakemoney+1000);
                                }
                            }else{
                                $resb[$k1]["allmoney"] =rand($fakemoney,$fakemoney+500);
                            }

                        }else{
                            $resb[$k1]["allmoney"] =rand($highmoneyone,$highmoneyone+1000);

                        }
                        $redis->set($redis_fakeuid,$resb[$k1]["allmoney"]);
                    }elseif($k1==1){
                        if($redis->exists($redis_fakeuid)){
                            $fakemoney=$redis->get($redis_fakeuid);
                            $gap_money=$highmoneythree-$fakemoney;
                            if($gap_money>0){
                                if($gap_money>1000){
                                    $resb[$k1]["allmoney"] =rand($highmoneythree-1000,$highmoneythree+1000);
                                }else{
                                    $resb[$k1]["allmoney"] =rand($fakemoney,$fakemoney+1000);
                                }
                            }else{
                                $resb[$k1]["allmoney"] =rand($fakemoney,$fakemoney+500);
                            }

                        }else{
                            $resb[$k1]["allmoney"] =rand($highmoneythree,$highmoneythree+1000);

                        }
                        $redis->set($redis_fakeuid,$resb[$k1]["allmoney"]);
                    }elseif($k1==2){
                        if($redis->exists($redis_fakeuid)){
                            $fakemoney=$redis->get($redis_fakeuid);
                            $gap_money=$highmoneyfive-$fakemoney;
                            if($gap_money>0){
                                if($gap_money>1000){
                                    $resb[$k1]["allmoney"] =rand($highmoneyfive-1000,$highmoneyfive+1000);
                                }else{
                                    $resb[$k1]["allmoney"] =rand($fakemoney,$fakemoney+1000);
                                }
                            }else{
                                $resb[$k1]["allmoney"] =rand($fakemoney,$fakemoney+500);
                            }

                        }else{
                            $resb[$k1]["allmoney"] =rand($highmoneyfive,$highmoneyfive+1000);

                        }
                        $redis->set($redis_fakeuid,$resb[$k1]["allmoney"]);
                    }

                }
                $res=array_merge($resb,$res);
                $date = array_column($res, 'allmoney');
                array_multisort($date,SORT_DESC ,$res);
                $redis_rankinglist=json_encode($res);
                $getres=$redis->set($reidsname,$redis_rankinglist,0,0,60*60*12);
                if(!$getres){
                    return false;
                }
            }

        }

        $return['data']['list'] = $res;
        $return['message'] = $this->L("CHENGGONG");
        Push_data($return);

    }
    protected function getShiJianChuo(){
            $now = time();
            $nian = date("Y",$now);
            $yue =  date("m",$now);

        $time['begin'] = mktime(0,0,0,$yue,1,$nian);
        $time['end'] = mktime(23,59,59,($yue+1),0,$nian);
        return $time;
    }


}
