<?php
/**
 * 支付宝支付
 */
class AlipayController extends BaseController {
    public function __construct(){
        //parent::__construct();

    }
    /**
     * notify_url接收页面
     */
    // 支付异步回调请求
    public function msg(){
        Vendor('alisdk.AopSdk');
        $aop = new \AopClient;
        $alipay_config=C('ALIPAY_CONFIG');
        $aop->alipayrsaPublicKey =$alipay_config['ALIPAYRSAPUBLICKEY'];// 填写支付宝公钥，一行字符串
        $flag = $aop->rsaCheckV1($_POST, NULL, "RSA2");// 验签
        //写入日志
        $this->writeLog(var_export($_POST, true));
        // 判断验签
        if ($flag) {#验签通过
            $trade_status = $_POST['trade_status'];
            $backdata =unserialize(base64_decode($_POST['passback_params']));//公用回传参数
            $backdata['translateid']=$_POST['out_trade_no'];
            $vipgrade = $backdata['vipgrade'];
            unset($backdata['vipgrade']);
            $this->writeLog(var_export($backdata, true));
            // 判断交易状态
            if ($trade_status == 'TRADE_FINISHED' || $trade_status == 'TRADE_SUCCESS') {
                //书写逻辑代码
                $RechargeM = new RechargeModel();
                $payid = $RechargeM->addOne($backdata);
                $UserBaseM = new UserBaseModel();
                if ($backdata['type']==1) {
                    //add money start
                    $MoneyC = new MoneyController();
                   $MoneyC->rechargevip($backdata, $payid);
                    //add money end
                    $resultu = $UserBaseM->updateOne(array("uid" =>$backdata['uid']), array("viptime" =>$backdata['vipgold'],"vipgrade"=>$vipgrade));
                    $this->set_user_field($backdata['uid'],'viptime',$backdata['vipgold']);
                    $this->set_user_field($backdata['uid'],'vipgrade',$backdata['$vipgrade']);
                    //$this->get_user($backdata['uid'], 1);
                } elseif ($backdata['type'] == 2) {
                    //add money start
                    $MoneyC = new MoneyController();
                    $MoneyC->rechargevip($backdata, $payid);
                    //add money end
                    $resultu = $UserBaseM->updateOne(array("uid" => $backdata['touid']), array("viptime" => $backdata['vipgold'],"vipgrade"=>$vipgrade));
                    $this->set_user_field($backdata['touid'],'viptime',$backdata['vipgold']);
                    $this->set_user_field($backdata['touid'],'vipgrade',$backdata['$vipgrade']);
                    //$this->get_user($backdata['touid'], 1);
                } elseif ($backdata['type'] == 3) {
                    //增加金币
                    $resultu = $this->gold_add($backdata['uid'], $backdata['days']);
                    //$this->get_user($backdata['uid'], 1);//更新用户金币字段缓存
                } elseif ($backdata['type'] == 4) {
                    //帮别人充值金币
                    $resultu = $this->gold_add($backdata['touid'], $backdata['days']);
                    //$this->get_user($backdata['touid'], 1);//更新用户金币字段缓存
                }

                if ($resultu) {
                    echo "success";// 告知支付宝支付成功 请勿修改,否则支付宝会一直回调！！！
                } else {
                    $RechargeM = new RechargeModel();
                    $backdata['status']=4;
                    $payid = $RechargeM->addOne($backdata);
                    echo "success";
                }
            }

            } else {
                echo "fail";
            }
    }
    //启动支付
    public function pay($data){
        Vendor('alisdk.AopSdk');//载入sdk类
        $aop = new \AopClient;
        $alipay_config=C('ALIPAY_CONFIG');
        $config=array(
            'appId'=>$alipay_config['APPID'],
            'rsaPrivateKey'=>$alipay_config['RSAPRIVATEKEY'],
            'alipayrsaPublicKey'=>$alipay_config['ALIPAYRSAPUBLICKEY'],
            'notify_url'=>$alipay_config['NOTIFY_URL'],
            'gatewayUrl'=>$alipay_config['GATEWAYURL'],
        );
        //传回支付宝服务器的封装数据(公用回传参数)
        unset($data['translateid']);
        unset($data['purchasetoken']);
        unset($data['packagename']);
        $passback_params =serialize($data);
        $passback_params=base64_encode($passback_params);
        //获取订单价钱
        $price =$data['money']/100;//支付宝单位为元
        //订单号
        $out_trade_no = $data['uid'].mt_rand(10000,99999);
        $aop->gatewayUrl = $config['gatewayUrl'];#此处为真实支付网关,若用沙箱环境记得使用沙箱测试的网关
        $aop->appId = $config['appId'];
        $aop->rsaPrivateKey =$config['rsaPrivateKey'];//请填写开发者私钥去头去尾去回车，一行字符串
        $aop->format = "json";
        $aop->charset = "UTF-8";
        $aop->signType = "RSA2";
        $aop->alipayrsaPublicKey =$config['alipayrsaPublicKey'];//填写支付宝公钥，一行字符串
        //实例化具体API对应的request类
        $request = new \AlipayTradeAppPayRequest();
        //SDK已经封装掉了公共参数，这里只需要传入业务参数
        $order=array(
            "subject"   =>'测试数据',//支付的标题
            "passback_params" => UrlEncode($passback_params),//公用回传参数
            "out_trade_no"  => $out_trade_no,//支付宝订单号必须是唯一的，不能在支付宝再次使用，必须重新生成，哪怕是同一个订单，不能重复。否则二次支付时候会失败，订单号可以在自己订单那里保持一致，但支付宝那里必须要唯一，具体处理自己操作！
            "timeout_express"   => '2m',//过期时间（分钟）
            "product_code"  => "QUICK_MSECURITY_PAY",
            "total_amount"  => $price,//金額最好能要保留小数点后两位数
        );
        $bizcontent = json_encode($order);
        $request->setNotifyUrl($config['notify_url']);//商户外网可以访问的异步回调地址
        $request->setBizContent($bizcontent);
        //这里和普通的接口调用不同，使用的是sdkExecute
        $response = $aop->sdkExecute($request);
        $return['data']=$response;
        //htmlspecialchars是为了输出到页面时防止被浏览器将关键参数html转义，实际打印到日志以及http传输不会有这个问题
        // echo htmlspecialchars($response);//就是orderString 可以直接给客户端请求，无需再做处理。
       // $this->shows(200,'',$response);
        Push_data($return);
    }

    //跳转到手机app支付
    public function webpay()
    {
        parent::__construct();
        Vendor('alisdk.wappay.service.AlipayTradeService');
        Vendor('alisdk.wappay.buildermodel.AlipayTradeWapPayContentBuilder');
        $config = C('ALIPAY_CONFIG');
        //商户订单号，商户网站订单系统中唯一订单号，必填
        //$out_trade_no = $_POST['WIDout_trade_no'];
        if ($_POST['WIDtotal_amount']) {
            $out_trade_no = '27201' . date('YmdHis', time());
            //订单名称，必填
            $subject = $_POST['WIDsubject'];
            //付款金额，必填
            $total_amount = $_POST['WIDtotal_amount'];
            //$total_amount = '0.01';

            //商品描述，可空
            $body = $_POST['WIDbody'];

            //超时时间
            $timeout_express = "1m";

            $payRequestBuilder = new \AlipayTradeWapPayContentBuilder();
            $payRequestBuilder->setBody($body);
            $payRequestBuilder->setSubject($subject);
            $payRequestBuilder->setOutTradeNo($out_trade_no);
            $payRequestBuilder->setTotalAmount($total_amount);
            $payRequestBuilder->setTimeExpress($timeout_express);

            $payResponse = new \AlipayTradeService($config);
            $result = $payResponse->wapPay($payRequestBuilder, $config['RETURN_URL'], $config['WEB_NOTIFY_URL']);
            return;
        } else {
            $this->display();
        }

    }

    //跳转到手机app支付异步回调
    public function notify_url()
    {

        Vendor('alisdk.wappay.service.AlipayTradeService');
        $config = C('ALIPAY_CONFIG');
        $arr = $_POST;
        $alipaySevice = new AlipayTradeService($config);
        $alipaySevice->writeLog(var_export($_POST, true));
        $result = $alipaySevice->check($arr);

        /* 实际验证过程建议商户添加以下校验。
        1、商户需要验证该通知数据中的out_trade_no是否为商户系统中创建的订单号，
        2、判断total_amount是否确实为该订单的实际金额（即商户订单创建时的金额），
        3、校验通知中的seller_id（或者seller_email) 是否为out_trade_no这笔单据的对应的操作方（有的时候，一个商户可能有多个seller_id/seller_email）
        4、验证app_id是否为该商户本身。
        */
        if ($result) {//验证成功
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //请在这里加上商户的业务逻辑程序代
            //——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
            //获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表
            //商户订单号
            $out_trade_no = $_POST['out_trade_no'];
            //支付宝交易号
            $trade_no = $_POST['trade_no'];
            //交易状态
            $trade_status = $_POST['trade_status'];
            if ($_POST['trade_status'] == 'TRADE_FINISHED') {
                $backdata['money']=100;
                $backdata['uid']=1111;
                $RechargeM = new RechargeModel();
                $res =$RechargeM->addOne($backdata);

                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //请务必判断请求时的total_amount与通知时获取的total_fee为一致的
                //如果有做过处理，不执行商户的业务程序

                //注意：
                //退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
            } else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {

                $backdata['money']=100;
                $backdata['uid']=1111;
                $RechargeM = new RechargeModel();
                $res =$RechargeM->addOne($backdata);
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //请务必判断请求时的total_amount与通知时获取的total_fee为一致的
                //如果有做过处理，不执行商户的业务程序
                //注意：
                //付款完成后，支付宝系统发送该交易状态通知
            }

            //——请根据您的业务逻辑来编写程序

            echo "success";        //请不要修改或删除

        } else {
            //验证失败
            echo "fail";    //请不要修改或删除
        }

    }

    function writeLog($text) {
        // $text=iconv("GBK", "UTF-8//IGNORE", $text);
        //$text = characet ( $text );
        file_put_contents ( ALIPAY_CALLBACK."log.txt", date ( "Y-m-d H:i:s" ) . "  " . $text . "\r\n", FILE_APPEND );
    }

}