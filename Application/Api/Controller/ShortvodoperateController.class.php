<?php
require_once WR.'/lib/ali_voduploadsdk/aliyun-php-sdk-core/Config.php';   // 假定您的源码文件和aliyun-php-sdk处于同一目录
use vod\Request\V20170321 as vod;
date_default_timezone_set('PRC');
class ShortvodoperateController extends BaseController {
    const AccessKeyId='LTAI4FpbAAKxm6ppD1babNhg';
    const AccessKeySecret='siF61p3HZ8HdTOcqBnPrxIckfoWXCK';
    const GirlCateId = 20000000390;//视频分类ID 女性用户上传的视频
    const BoyCateId = 20000000389;//视频分类ID 女性用户上传的视频
    public function __construct(){
        parent::__construct();
        //$this->is_login();//检查用户是否登陆

    }

    function initVodClient($accessKeyId=self::AccessKeyId, $accessKeySecret=self::AccessKeySecret) {
        $regionId = 'ap-southeast-1';  // 点播服务接入区域

        $profile = DefaultProfile::getProfile($regionId, $accessKeyId, $accessKeySecret);
        return new DefaultAcsClient($profile);
    }

    /**
     * 获取视频上传地址和凭证
     * @param client 发送请求客户端
     * @return CreateUploadVideoResponse 获取视频上传地址和凭证响应数据
     */
    public function createUploadVideo() {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $UserInfo=$this->UserInfo;
        $CateId = $UserInfo['gender'] == 1 ? self::BoyCateId :self::GirlCateId;
        if(!isset($data['title']) or $data['title'] == ''){
            $return = [
                'code' => 4001,
                'message'  => "请输入标题"
            ];
            Push_data($return);
        }
        if(!isset($data['filename']) or $data['filename'] == ''){
            $return = [
                'code' => 4001,
                'message'  => "filename为空"
            ];
            Push_data($return);
        }
        $title = $data['title'];
        $filename = $data['filename'];
        $description = !empty($data['description']) ? $data['description'] :'';
        $coverurl = !empty($data['coverurl']) ? $data['coverurl'] :'';
        $tags = !empty($data['tags']) ? $data['tags'] :'';



        $client = $this->initVodClient();
        $request = new vod\CreateUploadVideoRequest();
        $request->setTitle($title);
        $request->setFileName($filename);
        $request->setDescription($description);
        $request->setCoverURL($coverurl);
        $request->setCateId($CateId);
        $request->setTags($tags);
        $request->setAcceptFormat('JSON');
        $res = object_array($client->getAcsResponse($request));
        if(isset($res['VideoId']) && $res['VideoId']!==''){
            $vod_info['uid'] = $uid;
            $vod_info['videoid'] = $res['VideoId'];
            $vod_info['title'] = $title;
            $vod_info['filename'] = $filename;
            $vod_info['createtime'] = date("Y-m-d H:i:s");
            $VodInfo = new VodInfoModel();
            $retuid = $VodInfo->addOne($vod_info);
            if(!empty($retuid)){
                Push_data(['data'=>$res]);
            }else{
                $return = [
                    'code' => 4001,
                    'message'  => "数据上传入库失败，请重新上传"
                ];
                Push_data($return);
            }

        }else{
            $return = [
                'code' => 4001,
                'message'  => "上传视频未获得VideoId，请重新上传"
            ];
            Push_data($return);
        }


        //var_dump(($client->getAcsResponse($request)));die;
        //Push_data($res);
        //var_dump( $client->getAcsResponse($request));


    }

    /**
     * 获取图片上传地址和凭证
     * @param client 发送请求客户端
     * @return CreateUploadImageResponse 获取图片上传地址和凭证响应数据
     */
    public function createUploadImage() {
        $data = $this->Api_recive_date;
        $uid = $this->uid;

        $client = $this->initVodClient();
        $imageType = empty($data['imageType'])?'default':'cover';
        $request = new vod\CreateUploadImageRequest();
        $request->setImageType($imageType);
        $request->setAcceptFormat('JSON');
        $res = object_array($client->getAcsResponse($request));
        Push_data(['data'=>$res]);
        //return $client->getAcsResponse($request);
    }


    /**
     * 判断视频是否上传成功接口
     * @param videoid is_success
     */
    public function uploadVideoBack(){
        $data = $this->Api_recive_date;
        $uid = $data['uid'];//$this->uid; //493151;// 平台 25100
        if(!isset($data['uid']) or $data['uid'] == ''){
            $return = [
                'code' => 4001,
                'message'  => "uid不允许为空"
            ];
            Push_data($return);
        }
        if(!isset($data['videoid']) or $data['videoid'] == ''){
            $return = [
                'code' => 4001,
                'message'  => "videoid不允许为空"
            ];
            Push_data($return);
        }
        if(!isset($data['is_success']) or $data['is_success'] == ''){
            $return = [
                'code' => 4001,
                'message'  => "is_success不允许为空"
            ];
            Push_data($return);
        }
        $is_success = $data['is_success'];
        //上传成功 赠送会员给女用户
        if($is_success == 1){
            //对上传成功的视频进行机器审核
            $client = $this->initVodClient();
            $request = new vod\SubmitAIMediaAuditJobRequest();
            // 设置视频ID
            $request->setMediaId($data['videoid']);
            $request->setTemplateId("a856bda56da722b2f1d76847ceefda6d");
            // 返回结果
            $client->getAcsResponse($request);
            //判断是否是女用户
            $User_base = new UserBaseModel();
            $result = $User_base->getOne(array('uid'=>$uid));
            //是女生用户赠送会员
            if(!empty($result) && isset($result['gender']) && $result['gender'] == 2){
                //获取后台赠送会员设置天数
                $VodPrivateSet = new VodPrivateSetModel();
                $vodpri_res = $VodPrivateSet->getOne('give_vip');
                $give_vip = !isset($vodpri_res['give_vip']) || $vodpri_res['give_vip'] == 0 ? 1:$vodpri_res['give_vip'];
                $viptime = 1*24*60*60*$give_vip;
                $rtime=$result["viptime"];
                if($rtime==0||$rtime<time()){
                    $viptime = $viptime+time();
                }else{
                    $viptime = $viptime+$rtime;
                }
                if($result['vipgrade']==0){
                    $User_base->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                    $this->set_user_field($uid,'viptime',$viptime);
                    $this->set_user_field($uid,'vipgrade',1);
                }else{
                    $User_base->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                    $this->set_user_field($uid,'viptime',$viptime);
                }

            }

        }

        $videoid = $data['videoid'];
        $vodInfo = new VodInfoModel();
        $vodInfo->updateOne(['videoid'=>$videoid],['is_success'=>$is_success]);
        Push_data();

    }

    /**
    * 搜索媒资信息
    * @param client 发送请求客户端
    * @return SearchMediaResponse 搜索媒资信息响应数据
    */
    public function searchMedia() {
        $data = $this->Api_recive_date;
        $uid = $this->uid ? $this->uid : 'non' ;

        $CateId = $data['sex'] == 1 ? self::BoyCateId : self::GirlCateId;
        $pageno = empty($data['page'])?1:$data['page'];
        $pagesize = empty($data['pagesize'])?20:$data['pagesize'];
        try {
            $client = $this->initVodClient();
            $request = new vod\SearchMediaRequest();
            $request->setFields("Title,CoverURL");
            $request->setMatch("Status in ('Normal') and CateId=" . $CateId);
            //$request->setMatch("Status in ('Normal','Checking') and CreationTime = ('2018-07-01T08:00:00Z','2018-08-01T08:00:00Z')");
            $request->setPageNo($pageno);
            $request->setPageSize($pagesize);
            $request->setSearchType("video");
            $request->setSortBy("CreationTime:Desc");
            $res = object_array($client->getAcsResponse($request));
        }catch (Exception $e) {
            Push_data();
        }
        //判断是否登录 获取是否点过赞
        $data = array();

        foreach ($res['MediaList'] as $k=>$MediaList){
            if(!$MediaList){
                continue;
            }
            $VodInfo = new VodInfoModel();
            $video = $VodInfo->getOne(array('videoid'=>$MediaList['Video']['VideoId']));
            //获取用户信息
            $User_baseM = new UserBaseModel();
            $baseInfo = $User_baseM->getOne(['uid'=>$video['uid']]);
            if(!$baseInfo){
                continue;
            }
                //查看是否在redis存在 存在即点过赞了
                $redis = $this->redisconn();
                $is_fab = $redis->exists('vod_fabulous_'.$uid."_".$MediaList['Video']['VideoId']);
                $data['data'][$k]['is_fabulous'] = 2;//未点赞
                if(!empty($is_fab)){
                    $data['data'][$k]['is_fabulous'] = 1;//已点赞
                }
                //从redis读取当前视频点赞总数
                $fab_count_num =  $redis->get('vod_fabulous_count_num_'.$MediaList['Video']['VideoId']);


                $data['data'][$k]['fab_count_num'] = $fab_count_num ?$video['fab'] :0;

                //获取是否在线 随机 并保持一致存入redis
                $redis = $this->redisconn();
                $keyexitone=$redis->exists('onlinestatus_25100_'.$video['uid']);
                if(!empty($keyexitone)){
                    $data['data'][$k]['isonline_vod'] = $redis->get('onlinestatus_25100_'.$video['uid']);
                }else{
                    $ran = mt_rand(1,3);
                    $data['data'][$k]['isonline_vod'] = $ran;
                    $redis->set('onlinestatus_25100_'.$video['uid'],$ran,0,0,60*60*24);
                }
                //判断是否主播并且获取通话单价
                $compere_info = M("compere_info");
                $c_info = $compere_info->where(array("uid"=>$video['uid']))->find();



                $data['data'][$k]['uid'] = $video['uid'];//发布者uid
                $data['data'][$k]['nickname'] = isset($baseInfo['nickname'])?$baseInfo['nickname']:'';
                $data['data'][$k]['videoid'] = isset($MediaList['Video']['VideoId'])?$MediaList['Video']['VideoId']:'';
                $data['data'][$k]['isfollow'] = $this->isFollow($video['uid'],$uid);
                //获取用户头像
                $photo = new PhotoModel();
                $photores = $photo->getOne(['uid'=>$video['uid']]);
                $data['data'][$k]['imgurl'] = $photores['url'] ?C("IMAGEURL").$photores['url'] :'';
                $data['data'][$k]['coverurl'] = isset($MediaList['Video']['CoverURL'])?$MediaList['Video']['CoverURL']:'';
                $data['data'][$k]['title'] = isset($MediaList['Video']['Title'])?$MediaList['Video']['Title']:'';
                //获取播放地址

                $vod_info = $this->GetPlayInfo($MediaList['Video']['VideoId']);
                $data['data'][$k]['vod_play'] = isset($vod_info['PlayInfoList']['PlayInfo'][0]['PlayURL']) ? $vod_info['PlayInfoList']['PlayInfo'][0]['PlayURL'] : '';;
                //var_dump($data);die;
        }

        //获取用户信息

        Push_data($data);
    }


    public function GetPlayInfo($VideoId){
        try {
            $client_rs = $this->initVodClient();
            $pay_request = new vod\GetPlayInfoRequest();
            $pay_request->setVideoId($VideoId);
            $pay_request->setAuthTimeout(3600 * 24);
            $pay_request->setAcceptFormat('JSON');
            $vod_info = object_array($client_rs->getAcsResponse($pay_request));
            //$PlayURL = isset($vod_info['PlayInfoList']['PlayInfo'][0]['PlayURL']) ? $vod_info['PlayInfoList']['PlayInfo'][0]['PlayURL'] : '';
            return $vod_info;
        }catch (Exception $e) {
            print $e->getMessage()."\n";
        }
    }

    /*
 *是否关注 isFollow
*/
    public function isFollow($touid,$uid=0,$reset=0){
        $uid = $uid ? $uid : $this->uid;
        if($touid&&$uid){
            //$path = $this->get_userpath($uid, 'cache/follow/');
            $cache_name = 'follow_'.$uid.$touid;
            if($reset==1){
                return S($cache_name,null);
            }

            if(S($cache_name)){
                $cacheValue = S($cache_name);
            }else{
                $UserFollowM = new UserFollowModel();
                $return = $UserFollowM->getOne(array('touid'=>$touid,'uid'=>$uid));

                if($return){
                    $cacheValue = 1;
                }else{
                    $cacheValue = 0;
                }
                S($cache_name,$cacheValue,60*60*24);
            }

            return $cacheValue;
        }else{
            return 0;
        }
    }

    /**
     * 获取别人的短视频
     * @throws ClientException
     * @throws ServerException
     */
    public function getOtherVod(){
        $data = $this->Api_recive_date;
        $uid = $data['otheruid'];

        if(!empty($uid)){
            $vodInfo = new VodInfoModel();
            $where = array('uid'=>$uid,'is_del'=>1);
            $pageno = empty($data['page'])?1:$data['page'];
            $pagesize = empty($data['pagesize'])?20:$data['pagesize'];
            $res = $vodInfo->getListPage($where,$pageno,$pagesize);

            if(!empty($res['list'])){
                $data =array();
                foreach ($res['list'] as $k=>$v){
                    //获取视频播放地址
                    $VideoBase = $this->GetPlayInfo($v['videoid']);
                    $data['data'][$k]['coverurl'] = isset($VideoBase['VideoBase']['CoverURL'])?$VideoBase['VideoBase']['CoverURL']:'';
                    $data['data'][$k]['videoid'] = isset($VideoBase['VideoBase']['VideoId'])?$VideoBase['VideoBase']['VideoId']:'';
                    $data['data'][$k]['title'] = isset($VideoBase['VideoBase']['Title'])?$VideoBase['VideoBase']['Title']:'';
                    $data['data'][$k]['vod_play'] = isset($VideoBase['PlayInfoList']['PlayInfo'][0]['PlayURL'])?$VideoBase['PlayInfoList']['PlayInfo'][0]['PlayURL']:'';
                    //查看是否在redis存在 存在即点过赞了
                    $redis = $this->redisconn();
                    $is_fab = $redis->exists('vod_fabulous_'.$this->uid."_".$v['videoid']);
                    $data['data'][$k]['is_fabulous'] = 2;//未点赞
                    if(!empty($is_fab)){
                        $data['data'][$k]['is_fabulous'] = 1;//已点赞
                    }
                    //获取点赞总条数
                    //从redis读取当前视频点赞总数
                    $redis = $this->redisconn();
                    $fab_count_num =  $redis->get('vod_fabulous_count_num_'.$v['videoid']);
                    $VodInfo = new VodInfoModel();
                    $video = $VodInfo->getOne(array('videoid'=>$v['videoid']));
                    $data['data'][$k]['fab_count_num'] = $fab_count_num ?$video['fab'] :0;

                    //获取用户信息
                    $User_baseM = new UserBaseModel();
                    $baseInfo = $User_baseM->getOne(['uid'=>$uid]);
                    $data['data'][$k]['uid'] = $video['uid'];//发布者uid
                    $data['data'][$k]['nickname'] = isset($baseInfo['nickname'])?$baseInfo['nickname']:'';
                    //获取是否在线 随机 并保持一致存入redis
                    $redis = $this->redisconn();
                    $keyexitone=$redis->exists('onlinestatus_25100_'.$video['uid']);
                    if(!empty($keyexitone)){
                        $data['data'][$k]['isonline_vod'] = $redis->get('onlinestatus_25100_'.$video['uid']);
                    }else{
                        $ran = mt_rand(1,3);
                        $data['data'][$k]['isonline_vod'] = $ran;
                        $redis->set('onlinestatus_25100_'.$video['uid'],$ran,0,0,60*60*24);
                    }

                    //获取用户头像
                    $photo = new PhotoModel();
                    $photores = $photo->getOne(['uid'=>$uid]);
                    $data['data'][$k]['imgurl'] = $photores['url'] ?C("IMAGEURL").$photores['url'] :'';
                    $data['data'][$k]['isfollow'] = $this->isFollow($uid,$this->uid);
                }

            }else{
                Push_data();
            }
            Push_data($data);
        }else{
            $return = [
                'code' => 4002,
                'message'  => "UID不能为空"
            ];
            Push_data($return);
        }


    }





}



?>
