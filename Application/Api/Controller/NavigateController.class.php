<?php
class NavigateController extends BaseController {
	public function __construct(){
		parent::__construct();

	}
	//返回导航栏
	public function index(){
	    $vavigate= M('navigation')->where(array('type'=>1))->select();
	    $product=M('products')->where(array('type'=>1))->getField('product',true);
	    shuffle($product);
	    $res=array();
	    foreach($vavigate as $k=>$v){
	        $res[$k]['name']=$v['name'];
            $res[$k]['type']=4;
            $res[$k]['product']=$product[$k];
        }
        $return['data']=$res;
        Push_data($return);
    }
}

?>