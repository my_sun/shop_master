<?php
class VideoController extends BaseController {
    public function __construct(){
        parent::__construct();
        //$this->is_login();//检查用户是否登陆
        $this->fileurl = '/userdata/renzheng/'.date('Ymd',time())."/";
    }
    /**
     * 上传视频
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request int ltime 视频时长
     * @request file imageurl 视频第一帧图片
     * @return data
     */
    public function upload(){
        $data = $this->Api_recive_date;
        $ltime = $data["ltime"] ? $data["ltime"] : 0;
        $uid =$this->uid;
        $checkVideo=new CheckvideoController();
        $return = array();
        $return['code'] = ERRORCODE_201;
        $return['message'] =  $this->L("SHANGCHUANSHIBAI");
        $saveurl = $this->fileurl.'video/';
        $savePath = WR.$saveurl;

        createDir($savePath);
        if(!empty($_FILES)){
            $uploadList = $this->_upload($savePath);
            if($uploadList){
                $imageurlfile = array();
                $videofile = array();
                foreach($uploadList as $k=>$v){
                    if($v["key"]=="imageurl"){
                        $imageurlfile = $v;
                    }else{
                        $videofile = $v;
                    }
                }
                $VideoM = new VideoModel();
                $Indata = array();
                $Indata['uid'] = $uid;
                $Indata['type'] = $data['type'] ? $data['type'] : 1;//1视频认证，2上传视频
                $Indata['ltime'] = $ltime;//
                $Indata['imageurl'] = $saveurl.$imageurlfile['savename'];//
                $Indata['url'] = $saveurl.$videofile['savename'];
                $Indata['time'] = time();//
                $Indata['memo'] =$data['memo'] ? $data['memo'] : "";

                $checkVideos=$checkVideo->checkvideo2(WR.$Indata['url'],array("porn","terrorism","sface"));
                if($checkVideos->code == 200 & $checkVideos->msg == "OK"){
                    //审核视频语音
                    $videovoice=$checkVideo->videovoice($checkVideos->url);
                    if($videovoice->code == 200 & $videovoice->msg == "OK"){
                        $Indata['status'] = 1;//
                        $ret = $VideoM->addOne($Indata);
                        PutOss(array($Indata['url'],$Indata['imageurl']));
                        $this->get_user_video($uid,1);
                        //更新数据库状态
                        $userinfoM=new UserBaseModel();
                        $userinfoM->updateOne(array('uid'=>$uid),array('isvideo'=>2));
                        //更新缓存
                        $this->set_user_field($uid,'isvideo',2);
                        $return = array();
                        $this->sendsysmsg($uid, "视频已通過審核");
                        $return['message'] = "该视频已通過審核";
                        Push_data($return);
                    }
                }
                $this->sendsysmsg($uid, "该视频未通過審核");
                _unlink(WR.$Indata['url']);
                $return['message'] = "视频內容不合格！";
                Push_data($return);
            }
        }

    }

    protected function _upload($savePath) {
        import("Api.lib.Behavior.fileDirUtil");
        $fileutil = new fileDirUtil();
        $fileutil->createDir($savePath);
        import("Api.lib.Behavior.UploadFile");
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize = 3292200;
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
        //设置附件上传目录
        $upload->savePath = $savePath;
        //设置需要生成缩略图，仅对图像文件有效
        $upload->thumb = false;
        // 设置引用图片类库包路径
        $upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix = 'm_';  //生产2张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth = '400';
        //设置缩略图最大高度
        $upload->thumbMaxHeight = '400';
        //设置上传文件规则
        $upload->saveRule = "md5content";
        //删除原图
        $upload->thumbRemoveOrigin = false;
        if (!$upload->upload()) {
            $return = array();
            $return['code'] = ERRORCODE_501;
            $return['message'] = $upload->getErrorMsg();
            Push_data($return);
            //捕获上传异常
            //$this->error($upload->getErrorMsg());
        } else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo();
            return $uploadList;;
        }

    }
    /**
     * 删除视频
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request int id 视频id
     * @return data
     */
    public function delete(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $id = $data["id"] ? $data["id"] : 0;
        $return = array();
        $return['code'] = ERRORCODE_201;
        $return['message'] = $this->L("CANSHUYICHANG");
        if($id){
            //删除视频
            $VideoM = new VideoModel();
            $VideoInfo = $VideoM->getOne(array("id"=>$id));
            _unlink(WR.$VideoInfo["imageurl"]);
            _unlink(WR.$VideoInfo["url"]);
            DelOss(array($VideoInfo["imageurl"],$VideoInfo["url"]));
            $VideoM->delOne(array("id"=>$id));
            $return = array();
        }
        Push_data($return);
    }

    //我发布的是视频
    public function release()
    {
        $data = $this->Api_recive_date;
        $uid = $data['uid'] ? $data['uid'] :$this->uid;
        $return = array();
        //连接redis
        $redis = new RedisModel();
        //设置key

        $key = 'get_user_video_listss'.$uid;
        $values = $redis->exists($key);
        // dump($values);die;
        if($values){
            $vss = $redis->get($key);
            $ret = json_decode($vss,true);
        }else{
            $UserVideoM = new VideoModel();
            $where = array();
            $where['uid'] = $uid;
            $where['type'] = 2;
            $ret = $UserVideoM->getList($where);

            if($ret){
                foreach ($ret as $k => $v) {
                    $ret[$k]['id']=$v['id'];
                    $ret[$k]['ltime']=$v['ltime'];
                    $ret[$k]['url']=C("IMAGEURL").$v["url"];
                    $ret[$k]['imageurl']=C("IMAGEURL").$v["imageurl"];
                    $ret[$k]['status']=$v["status"];
                }
            }
            $rets = json_encode($ret);
            $redis->setex($key,$rets,$time = 3600);
        }
        $return['code'] = 200;
        $return['message'] = "获取我发布的视频列表成功";
        $return['data'] = $ret;
        Push_data($return);
    }

    //删除我发布的
    public function delrelease()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $id = $data["id"] ? $data["id"] : 0;
        $return = array();
        $return['code'] = ERRORCODE_201;
        $return['message'] = $this->L("CANSHUYICHANG");;
        if($id){
            //删除视频
            $VideoM = new VideoModel();
            $VideoInfo = $VideoM->updateOne(array("id"=>$id),array("type"=>3));
            $redis = new RedisModel();
            $key = 'get_user_video_listss'.$uid;
            // dump($key);die;
            $redis->del($key);
            $return = array();
        }
        $return['code'] = 200;
        $return['message'] = "删除我发布的成功";
        Push_data($return);
    }
    //视频点赞
    public function add_support()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $return = array();
        $return['code'] = ERRORCODE_201;
        $return['message'] = $this->L("CANSHUYICHANG");
        $videoid = $data['videoid']?$data['videoid']:0;
        if($videoid){
            $return = array();
            $addArr = array(
                "videoid"=>$videoid,
                "time"=>time(),
                "uid"=>$uid
            );

            $VideoM = new VideoModel();
            $video = $VideoM->getOne(array('id'=>$videoid));
            $m = M("support");
            $temp = $m->where(array("uid"=>$uid,"videoid"=>$videoid))->find();

            if(!$temp){
                $m->add($addArr);
                // 添加点赞数量
                $VideoM->updateOne(array("id"=>$videoid),array("fabulous"=>$video['fabulous']+1));
            }
            if($data['platforminfo']['product'] == "25100" && $video['uid'] !== $temp['uid']){
                // ------------------------------------------------------
                $User_baseM = new UserBaseModel();
                $where['uid'] = $uid;
                $retu = $User_baseM->getOne($where);

                //消息推送
                $res = $User_baseM->updateOne($where,array('unreadsupport'=>$retu['unreadsupport']+1));
                $retu = $User_baseM->getOne($where);
                $contents = [
                    'push_type' => "unreadsupport",
                    'data' =>  $retu['unreadsupport']
                ];
                $content=json_encode($contents);
                // dump($content);die;
                $argv1 = array();
                $argv1['url'] = "http://127.0.0.1:81/userauth/sendtzmsg/?uid=" . $retu['uid'] . "&content=" . $content ."&from=10005";
                $argv1['time'] =1;
                $argv1 = base64_encode(json_encode($argv1));
                $command = "/usr/bin/php " . WR . "/openurl.php " . $argv1 . " > /dev/null &";
                system($command);
                // ------------------------------------------------------
            }
            //清空点赞缓存
            $redis = new RedisModel();
            $key = 'get_video_support_listss'.$uid;
            // dump($key);die;
            $redis->del($key);
        }
        Push_data($return);
    }
    //取消点赞
    public function delsupport()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $return = array();
        $return['code'] = ERRORCODE_201;
        $return['message'] = $this->L("CANSHUYICHANG");
        $videoid = $data['videoid']?$data['videoid']:0;
        if($videoid){
            $m = M("support");
            $where['uid'] = $uid;
            $where['videoid'] = $videoid;
            $res = $m->where($where)->delete();
            if($res){
                //清空点赞缓存
                $redis = new RedisModel();
                $key = 'get_video_support_listss'.$uid;
                $redis->del($key);

                //视频点赞总数减一
                $VideoM = new VideoModel();
                $video = $VideoM->getOne(array('id'=>$videoid));
                $VideoM->updateOne(array("id"=>$videoid),array("fabulous"=>$video['fabulous']-1));
                $return = array();
                $return['code'] = 200;
                $return['message'] = "取消点赞成功";
            }else{
                $return = array();
                $return['code'] = 202;
                $return['message'] = "取消点赞失败";
            }
        }
        Push_data($return);
    }
    //收获的赞
    public function supportlist()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $return = array();
        $return['code'] = ERRORCODE_201;
        $return['message'] = $this->L("CANSHUYICHANG");
        //连接redis
        $redis = new RedisModel();
        $key = 'get_video_support_listss'.$uid;
        $values = $redis->exists($key);
        $pageNum = $data['pageNum'] ? $data['pageNum'] : 1;
        $pageSize = $data['pageSize'] ? $data['pageSize'] : 15;
        if($values){
            $vss = $redis->get($key);
            $ret = json_decode($vss,true);
        }else{
            import("Extend.Library.ORG.Util.Page");       //导入分页类
            $Page   = new Page($count,$pageSize,$pageNum);
            $db=M('video as a'); //主表
            $res=$db ->join("t_support b on a.id=b.videoid") //附表连主表
            ->where('a.uid='.$uid)
                ->where('a.type=2')
                ->where('a.status=1')
                ->order("b.time desc")
                ->field("*")//需要显示的字段
                ->limit($Page->firstRow. ',' . $Page->listRows)
                ->select();
            $ret = array();
            if($res){
                foreach ($res as $k => $v) {
                    if($v['uid'] != $uid){

                        $ret[$k]['id']=$v['id'];
                        $ret[$k]['time']=date("Y-m-d",$v['time']);
                        $ret[$k]['url']=C("IMAGEURL").$v["url"];
                        $ret[$k]['imageurl']=C("IMAGEURL").$v["imageurl"];

                        $ret[$k]['userinfo'] = $this->get_diy_user_field($v['uid']);

                    }
                }
            }
            $ret['totalcount'] = count($res);
            $ret['pageNum'] = $pageNum;
            $ret['pageSize'] = $pageSize;
            // 清空未读点赞
            $User_baseM = new UserBaseModel();
            $User_baseM->updateOne($where,array('unreadsupport'=>0));
            $rets = json_encode($ret);
            $redis->setex($key,$rets,$time = 5000);
        }
        $return['code'] = 200;
        $return['message'] = "获取收获的赞列表成功";
        $return['data'] = $ret;
        Push_data($return);
    }


}



?>
