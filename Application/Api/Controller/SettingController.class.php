<?php
class SettingController extends BaseController {
	public function __construct(){
		parent::__construct();
	}
	
	/**
	 * 修改用户信息
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request string 用户字段1 上传需要修改的用户字段和值
	 * @request string 用户字段2 上传需要修改的用户字段和值
	 * @request string 用户字段... 上传需要修改的用户字段和值
	 * @return data
	 */
	public function modifyuserinfo(){
		$data = $this->Api_recive_date;

		$userinfo = $this->get_UserInfo($data);
		$product=$this->platforminfo['product'];
		$basedata =$userinfo["base"];
		$extdata = $userinfo["ext"];
		$uid = $this->uid;
		if(!empty($basedata)){
            $nickname_array=array_keys($basedata);
            if(in_array('nickname',$nickname_array)) {
                //检测昵称是否合格
                $checktextC= new ChecktextController();
                $textdata=$checktextC->checktext($basedata['nickname'],array("antispam"));
                if($textdata['antispam']!='pass'){
                    Push_data(array('message'=>$this->L("昵稱內容不合格！"),'code'=>ERRORCODE_201));
                }
            }

			$User_baseM = new UserBaseModel();
			$ret = $User_baseM->updateOne(array('uid'=>$uid),$basedata);
            if($ret){
                foreach ($basedata as $k=>$v){
                    $this->set_user_field($uid,$k,$v);
                }
            }
            //修改昵称的时候，需要重新更新审核状态为未审核。

               /* if(in_array('nickname',$nickname_array)) {
                    $userinfo1 = $this->get_diy_user_field($uid, 'isnickname');
                    if ($userinfo1['isnickname'] != 2) {
                        //更新字段isnickname状态为2,未审核状态
                        $rese['isnickname'] = '2';
                        $ret = $User_baseM->updateOne(array('uid'=>$uid),$rese);
                        $this->set_user_field($uid,'isnickname','2');
                    }
                }*/
            }



		if(!empty($extdata)){

			$User_extendM = new UserExtendModel();
            $mood_array=array_keys($extdata);
            if(in_array('mood',$mood_array)) {
                //检测内心独白是否合格
                $checktextC= new ChecktextController();
                $textdata=$checktextC->checktext($extdata['mood'],array("antispam"));
                if($textdata['antispam']!='pass'){
                    Push_data(array('message'=>$this->L("内心独白內容不合格！"),'code'=>ERRORCODE_201));
                }
            }


			$ret = $User_extendM->updateOne(array('uid'=>$uid),$extdata);
			if($ret){
			    foreach ($extdata as $k=>$v){
                    $this->set_user_field($uid,$k,$v);
                }
            }

			//修改内心告白的时候，女性用户需要重新更新审核状态为未审核，男性用户不审核。
			/*$mood_array=array_keys($extdata);
            if(in_array('mood',$mood_array)) {
                $userinfo1 = $this->get_diy_user_field($uid, 'mood_status_first|gender');
                if($userinfo1['gender'] == 2 ){
                    if ($userinfo1['mood_status_first'] == 1 || $userinfo1['mood_status_first'] == 3) {
                        //$rese['mood_status_first'] = '2';
                        $rese['mood_status_first'] = '1';
                        $User_extendM->updateOne(array('uid' => $uid), $rese);
                        //$this->set_user_field($uid,'mood_status_first','2');
                        $this->set_user_field($uid,'mood_status_first','1');
                    }
                }else{
                    if ($userinfo1['mood_status_first'] != 1){
                        $rese['mood_status_first'] = '1';
                        $User_extendM->updateOne(array('uid' => $uid), $rese);
                        $this->set_user_field($uid,'mood_status_first','1');
                    }

                }

            }*/
            //
            if(!$ret){
                $extinfo = $User_extendM->getOne(array('uid'=>$uid));
                if(!$extinfo){
                    $extdata["uid"] = $uid;
                    $User_extendM->addOne($extdata);
                }
            }
            }



		//$this->get_user($uid,1);//更新用户缓存信息
		//$return["data"]['user'] = $this->get_user($uid);
        $return['message'] = $this->L("SHEZHICHENGGONG");
		Push_data($return);
		
	}
    private function get_UserInfo($data){
        $Userdata = array();
        $baseField = array(
            "nickname"=>"nickname",
            "email"=>"email",
            "gender"=>"gender",
            "age"=>"age",
            "height"=>"height",
            "weight"=>"weight",
            "area"=>"area",
            "income"=>"income",
            "marriage"=>"marriage",
            "education"=>"education",
            "work"=>"work",
            "password"=>"password",
            "product"=>"product",
            "phoneid"=>"phoneid",
            "country"=>"country",
            "language"=>"language",
            "version"=>"version",
            "platformnumber"=>"platformnumber",
            "fid"=>"fid",
            "systemversion"=>"systemversion",
            "birth"=>"birth"
        );
        $baseFieldKey = array_keys($baseField);
        
        $ExtField = array(
            "mood"=>"mood",
            "friendsfor"=>"friendsfor",
            "cohabitation"=>"cohabitation",
            "dateplace"=>"dateplace",
            "lovetimes"=>"lovetimes",
            "charactertype"=>"charactertype",
            "hobby"=>"hobby",
            "wantchild"=>"wantchild",
            "house"=>"house",
            "car"=>"car",
            "line"=>"line",
            "tinder"=>"tinder",
            "twitter"=>"twitter",
            "wechat"=>"wechat",
            "facebook"=>"facebook",
            "phonenumber"=>"phonenumber",
            "channelid"=>"channelid",
            "phonetype"=>"phonetype",
            "blood"=>"blood",
            "isopen"=>"isopen",
            "exoticlove"=>"exoticlove",
            "sexual"=>"sexual",
            "livewithparents"=>"livewithparents",
            "personalitylabel"=>"personalitylabel",
            "liketype"=>"liketype",
            "glamour"=>"glamour",
            "birthday"=>"birthday",
            "longitude"=>"longitude",
            "latitude"=>"latitude",
            "audioprice"=>"audioprice",
            "videoprice"=>"videoprice"

        );
        $ExtFieldKey = array_keys($ExtField);
        
        foreach($data as $k=>$v){
            $str = preg_replace('/($s*$)|(^s*^)/m', '',$v);
            if($str!=""){
                if(in_array($k, $baseFieldKey)){
                    $Userdata["base"][$baseField[$k]] = $str;
                }elseif(in_array($k, $ExtFieldKey)){
                    $Userdata["ext"][$ExtField[$k]] = $str;
                }
            }
        }
        return $Userdata;
    }
}

?>
