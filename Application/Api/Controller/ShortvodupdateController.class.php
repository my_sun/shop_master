<?php
require_once WR.'/lib/ali_voduploadsdk/aliyun-php-sdk-core/Config.php';   // 假定您的源码文件和aliyun-php-sdk处于同一目录
use vod\Request\V20170321 as vod;
date_default_timezone_set('PRC');
class ShortvodupdateController extends BaseController {
    const AccessKeyId='LTAI4FpbAAKxm6ppD1babNhg';
    const AccessKeySecret='siF61p3HZ8HdTOcqBnPrxIckfoWXCK';
    public function __construct(){
        parent::__construct();
        //$this->is_login();//检查用户是否登陆

    }

    function initVodClient($accessKeyId=self::AccessKeyId, $accessKeySecret=self::AccessKeySecret) {
        $regionId = 'cn-shanghai';  // 点播服务接入区域
        $profile = DefaultProfile::getProfile($regionId, $accessKeyId, $accessKeySecret);
        return new DefaultAcsClient($profile);
    }

    /**
     * 获取视频上传地址和凭证
     * @param client 发送请求客户端
     * @return CreateUploadVideoResponse 获取视频上传地址和凭证响应数据
     */
    public function createUploadVideo() {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        if(!isset($data['title']) or $data['title'] == ''){
            $return = [
                'code' => 4001,
                'message'  => "请输入标题"
            ];
            Push_data($return);
        }
        if(!isset($data['filename']) or $data['filename'] == ''){
            $return = [
                'code' => 4001,
                'message'  => "filename为空"
            ];
            Push_data($return);
        }
        $title = $data['title'];
        $filename = $data['filename'];
        $description = !empty($data['description']) ? $data['description'] :'';
        $coverurl = !empty($data['coverurl']) ? $data['coverurl'] :'';
        $tags = !empty($data['tags']) ? $data['tags'] :'';



        $client = $this->initVodClient();
        $request = new vod\CreateUploadVideoRequest();
        $request->setTitle($title);
        $request->setFileName($filename);
        $request->setDescription($description);
        $request->setCoverURL($coverurl);
        $request->setTags($tags);
        $request->setAcceptFormat('JSON');
        $res = object_array($client->getAcsResponse($request));
        if(isset($res['VideoId']) && $res['VideoId']!==''){
            $vod_info['uid'] = $uid;
            $vod_info['videoid'] = $res['VideoId'];
            $vod_info['title'] = $title;
            $vod_info['filename'] = $filename;
            $vod_info['createtime'] = strtotime(date("Y-m-d H:i:s",time()));
            $VodInfo = new VodInfoModel();
            $retuid = $VodInfo->addOne($vod_info);
            if(!empty($retuid)){
                Push_data(['data'=>$res]);
            }else{
                $return = [
                    'code' => 4001,
                    'message'  => "数据上传入库失败，请重新上传"
                ];
                Push_data($return);
            }

        }else{
            $return = [
                'code' => 4001,
                'message'  => "上传视频未获得VideoId，请重新上传"
            ];
            Push_data($return);
        }


        //var_dump(($client->getAcsResponse($request)));die;
        //Push_data($res);
        //var_dump( $client->getAcsResponse($request));


    }

    /**
     * 获取图片上传地址和凭证
     * @param client 发送请求客户端
     * @return CreateUploadImageResponse 获取图片上传地址和凭证响应数据
     */
    public function createUploadImage() {
        $data = $this->Api_recive_date;
        $uid = $this->uid;

        $client = $this->initVodClient();
        $imageType = empty($data['imageType'])?'default':'cover';
        $request = new vod\CreateUploadImageRequest();
        $request->setImageType($imageType);
        $request->setAcceptFormat('JSON');
        $res = object_array($client->getAcsResponse($request));
        Push_data($res);
        //return $client->getAcsResponse($request);
    }


    /**
     * 判断视频是否上传成功接口
     * @param videoid is_success
     */
    public function uploadVideoBack(){
        $data = $this->Api_recive_date;

        if(!isset($data['videoid']) or $data['videoid'] == ''){
            $return = [
                'code' => 4001,
                'message'  => "videoid不允许为空"
            ];
            Push_data($return);
        }
        if(!isset($data['is_success']) or $data['is_success'] == ''){
            $return = [
                'code' => 4001,
                'message'  => "is_success不允许为空"
            ];
            Push_data($return);
        }
        $is_success = $data['is_success'];
        $videoid = $data['videoid'];
        $vodInfo = new VodInfoModel();
        $vodInfo->updateOne(['videoid'=>$videoid],['is_success'=>$is_success]);

    }








}



?>