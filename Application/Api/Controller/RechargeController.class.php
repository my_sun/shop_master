<?php
use Think\Controller;
use Omnipay\Omnipay;
class RechargeController extends BaseController {
    private $appId;
    private $appKey;
	public function __construct(){
		parent::__construct();
        $this->appId = 'LiMiDa';
        $this->appKey = 'HenanLimidaWangluokejigongsi';
		//write_logs();
	}
	/**
	 * 安卓充值
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int type 1会员充值 2帮别人充值VIP3金币充值 4帮别人充值金币
	 * @request int isauto 是否订阅充值,1是订阅0是非订阅Type为1时必填,
	 * @request int count Type为1,2时是充值天数Type为3,4时是充值金币数量
	 * @request int money 用户充值多少钱
	 * @request int touid 帮别人充值,如果是自己充值,这个字段不上传
	 * @request int translateid 支付订单号
	 * @request int productid productid
	 * @request int purchasetoken purchasetoken
	 * @request int packagename packagename
	 * @return data
	 */
	public function recharge(){
		$data = $this->Api_recive_date;
		$uid = $this->uid?$this->uid:$data['uid'];
        $touid = $data['touid'] ? $data['touid'] : "";
		$userinfo = $this->uid?$this->UserInfo:$this->get_user($uid);
		$touserinfo=$this->get_user($touid);
		$product = $this->uid?$userinfo["product"]:$data['product'];
		$paytime = time();
		$vipgrade = $data['vipgrade'] ? $data['vipgrade'] : 1;//会员等级

		$type = $data['type'] ? $data['type'] : 1;
		$status=$data['status'] ? $data['status'] : 1;//充值类型：1：谷歌支付2：微信支付3：支付宝支付
		$isauto = $data['isauto'] ? $data['isauto'] : 0;
		$count = $data['count'] ? $data['count'] : "0";//充值天数或金币数量
		$money = $data['money'] ? $data['money'] : "0";
		$translateid = $data['translateid'] ? $data['translateid']: "translateid";
		$productid = $data['productid'] ? $data['productid'] : "productid";
		$purchasetoken = $data['purchasetoken'] ? $data['purchasetoken'] : "purchasetoken";
		$packagename = $data['packagename'] ? $data['packagename'] : "packagename";
		$RechargeM = new RechargeModel();
		$InData = array();
		$InData['uid'] = $uid;
		$InData['days'] = $count;
		$InData['translateid'] = $translateid;
		$InData['paytime'] = $paytime;//服务器时间
		$InData['type'] = $type;//1:用户自己充值2：用户帮陪聊用户充值3钻石充值4钻石帮陪聊充值
		$InData['to_uid'] = $touid;
		$InData['money'] = intval($money*100);//充值金额计算
		$InData['productid'] = $productid;
		$InData['purchasetoken'] = $purchasetoken;
		$InData['packagename'] = $packagename;
		$InData['isauto'] = $isauto;
		$InData['product'] = $product;
        $InData['status'] = $status;
        $Pushdata = array();
        
        if($status==1){
            //google 订单验证
            $allproduct=array();
            $orderdata=M('order_switch')->select();
            foreach($orderdata as $k1=>$v1){
                $allproduct[$k1]=$v1['product'];
            }
            if(in_array($product,$allproduct)){
                foreach($orderdata as $k=>$v){
                    if($v['product']==$product){
                        if($v['isopenin']==1){
                            $ores = PayStatus($packagename,$productid,$purchasetoken,$product,$isauto);
                            if(is_array($ores)){
                                $checkpay=true;
                            }else{
                                $checkpay=false;
                            }
                        }else{
                            $checkpay=true;
                        }
                        break;
                    }

                }
            }else{
                $ores = PayStatus($packagename,$productid,$purchasetoken,$product,$isauto);
                if(is_array($ores)){
                    $checkpay=true;
                }else{
                    $checkpay=false;
                }
            }
        }else{
            $checkpay=true;
        }

        if($checkpay){
		    if($type==1){
		        $viptime = $count*24*60*60;
		        $rtime=$this->UserInfo["viptime"];
		        if($rtime==0||$rtime<time()){
		            $viptime = $viptime+time();
		        }else{
		            $viptime = $viptime+$rtime;
		        }
		       //$resultr=$RechargeM->where('id='.$payid)->setField('vipgold',$viptime);
		        $InData['vipgold'] = $viptime;
		    }elseif($type==2){
		        $viptime = $count*24*60*60;
		        $rtime=$touserinfo["viptime"];
		        if($rtime==0){
		            $viptime = $viptime +time();
		        }else{
		            $viptime = $viptime+$rtime;
		        }
		        //$resultr=$RechargeM->where('id='.$payid)->setField('vipgold',$viptime);
		        $InData['vipgold'] = $viptime;
		    }elseif($type==3){
		        $rgold=$this->UserInfo["gold"];
		        $InData['vipgold']=$rgold+$count;
		    }elseif($type==4){
		        $rgold=$touserinfo["gold"];
		        $InData['vipgold']=$rgold+$count;
		    }
		    //支付宝支付
            if($status==3){
                $InData['vipgrade']=$vipgrade;
		        $alipayM=new AlipayController();
		        $alipayM->pay($InData);
            }
            //微信支付
            if($status==2){
		       $wecharpayM=new WeixinpayController();
		       $wecharpayM->pay($InData);
            }
				$payid =$RechargeM->addOne($InData);
            //添加首页充值轮播图
            if($type==1 || $type==3){
                $userregistlist = "userregistlist-" . $product;
                $redis = $this->redisconn();
                $redis->listRemove($userregistlist, $uid.'-2', 0);
                $redis->listPush($userregistlist, $uid.'-2');
                $totalCount = $redis->listSize($userregistlist);
                if ($totalCount > 100) {
                    $redis->listPop($userregistlist, 1);
                }
            }
            if($type==2||$type==4){
                if($touid!=''){
                    $userregistlist = "userregistlist-" . $product;
                    $redis = $this->redisconn();
                    $redis->listRemove($userregistlist, $touid.'-2', 0);
                    $redis->listPush($userregistlist, $touid.'-2');
                    $totalCount = $redis->listSize($userregistlist);
                    if ($totalCount > 100) {
                        $redis->listPop($userregistlist, 1);
                    }
                }
            }

            //添加完毕
                $UserBaseM = new UserBaseModel();
                if($type==1){
                    //add money start
                    $MoneyC = new MoneyController();
                    $MoneyC->rechargevip($InData,$payid);
                    //add money end
                    if($userinfo['vipgrade']!=0 & $userinfo['vipgrade']>$vipgrade){
                        $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                        $this->set_user_field($uid,'viptime',$viptime);
                    }else{
                        $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>$vipgrade));
                        $this->set_user_field($uid,'viptime',$viptime);
                        $this->set_user_field($uid,'vipgrade',$vipgrade);
                    }

                    //检查该用户充值记录是否在意向表里边存在
                    $data=M('user_yixiang')->where(array('uid'=>$uid))->find();
                    //若存在则删除
                    if($data) {
                        $del = M('user_yixiang')->where(array('uid' => $uid))->delete();
                    }
                    if($resultu!== false){
                        $Pushdata['message'] = $this->L("充值vip天数成功");
                    }else{
                        $Pushdata['code'] = ERRORCODE_201;
                        $Pushdata['message'] = $this->L("充值vip天数不成功");
                    }
                    $this->get_user($uid,1);
                }
                //给别人充值
             elseif($type==2){
                 //add money start
                 $MoneyC = new MoneyController();
                 $MoneyC->rechargevip($InData,$payid);
                 //add money end
                 $resultu=$UserBaseM->updateOne(array("uid"=>$touid), array("viptime"=>$viptime,"vipgrade"=>$vipgrade));
                    $this->set_user_field($touid,'viptime',$viptime);
                 $this->set_user_field($touid,'vipgrade',$vipgrade);
                 if($resultu){
                        $Pushdata['message'] = $this->L("给他人充值vip天数成功");
                    }else{
                        $Pushdata['code'] = ERRORCODE_201;
                        $Pushdata['message'] = $this->L("给他人充值vip天数不成功");
                    }
                    $this->get_user($touid,1);
            }
                elseif($type==3){
                    //增加金币
                    $result=$this->gold_add($uid, $count);
                    $this->set_user_field($uid,"gold",$userinfo["gold"]+$count);//更新用户金币字段缓存
                    //检查该用户充值记录是否在意向表里边存在
                    $data=M('user_yixiang')->where(array('uid'=>$uid))->find();
                    //若存在则删除
                    if($data) {
                        $del = M('user_yixiang')->where(array('uid' => $uid))->delete();
                    }
                    if($result){
                        //会员赠送  充值钻石送会员 300赠送1天 500钻石赠送1天会员   1000赠送3天  2000 赠送7天 5000赠送20天  10000 赠送50天  12000赠送 60天（首次充值钻石不赠送，非首次都赠送）
                        /*if($userinfo['isfirstrecharge']==1){
                            if($count<=500){
                                $viptime = 1*24*60*60;
                                $rtime=$userinfo["viptime"];
                                if($rtime==0||$rtime<time()){
                                    $viptime = $viptime+time();
                                }else{
                                    $viptime = $viptime+$rtime;
                                }
                                if($userinfo['vipgrade']==0){
                                   $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                                    $this->set_user_field($uid,'viptime',$viptime);
                                    $this->set_user_field($uid,'vipgrade',1);
                                }else{
                                    $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                                    $this->set_user_field($uid,'viptime',$viptime);
                                }

                            }elseif($count<=1000 & $count>500){
                                $viptime = 3*24*60*60;
                                $rtime=$userinfo["viptime"];
                                if($rtime==0||$rtime<time()){
                                    $viptime = $viptime+time();
                                }else{
                                    $viptime = $viptime+$rtime;
                                }
                                if($userinfo['vipgrade']==0){
                                    $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                                    $this->set_user_field($uid,'viptime',$viptime);
                                    $this->set_user_field($uid,'vipgrade',1);
                                }else{
                                    $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                                    $this->set_user_field($uid,'viptime',$viptime);
                                }


                            }elseif($count<=2000 & $count>1000){
                                $viptime = 7*24*60*60;
                                $rtime=$userinfo["viptime"];
                                if($rtime==0||$rtime<time()){
                                    $viptime = $viptime+time();
                                }else{
                                    $viptime = $viptime+$rtime;
                                }
                                if($userinfo['vipgrade']==0){
                                    $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                                    $this->set_user_field($uid,'viptime',$viptime);
                                    $this->set_user_field($uid,'vipgrade',1);
                                }else{
                                    $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                                    $this->set_user_field($uid,'viptime',$viptime);
                                }

                            }elseif($count<=5000 & $count>2000){
                                $viptime = 20*24*60*60;
                                $rtime=$userinfo["viptime"];
                                if($rtime==0||$rtime<time()){
                                    $viptime = $viptime+time();
                                }else{
                                    $viptime = $viptime+$rtime;
                                }
                                if($userinfo['vipgrade']==0){
                                    $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                                    $this->set_user_field($uid,'viptime',$viptime);
                                    $this->set_user_field($uid,'vipgrade',1);
                                }else{
                                    $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                                    $this->set_user_field($uid,'viptime',$viptime);
                                }


                            }elseif($count<=10000 & $count>5000){
                                $viptime = 50*24*60*60;
                                $rtime=$userinfo["viptime"];
                                if($rtime==0||$rtime<time()){
                                    $viptime = $viptime+time();
                                }else{
                                    $viptime = $viptime+$rtime;
                                }
                                if($userinfo['vipgrade']==0){
                                    $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                                    $this->set_user_field($uid,'viptime',$viptime);
                                    $this->set_user_field($uid,'vipgrade',1);
                                }else{
                                    $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                                    $this->set_user_field($uid,'viptime',$viptime);
                                }


                            }elseif($count<=12000 & $count>10000){
                                $viptime = 60*24*60*60;
                                $rtime=$userinfo["viptime"];
                                if($rtime==0||$rtime<time()){
                                    $viptime = $viptime+time();
                                }else{
                                    $viptime = $viptime+$rtime;
                                }
                                if($userinfo['vipgrade']==0){
                                    $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                                    $this->set_user_field($uid,'viptime',$viptime);
                                    $this->set_user_field($uid,'vipgrade',1);
                                }else{
                                    $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                                    $this->set_user_field($uid,'viptime',$viptime);
                                }


                            }

                        }else{
                            $this->set_user_field($uid,"isfirstrecharge",1);
                            M('user_extend')->where(array('uid' => $uid))->setField('isfirstrecharge',1);
                        }*/
                        $Pushdata['message'] = $this->L("充值金币成功");
                    }else{
                        $Pushdata['code'] = ERRORCODE_201;
                        $Pushdata['message'] = $this->L("充值金币不成功,count字段需传值");
                    }
                    $this->get_user($uid,1);
                }
                elseif($type==4){
                    //帮别人充值金币
                    $result=$this->gold_add($touid, $count);
                    $this->set_user_field($touid,"gold",$touserinfo["gold"]+$count);//更新用户金币字段缓存
                    if($result){

                        $Pushdata['message'] = $this->L("给他人充值金币成功");
                    }else{
                        $Pushdata['code'] = ERRORCODE_201;
                        $Pushdata['message'] = $this->L("给他人充值金币不成功,count需传值或touid需传值并正确");
                    }
                    $this->get_user($touid,1);
                }
				
		}else{
				//if(strstr($data['translateId'], "GPA.")){
					$RechargeDelM = new RechargeDelModel();
					$RechargeDelM->addOne($InData);

                    $Pushdata['code'] = ERRORCODE_201;
                    $Pushdata['message'] = $this->L("谷歌订单验证失败");
				//}

		}
		Push_data($Pushdata);
	}
	
	/**
	 * ios 充值
	 * @desc productid字段说明
	 *  jiaoyou.类型.数量.金额*100.vip等级
	 *  jiaoyou.vipf.30.2000.1  非订阅会员
        jiaoyou.vip.30.2000.1  订阅会员
        jiaoyou.diamonds.300.3000 钻石充值
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request string productid jiaoyou.类型.数量.金额*100.vip等级
	 * @request string touid 帮别人充值,如果是自己充值,这个字段不上传
	 * @request string translateid 支付订单号
	 * @request string purchasetoken receipt-data
	 * @request int issandbox 1是沙箱测试环境，默认0不是
	 * @return data
	 */
	public function rechargeios(){
	    $data = $this->Api_recive_date;
	    $uid = $this->uid;
	    $touid = $data['touid'] ? $data['touid'] : "";
	    $userinfo = $this->UserInfo;
	    $touserinfo=$this->get_user($touid);
	    $product = $userinfo["product"];
	    $paytime = time();
	    $productid = $data['productid'] ? $data['productid'] : "0";
	    $issandbox = $data['issandbox'] ? $data['issandbox'] : "0";
	    $productidArr = explode(".", $productid);
	    
	    if(count($productidArr)<4){
	        $Pushdata['code'] = ERRORCODE_201;
	        $Pushdata['message'] = "productid error";
	        Push_data($Pushdata);
	    }
	    if($productidArr[1]=="vip"){
	        $isauto=1;
	        if($touid){
	            $type=2;
	        }else{
	            $type=1;
	        }
	    }elseif($productidArr[1]=="vipf"){
	        $isauto=0;
	        if($touid){
	            $type=2;
	        }else{
	            $type=1;
	        }
	    }elseif($productidArr[1]=="diamonds"){
	        $isauto=0;
	        if($touid){
	            $type=4;
	        }else{
	            $type=3;
	        }
	    }else{
	        $Pushdata['code'] = ERRORCODE_201;
	        $Pushdata['message'] = "productid error";
	        Push_data($Pushdata);
	    }
	    $vipgrade =$productidArr[4] ? $productidArr[4] : 0;//会员等级
	    $count = $productidArr[2];//充值天数或金币数量
	    $money = $productidArr[3];
	    $translateid = $data['translateid'] ? $data['translateid']: "translateid";
	    $purchasetoken = $data['purchasetoken'] ? $data['purchasetoken'] : "purchasetoken";
	   
	    $InData = array();
	    $InData['uid'] = $uid;
	    $InData['days'] = $count;
	    $InData['translateid'] = $translateid;
	    $InData['paytime'] = $paytime;//服务器时间
	    $InData['type'] = $type;//1:用户自己充值2：用户帮陪聊用户充值3钻石充值4钻石帮陪聊充值
	    $InData['to_uid'] = $touid;
	    $InData['money'] = intval($money);//充值金额计算
	    $InData['productid'] = $productid;
	    $InData['purchasetoken'] = $purchasetoken;
	    $InData['isauto'] = $isauto;
	    $InData['product'] = $product;
	    $Pushdata = array();
	    $ores = $this->checkIosorder($InData,$issandbox,$product);
	    if(is_array($ores)){
	        $Pushdata= $this->_addpay($InData,$userinfo,$touserinfo,$vipgrade);
	    }else{
	        $Pushdata['code'] = ERRORCODE_201;
	        $Pushdata['message'] = "验证失败";
	        //if(strstr($data['translateId'], "GPA.")){
	            $RechargeDelM = new RechargeDelModel();
	            $RechargeDelM->addOne($InData);
	       // }
	    }
	    Push_data($Pushdata);
	}
	
	protected function checkIosorder($InData,$issandbox,$product){
	    $receipt = $InData["purchasetoken"];
	    //echo $receipt;
	    //$username = addslashes($_REQUEST['username']);//用户名
	    $tc =  $InData["productid"];//套餐类型
	    //$tc = "com.wbkj.jiaoyou.vip230";
	    
	    /**
	     * 21000 App Store不能读取你提供的JSON对象
	     * 21002 receipt-data域的数据有问题
	     * 21003 receipt无法通过验证
	     * 21004 提供的shared secret不匹配你账号中的shared secret
	     * 21005 receipt服务器当前不可用
	     * 21006 receipt合法，但是订阅已过期。服务器接收到这个状态码时，receipt数据仍然会解码并一起发送
	     * 21007 receipt是Sandbox receipt，但却发送至生产系统的验证服务
	     * 21008 receipt是生产receipt，但却发送至Sandbox环境的验证服务
	     */
	    function acurl($receipt_data, $sandbox=0,$product){
	        //如果是沙盒模式，请求苹果测试服务器,反之，请求苹果正式的服务器
	        if ($sandbox==1) {
	            $endpoint = 'https://sandbox.itunes.apple.com/verifyReceipt';
	        }
	        else {
	            $endpoint = 'https://buy.itunes.apple.com/verifyReceipt';
	        }
	        if($product==20210){
                $postData = json_encode(
                    array('receipt-data' => $receipt_data,"password"=>"799a76ea26fd4ebf87051d946ce04e84")
                );
            }elseif($product==20211){
                $postData = json_encode(
                    array('receipt-data' => $receipt_data,"password"=>"6e05fa19d462406d897b96536c1709bc")
                );
            }elseif($product==21210){
                $postData = json_encode(
                    array('receipt-data' => $receipt_data,"password"=>"0ed4161d24984d309bc5b6a3202eb6e5")
                );
            }else{
                $postData = json_encode(
                    array('receipt-data' => $receipt_data,"password"=>"799a76ea26fd4ebf87051d946ce04e84")
                );
            }

	        $ch = curl_init($endpoint);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
	        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);  //这两行一定要加，不加会报SSL 错误
	        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
	        
	        
	        $response = curl_exec($ch);
	        $errno    = curl_errno($ch);
	        $errmsg   = curl_error($ch);
	        curl_close($ch);
	        $res["response"] = $response;
	        $res["errno"] = $errno;
	        $res["errmsg"] = $errmsg;
	        return $res;
	    }
	    
	    // 请求验证
	    $html = acurl($receipt,'',$product);
	    
	    $data = json_decode($html["response"]);
	    
	    // 如果是沙盒数据 则验证沙盒模式
	    if($data->status=='21007'){
	        // 请求验证
	        $html = acurl($receipt, 1,$product);
	        $data = json_decode($html["response"]);
	        $data->sandbox = '1';
	    }
	    
	    //判断时候出错，抛出异常
	    if ($html["errno"] != 0) {
	        return 0;
	    }
	    //判断返回的数据是否是对象
	    if (!is_object($data)) {
	        return 0;
	    }
	    
	    //判断购买时候成功
	    if (!isset($data->status) || $data->status != 0) {
	        //Dump($data);
	        return 0;
	    }
	    
	    $order = $data->receipt->in_app;//所有的订单的信息
	    $k = count($order) -1;
	    $need = object_array($order[$k]);//需要的那个订单
	    
	    return $need;
	}
	protected function _addpay($InData,$userinfo,$touserinfo,$vipgrade){
	    $type = $InData["type"];
	    $count = $InData["days"];
	    $uid = $userinfo["uid"];
        $product = $userinfo["product"];
	    //$touid = $touserinfo["uid"];
	    $RechargeM = new RechargeModel();
	    //////
        if($type==1){
            $viptime = $count*24*60*60;
            $rtime=$userinfo["viptime"];
            if($rtime==0||$rtime<time()){
                $viptime = $viptime+time();
            }else{
                $viptime = $viptime+$rtime;
            }
            $InData['vipgold'] = $viptime;
        }elseif($type==2){
            $viptime = $count*24*60*60;
            $rtime=$touserinfo["viptime"];
            if($rtime==0){
                $viptime = $viptime +time();
            }else{
                $viptime = $viptime+$rtime;
            }
            $InData['vipgold'] = $viptime;
        }elseif($type==3){
            $rgold=$userinfo["gold"];
            $InData['vipgold']=$rgold+$count;
        }elseif($type==4){
            $rgold=$touserinfo["gold"];
            $InData['vipgold']=$rgold+$count;
        }
        ///
	    $payid =$RechargeM->addOne($InData);
        //添加首页充值轮播图
        if($type==1 || $type==3){
            $userregistlist = "userregistlist-" . $product;
            $redis = $this->redisconn();
            $redis->listRemove($userregistlist, $uid.'-2', 0);
            $redis->listPush($userregistlist, $uid.'-2');
            $totalCount = $redis->listSize($userregistlist);
            if ($totalCount > 50) {
                $redis->listPop($userregistlist, 1);
            }
        }

        //添加完毕
	    $UserBaseM = new UserBaseModel();
	    ///
        if($type==1){
            //add money start
            $MoneyC = new MoneyController();
            $MoneyC->rechargevip($InData,$payid);
            //add money end
            $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>$vipgrade));
            $this->set_user_field($uid,'viptime',$viptime);
            $this->set_user_field($uid,'vipgrade',$vipgrade);
            //检查该用户充值记录是否在意向表里边存在
            $data=M('user_yixiang')->where(array('uid'=>$uid))->find();
            //若存在则删除
            if($data) {
                $del = M('user_yixiang')->where(array('uid' => $uid))->delete();
            }
            if($resultu!== false){
                $Pushdata['code']='200';
                $Pushdata['message'] = $this->L("充值vip天数成功");
            }else{
                $Pushdata['code'] = ERRORCODE_201;
                $Pushdata['message'] = $this->L("充值vip天数不成功");
            }
            $this->get_user($uid,1);
        }
        elseif($type==3){
            //增加金币
            $result=$this->gold_add($uid, $count);
            $this->set_user_field($uid,"gold",$userinfo["gold"]+$count);//更新用户金币字段缓存
            //检查该用户充值记录是否在意向表里边存在
            $data=M('user_yixiang')->where(array('uid'=>$uid))->find();
            //若存在则删除
            if($data) {
                $del = M('user_yixiang')->where(array('uid' => $uid))->delete();
            }
            if($result){
                $Pushdata['code']='200';
                $Pushdata['message'] = $this->L("充值金币成功");
            }else{
                $Pushdata['code'] = ERRORCODE_201;
                $Pushdata['message'] = $this->L("充值金币不成功,count字段需传值");
            }

             $this->get_user($uid,1);
        }
        /// ///
	    return $Pushdata;
	}
    protected function mycard_addpay($InData,$userinfo,$touserinfo,$vipgrade,$payid){
        $type = $InData["type"];
        $count = $InData["days"];
        $uid = $userinfo["uid"];
        $product = $userinfo["product"];
        //$touid = $touserinfo["uid"];
        //$RechargeM = new RechargeModel();
        //////
        if($type==1){
            $viptime = $count*24*60*60;
            $rtime=$userinfo["viptime"];
            if($rtime==0||$rtime<time()){
                $viptime = $viptime+time();
            }else{
                $viptime = $viptime+$rtime;
            }
            $InData['vipgold'] = $viptime;
        }elseif($type==2){
            $viptime = $count*24*60*60;
            $rtime=$touserinfo["viptime"];
            if($rtime==0){
                $viptime = $viptime +time();
            }else{
                $viptime = $viptime+$rtime;
            }
            $InData['vipgold'] = $viptime;
        }elseif($type==3){
            $rgold=$userinfo["gold"];
            $InData['vipgold']=$rgold+$count;
        }elseif($type==4){
            $rgold=$touserinfo["gold"];
            $InData['vipgold']=$rgold+$count;
        }
        //添加首页充值轮播图
        if($type==1 || $type==3){
            $userregistlist = "userregistlist-" . $product;
            $redis = $this->redisconn();
            $redis->listRemove($userregistlist, $uid.'-2', 0);
            $redis->listPush($userregistlist, $uid.'-2');
            $totalCount = $redis->listSize($userregistlist);
            if ($totalCount > 50) {
                $redis->listPop($userregistlist, 1);
            }
        }

        //添加完毕
        $UserBaseM = new UserBaseModel();
        ///
        if($type==1){
            //add money start
            $MoneyC = new MoneyController();
            $MoneyC->rechargevip($InData,$payid);
            //add money end
            if($userinfo['vipgrade']!=0 & $userinfo['vipgrade']>$vipgrade){
                $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                $this->set_user_field($uid,'viptime',$viptime);
            }else{
                $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>$vipgrade));
                $this->set_user_field($uid,'viptime',$viptime);
                $this->set_user_field($uid,'vipgrade',$vipgrade);
            }
            /*$resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>$vipgrade));
            $this->set_user_field($uid,'viptime',$viptime);
            $this->set_user_field($uid,'vipgrade',$vipgrade);*/
            //检查该用户充值记录是否在意向表里边存在
            $data=M('user_yixiang')->where(array('uid'=>$uid))->find();
            //若存在则删除
            if($data) {
                $del = M('user_yixiang')->where(array('uid' => $uid))->delete();
            }
            if($resultu!== false){
                $Pushdata['code']='200';
                $Pushdata['message'] = $this->L("充值vip天数成功");
            }else{
                $Pushdata['code'] = ERRORCODE_201;
                $Pushdata['message'] = $this->L("充值vip天数不成功");
            }
            $this->get_user($uid,1);
        }
        elseif($type==3){
            //增加金币
            $result=$this->gold_add($uid, $count);
            $this->set_user_field($uid,"gold",$userinfo["gold"]+$count);//更新用户金币字段缓存
            //检查该用户充值记录是否在意向表里边存在
            $data=M('user_yixiang')->where(array('uid'=>$uid))->find();
            //若存在则删除
            if($data) {
                $del = M('user_yixiang')->where(array('uid' => $uid))->delete();
            }
            if($result){
//会员赠送  充值钻石送会员 300赠送1天 500钻石赠送1天会员   1000赠送3天  2000 赠送7天 5000赠送20天  10000 赠送50天  12000赠送 60天（首次充值钻石不赠送，非首次都赠送）
                if($userinfo['isfirstrecharge']==1){
                    if($count<=500){
                        $viptime = 1*24*60*60;
                        $rtime=$userinfo["viptime"];
                        if($rtime==0||$rtime<time()){
                            $viptime = $viptime+time();
                        }else{
                            $viptime = $viptime+$rtime;
                        }
                        if($userinfo['vipgrade']==0){
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                            $this->set_user_field($uid,'viptime',$viptime);
                            $this->set_user_field($uid,'vipgrade',1);
                        }else{
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                            $this->set_user_field($uid,'viptime',$viptime);
                        }

                    }elseif($count<=1000 & $count>500){
                        $viptime = 3*24*60*60;
                        $rtime=$userinfo["viptime"];
                        if($rtime==0||$rtime<time()){
                            $viptime = $viptime+time();
                        }else{
                            $viptime = $viptime+$rtime;
                        }
                        if($userinfo['vipgrade']==0){
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                            $this->set_user_field($uid,'viptime',$viptime);
                            $this->set_user_field($uid,'vipgrade',1);
                        }else{
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                            $this->set_user_field($uid,'viptime',$viptime);
                        }


                    }elseif($count<=2000 & $count>1000){
                        $viptime = 7*24*60*60;
                        $rtime=$userinfo["viptime"];
                        if($rtime==0||$rtime<time()){
                            $viptime = $viptime+time();
                        }else{
                            $viptime = $viptime+$rtime;
                        }
                        if($userinfo['vipgrade']==0){
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                            $this->set_user_field($uid,'viptime',$viptime);
                            $this->set_user_field($uid,'vipgrade',1);
                        }else{
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                            $this->set_user_field($uid,'viptime',$viptime);
                        }

                    }elseif($count<=5000 & $count>2000){
                        $viptime = 20*24*60*60;
                        $rtime=$userinfo["viptime"];
                        if($rtime==0||$rtime<time()){
                            $viptime = $viptime+time();
                        }else{
                            $viptime = $viptime+$rtime;
                        }
                        if($userinfo['vipgrade']==0){
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                            $this->set_user_field($uid,'viptime',$viptime);
                            $this->set_user_field($uid,'vipgrade',1);
                        }else{
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                            $this->set_user_field($uid,'viptime',$viptime);
                        }


                    }elseif($count<=10000 & $count>5000){
                        $viptime = 50*24*60*60;
                        $rtime=$userinfo["viptime"];
                        if($rtime==0||$rtime<time()){
                            $viptime = $viptime+time();
                        }else{
                            $viptime = $viptime+$rtime;
                        }
                        if($userinfo['vipgrade']==0){
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                            $this->set_user_field($uid,'viptime',$viptime);
                            $this->set_user_field($uid,'vipgrade',1);
                        }else{
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                            $this->set_user_field($uid,'viptime',$viptime);
                        }


                    }elseif($count<=12000 & $count>10000){
                        $viptime = 60*24*60*60;
                        $rtime=$userinfo["viptime"];
                        if($rtime==0||$rtime<time()){
                            $viptime = $viptime+time();
                        }else{
                            $viptime = $viptime+$rtime;
                        }
                        if($userinfo['vipgrade']==0){
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                            $this->set_user_field($uid,'viptime',$viptime);
                            $this->set_user_field($uid,'vipgrade',1);
                        }else{
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                            $this->set_user_field($uid,'viptime',$viptime);
                        }


                    }

                }else{
                    $this->set_user_field($uid,"isfirstrecharge",1);
                    M('user_extend')->where(array('uid' => $uid))->setField('isfirstrecharge',1);
                }



                $Pushdata['code']='200';
                $Pushdata['message'] = $this->L("充值金币成功");
            }else{
                $Pushdata['code'] = ERRORCODE_201;
                $Pushdata['message'] = $this->L("充值金币不成功,count字段需传值");
            }

             $this->get_user($uid,1);
        }
        /// ///
        return $Pushdata;
    }
	public function paypalreturn(){
        if(!empty($_POST)){
            $notify_str = "支付回调信息:\r\n";
            foreach ($_POST as $key => $value) {
                $notify_str.=$key."=".$value.";\r\n";
            }
            //记录paypal订单信息
            log_result($notify_str,"paypal");
        }
        log_result($notify_str,"paypal");
	    $custom = json_decode(base64_decode($_POST['custom']));
        $payerId = $_POST['payer_email'];
	    $uid=$custom->uid;
        $UserInfo = $this->get_user($uid);
        $product = $UserInfo["product"];
        $paytime = time();
        $vipgrade =$custom->status?$custom->status : 1;//会员等级
        $paytype = $custom->type?$custom->type: 1;//1:viptime2:金币
        $count = $custom->days? $custom->days:"0";//充值天数或金币数量
        $money = $custom->money? $custom->money:"0";
        //汇率换算
        $money=$money/32.5;
        $RechargeM = new RechargeModel();
        $reData = array();
        $reData['uid'] = $uid;
        $reData['days'] = $count;
        $reData['paytime'] = $paytime;//服务器时间
        $reData['product'] = $product;
        $reData['status'] = 5;
        $reData['translateid']=$payerId;

        if($paytype==1){
            $reData['type'] =1;//1:用户自己充值2：用户帮陪聊用户充值3钻石充值4钻石帮陪聊充值
        }else{
            $reData['type'] =3;//1:用户自己充值2：用户帮陪聊用户充值3钻石充值4钻石帮陪聊充值
        }

        $reData['money'] = intval($money*100);//充值金额计算

        if($paytype==1){
            $viptime = $count*24*60*60;
            $rtime=$UserInfo["viptime"];
            if($rtime==0||$rtime<time()){
                $viptime = $viptime+time();
            }else{
                $viptime = $viptime+$rtime;
            }
            $reData['vipgold'] = $viptime;
        }elseif($paytype==2){
            $rgold=$UserInfo["gold"];
            $reData['vipgold']=$rgold+$count;
        }

        $payid =$RechargeM->addOne($reData);
        //添加首页充值轮播图
            $userregistlist = "userregistlist-" . $product;
            $redis = $this->redisconn();
            $redis->listRemove($userregistlist, $uid.'-2', 0);
            $redis->listPush($userregistlist, $uid.'-2');
            $totalCount = $redis->listSize($userregistlist);
            if ($totalCount > 50) {
                $redis->listPop($userregistlist, 1);
            }
        //end

        $UserBaseM = new UserBaseModel();
        if($paytype==1){
            if($UserInfo['vipgrade']!=0 & $UserInfo['vipgrade']>$vipgrade){
                $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                $this->set_user_field($uid,'viptime',$viptime);
            }else{
                $resultu=$UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>$vipgrade));
                $this->set_user_field($uid,'viptime',$viptime);
                $this->set_user_field($uid,'vipgrade',$vipgrade);
            }
            //检查该用户充值记录是否在意向表里边存在
            $data=M('user_yixiang')->where(array('uid'=>$uid))->find();
            //若存在则删除
            if($data) {
                $del = M('user_yixiang')->where(array('uid' => $uid))->delete();
            }
            if($resultu!== false){
                $Pushdata['message'] = $this->L("充值vip天数成功");
                $Pushdata['data']['leftviptime']=$viptime/(24*60*60);
            }else{
                $Pushdata['code'] = ERRORCODE_201;
                $Pushdata['message'] = $this->L("充值vip天数不成功");

            }
            $this->get_user($uid,1);
        }elseif($paytype==2){
            //增加金币
            $result=$this->gold_add($uid, $count);
            //检查该用户充值记录是否在意向表里边存在
            $data=M('user_yixiang')->where(array('uid'=>$uid))->find();
            //若存在则删除
            if($data) {
                $del = M('user_yixiang')->where(array('uid' => $uid))->delete();
            }
            if($result){
                //会员赠送  充值钻石送会员 300赠送1天 500钻石赠送1天会员   1000赠送3天  2000 赠送7天 5000赠送20天  10000 赠送50天  12000赠送 60天（首次充值钻石不赠送，非首次都赠送）
                if($UserInfo['isfirstrecharge']==1){
                    if($count<=500){
                        $viptime = 1*24*60*60;
                        $rtime=$UserInfo["viptime"];
                        if($rtime==0||$rtime<time()){
                            $viptime = $viptime+time();
                        }else{
                            $viptime = $viptime+$rtime;
                        }
                        if($UserInfo['vipgrade']==0){
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                            $this->set_user_field($uid,'viptime',$viptime);
                            $this->set_user_field($uid,'vipgrade',1);
                        }else{
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                            $this->set_user_field($uid,'viptime',$viptime);
                        }

                    }elseif($count<=1000 & $count>500){
                        $viptime = 3*24*60*60;
                        $rtime=$UserInfo["viptime"];
                        if($rtime==0||$rtime<time()){
                            $viptime = $viptime+time();
                        }else{
                            $viptime = $viptime+$rtime;
                        }
                        if($UserInfo['vipgrade']==0){
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                            $this->set_user_field($uid,'viptime',$viptime);
                            $this->set_user_field($uid,'vipgrade',1);
                        }else{
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                            $this->set_user_field($uid,'viptime',$viptime);
                        }


                    }elseif($count<=2000 & $count>1000){
                        $viptime = 7*24*60*60;
                        $rtime=$UserInfo["viptime"];
                        if($rtime==0||$rtime<time()){
                            $viptime = $viptime+time();
                        }else{
                            $viptime = $viptime+$rtime;
                        }
                        if($UserInfo['vipgrade']==0){
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                            $this->set_user_field($uid,'viptime',$viptime);
                            $this->set_user_field($uid,'vipgrade',1);
                        }else{
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                            $this->set_user_field($uid,'viptime',$viptime);
                        }

                    }elseif($count<=5000 & $count>2000){
                        $viptime = 20*24*60*60;
                        $rtime=$UserInfo["viptime"];
                        if($rtime==0||$rtime<time()){
                            $viptime = $viptime+time();
                        }else{
                            $viptime = $viptime+$rtime;
                        }
                        if($UserInfo['vipgrade']==0){
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                            $this->set_user_field($uid,'viptime',$viptime);
                            $this->set_user_field($uid,'vipgrade',1);
                        }else{
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                            $this->set_user_field($uid,'viptime',$viptime);
                        }


                    }elseif($count<=10000 & $count>5000){
                        $viptime = 50*24*60*60;
                        $rtime=$UserInfo["viptime"];
                        if($rtime==0||$rtime<time()){
                            $viptime = $viptime+time();
                        }else{
                            $viptime = $viptime+$rtime;
                        }
                        if($UserInfo['vipgrade']==0){
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                            $this->set_user_field($uid,'viptime',$viptime);
                            $this->set_user_field($uid,'vipgrade',1);
                        }else{
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                            $this->set_user_field($uid,'viptime',$viptime);
                        }


                    }elseif($count<=12000 & $count>10000){
                        $viptime = 60*24*60*60;
                        $rtime=$UserInfo["viptime"];
                        if($rtime==0||$rtime<time()){
                            $viptime = $viptime+time();
                        }else{
                            $viptime = $viptime+$rtime;
                        }
                        if($UserInfo['vipgrade']==0){
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime,"vipgrade"=>1));
                            $this->set_user_field($uid,'viptime',$viptime);
                            $this->set_user_field($uid,'vipgrade',1);
                        }else{
                            $UserBaseM->updateOne(array("uid"=>$uid), array("viptime"=>$viptime));
                            $this->set_user_field($uid,'viptime',$viptime);
                        }


                    }

                }else{
                    $this->set_user_field($uid,"isfirstrecharge",1);
                    M('user_extend')->where(array('uid' => $uid))->setField('isfirstrecharge',1);
                }
                $Pushdata['message'] = $this->L("充值金币成功");
            }else{
                $Pushdata['code'] = ERRORCODE_201;
                $Pushdata['message'] = $this->L("充值金币不成功,count字段需传值");
            }
            $this->get_user($uid,1);
        }



	    echo 1;
	    return 1;
	}
	/**
	 * 充值意向
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @return data
	 */
	public function yixiang(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $user=M('user_yixiang');
        $time=time();
        $data=$user->where(array('uid'=>$uid))->find();
            //如果库中已有记录，则更新字段，count加1.
            if($data){
                $res=array(
                    'count'=>$data['count']+1,
                    'last_time'=>$time
                );
                $ret=$user->where(array('uid'=>$uid))->setField($res);
                if($ret){
                    $return['message'] = $this->L ("CHENGGONG");
                    Push_data($return);
                }
                //库中没有记录则插入
            }else{
                $count=1;
                $insert=array();
                $insert['count']=$count;
                $insert['last_time']=$time;
                $insert['uid']=$uid;
                $ins=$user->data($insert)->add();
                if($ins){
                    $return['message'] = $this->L ("CHENGGONG");
                    Push_data($return);
                }
            }
    }

    /*
     * 其他支付方式
     * 这个开关要根据特定平台单独打开，默认关闭
     */
    public function otherpay(){
	    //会员支付链接
            $Pushdata['data']['isshow']=0;
            $version = str_replace('.', '', $this->platforminfo['version']);
            $otherpaydata=M('paypal_switch')->select();
            foreach($otherpaydata as $k=>$v){
                $otherpayversion = str_replace('.', '', $v['version']);
                if($version<$otherpayversion&$this->platforminfo['product']==$v['product']){
                    if($v['isopenout']==1){
                        $Pushdata['data']['isshow']=1;
                    }
                    break;
                }
            }
        //$Pushdata['data']['url']='http://47.52.73.213:81/pay/index.php';
        //钻石支付链接
        //$Pushdata['data']['goldurl']='http://47.52.73.213:81/pay/z.php';
	//立米哒链接      
  $Pushdata['data']['url']='http://www.limidanet.com/website/index/product_display';
  $Pushdata['data']['goldurl']='http://www.limidanet.com/website/index/zsbug.html';
        Push_data($Pushdata);
    }

    /*
     * 支付类型、价格以及启用开关
     * 这个开关要根据特定平台单独打开，默认关闭
     */
    public function payprice(){
        $data = $this->Api_recive_date;
        $userinfo=$this->UserInfo;
        $gender=$userinfo['gender'];
        $product=$userinfo['product'];
        $type=$data['type'];
        $res=array();
        $payprice=M('payprice');

        $huiyuan=$payprice->where(array('type'=>1,'common'=>$product,'istrue'=>1,'gender'=>$gender))->select();
        $zuanshi=$payprice->where(array('type'=>2,'common'=>$product,'istrue'=>1,'gender'=>$gender))->select();
        if(empty($huiyuan)){
            $huiyuan=$payprice->where(array('type'=>1,'common'=>'24111','istrue'=>1,'gender'=>$gender))->select();
            $zuanshi=$payprice->where(array('type'=>2,'common'=>'24111','istrue'=>1,'gender'=>$gender))->select();
        }


        //是否开启订阅
        $res['isdingyue']='true';
        if($res['isdingyue']=='true'){
            $count=count($huiyuan);
            foreach ($huiyuan as $k=>$v){
                $res['huiyuan'][$k]['grade']=$v['grade'];
                $res['huiyuan'][$k]['price']=$v['price'];
                $res['huiyuan'][$k]['days']=$v['days'];
                $res['huiyuan'][$k]['isopen']=$v['isopen'];
                $res['huiyuan'][$k]['paykey']=$v['paykey'];
            }
            $data=$payprice->where(array('type'=>5,'common'=>$product,'istrue'=>1))->find();
            if($data){
                if($gender==2){
                    $girlhuiyuan=array();
                    $girlhuiyuan['grade']=$data['grade'];
                    $girlhuiyuan['price']=$data['price'];
                    $girlhuiyuan['days']=$data['days'];
                    $girlhuiyuan['paykey']=$data['paykey'];
                    $girlhuiyuan['isopen']=$data['isopen'];
                    $girlhuiyuan['paykey']=$data['paykey'];
                    //$res['huiyuan'][$count]=$girlhuiyuan;
                    array_unshift($res['huiyuan'],$girlhuiyuan);
                }
            }
        }
        foreach ($zuanshi as $k=>$v){
            $res['zuanshi'][$k]['grade']=$v['grade'];
            $res['zuanshi'][$k]['price']=$v['price'];
            $res['zuanshi'][$k]['days']=$v['days'];
            $res['zuanshi'][$k]['isopen']=$v['isopen'];
            $res['zuanshi'][$k]['paykey']=$v['paykey'];
        }

        //paypal支付开关
        $res['paypalswitch']='false';
        $version = str_replace('.', '', $this->platforminfo['version']);
        $otherpaydata=M('paypal_switch')->select();
        foreach($otherpaydata as $k=>$v){
            $otherpayversion = str_replace('.', '', $v['version']);
            if($version<$otherpayversion&$this->platforminfo['product']==$v['product']){
                if($v['isopenin']==1){
                    $res['paypalswitch']='true';
                }
                break;
            }
        }
        $res['paypalurl']='http://47.52.73.213:81/pay/index.php';
        //钻石支付链接
        $res['paypalgoldurl']='http://47.52.73.213:81/pay/z.php';
        //谷歌支付key
        $paykey=M('products')->where(array('product'=>$product))->getField('paykey');
        if ($paykey!=null) {
            $res['goolepaykey']=$paykey;
        }
        $Pushdata['data']=$res;
        Push_data($Pushdata);
    }

    //mycard支付
    public function Purchase()
    {
        // Initialize
        $config = [
            'appId' => $this->appId,
            'appKey' => $this->appKey,
        ];
        $gateway =Omnipay::create('MyCard');
        $gateway->initialize($config);
        //$gateway->setTestMode('test');

        $recharge=array();
        $transactionid=create_order('mycard');
        if($_GET['type']==1){
            $productname='会员充值';
            $recharge['status']=$_GET['status'];
        }elseif($_GET['type']==2){
            $productname='购买礼物';
        }
        $recharge['type']=$_GET['type'];
        $recharge['translateid']=$transactionid;
        $recharge['uid']=$_GET['uid'];
        $recharge['paytime']=time();
        $recharge['money']=$_GET['money']*100/32.5;
        $recharge['days']=$_GET['days'];
        $recharge['ischeck']=0;
        // Send purchase request
        $response = $gateway->purchase(
           [
               'amount'        => $_GET['money'],
               'currency'      => 'TWD',
               'description'   => $productname,
               'transactionId' => $transactionid,
               'serverId'=>$recharge['uid'],
           ]
        )->send();

        // Process response
        if ($response->isRedirect()) {
            // doing something here
             $token = $response->getToken();
             //$data = $response->getData();
             $transactionReference = $response->getTransactionReference();
             //将订单信息以及验证码储存在数据库中
            $recharge['authcode']=$token;
            $recharge['transactionreference']=$transactionReference;
            $mycard_recharge=M('mycard_recharge');
            $mycard_recharge->data($recharge)->add();
            $response->redirect();
        }
        elseif ($response->isSuccessful()) {
            // doing something here
            print_r($response);
        }
        else {
            echo $response->getMessage();
        }
    }
    //mycard方支付回调函数
    public function MyCardReturnC(){
        // Notify
        $mycard_recharge=M('mycard_recharge');
        $config = [
            'appId' => $this->appId,
            'appKey' => $this->appKey
        ];
        $gateway =Omnipay::create('MyCard');
        $gateway->initialize($config);
        $gateway->setTestMode('test');
        try {//acceptNotification*/
            $response = $gateway->acceptNotification()->send();
            // set token (which saved when send a purchase @see Usage For Purchase)
             $transactionId = $response->getTransactionId();
            $authcode=$mycard_recharge->where(array('translateid'=>$transactionId))->getField('authcode');
            $response->setToken($authcode);
            // confirm
            $response->confirm();
            if ($response->isSuccessful()) {
                // doing something here
                // save $response->getData()['confirmData'] for further compare
                $data = $response->getData();
                //echo "<pre>";print_r(urldecode($data['message']));echo "<pre>";
                //echo "<pre>";print_r($data);echo "<pre>";
                if($data['raw']['PayResult']==0){
                    echo urldecode($data['message']);
                }else{
                    $MyCardTradeNo=$data['raw']['MyCardTradeNo'];
                    $paymenttype=$data['raw']['PaymentType'];
                    //echo "<pre>";print_r($MyCardTradeNo);echo "<pre>";
                    $mycard_recharge->where(array('translateid'=>$transactionId))->save(array('ischeck'=>1,'mycardtradeno'=>$MyCardTradeNo,'paymenttype'=>$paymenttype));
                    $touserinfo=array();
                    $datamycard=$mycard_recharge->where(array('translateid'=>$transactionId))->find();
                    $vipgrade=$datamycard['status'];
                    $InData = array();
                    $userinfo=$this->get_user($datamycard['uid']);
                    $InData['uid'] = $datamycard['uid'];
                    $InData['days'] = $datamycard['days'];
                    $InData['translateid'] =$datamycard['translateid'];
                    $InData['paytime'] = $datamycard['paytime'];//服务器时间
                    $payid=$datamycard['id'];
                    if($datamycard['type']==1){
                        $InData['type'] = 1;//1:用户自己充值
                    }else{
                        $InData['type'] = 3;//3：用户自己钻石充值
                    }
                    $InData['money'] = intval($datamycard['money']*100);//充值金额计算
                    $InData['status'] = $datamycard['status'];
                    $Pushdata= $this->mycard_addpay($InData,$userinfo,$touserinfo,$vipgrade,$payid);
                    //将成功支付订单保存在日志记录中以便以后查看记录
                   $mycard_success_log=json_encode($datamycard,true);
                    log_result($mycard_success_log,'mycardsuccessorder');
                     if($Pushdata['code']=='200'){
                         header('Location: /mycardpay/success.php');
                     }
                }
            }
        } catch (\Exception $e) {
            echo urldecode($e->getMessage());
        }
    }

    //mycard方支付回调函数，正式环境
    public function MyCardReturn(){
        // Notify
        $mycard_recharge=M('mycard_recharge');
        $config = [
            'appId' => $this->appId,
            'appKey' => $this->appKey
        ];
        $gateway =Omnipay::create('MyCard');
        $gateway->initialize($config);
        //$gateway->setTestMode('test');
        try {
            $response = $gateway->acceptNotification()->send();
            // set token (which saved when send a purchase @see Usage For Purchase)
            $transactionId = $response->getTransactionId();
            $authcode=$mycard_recharge->where(array('translateid'=>$transactionId))->getField('authcode');
            $response->setToken($authcode);
            // confirm
            $response->confirm();
            if ($response->isSuccessful()) {
                // doing something here
                // save $response->getData()['confirmData'] for further compare
                $data = $response->getData();
                //echo "<pre>";print_r(urldecode($data['message']));echo "<pre>";
                //echo "<pre>";print_r($data);echo "<pre>";
                if($data['raw']['PayResult']==0){
                    echo urldecode($data['message']);
                }else{
                    $MyCardTradeNo=$data['raw']['MyCardTradeNo'];
                    $paymenttype=$data['raw']['PaymentType'];
                    //echo "<pre>";print_r($MyCardTradeNo);echo "<pre>";
                    $datamycard=$mycard_recharge->where(array('translateid'=>$transactionId))->find();
                    if($datamycard['ischeck']==0){
                        $mycard_recharge->where(array('translateid'=>$transactionId))->save(array('ischeck'=>1,'mycardtradeno'=>$MyCardTradeNo,'paymenttype'=>$paymenttype));
                        $touserinfo=array();

                        $vipgrade=$datamycard['status'];
                        $InData = array();
                        $userinfo=$this->get_user($datamycard['uid']);
                        $InData['uid'] = $datamycard['uid'];
                        $InData['days'] = $datamycard['days'];
                        $InData['translateid'] =$datamycard['translateid'];
                        $InData['paytime'] = $datamycard['paytime'];//服务器时间
                        $payid=$datamycard['id'];
                        if($datamycard['type']==1){
                            $InData['type'] = 1;//1:用户自己充值
                        }else{
                            $InData['type'] = 3;//3：用户自己钻石充值
                        }
                        $InData['money'] = intval($datamycard['money']);//充值金额计算
                        $InData['status'] = $datamycard['status'];
                        $Pushdata= $this->mycard_addpay($InData,$userinfo,$touserinfo,$vipgrade,$payid);

                        //如果有推广人id，计算推广人的提成
                        $userinfo['tg_uid']=M('user_base')->where(array('uid'=>$datamycard['uid']))->getField('tg_uid');
                        if($userinfo['tg_uid']){
                            $moneyC = new MoneyController();
                            $moneyC->vipRecharge($InData,$userinfo);
                        }
                        //将成功支付订单保存在日志记录中以便以后查看记录
                        $mycard_success_log=json_encode($datamycard,true);
                        log_result($mycard_success_log,'mycardsuccessorder');
                        //if($Pushdata['code']=='200'){
                            header('Location: /mycardpay/success.php');
                        //}
                    }else{
                        echo '该订单已经支付成功！';
                    }

                }
            }
        } catch (\Exception $e) {
            echo urldecode($e->getMessage());
        }
    }

    //订单差异对比
public function MyCardDfReturnC(){
    $config = [
        'appId' => $this->appId,
        'appKey' => $this->appKey
    ];
    $gateway =Omnipay::create('MyCard');
    $gateway->initialize($config);
    $gateway->setTestMode('test');

    $compare = $gateway->compareTransaction();

// Get Params, Exp: ["card"=>"MC123456"] or ["startTime"=>1500000000,"endTime"=>1560000000];
    $params = $compare->getParams();
    $mycard_recharge=M('mycard_recharge');
    $data=array();
    if(empty($params['card'])){
        //多笔查询
        $startTime=$params['startTime'];
        $endTime=$params['endTime'];
        $map['paytime'] = array(array('EGT', $startTime), array('ELT', $endTime));
        $map['ischeck']=1;
        $mycarddate=$mycard_recharge->where($map)->select();
        foreach($mycarddate as $k=>$v){
            $data[$k]['type']=$v['paymenttype'];
            $data[$k]['transactionId']=$v['translateid'];
            $data[$k]['transactionReference']=$v['transactionreference'];
            $data[$k]['card']=$v['mycardtradeno'];
            $data[$k]['amount']=$v['money']/100*32.5;
            $data[$k]['currency']='TWD';
            $data[$k]['account']=$v['uid'];
            $data[$k]['time']=$v['paytime'];
        }
    }else{
        //单笔查询
        $card=$params['card'];
        $mycarddate=$mycard_recharge->where(array('mycardtradeno'=>$card,'ischeck'=>1))->find();
        $data = [
            [
                'type'                 => $mycarddate['paymenttype'],         // INGAME, COSTPOINT Or Something Else
                'transactionId'        => $mycarddate['translateid'],       // My Transaction Id
                'transactionReference' => $mycarddate['transactionreference'],       // MyCard Transaction Id
                'card'                 => $mycarddate['mycardtradeno'],    // Card Number Or Something Else
                'amount'               => $mycarddate['money']/100*32.5,          // Amount
                'currency'             => 'TWD',            // Currency
                'account'              => $mycarddate['uid'],        // User Id
                'time'                 => $mycarddate['paytime'],       // Timestamp
            ],
            // ... more
        ];

    }

// Get data from database with the $params above

// Output data
    $compare->setData($data)->send();

}

//订单差异对比正式环境
    public function MyCardDfReturn(){
        $config = [
            'appId' => $this->appId,
            'appKey' => $this->appKey
        ];
        $gateway =Omnipay::create('MyCard');
        $gateway->initialize($config);
        //$gateway->setTestMode('test');

        $compare = $gateway->compareTransaction();

// Get Params, Exp: ["card"=>"MC123456"] or ["startTime"=>1500000000,"endTime"=>1560000000];
        $params = $compare->getParams();
        $mycard_recharge=M('mycard_recharge');
        $data=array();
        if(empty($params['card'])){
            //多笔查询
            $startTime=$params['startTime'];
            $endTime=$params['endTime'];
            $map['paytime'] = array(array('EGT', $startTime), array('ELT', $endTime));
            $map['ischeck']=1;
            $mycarddate=$mycard_recharge->where($map)->select();
            foreach($mycarddate as $k=>$v){
                $data[$k]['type']=$v['paymenttype'];
                $data[$k]['transactionId']=$v['translateid'];
                $data[$k]['transactionReference']=$v['transactionreference'];
                $data[$k]['card']=$v['mycardtradeno'];
                $data[$k]['amount']=ceil($v['money']/100*32.5);
                $data[$k]['currency']='TWD';
                $data[$k]['account']=$v['uid'];
                $data[$k]['time']=$v['paytime'];
            }
        }else{
            //单笔查询
            $card=$params['card'];
            $mycarddate=$mycard_recharge->where(array('mycardtradeno'=>$card,'ischeck'=>1))->find();
            $data = [
                [
                    'type'                 => $mycarddate['paymenttype'],         // INGAME, COSTPOINT Or Something Else
                    'transactionId'        => $mycarddate['translateid'],       // My Transaction Id
                    'transactionReference' => $mycarddate['transactionreference'],       // MyCard Transaction Id
                    'card'                 => $mycarddate['mycardtradeno'],    // Card Number Or Something Else
                    'amount'               => ceil($mycarddate['money']/100*32.5),          // Amount
                    'currency'             => 'TWD',            // Currency
                    'account'              => $mycarddate['uid'],        // User Id
                    'time'                 => $mycarddate['paytime'],       // Timestamp
                ],
                // ... more
            ];

        }

// Get data from database with the $params above

// Output data
        $compare->setData($data)->send();

    }

}
?>
