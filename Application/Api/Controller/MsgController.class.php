<?php

class MsgController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        //echo date("Y-m-d",time());exit;
        $this->msgfileurl = '/userdata/msgfile/' . date('Ymd', time()) . "/";
        $this->cachepath = WR . '/userdata/cache/msg/';
    }

    /**
     * 打招呼
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request int touid 对方用户id
     * @return data
     */
        public function sayhello()
    {
        $data = $this->Api_recive_date;
        $touid = $data["touid"] ? $data["touid"] : 0;
        $uid = $this->uid;
        //测试回信 start
        //$AutomsgCon = new AutomsgController();
        //$AutomsgCon->testreply($touid,$uid,"打招呼");
        //测试回信 end
        $redis = $this->redisconn();
        $redisStr = "sayhello_" . $uid . "_" . $touid;
        if ($uid && $touid) {
            if ($redis->exists($redisStr)) {
                $return['code'] = ERRORCODE_201;
                $return['message'] = $this->L("yijingdaguozhaohule");
            } else {
                $MyUserInfo = $this->UserInfo;
                $ToUserInfo = $this->get_user($touid);

                if ($MyUserInfo["user_type"] == 1 && $ToUserInfo["user_type"] == 2 && $MyUserInfo['gender'] == 1) {
                    if ($MyUserInfo["vip"] <= 0) {
                        //$word = "Hi!";
                        //判断缓存中是否存在用户类型为3用户的在线状态
                      //  $gender = $MyUserInfo["gender"] == 1 ? 2 : 1;
                      //  $redis = $this->redisconn();
                     //   $keyexit = $redis->exists('get_yingxiao_user-zx-status-' . $gender);
                        //获取在线陪聊女用户
                       /* if ($keyexit) {
                            $result = $redis->get('get_yingxiao_user-zx-status-' . $gender);
                            $useronlinelistkey = json_decode($result, true);
                            if($_REQUEST['ttest']==1){
                                dump($useronlinelistkey);
                            }
                        } else {
                            //如果缓存中不存在，则取数据库最新数据存入
                            $m = M('user_base');
                            $sql = "SELECT uid from `t_user_base` where `gender`= ".$gender." and `user_type`=3 or `user_type`=9 OR `user_type`=10 OR `user_type`=11";
                            $resultata = $m->query($sql);
                            if($_REQUEST['ttest']==1){
                                dump($resultata);
                            }
                            $useronlinelistkey=array();
                            foreach($resultata as $k=>$v){
                                $useronlinelistkey[]=$v['uid'];
                            }
                            $resultxjson=json_encode($useronlinelistkey,true);
                            $redis = $this->redisconn();
                            $redis->set('get_yingxiao_user-zx-status-' . $gender,$resultxjson,0,0,60*60*24);
                        }*/
                            ///////////////////////////
                            //获取优质女用户
                           /* $lang = $redis->listSize('superusers');
                            ///////////////
                            //如果缓存中不存在，则取数据库最新数据存入
                            if ($lang == 0) {
                                $superusers = M('superusers');
                                $Dres = $superusers->select();
                                $res = array();
                                foreach ($Dres as $k => $v) {
                                    $res[$k] = $v['uid'];
                                    $value = json_encode($res[$k], true);
                                    $ret[] = $redis->listPush('superusers', $value);
                                }
                                $result1 = $res;
                            } else {
                                $result1 = $redis->listLrange('superusers', 0, -1);

                            }
                            $result = array_unique($result1);
                            $useronlinelistkey = array_merge($useronlinelistkey, $result);
                            $useronlinelistkey = array_slice($useronlinelistkey, 0, 499);
                            $tentxunstatus = tencent_onlinestate($useronlinelistkey, C("IMSDKID")['24110']);

                        $word = "Hi!";
                        foreach ($tentxunstatus as $k1 => $v1) {

                            if ($v1["State"] == "Online") {
                                $onlinelist[] = $v1['To_Account'];
                            }
                            if ($v1["State"] == "PushOnline") {
                                $pushonlinelist[] = $v1['To_Account'];
                            }
                        }
                        if ($_REQUEST['ptest'] == 1) {
                            dump($onlinelist);
                            //dump($pushonlinelist);
                        }*/
                        /*向用户类型为3的用户打招呼*/
                       // shuffle($onlinelist);
                     /*   $onlinelist = array('402759', '407676', '395030', '431470', '392255', '447346', '407676', '431470', '431471',
                            '394336', '447810', '429959', '395019', '395221', '447740', '394026', '406694', '394526', '446203', '415621', '394337', '394331',
                            '419734', '408443', '391707');*/
                        /*for ($i = 0; $i < count($onlinelist); $i++) {
                            $issayhello = $this->isSayHello($onlinelist[$i], $uid);
                            if ($issayhello) {
                                if ($i == count($onlinelist) - 1) {
                                    $tothreekey = array_rand($pushonlinelist, 1);
                                    $tothreeuid = $pushonlinelist[$tothreekey];
                                    $issayhello = $this->isSayHello($tothreeuid, $uid);
                                    if ($issayhello) {
                                        break;
                                    } else {
                                        $AutomsgCon = new AutomsgController();
                                        $AutomsgCon->send_max_msg($tothreeuid, $uid, $word);
                                        $nowtime = time();
                                        $endToday = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
                                        $cachetime = $endToday - $nowtime;
                                        $redis->set("sayhello_" . $uid . "_" . $tothreeuid, "1", 0, 0, $cachetime);
                                        break;
                                    }
                                }
                                continue;
                            } else {
                                //发送打招呼信息
                                $AutomsgCon = new AutomsgController();
                                $AutomsgCon->send_max_msg($onlinelist[$i], $uid, $word);
                                $nowtime = time();
                                $endToday = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
                                $cachetime = $endToday - $nowtime;
                                $redis->set("sayhello_" . $uid . "_" . $onlinelist[$i], "1", 0, 0, $cachetime);
                                break;
                            }
                        }*/

                        $randsa = rand(1, 100);
                        if ($randsa > 70) {
                            $AutomsgCon = new AutomsgController();
                            $AutomsgCon->rundmsg($uid, $touid);
                        }

                    }

                  //  if ($MyUserInfo["vip"] > 0) {
                       // $word = "Hi!";
                        //判断缓存中是否存在用户类型为3用户的在线状态
                      /*  $gender = $MyUserInfo["gender"] == 1 ? 2 : 1;
                        $redis = $this->redisconn();
                        $keyexit = $redis->exists('get_yingxiao_user-zx-status-' . $gender);
                        if ($keyexit) {
                            $result = $redis->get('get_yingxiao_user-zx-status-' . $gender);
                            $useronlinelistkey = json_decode($result, true);
                        } else {
                            //如果缓存中不存在，则取数据库最新数据存入
                            $m = M('user_base');
                            $sql = "SELECT uid from `t_user_base` where `user_type`=3 or `user_type`=9 OR `user_type`=10 OR `user_type`=11 and `gender`=" . $gender;
                            $resultata = $m->query($sql);
                            $useronlinelistkey=array();
                            foreach($resultata as $k=>$v){
                                $useronlinelistkey[]=$v['uid'];
                            }
                            $resultxjson=json_encode($useronlinelistkey,true);
                            $redis = $this->redisconn();
                            $redis->set('get_yingxiao_user-zx-status-' . $gender,$resultxjson,0,0,60*60*24);
                        }
                        ///////////////////////////
                        //获取优质女用户
                        $lang = $redis->listSize('superusers');
                        ///////////////
                        //如果缓存中不存在，则取数据库最新数据存入
                        if ($lang == 0) {
                            $superusers = M('superusers');
                            $Dres = $superusers->select();
                            $res = array();
                            foreach ($Dres as $k => $v) {
                                $res[$k] = $v['uid'];
                                $value = json_encode($res[$k], true);
                                $ret[] = $redis->listPush('superusers', $value);
                            }
                            $result1 = $res;
                        } else {
                            $result1 = $redis->listLrange('superusers', 0, -1);

                        }
                        $result = array_unique($result1);
                        ////////////////////////////////////
                        $useronlinelistkey = array_merge($useronlinelistkey, $result);
                        $useronlinelistkey = array_slice($useronlinelistkey, 0, 499);
                        $tentxunstatus = tencent_onlinestate($useronlinelistkey, C("IMSDKID")['24110']);

                        $word = "Hi!";
                        foreach ($tentxunstatus as $k1 => $v1) {

                            if ($v1["State"] == "Online") {
                                $onlinelist[] = $v1['To_Account'];
                            }
                            if ($v1["State"] == "PushOnline") {
                                $pushonlinelist[] = $v1['To_Account'];
                            }
                        }*/
                        /*向用户类型为3的用户打招呼*/
                       /* shuffle($onlinelist);
                        for ($i = 0; $i < count($onlinelist); $i++) {
                            $issayhello = $this->isSayHello($onlinelist[$i], $uid);
                            if ($issayhello) {
                                if ($i == count($onlinelist) - 1) {
                                    $tothreekey = array_rand($pushonlinelist, 1);
                                    $tothreeuid = $pushonlinelist[$tothreekey];
                                    $issayhello = $this->isSayHello($tothreeuid, $uid);
                                    if ($issayhello) {
                                        break;
                                    } else {
                                        $AutomsgCon = new AutomsgController();
                                        $AutomsgCon->send_max_msg($tothreeuid, $uid, $word);
                                        $nowtime = time();
                                        $endToday = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
                                        $cachetime = $endToday - $nowtime;
                                        $redis->set("sayhello_" . $uid . "_" . $tothreeuid, "1", 0, 0, $cachetime);
                                        break;
                                    }
                                }
                                continue;
                            } else {
                                //发送打招呼信息
                                $AutomsgCon = new AutomsgController();
                                $AutomsgCon->send_max_msg($onlinelist[$i], $uid, $word);
                                $nowtime = time();
                                $endToday = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
                                $cachetime = $endToday - $nowtime;
                                $redis->set("sayhello_" . $uid . "_" . $onlinelist[$i], "1", 0, 0, $cachetime);
                                break;
                            }
                        }*/

                    //}
                }
                elseif ($MyUserInfo["user_type"] == 1 && $ToUserInfo["user_type"] == 2 && $MyUserInfo['gender'] == 2) {
                    //$word="Hi!";
                    //$redis = $this->redisconn();
                    if ($MyUserInfo["vip"] <= 0) {
                        $randsa = rand(1, 100);
                        if ($randsa > 70) {
                            $AutomsgCon = new AutomsgController();
                            $AutomsgCon->rundmsg($uid, $touid);
                        }

                        //携带打招呼
                        //获取在线男用户,从缓存中读取，缓存中没有的话重新调取，缓存10分钟
                    /*    $keyexit = $redis->exists('onlinenembermenusers');
                        if ($keyexit) {
                            $result = $redis->get('onlinenembermenusers');
                            $onlinenembermenusers = json_decode($result, true);
                            $onlinevipusers=$onlinenembermenusers['vipusers'];
                            $onlinecommonusers=$onlinenembermenusers['commonusers'];
                            $onlinecommonusers=array_merge($onlinecommonusers,$onlinevipusers);
                        } else {
                            //如果缓存中不存在，则取数据库最新数据存入
                            $onlinenembermenusers=$this->onlinemenusers();
                            $onlinevipusers=$onlinenembermenusers['vipusers'];
                            $onlinecommonusers=$onlinenembermenusers['commonusers'];
                            $onlinecommonusers=array_merge($onlinecommonusers,$onlinevipusers);
                            $resultxjson=json_encode($onlinenembermenusers,true);
                            $redis = $this->redisconn();
                            $redis->set('onlinenembermenusers',$resultxjson,0,0,60*10);
                        }*/
                        /*shuffle($onlinecommonusers);


                        for ($i = 0; $i < count($onlinecommonusers); $i++) {
                            $issayhello = $this->isSayHello($onlinecommonusers[$i], $uid);
                            if ($issayhello) {
                                if ($i == count($onlinecommonusers) - 1) {
                                    break;
                                }
                                continue;
                            } else {
                                //发送打招呼信息
                                $AutomsgCon = new AutomsgController();
                                $AutomsgCon->send_max_msg($onlinecommonusers[$i], $uid, 'hi!');
                                $nowtime = time();
                                $endToday = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
                                $cachetime = $endToday - $nowtime;
                                $redis->set("sayhello_" . $uid . "_" . $onlinecommonusers[$i], "1", 0, 0, $cachetime);
                                break;
                            }
                        }*/


                    }/*else{
                        //获取优质女用户
                        $lang = $redis->listSize('superusers');
                        //如果缓存中不存在，则取数据库最新数据存入
                        if ($lang == 0) {
                            $superusers = M('superusers');
                            $Dres = $superusers->select();
                            $res = array();
                            foreach ($Dres as $k => $v) {
                                $res[$k] = $v['uid'];
                                $value = json_encode($res[$k], true);
                                $ret[] = $redis->listPush('superusers', $value);
                            }
                            $result1 = $res;
                        } else {
                            $result1 = $redis->listLrange('superusers', 0, -1);

                        }
                        $result = array_unique($result1);
                        //判断是否是优质女用户
                        if(in_array($uid,$result)){
                            //优质女用户打招呼转向在线会员男用户
                            //获取在线男用户,从缓存中读取，缓存中没有的话重新调取，缓存10分钟
                            $keyexit = $redis->exists('onlinenembermenusers');
                            if ($keyexit) {
                                $result = $redis->get('onlinenembermenusers');
                                $onlinenembermenusers = json_decode($result, true);
                                $onlinevipusers=$onlinenembermenusers['vipusers'];
                            } else {
                                //如果缓存中不存在，则取数据库最新数据存入
                                $onlinenembermenusers=$this->onlinemenusers();
                                $onlinevipusers=$onlinenembermenusers['vipusers'];
                                $resultxjson=json_encode($onlinenembermenusers,true);
                                $redis = $this->redisconn();
                                $redis->set('onlinenembermenusers',$resultxjson,0,0,60*10);
                            }
                            shuffle($onlinevipusers);
                            if($_REQUEST['csptdah']==1){
                                dump($onlinevipusers);
                            }

                            for ($i = 0; $i < count($onlinevipusers); $i++) {
                                $issayhello = $this->isSayHello($onlinevipusers[$i], $uid);
                                if ($issayhello) {
                                    if ($i == count($onlinevipusers) - 1) {
                                        break;
                                    }
                                    continue;
                                } else {
                                    //发送打招呼信息
                                    $AutomsgCon = new AutomsgController();
                                    $AutomsgCon->send_max_msg($onlinevipusers[$i], $uid, $word);
                                    $nowtime = time();
                                    $endToday = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
                                    $cachetime = $endToday - $nowtime;
                                    $redis->set("sayhello_" . $uid . "_" . $onlinevipusers[$i], "1", 0, 0, $cachetime);
                                    if($_REQUEST['csptdah']==1){
                                        dump($onlinevipusers[$i]);
                                    }
                                    break;
                                }
                            }
                        }else{
                            $issayhello = $this->isSayHello('399808', $uid);
                            if(!$issayhello){
                                //发送打招呼信息
                                $AutomsgCon = new AutomsgController();
                                $AutomsgCon->send_max_msg('399808', $uid, $word);
                                $nowtime = time();
                                $endToday = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
                                $cachetime = $endToday - $nowtime;
                                $redis->set("sayhello_" . $uid . "_399808", "1", 0, 0, $cachetime);
                            }
                        }
                    }*/
                }
                $nowtime = time();
                $endToday = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
                $cachetime = $endToday - $nowtime;
                $redis->set($redisStr, "1", 0, 0, $cachetime);
                $return['code'] = ERRORCODE_200;
                $return['message'] = $this->L("CHENGGONG");

                //如果为用户类型为3则采用策略语句第一句
                if($MyUserInfo["user_type"] == 3){
                /*if($_REQUEST['cszj']==1){*/
                    $AutomsgCon = new AutomsgController();
                    $msgauto=$AutomsgCon->get_rand_msg();
                    $return['data']['hellomsg']=$msgauto['value'];
                }else{
                    $return['data']['hellomsg'] = "Hi!";
                }


            }
        } else {
            $return['code'] = ERRORCODE_201;
            $return['message'] = $this->L("CANSHUYICHANG");
        }
        Push_data($return);
    }

    /**
     * 批量打招呼
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request string touid 对方用户id1|id2|id3|id4
     * @return data
     */
    public function sayhelloall()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $touid_all = explode("|", $data['touid']);
        $result = array();
        $return['code'] = ERRORCODE_200;
        $return['message'] = $this->L("CHENGGONG");
        $result = $return;
        foreach ($touid_all as $k => $v) {
            $touid = $v;
            $redis = $this->redisconn();
            $redisStr = "sayhello_" . $uid . "_" . $touid;
            if ($uid && $touid) {
                if ($redis->exists($redisStr)) {
                    continue;
                    /* $return[$v]['code'] = ERRORCODE_201;
                    $return[$v]['message'] = $this->L ("yijingdaguozhaohule");
                    $result['data']=$return; */
                } else {
                    $nowtime = time();
                    $endToday = mktime(0, 0, 0, date('m'), date('d') + 1, date('Y')) - 1;
                    $cachetime = $endToday - $nowtime;
                    $redis->set($redisStr, "1", 0, 0, $cachetime);


                }
            } else {
                $return['code'] = ERRORCODE_201;
                $return['message'] = $this->L("CANSHUYICHANG");
                $result = $return;

            }
        }
        Push_data($result);
    }

    //设为已读
    public function setread()
    {
        $data = $this->Api_recive_date;
        //$platforminfo = $this->platforminfo;
        //$uid = $this->uid;
        if (isset($data['id'])) {
            $id = $data['id'];
            $map['id'] = $id;
            $map1['id'] = $id;
            $map1['isread'] = 1;
            $data1['isread'] = 1;
            $MsboxM = new MsgBoxModel();
            $result1 = $MsboxM->getOne1($map1);
            if ($result1) {
                Push_data(array('message' => $this->L("已经设置为已读"), 'code' => ERRORCODE_501));
            }
            $result = $MsboxM->updateOne1($map, $data1);
            if ($result) {
                $this->get_msgboxlist(0, 0, 0, 1);
                $return = array();
                $return['message'] = $this->L("CHENGGONG");
                Push_data($return);
            }
        } else {
            Push_data(array('message' => $this->L("id字段不能为空"), 'code' => ERRORCODE_501));
        }
    }

    /*
     *是否打招呼
     */
    public function isSayHello($touid, $uid = 0)
    {
        $uid = $uid ? $uid : $this->uid;
        $redis = $this->redisconn();
        $redisStr = "sayhello_" . $uid . "_" . $touid;
        if ($redis->exists($redisStr)) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * 发信
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @request string msgtype 发信类型:text文本消息，voice语音消息，video视频消息，location位置信息，gift礼物消息,askgift索要礼物,image图片，redpacket红包,askVip索要会员,vip赠送会员
     * @request string content 发信内容//红包内容
     * @request string touid 收信人用户ID
     * @request string contentint 数字内容:礼物数量、语音或视频时长、红包金币数量定位时是longitude、vip天数
     * @request string contentbody 礼物id、语音url或视频url,定位时是latitude、vip等级
     * @request string contentsub 视频第一帧图片url
     * @return data
     */
    public function sendmsg()
    {
        $data = $this->Api_recive_date;
        $platforminfo = $this->platforminfo;
        $uid = $this->uid;
        $MyUserInfo = $this->UserInfo;
        //参数完整性验证start
        if (!isset($data['touid'])) {
            Push_data(array('message' => $this->L("touid字段不能为空"), 'code' => ERRORCODE_501));
        }
        /* if (!isset($data['content'])) {
            Push_data (array('message' => $this->L ("content字段不能为空"), 'code' => ERRORCODE_501));
        } */
        //参数完整性验证end

        $redis = $this->redisconn();
        $redisStrj = "Smsg_" . $uid;
        $leftgold = $MyUserInfo["gold"];
        $resultj = true;
        if ($resultj) {
            $msgtype = $data['msgtype'] ? $data['msgtype'] : "text";//发信类型:text文本消息，voice语音消息，video视频消息，location位置信息，gift礼物消息,image图片 hello打招呼
            $content = $data['content'] ? $data['content'] : "";//发信内容//文本时
            $touid = $data['touid'] ? $data['touid'] : "";//收信人用户ID
            $ToUserInfo = $this->get_user($touid);//收信人用户信息
            $contentint = $data['contentint'] ? $data['contentint'] : "0";//礼物数量、语音、红包金币数量或视频时长
            $contentbody = $data['contentbody'] ? $data['contentbody'] : "";//礼物id、语音url或视频url，图片ur
            $contentsub = $data['contentsub'] ? $data['contentsub'] : "";//视频第一帧图片url
            $datapuid = $data['puid'] ? $data['puid'] : "0";//puid
            $mybox_id = $uid . $touid;
            $tobox_id = $touid . $uid;
            $MyBoxinfo = array();
            $ToBoxinfo = array();
            $Msginfo = array();
            switch ($msgtype) {
                //voice语音消息
                case "voice":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;

                    $messageBody = array(
                        "msgtype" => $msgtype,
                        "content" => $content,
                        "sendTime" => time(),
                        "voice" => array("url" => $contentbody, "ltime" => $contentint)
                    );
                    break;
                //video视频消息
                case "video":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    $Msginfo['contentsub'] = $contentsub;
                    $messageBody = array(
                        "msgtype" => $msgtype,
                        "content" => $content,
                        "sendTime" => time(),
                        "video" => array("url" => $contentbody, "ltime" => $contentint, "imageurl" => $contentsub)
                    );
                    break;
                //location位置信息
                case "location":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    $messageBody = array(
                        "msgtype" => $msgtype,
                        "content" => $content,
                        "sendTime" => time(),
                        "location" => array("longitude" => $contentint, "latitude" => $contentbody)
                    );
                    break;
                //gift礼物消息
                case "gift":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    $result = $this->sendgift($uid, $touid, $contentint, $contentbody);
                    $giftobj = $this->giftextend($contentbody, $touid);
                    $giftobj["url"] = C("IMAGEURL") . $giftobj["url"];
                    $giftobj["count"] = $contentint;
                    $giftobj["demand"] = false;
                    $messageBody = array(
                        "msgtype" => $msgtype,
                        "content" => $content,
                        "sendTime" => time(),
                        "gift" => $giftobj
                    );

                    if ($result === 'false') {
                        Push_data(array('message' => $this->L("系统错误,插入数据库失败"), 'code' => ERRORCODE_501));
                    } elseif ($result == true || $result == 0) {
                        if ($result == 'userthree') {
                            $result = 0;
                        }
                        $leftgold = !empty($result)?$result:0;
                        break;

                    } else {
                        Push_data(array('message' => $this->L("金币不足请充值！"), 'code' => ERRORCODE_201));
                    }

                    break;
                //askgift索要礼物消息    
                case "askgift":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    $giftobj = $this->giftextend($contentbody, $touid);
                    $giftobj["url"] = C("IMAGEURL") . $giftobj["url"];
                    $giftobj["count"] = $contentint;
                    $giftobj["demand"] = true;
                    $messageBody = array(
                        "msgtype" => $msgtype,
                        "content" => $content,
                        "sendTime" => time(),
                        "gift" => $giftobj
                    );
                    break;
                //askVip索要Vip消息
                case "askVip":
                    $Msginfo['contentint'] = $contentint;//天数
                    $Msginfo['contentbody'] = $contentbody;//等级
                    $askvipobj = array(
                        "contentint" => $contentint,
                        "contentbody" => $contentbody
                    );
                    $messageBody = array(
                        "msgtype" => $msgtype,
                        "content" => $content,
                        "sendTime" => time(),
                        "askvip" => $askvipobj
                    );
                    break;
                //赠送Vip消息
                case "Vip":
                    $Msginfo['contentint'] = $contentint;//天数
                    $Msginfo['contentbody'] = $contentbody;//等级
                    $vipobj = array(
                        "contentint" => $contentint,
                        "contentbody" => $contentbody
                    );
                    $messageBody = array(
                        "msgtype" => $msgtype,
                        "content" => $content,
                        "sendTime" => time(),
                        "vip" => $vipobj
                    );

                    break;
                //,image图片
                case "image":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    $imageobj = array(
                        "id" => $contentint,
                        "url" => $contentbody,
                        "status" => 1,
                        "seetype" => 1
                    );
                    $messageBody = array(
                        "msgtype" => $msgtype,
                        "content" => $content,
                        "sendTime" => time(),
                        "image" => $imageobj
                    );
                    break;
                case "redpacket":
                    $Msginfo['contentint'] = $contentint;
                    $result = $this->sendredPacket($uid, $touid, $contentint, $content);
                    $messageBody = array(
                        "msgtype" => $msgtype,
                        "content" => $content,
                        "sendTime" => time(),
                        "redPacket" => array("diamondCount" => $contentint, "describe" => $content)
                    );
                    if ($result == 'error') {
                        Push_data(array('message' => $this->L("系统错误,插入数据库失败"), 'code' => ERRORCODE_501));
                    } elseif ($result == 'true') {
                        break;

                    } else {
                        Push_data(array('message' => $this->L("金币不足请充值！"), 'code' => ERRORCODE_201));
                    }
                    break;
                default:
                    $messageBody = array(
                        "msgtype" => $msgtype,
                        "content" => $content,
                        "sendTime" => time()
                    );
                    //如果不是会员检测是否需要扣除金币 start
                    if ($MyUserInfo["vip"] < 1 && $MyUserInfo["gold"] > 0) {
                        $resultj1 = $this->judgeAuth($MyUserInfo);
                        if ($resultj1 == 2) {
                            if ($msgtype != 'hello' && $touid!=10002) {
                                $leftgold = $MyUserInfo["gold"] - 1;
                                $subgold = $this->gold_reduce($uid, $touid, 1, $MyUserInfo["gold"], 4, '', time());
                            }
                        }
                    }
                    //如果不是会员检测是否需要扣除金币 end
                    break;
            }


            if ($ToUserInfo["user_type"] == 3) {
                $redisStr = "addmoneymsg_" . $uid . "_" . $touid;
                if ($redis->exists($redisStr)) {
                    $redis->delete($redisStr);
                }
            }


            //auto and money start
            if ($ToUserInfo["user_type"] == 2) {
                $msglistkey = "automsglist_" . $mybox_id;
                $AutomsgCon = new AutomsgController();
                if ($redis->exists($msglistkey)) {
                    $msgisreplykey = "msgisreply_" . $mybox_id;
                    $redis->set($msgisreplykey, 2, 0, 0, 60 * 60);//1未回复2已回复
                    $msgret = $AutomsgCon->rundmsg($uid, $touid);
                    if ($msgret == 3) {
                        $Msginfo['isshow'] = 9;
                    }
                }
                /*
                $AutomsgCon = new AutomsgController();
                $redisStr = "pmsg_".$uid."_".$touid;
                if($redis->exists($redisStr)){
                    $puid = $redis->get($redisStr);
                   
                    //客户端消息对象封装
                    $clientMsgObj = array();
                    $clientMsgObj["touid"]=$puid;
                    $clientMsgObj["msgid"]=substr(md5($AutomsgCon->getMillisecond()), 8, 16);//substr(md5(time()), 8, 16)
                    $body = array();
                    $body["id"]=$clientMsgObj["msgid"];
                    if($msgtype=="askgift"){
                        $body["msgType"]="gift";
                    }else{
                        $body["msgType"]=$msgtype;
                    }
                    $body["user"]=$this->get_diy_user_field($uid,"uid|gender|age|nickname|vipgrade|vip|gold|head");
                    $body["puser"]=$this->get_diy_user_field($touid,"uid|gender|age|nickname|vipgrade|vip|gold|head");
                    $image = array();
                    $image['id']=1;
                    $image['url']=C("IMAGEURL").'/1.png';//原图url
                    $image['thumbnaillarge']=C("IMAGEURL").'/1.png';//大图url
                    $image['thumbnailsmall']=C("IMAGEURL").'/1.png';//小图url
                    $image['status']=1;//状态1审核通过;2正在审核3审核未通过已删除
                    $image['seetype'] =1; //可见级别(1所有用户可见,2会员可见)	
                    $body["user"]["nickname"] = "男士";
                    $body["user"]["head"] = $image;
                    $body["puser"]["head"] = $image;
                    $body["messageBody"]=$messageBody;
                    $clientMsgObj["msgbody"]=$body;
                    LM_sendmsg($clientMsgObj);
                    $Msginfo['isshow'] = 9;
                }else{
                    $msglistkey = "automsglist_".$mybox_id;
                    if($redis->exists($msglistkey)){
                        $msgisreplykey = "msgisreply_".$mybox_id;
                        $redis->set($msgisreplykey,2,0,0,60*60);//1未回复2已回复
                       $msgret =  $AutomsgCon->rundmsg($uid,$touid);
                       if($msgret==3){
                           $Msginfo['isshow'] = 9;
                       }
                    }
                }
                */
            }

            /*if ($MyUserInfo["user_type"] == 3) {
                $MoneyC = new MoneyController();
                $MoneyC->set_moneykey($uid, $touid);
            }*/
            if ($MyUserInfo["vip"] < 1) {
                if ($ToUserInfo["user_type"] == 3 || $ToUserInfo["user_type"] == 9 || $ToUserInfo["user_type"] == 10 || $ToUserInfo["user_type"] == 11) {
                    if ($msgtype != 'hello') {
                        $MoneyC = new MoneyController();
                        $MoneyC->set_moneykey_num($uid, $touid);
                    }
                }
            }

            //auto and money end
            $MyBoxinfo['type'] = $msgtype;
            //if($msgtype==1) {

            $MyBoxinfo['id'] = $mybox_id;
            $MyBoxinfo['content'] = $content;
            $MyBoxinfo['uid'] = $uid;
            $MyBoxinfo['touid'] = $touid;
            $MyBoxinfo['country'] = $platforminfo['country '] ? $platforminfo['country '] : 'TW';
            $MyBoxinfo['language'] = $platforminfo['language '] ? $platforminfo['language '] : 'zh-tw';
            $MyBoxinfo['sendtime'] = time();
            //$where = array('uid'=>$uid);

            //信箱所需字段信息
            $Msginfo['type'] = $msgtype;
            $Msginfo['sendtime'] = $MyBoxinfo['sendtime'];
            $Msginfo['language'] = $MyBoxinfo['language'];
            $Msginfo['country'] = $MyBoxinfo['country'];
            $Msginfo['uid'] = $uid;
            $Msginfo['touid'] = $touid;
            $Msginfo['content'] = $content;
            $Msginfo['box_id'] = $mybox_id;
            $MsgM = new MsgModel();
            $MsgBoxm = new MsgBoxModel();
            //将聊天列表记录在redis中
            $this->setchatbox($uid, $touid);
            //将聊天记录储存在redis中
            $reidsvalue = $this->setChatRecord($uid, $touid, $messageBody);
            $Msginfo['reidsvalue'] = $reidsvalue;
            $result = $MsgM->addOne($Msginfo);
            //消息列表储存过后将消息储存在信箱列表中，运用redis查询信息列表中是否已有来往信息
            if ($result) {
                //统计用户发送次数start
                if ($msgtype != 'hello') {
                    $val = $redis->get($redisStrj);
                    $redis->set($redisStrj, $val + 1, 0, 0, 60 * 60 * 24 * 3);
                }
                //统计用户发送次数end

                $redisStr = "Gmsgbox_" . $mybox_id;

                $cachetime = 60 * 60 * 24 * 3;
                $ab['id'] = $mybox_id;
                if ($redis->exists($redisStr)) {
                    $MsgBoxm->updateOne1($ab, $MyBoxinfo);
                } else {
                    $value = $MsgBoxm->getOne1($ab);
                    if ($value) {
                        $MsgBoxm->updateOne1($ab, $MyBoxinfo);
                        $redis->set($redisStr, $mybox_id, 0, 0, $cachetime);
                    } else {
                        $MsgBoxm->addOne1($MyBoxinfo);
                        $redis->set($redisStr, $mybox_id, 0, 0, $cachetime);
                    }
                }

            }
            //用户登录时间记录
            $logintime = time();
            $this->set_user_field($uid, "logintime", $logintime);
            $User_baseM = new UserBaseModel();
            $User_baseM->updateOne(array("uid" => $uid), array('logintime' => $logintime));
            //登录保存uid,将用户存进在线redis中
            $OnlineCon = new OnlineController();
            $OnlineCon->setUserOnline($this->UserInfo);

            //end
            $return = array();
            $return['message'] = $this->L("CHENGGONG");
            $return['data']['leftgold'] = $leftgold;
            Push_data($return);
        } else {
            Push_data(array('message' => $this->L("请充值！"), 'code' => ERRORCODE_201));
        }
    }


    /**
     * 是否可以发信
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @return data
     */
    public function cansendmsg()
    {
        $dataUser = $this->UserInfo;
        $uid = $this->uid;
        $resultj = $this->judgeAuth($dataUser);
        $return = array();
        if ($resultj) {
            $return['data']["cansendmsg"] = "1";
        } else {
            $return['data']["cansendmsg"] = "0";
        }
        Push_data($return);
    }

    /*
     * 判断发送权限
     */
    public function judgeAuth($dataUser)
    {
        $redisStrj = "Smsg_" . $dataUser["uid"];
        $redis = $this->redisconn();
        $cachetimej = 60 * 60 * 24 * 3;
        $vip = $dataUser['vip'];
	$gold = $dataUser['gold'];
        $regtime = $dataUser['regtime'];
        $time = time();
        //type为3的用户发信不拦截
        if ($dataUser['user_type'] == 3 || $dataUser['user_type'] == 7 || $dataUser['user_type'] == 9 || $dataUser['user_type'] == 10 || $dataUser['user_type'] == 11) {
            return true;
        }

        //type为6的用户为禁止发信用户
        if ($dataUser['user_type'] == 6) {
            return false;
        }
        if ($vip > 0) {
            //return "vip";
            if($_REQUEST['ptext']==1){
                dump('1');
            }
            return true;

        } elseif($gold > 0){
	if($_REQUEST['ptext']==1){
                dump('1');
            }
            return true;
	
	}else {
            //需求：所有平台普通女用户限制文字聊天
            if ($dataUser['user_type'] == 1 && $dataUser['gender'] == 2) {
                return false;
            }

            if ($time < $regtime + $cachetimej) {
                if ($time < $regtime + (60 * 5)) {
                    //return 3;
                    if($_REQUEST['ptext']==1){
                        dump('2');
                    }
                    return true;
                } else {
                    if ($redis->exists($redisStrj)) {
                        $val = $redis->get($redisStrj);
                        if ($val < 8) {
                            //return 3;
                            if($_REQUEST['ptext']==1){
                                dump('3');
                                dump($val);
                            }
                            return true;
                        } else {
                            if ($dataUser['gold'] > 0) {
                                if($_REQUEST['ptext']==1){
                                    dump('4');
                                }
                                return 2;
                            } else {
                                // return $val;
                                return false;
                            }
                        }

                    } else {
                        //return 5;
                        if($_REQUEST['ptext']==1){
                            dump('5');
                        }
                        return true;
                    }
                }


            } else {
                //需求：20114 钻石大于30才可以使用钻石聊天
                if ($dataUser['product'] == 20114) {
                    if ($dataUser['gold'] > 30) {
                        if($_REQUEST['ptext']==1){
                            dump('6');
                        }
                        return 2;
                    } else {
                        // return $val;
                        return false;
                    }
                } else {
                    if ($dataUser['gold'] > 0) {
                        return 2;
                    } else {
                        // return $val;
                        return false;
                    }
                }

            }
        }
        return false;
    }

    //获取礼物列表中相关数据
    public function giftextend($id, $touid = '0')
    {
        $userinfo = $this->UserInfo;
        //如果用户类型为3的用户，则根据对方的平台号发送礼物
        if ($userinfo['user_type'] == 3) {
            $platforminfo = $this->get_user($touid);
            $product = $platforminfo['product'];
            $language = $platforminfo['language'];
        } else {
            $platforminfo = $this->platforminfo;
            $product = $platforminfo['product'];
            $language = $platforminfo['language'];
        }

        $PubC = new PublicController();
        $result = $PubC->giftextend($id, $product, $language);
        return $result;
    }

    /*
     * 获取信箱
     */
    public function getmsgboxlist()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $pageNum = $data['page'] ? $data['page'] : 1;
        $pageSize = $data['pagesize'] ? $data['pagesize'] : 10;
        $page = array();
        $page["pagesize"] = $pageSize;
        $page["pageNum"] = $pageNum;
        $Wget = array();
        $Wget['uid'] = $uid;
        $MsboxM = new MsgBoxModel();
        $msgbox = $MsboxM->getListPage1($Wget, $pageNum, $pageSize);
        //$msgbox = $this->get_msgboxlist($Wget, $pageNum, $pageSize);
        $page["totalcount"] = $msgbox["totalCount"];
        $msgbox1 = array();
        $touidmsg = array();
        foreach ($msgbox['msgbox'] as $k => $v) {

            $msgbox1[$k]['id'] = $msgbox['msgbox'][$k]['id'];
            $msgbox1[$k]['isread'] = $msgbox['msgbox'][$k]['isread'];
            $msgbox1[$k]['type'] = $msgbox['msgbox'][$k]['type'];
            $timtamp = $msgbox['msgbox'][$k]['sendtime'];
            $msgbox1[$k]['sendtime'] = date("Y-m-d H:i", $timtamp);
            $msgbox1[$k]['content'] = $msgbox['msgbox'][$k]['content'];
            $touidUserInfo = $this->get_user($v['touid']);
            if ($touidUserInfo) {
                //头像url、昵称、用户id、会员天数、金币数量、会员等级
                $touidmsg['url'] = $touidUserInfo['head']['url'];
                $touidmsg['nickname'] = $touidUserInfo['nickname'];
                $touidmsg['uid'] = $touidUserInfo['uid'];
                $touidmsg['vip'] = $touidUserInfo['vip'];
                $touidmsg['gold'] = $touidUserInfo['gold'];
                $touidmsg['vipgrade'] = $touidUserInfo['vipgrade'];
            }
            $msgbox1[$k]['touser'] = $touidmsg;
        }

        $result['msgboxlist'] = $msgbox1;
        $result['page'] = $page;
        $return['data']["msgboxlist"] = $result;
        //$return['data']["page"] = $pageNum;
        $return['message'] = $this->L("CHENGGONG");
        Push_data($return);

    }

    //获取缓存信箱
    protected function get_msgboxlist($Wget, $pageNum, $pageSize, $reset = 0)
    {
        $path = $this->cachepath . "msgboxlist/";
        $cache_name = 'msgboxlist' . md5(json_encode($Wget) . $pageNum . $pageSize);
        if ($reset == 1) {
            return deldir($path);
        }
        if (F($cache_name, '', $path)) {
            $msgbox = F($cache_name, '', $path);
        } else {
            $MsboxM = new MsgBoxModel();
            $msgbox = $MsboxM->getListPage1($Wget, $pageNum, $pageSize);
            F($cache_name, $msgbox, $path);
        }
        return $msgbox;
    }

    /*
     * 获取消息列表
     */

    public function getmsglist()
    {
        $data = $this->Api_recive_date;
        $selfuid = $this->uid;
        $uid = $data['uid'];
        $touid = $data['touid'];
        $pageNum = $data['page'] ? $data['page'] : 1;
        $pageSize = $data['pagesize'] ? $data['pagesize'] : 15;
        $page = array();
        $page["pagesize"] = $pageSize;
        $page["pageNum"] = $pageNum;
        $Wget = array();
        if (!$uid) {
            $Wget['uid'] = $selfuid;
        } else {
            $Wget['uid'] = $uid;
        }
        $Wget['touid'] = $touid;
        $msg = $this->get_msglist($Wget, $pageNum, $pageSize);
        $page["totalcount"] = $msg["totalcount"] ? $msg["totalcount"] : 0;
        $msg1 = array();
        $msg1['uid'] = $this->get_diy_user_field($Wget['uid']);
        $msg1['touid'] = $this->get_diy_user_field($Wget['touid']);
        $image = array();
        $image['id'] = 1;
        $image['url'] = C("IMAGEURL") . '/1.png';//原图url
        $image['thumbnaillarge'] = C("IMAGEURL") . '/1.png';//大图url
        $image['thumbnailsmall'] = C("IMAGEURL") . '/1.png';//小图url
        $image['status'] = 1;//状态1审核通过;2正在审核3审核未通过已删除
        $image['seetype'] = 1; //可见级别(1所有用户可见,2会员可见)
        $msg1["touid"]["nickname"] = "男士";
        $msg1["touid"]["head"] = $image;
        $msg1["uid"]["head"] = $image;

        $msg1['list'] = array();
        $isshow = false;
        $temppre = array();
        foreach ($msg['val'] as $k => $v) {

            if ($isshow) {
                $msg1['list'][] = $v;
            } else {
                $temppre[] = $v;
                if ($v["isshow"] == 9) {
                    $isshow = true;
                    if (!empty($temppre)) {
                        $temppre = array_reverse($temppre);
                        $tempprebak = array();
                        foreach ($temppre as $v1) {
                            $tempprebak[] = $v1;
                            if ($v1["uid"] == $data['uid']) {
                                break;
                            }
                        }
                        $tempprebak = array_reverse($tempprebak);
                        $msg1['list'] = $tempprebak;
                        $temppre = array();
                    }
                    //$msg1['list'][] =$v;
                }

            }

        }
        $result = $msg1;

        $result['page'] = $page;
        if ($pageNum > ceil($page["totalcount"] / $pageSize)) {
            $return['data'] = $result;
        } else {
            $return['data'] = $result;
        }
        $return['message'] = $this->L("CHENGGONG");
        Push_data($return);

    }

    //获取消息缓存列表
    protected function get_msglist($Wget, $pageNum, $pageSize, $reset = 0)
    {
        //$path = $this->cachepath . "msglist/";
        //  $cache_name = 'msglist' . md5 (json_encode ($Wget) . $pageNum . $pageSize);
        //  if ($reset == 1) {
        //      return deldir ($path);
        //  }
        //   if (F ($cache_name, '', $path)) {
        //       $msg = F ($cache_name, '', $path);
        //   } else {
        $msgM = new MsgModel();
        $msg = $msgM->getListPage1($Wget, $pageNum, $pageSize);
        //      F ($cache_name, $msg, $path);
        //  }
        return $msg;
    }

    /*
     * 发送信息上传文件
     */
    public function uploadfile()
    {
        $data = $this->Api_recive_date;
        $type = $data["type"] ? $data["type"] : 1;
        $filetype = "image";
        if ($type == 2) {
            $filetype = "audio";
        } elseif ($type == 3) {
            $filetype = "video";
        }
        $uid = $this->uid;
        $return = array();
        $return['code'] = ERRORCODE_201;
        $return['message'] = $this->L("SHANGCHUANSHIBAI");

        if (!empty($_FILES)) {
            //$_FILES['file']['type'] = 'image/jpeg';
            $saveUrl = $this->msgfileurl . $filetype . "/";
            $savePath = WR . $saveUrl;
            //echo $savePath;exit;
            $uploadList = $this->_upload($savePath);
            if (!empty($uploadList)) {
                $UserPhotoM = new PhotoModel();
                $PutOssarr = array();
                $returnurlarr = array();
                foreach ($uploadList as $k => $v) {
                    $PutOssarr[] = $saveUrl . $v['savename'];
                    $returnurlarr[] = C("IMAGEURL") . $saveUrl . $v['savename'];
                }
                PutOss($PutOssarr);
                $return = array();
                $return["data"]["url"] = $returnurlarr;
            }
        }
        Push_data($return);
    }

// 文件上传
    protected function _upload($savePath)
    {
        import("Api.lib.Behavior.fileDirUtil");
        $fileutil = new fileDirUtil();
        $fileutil->createDir($savePath);
        import("Api.lib.Behavior.UploadFile");
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize = 5242880;
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
        //设置附件上传目录
        $upload->savePath = $savePath;
        //设置需要生成缩略图，仅对图像文件有效
        $upload->thumb = false;
        // 设置引用图片类库包路径
        $upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix = 'm_';  //生产2张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth = '400';
        //设置缩略图最大高度
        $upload->thumbMaxHeight = '400';
        //设置上传文件规则
        $upload->saveRule = "md5content";
        //删除原图
        $upload->thumbRemoveOrigin = false;
        if (!$upload->upload()) {
            $return = array();
            $return['code'] = ERRORCODE_501;
            $return['message'] = $upload->getErrorMsg();
            Push_data($return);
            //捕获上传异常
            //$this->error($upload->getErrorMsg());
        } else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo();
            //$uploadList = $uploadList[0];
            //$uploadList['imageUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . $upload->saveRule;
            //$uploadList['thumbnailUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . 'm_' . $upload->saveRule;

            return $uploadList;
            //import("Extend.Library.ORG.Util.Image");
            //给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
            //Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '/logo2.png');
            //$_POST['image'] = $uploadList[0]['savename'];
        }

    }


    /*
         * 发送礼物接口
         */
    public function sendgift($uid, $touid, $contentint, $contentbody)
    {

        //需求：用户类型3赠送礼物只允许赠送给用户类型为1的男性用户
        $userinfo = $this->UserInfo;
        $touserinfo = $this->get_user($touid);
        $giftlistM = new GiftListModel();
        if ($userinfo['user_type'] == 3 || $userinfo['user_type'] == 9 || $userinfo['user_type'] == 10 || $userinfo['user_type'] == 11) {
            if ($userinfo['gold'] == 0) {
                return 'userthree';
            } else {
                $giftdata = array();
                $giftdata['uid'] = $uid;
                $giftdata['touid'] = $touid;
                $giftdata['giftcount'] = $contentint;
                $giftdata['giftid'] = $contentbody;


                $giftextend = $this->giftextend($contentbody);
                $giftdata['price'] = $giftextend['price'];
                $giftdata['title'] = $giftextend['title'];
                $giftdata['totalprice'] = $giftextend['price'] * $contentint;
                $giftdata['time'] = time();
                $result = $giftlistM->addOne($giftdata);
                if ($result) {
                    S('SendGiftList' . $uid, null);
                    S('GetGiftList' . 'to' . $touid, null);
                    S('SendGiftL' . $uid, null);
                    S('GetGiftL' . 'to' . $touid, null);
                }

                //////////////////////////////
                //查看双方是否开通爱情岛
                if($uid>$touid){
                    $boxuid=$touid.$uid;
                }else{
                    $boxuid=$uid.$touid;
                }

                $aiqingdao=$this->get_exclusive_message($uid,$touid);
                if($aiqingdao){
                    $aiqingdaodata=array();
                    if($uid==$aiqingdao['biguid']){
                        $mygiftids=explode('|',$aiqingdao['biggiftids']);
                        array_push($mygiftids, $giftextend["id"]);
                        $aiqingdaodata['biggiftids']=implode("|",$mygiftids);
                        $aiqingdaodata['bigcharmmoney']=$aiqingdao['bigcharmmoney']+$giftdata['totalprice']*0.65/30*100;
                        $aiqingdaodata['allcharmmoney']=$aiqingdao['allcharmmoney']+$giftdata['totalprice']*0.65/30*100;
                        M('exclusive')->where(array('boxuid'=>$boxuid))->save($aiqingdaodata);
                        $this->get_exclusive_message($uid,$touid,1);
                    }else{
                        if($aiqingdao['smallgiftids']==''){
                            $aiqingdaodata['smallgiftids']=$giftextend["id"];
                        }else{
                            $mygiftids=explode('|',$aiqingdao['smallgiftids']);
                            array_push($mygiftids, $giftextend["id"]);
                            $aiqingdaodata['smallgiftids']=implode("|",$mygiftids);
                        }

                        $aiqingdaodata['smallcharmmoney']=$aiqingdao['smallcharmmoney']+$giftdata['totalprice']*0.65/30*100;
                        $aiqingdaodata['allcharmmoney']=$aiqingdao['allcharmmoney']+$giftdata['totalprice']*0.65/30*100;
                        M('exclusive')->where(array('boxuid'=>$boxuid))->save($aiqingdaodata);
                        $this->get_exclusive_message($uid,$touid,1);
                    }

                }
                /// /////////////////////
                //将该用户所送的礼物转化为魅力值存入对方抽奖的账户当中
                $userextendM=M('user_extend');
                $newmoney=$giftdata['totalprice']*0.65*0.2/30*100;
                $conversiondata=$userextendM->where(array('uid'=>$touid))->getField('conversiongift');
                if($conversiondata){
                    $oldmoney=$conversiondata;
                    $conversionmoney=$oldmoney+$newmoney;
                    $userextendM->where(array('uid'=>$touid))->setField('conversiongift',$conversionmoney);
                }else{
                    $userextendM->where(array('uid'=>$touid))->setField('conversiongift',$newmoney);
                }
                //end

                return $userinfo['gold'];
            }
        }
        $giftdata = array();
        $giftdata['uid'] = $uid;
        $giftdata['touid'] = $touid;
        $giftdata['giftcount'] = $contentint;
        $giftdata['giftid'] = $contentbody;

        $giftextend = $this->giftextend($contentbody);
        $giftdata['price'] = $giftextend['price'];
        $giftdata['title'] = $giftextend['title'];
        $giftdata['totalprice'] = $giftextend['price'] * $contentint;

        //判断用户是否用足够金币发送礼物
        $GoldCount = $userinfo['gold'];
        $totalprice = $giftdata['totalprice'];
        if ($GoldCount >= $totalprice) {
            $giftdata['time'] = time();

            $type = 1;
            $result = $giftlistM->addOne($giftdata);
            if ($result) {
                S('SendGiftList' . $uid, null);
                S('GetGiftList' . 'to' . $touid, null);
                S('SendGiftL' . $uid, null);
                S('GetGiftL' . 'to' . $touid, null);

                //扣除用户账户所对应的金币
                $subgold = $this->gold_reduce($uid, $touid, $totalprice, $GoldCount, $type, '', $giftdata['time']);
                if ($subgold) {
                    //add money start
                    $puid = $touid;
                    $redis = $this->redisconn();
                    $redisStr = "pmsg_" . $uid . "_" . $touid;
                    if ($redis->exists($redisStr)) {
                        $puid = $redis->get($redisStr);
                    }
                    $MoneyC = new MoneyController();
                    $MoneyC->sendgift($giftextend, $this->UserInfo, $puid, $contentint, $giftdata['time']);
                    //add money end
                    //将该用户所送的礼物转化为魅力值存入对方抽奖的账户当中
                    $userextendM=M('user_extend');
                    $newmoney=$giftdata['totalprice']*0.65*0.2/30*100;
                    $conversiondata=$userextendM->where(array('uid'=>$touid))->getField('conversiongift');
                    if($conversiondata){
                        $oldmoney=$conversiondata;
                        $conversionmoney=$oldmoney+$newmoney;
                        $userextendM->where(array('uid'=>$touid))->setField('conversiongift',$conversionmoney);
                    }else{
                        $userextendM->where(array('uid'=>$touid))->setField('conversiongift',$newmoney);
                    }
                    //end
                    $leftgold = $GoldCount - $totalprice;
                    return $leftgold;//返回1：插入数据库成功，扣除金额成功。
                }
                return 'false';//返回2：插入数据库成功，扣除金额失败。

            }
        } else {
            return false;//返回flase:余额不足，请充值
        }
    }

    //发送红包
    public function sendredpacket($uid, $touid, $contentint, $content)
    {
        $packetdata = array();
        $packetdata['uid'] = $uid;
        $packetdata['touid'] = $touid;
        $packetdata['money'] = $contentint;
        $packetdata['content'] = $content;


        //判断用户是否用足够金币发送红包
        //发送方剩余金币数量
        $GoldCount = $this->UserInfo['gold'];
        //接收方剩余金币数量
        $touser = $this->get_user($touid);
        $getgoldcount = $touser['gold'];
        if ($GoldCount >= $contentint) {
            $packetdata['paytime'] = time();
            //扣除用户账户所对应的金币
            $UserBaseM = new UserBaseModel();
            $res = $this->gold_reduce($uid, $uid, $contentint, $GoldCount, 2, '', $packetdata['paytime']);
            /* $res1=$UserBaseM->where('uid='.$touid)->setInc('gold',$contentint);*/
            if ($res) {
                //$this->get_user($touid,1);
                $redpacketM = M("red_packet_consume");
                $packetdata['sendleftgold'] = $GoldCount - $contentint;
                $packetdata['getleftgold'] = $getgoldcount + $contentint;
                $dat = $redpacketM->add($packetdata);

                if ($dat) {
                    //add money start
                    $puid = $touid;
                    $redis = $this->redisconn();
                    $redisStr = "pmsg_" . $uid . "_" . $touid;
                    if ($redis->exists($redisStr)) {
                        $puid = $redis->get($redisStr);
                    }
                    $MoneyC = new MoneyController();
                    $MoneyC->sendhongbao($contentint, $this->UserInfo, $puid, $packetdata['paytime']);
                    //add money end
                    return 'true';//插入数据库成功，扣除金额成功。
                }
                return 'error';//插入数据库成功，扣除金额失败。

            }
        } else {
            return false;//返回flase:余额不足，请充值
        }
    }

    public function getstate()
    {
        $get_data = $this->Api_recive_date;
        $procuct = $get_data['platforminfo']['product'];
        $uids = explode('|', $get_data['uids']);
        $txkey=M('txmy')->where(array('product'=>$procuct))->getField('miyao');
        $tentxunstatus = tencent_onlinestate($uids,$txkey);
        $return = array();
        if($_REQUEST['ppptest']==1){
            dump($tentxunstatus);
        }
        $zhuanhuan=array();
        foreach ($tentxunstatus as $k1 => $v1) {
           /* if ($v1['To_Account'] == '@TLS#NOT_FOUND') {
                continue;
            } else {*/
                $zhuanhuan[$k1]['uid'] = $v1['To_Account'];
                $userinfo = $this->get_user($v1['To_Account']);
                $zhuanhuan[$k1]['logintime'] = $userinfo['logintime'];
                $zhuanhuan[$k1]['State'] = $v1["State"];
            }

        //}
        $return["data"]['state']=$zhuanhuan;
        Push_data($return);

    }

    /*
*发送消息时保存聊天记录
* 这里用的redis存储是list数据类型
* 两个人的聊天用一个list保存
*
* @from 消息发送者id
* @to 消息接受者id
* @meassage 消息内容
*
* 返回值，当前聊天的总聊天记录数
*/
    public function setChatRecord($from, $to, $message)
    {
        $redis = $this->redisconn();
        $data = array('from' => $from, 'to' => $to, 'message' => $message, 'sent' => time());
        $value = json_encode($data);
        //生成json字符串
        //$keyName = 'rec:' . $this->getRecKeyName($from, $to);
        //echo $keyName;
       // $lang = $redis->listSize($keyName);
       // if ($lang >= 1000) {
        //    $redis->listPop($keyName, 1);
       // }
       // $res = $redis->listPush($keyName, $value);
        //$redis->setListKeyExpire($keyName, 60 * 60 * 24 * 7);
        return $value;

    }

    //将聊天列表记录在redis中
    public function setchatbox($uid, $touid)
    {
        $redis = $this->redisconn();
        //生成json字符串
        $keyNamea = 'recbox:' . $uid;
        $keyNameb = 'recbox:' . $touid;
        //echo $keyName;
        $langa = $redis->listSize($keyNamea);
        $redis->listRemove($keyNamea, $touid);
        if ($langa >= 100) {
            $redis->listPop($keyNamea, 1);
        }
        $res = $redis->listPush($keyNamea, $touid);
        $redis->setListKeyExpire($keyNamea, 60 * 60 * 24);

        $langb = $redis->listSize($keyNameb);
        $redis->listRemove($keyNameb, $uid);
        if ($langb >= 100) {
            $redis->listPop($keyNameb, 1);
        }
        $resb = $redis->listPush($keyNameb, $uid);
        $redis->setListKeyExpire($keyNameb, 60 * 60 * 24);
        return $res;
    }

    /*生成聊天记录的键名，即按大小规则将两个数字排序
    * @from 消息发送者id
    * @to 消息接受者id
    *
    *
    */
    private function getRecKeyName($from, $to)
    {
        return ($from > $to) ? $to . '_' . $from : $from . '_' . $to;
    }

    public function onlinemenusers(){
        $OnlineCon = new OnlineController();
        $dataproduct=M('products')->getField('product',true);
        $countproduct=count($dataproduct);
        $uids=array();
        for($i=0;$i<$countproduct;$i++){
            $uids[$i]=$OnlineCon->getUserOnline($dataproduct[$i], 1, 1, '','',2);
        }
        $dataonline=array();
        for($i=0;$i<$countproduct;$i++){
            if(empty($uids[$i]['uids'])||$uids[$i]['uids']==null){
                continue;
            }
            $dataonline=array_merge($dataonline,$uids[$i]['uids']);
        }
        $dataonline=assoc_unique($dataonline,'uid');
        $onlinevipusers=array();
        $onlinecommonusers=array();
        foreach ($dataonline as $k=>$v){
            $userinfo=$this->get_user($v['uid']);
            if($userinfo['vip']>0){
                $onlinevipusers[]=$v['uid'];
            }else{
                $onlinecommonusers[]=$v['uid'];
            }
        }
        $onlineusers['vipusers']=$onlinevipusers;
        $onlineusers['commonusers']=$onlinecommonusers;
        if($_REQUEST['pttest']==1){
            dump($onlineusers);
        }
        return $onlineusers;

    }


}

?>

