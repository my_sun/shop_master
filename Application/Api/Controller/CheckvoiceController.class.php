<?php
include_once   WR.'/lib/aliyuncs/aliyun-php-sdk-core/Config.php';
require_once  WR.'/lib/aliyuncs/aliyun-oss-php-sdk/autoload.php';
use Green\Request\V20180509 as Green;
use Green\Request\Extension\ClientUploader;

class CheckvoiceController extends BaseController
{

    public function checkvoice($voiceurl)
    {
        $iClientProfile = DefaultProfile::getProfile("cn-shanghai", "LTAIR8tjvGtclDfZ", "gMJGibkaW2t44UnflpauLK7rADmdWB");
        DefaultProfile::addEndpoint("cn-shanghai", "cn-shanghai", "Green", "green.cn-shanghai.aliyuncs.com");
        $client = new DefaultAcsClient($iClientProfile);
        $request = new Green\VoiceAsyncScanRequest();
        $request->setMethod("POST");
        $request->setAcceptFormat("JSON");
        $task1 = array('dataId' =>  uniqid(),
            'url' => $voiceurl
        );

        /**
         * 语音垃圾内容检测： antispam
         **/
        //如果是语音流检测，则live参数修改为true
        $request->setContent(json_encode(array("tasks" => array($task1),
            "scenes" => array("antispam"), "live" => false)));
        try {
            $response = $client->getAcsResponse($request);
            print_r($response);
            if(200 == $response->code){
                $taskResults = $response->data;
                foreach ($taskResults as $taskResult) {
                    if(200 == $taskResult->code){
                        $taskId = $taskResult->taskId;
                        // 将taskId保存下来，间隔一段时间来轮询结果。
                        print_r($taskId);
                    }else{
                        print_r("task process fail:" + $response->code);
                    }
                }
            }else{
                print_r("detect not success. code:" + $response->code);
            }
        } catch (Exception $e) {
            print_r($e);
        }
    }

    //提交语音本地文件示例代码
    public function localvoice($voiceurl)
    {
        $iClientProfile = DefaultProfile::getProfile("cn-shanghai", "LTAIR8tjvGtclDfZ", "gMJGibkaW2t44UnflpauLK7rADmdWB");
        DefaultProfile::addEndpoint("cn-shanghai", "cn-shanghai", "Green", "green.cn-shanghai.aliyuncs.com");
        $client = new DefaultAcsClient($iClientProfile);
        $request = new Green\VoiceAsyncScanRequest();
        $request->setMethod("POST");
        $request->setAcceptFormat("JSON");
        $uploader = ClientUploader::getVoiceClientUploader($client);
        $url = $uploader->uploadFile($voiceurl);
        // 如果是语音文件检测 type => file
        $task1 = array('dataId' =>  uniqid(),
            'url' => $url,
            'type' => 'file'
        );

        /**
         * 语音垃圾内容检测： antispam
         **/
        $request->setContent(json_encode(array("tasks" => array($task1),
            "scenes" => array("antispam"))));
        try {
            $response = $client->getAcsResponse($request);
            // print_r($response);
            if(200 == $response->code){
              $taskResults = $response->data;
              foreach ($taskResults as $taskResult) {
                  if(200 == $taskResult->code){
                      $taskId = $taskResult->taskId;
                      // 将taskId保存下来，间隔一段时间来轮询结果。具体请参照VideoAsyncScanResultsRequest接口说明
                      // print_r($taskId);
                      return $taskResult;
                  }else{
                      print_r("task process fail:" + $response->code);
                  }
              }
          }else{
              // print_r("detect not success. code:" + $response->code);
          }
      } catch (Exception $e) {
          print_r($e);
      }
    }


}


