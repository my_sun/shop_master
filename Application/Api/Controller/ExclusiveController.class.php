<?php
/**
 * aiqingdao管理
 */

class ExclusiveController extends BaseController
{
    public function __construct ()
    {
        parent::__construct ();
        
    }
    /**
     * 男女之间专属信息
     *
     * @request json platforminfo 客户端信息对象
     * @request string token 用户token
     * @return data
     */

    //申请开通专属爱情岛
    public function exclusiveisland(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $touid=$data['touid'];

        if($uid>$touid){
            $boxuid=$touid.$uid;
            $biguid=$uid;
            $smalluid=$touid;
        }else{
            $boxuid=$uid.$touid;
            $biguid=$touid;
            $smalluid=$uid;
        }
        //首先获取缓存，缓存中没有的话查询数据库

        $data=$this->get_exclusive_message($uid,$touid);
        if(!$data){
            //还么有开通，现在开通
            $result=array();
            $result['boxuid']=$boxuid;
            $result['biguid']=$biguid;
            $result['smalluid']=$smalluid;
            $res=M('exclusive')->data($result)->add();
            if($res){
                $this->get_exclusive_message($uid,$touid,1);
                $return['message'] = $this->L("CHENGGONG");
                Push_data($return);

            }else{
                $return['code'] = ERRORCODE_201;
                $return['message'] = "参数错误";
                Push_data($return);
            }
        }else{
            //已经开通成功。
            $return['message'] = $this->L("已经开通成功了");
            Push_data($return);
        }

    }
    //获取二人专属信息
    public function  getmessage(){

        $dataa = $this->Api_recive_date;
        $uid = $this->uid;
        $PubC = new PublicController();
        $touid=$dataa['touid'];
        $message=array();
        $data=$this->get_exclusive_message($uid,$touid);
        if($data){
            if($uid==$data['biguid']){
                $message['myselfcharmmoney']=$data['bigcharmmoney'];
                $message['touidcharmmoney']=$data['smallcharmmoney'];
                $message['allcharmmoney']=$data['allcharmmoney'];
                $message['house']=$data['house'];
                $message['car']=$data['car'];
                $message['pool']=$data['pool'];
                $message['garden']=$data['garden'];
                $message['pet']=$data['pet'];
                $mygiftuids=explode('|',$data['biggiftids']);
                $mygiftuids=array_unique($mygiftuids);
                $togiftuids=explode('|',$data['smallgiftids']);
                $togiftuids=array_unique($togiftuids);
                $mygiftuidscount=count($mygiftuids);
                $togiftuidscount=count($togiftuids);
                if($mygiftuidscount==41&$togiftuidscount==41){
                    $message['isgiftbefull']=1;
                }else{
                    $message['isgiftbefull']=0;
                }
                foreach($mygiftuids as $k=>$v){
                    $message['myselfgifts'][] = $PubC->giftextend($v, '', $this->platforminfo["language"]);
                }
                foreach($togiftuids as $k1=>$v1){
                    $message['togifts'][] = $PubC->giftextend($v1, '', $this->platforminfo["language"]);
                }



            }else{
                $message['myselfcharmmoney']=$data['smallcharmmoney'];
                $message['touidcharmmoney']=$data['bigcharmmoney'];
                $message['allcharmmoney']=$data['allcharmmoney'];
                $mygiftuids=explode('|',$data['smallgiftids']);
                $mygiftuids=array_unique($mygiftuids);
                $togiftuids=explode('|',$data['biggiftids']);
                $togiftuids=array_unique($togiftuids);
                $mygiftuidscount=count($mygiftuids);
                $togiftuidscount=count($togiftuids);
                if($mygiftuidscount==41&$togiftuidscount==41){
                    $message['isgiftbefull']=1;
                }else{
                    $message['isgiftbefull']=0;
                }
                foreach($mygiftuids as $k=>$v){
                    $message['myselfgifts'][] = $PubC->giftextend($v, '', $this->platforminfo["language"]);
                }
                foreach($togiftuids as $k1=>$v1){
                    $message['togifts'][] = $PubC->giftextend($v1, '', $this->platforminfo["language"]);
                }

            }
            $return['message'] = $this->L("CHENGGONG");

        }
        $return['data']=$message;

        Push_data($return);
    }

    //购买爱情岛中的礼物
    public function buygift(){
        //获取礼物名称，类型，价格等信息
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $touid=$data['touid'];
        $dataa=$this->get_exclusive_message($uid,$touid);
        $allcharmmoney=$dataa['allcharmmoney'];
        $garde=$data['garde'];
        $gifttype=$data['type'];
        $housestatus=$dataa['house'];

        if($gifttype!='house'&$housestatus<3){
            $return['message'] = $this->L("请先升级房子到3级");
            $return['data']['status'] =3;
            Push_data($return);
        }
        $updatemsg=array();
        if($uid>$touid){
            $boxuid=$touid.$uid;
        }else{
            $boxuid=$uid.$touid;
        }
        switch ($gifttype) {
            case 'house':
                if($garde==1){
                    $updatemsg['allcharmmoney']=$allcharmmoney-1000;

                }elseif($garde==2){
                    $updatemsg['allcharmmoney']=$allcharmmoney-2000;

                }elseif($garde==3){
                    $updatemsg['allcharmmoney']=$allcharmmoney-3000;
                }else{
                    $updatemsg['allcharmmoney']=$allcharmmoney-4000;
                }


  break;
            case 'car':
                if($garde==1){
                    $updatemsg['allcharmmoney']=$allcharmmoney-1000;

                }else{
                    $updatemsg['allcharmmoney']=$allcharmmoney-4000;
                }

  break;
            case 'pool':
                if($garde==1){
                    $updatemsg['allcharmmoney']=$allcharmmoney-1000;

                }else{
                    $updatemsg['allcharmmoney']=$allcharmmoney-4000;
                }

                break;
            case 'garden':
                if($garde==1){
                    $updatemsg['allcharmmoney']=$allcharmmoney-1000;

                }else{
                    $updatemsg['allcharmmoney']=$allcharmmoney-4000;
                }

                break;
            case 'pet':
                if($garde==1){
                    $updatemsg['allcharmmoney']=$allcharmmoney-1000;

                }else{
                    $updatemsg['allcharmmoney']=$allcharmmoney-1000;
                }
                break;

            default:
                $updatemsg['allcharmmoney']=$allcharmmoney;

        }
            $updatemsg[$gifttype]=$garde;
        if($updatemsg['allcharmmoney']<0){
            $return['message'] = $this->L("魅力值不足，请充值哦！");
            $return['data']['status'] = 2;
            Push_data($return);
        }
            $res=M('exclusive')->where(array('boxuid'=>$boxuid))->save($updatemsg);
            if($res){
                //删除缓存，重新从数据库中取数据
                $this->get_exclusive_message($uid,$touid,1);
                $return['message'] = $this->L("CHENGGONG");
                $return['data']['status'] = 1;

                Push_data($return);
            }else{
                $return['code'] = ERRORCODE_201;
                $return['message'] = "参数错误";
                Push_data($return);
            }
    }







}