<?php
class SpaceController extends BaseController {
	public function __construct(){
		parent::__construct();
		//$this->is_login();//检查用户是否登陆
		//$this->Api_recive_date;
        $this->msgfileurl = '/userdata/reportfile/'.date('Ymd',time())."/";
        $this->cachepath = WR.'/userdata/cache/report/';
	
		
	}
	
	
	public function uploadaudio(){
		//header("Content-type: text/html; charset=utf-8");
		$data = $this->Api_recive_date;
		$uid = $this->uid;	
		$audiotime = $data["audiotime"];	
		$user_path = $this->get_userpath($uid, 'images').$uid.'/myaudio/';
		mkdirs($user_path);
		import("Api.lib.Behavior.UploadFile");
		//导入上传类
		$upload = new UploadFile();
		//设置上传文件大小
		$upload->maxSize = 32922000;
		//设置上传文件类型
		$upload->allowExts = explode(',', 'mp3,arm');
		//设置附件上传目录
		$upload->savePath = $user_path;
		$upload->saveRule =time().".mp3";
		
		if (!$upload->upload()) {
				$RetArray['isSucceed'] = 0;
				$RetArray['msg'] = $upload->getErrorMsg();
		} else {
			$RetArray['url'] = 'http://newtaiwan.oss-cn-hongkong.aliyuncs.com/data'.$this->get_userurl($uid, 'images').$uid.'/myaudio/' . $upload->saveRule;
			$RetArray['isSucceed'] = 1;
			$RetArray['msg'] = "ok";
			$audiourl=$this->get_userurl($uid, 'images').$uid.'/myaudio/' .$upload->saveRule;
			$UseraudioM = new AudioModel();
			$AddData = array();
			$AddData["uid"]=$uid;
			$AddData["audiourl"]=$audiourl;
			$AddData["audiotime"]=$audiotime;
			$AddData["addtime"]=time();
			$res = $UseraudioM->addOne($AddData);
			PutOss($audiourl);
		}	
	
		Push_data($RetArray);
	}
	/*
	 *已收到礼物列表
	*/
	
	public function mygiftslist(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$pageNum = $data['pageNum'] ? $data['pageNum'] : 1;
		$pageSize = 30;
		
		$WhereArr = array();
		$WhereArr['to_uid'] = $myid;
		$WhereArr['type'] = 1;
		$PointsBaseM = new PointsModel();
		$res = $PointsBaseM->getListPage($WhereArr,$pageNum,$pageSize);
		
		$gifts = array();
		$allmoney=0;
		foreach($res["list"] as $k=>$v){
			if($v["typeid"]){
				$gifts[$v["typeid"]]=$gifts[$v["typeid"]]+$v["typecount"];
				$allmoney+=$v["typecount"]*$v["points"];
			}
		}
		$list = array();
		
		foreach($gifts as $k=>$v){
			$temp["giftid"]=$k;
			$temp["gifCounts"]=$v;
			$list[] = $temp;
		}
		
		$allmoney=ceil($allmoney/2);
		/***
		 * 返回信息定义
		*/
		$RetArray = array();
		$RetArray["list"] = $list;
		$RetArray["allmoney"] = $allmoney;
		$RetArray['isSucceed'] = 1;
		$RetArray['msg'] = "ok";
	
		Push_data($RetArray);
	}
	/*
	 *送出的礼物列表
	*/
	public function sendgiftslist(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$pageNum = $data['pageNum'] ? $data['pageNum'] : 1;
		$pageSize = 30;
		
		$WhereArr = array();
		$WhereArr['uid'] = $myid; 
		$WhereArr['type'] = 1;
		$PointsBaseM = new PointsModel();
		$res = $PointsBaseM->getListPage($WhereArr,$pageNum,$pageSize);
		$gifts = array();
		foreach($res["list"] as $k=>$v){
			if($v["typeid"]){
				$gifts[$v["typeid"]]=$gifts[$v["typeid"]]+$v["typecount"];
			}
		}
		$list = array();
		foreach($gifts as $k=>$v){
			$temp["giftid"]=$k;
			$temp["gifCounts"]=$v;
			$list[] = $temp;
		}
		
		/***
		 * 返回信息定义
		*/
		$RetArray = array();
		$RetArray["list"] = $list;
		$RetArray['isSucceed'] = 1;
		$RetArray['msg'] = "ok";
	
		Push_data($RetArray);
	}
	
	/**
	 * 获取最近访客
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int page 分页页码
	 * @request string userfield 需要的用户字段默认 uid|head|nickname
	 * @return data
	 */
	public function getseemelist(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$pageNum = $data['page'] ? $data['page'] : 1;
		$pageSize = 15;
		$Wdata['uid'] = $myid;
		$userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";
		
		$UserSeeM = new UserSeeModel();
		$ret = $UserSeeM->getListPage($Wdata,$pageNum,$pageSize);
		
		foreach($ret['listSeeMe'] as $k=>$v){
			
			$ret['listSeeMe'][$k] = array();
			$userinfo = $this->get_user($v['touid']);
			
			$userinfo = $this->get_user_field($userinfo,$userfield);
			$userinfo['issayhello'] = $this->isSayHello($v['touid']);
			$ret['listSeeMe'][$k]['user'] =$userinfo;
			
		}
		$return = array();
		$userlist = $ret['listSeeMe'];
		$return["data"]["userlist"] = $userlist;
		$page["pagesize"] = $pageSize;
		$page["page"] = $pageNum;
		$page["totalcount"] = $ret["totalCount"];
		$return["data"]["page"] = $page;
		
		
		Push_data($return);
	}



/**
	 * 谁看过我
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int page 分页页码
	 * @request string userfield 需要的用户字段默认 uid|head|nickname
	 * @return data
	 */
	public function whodolsee()
	{
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$pageNum = $data['page'] ? $data['page'] : 1;
		$pageSize = 15;
		
		$Wdata['touid'] = $myid;
		$Wdata['se2'] = 1;
		$userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";

		$UserSeeM = new UserSeeModel();
		$ret = $UserSeeM->getListPage($Wdata,$pageNum,$pageSize);
		$where['uid'] = $myid;
		$User_baseM = new UserBaseModel();
		$retu = $User_baseM->getOne($where);

		if($retu){
			//更新未读访客
			$res = $User_baseM->updateOne($where,array('unreadvisitors'=>0));
		}

		foreach($ret['listSeeMe'] as $k=>$v){
            if($v['uid'] == $myid){
                continue;
            }
			$ret['listSeeMe'][$k] = array();
			$userinfo = $this->get_user($v['uid']);
			
			$userinfo = $this->get_user_field($userinfo,$userfield);

			$userinfo['issayhello'] = $this->isSayHello($v['uid']);
			$ret['listSeeMe'][$k]['user'] =$userinfo;
			
		}

		$return = array();
		$userlist = $ret['listSeeMe'];
		$return["data"]["userlist"] = $userlist;
		$page["pagesize"] = $pageSize;
		$page["page"] = $pageNum;
		$page["totalcount"] = $ret["totalCount"];
		$return["data"]["page"] = $page;
		
		
		Push_data($return);
	}

	/**
	 * 删除访客
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int touid 访客id
	 * @request int type 访客类型  1  我访问的  2 访问我的
	 * @return data
	 */
	public function delvisitor()
	{
		$data = $this->Api_recive_date;
		$uid = $this->uid;

		//访客的id
		$touid = $data['touid'];

		if(empty($touid)){

			$return = [
				'code' => 4004,
				'message'  => "访客id不能为空"
			];
			Push_data($return);
		}


		$type = $data['type'] ? $data['type'] : 1;

		if($type == 1){
			//我看过谁
			$where['touid'] = $touid;
			$where['uid'] = $uid;

			$res = M('user_see')->where($where)->select();
			// dump($res);die;
			if(!empty($res)){

				$sql = " update t_user_see set se1 = 2 where id = ".$res[0]['id'];

				M()->execute($sql);

			}else{
				$return = [
					'code' => 4001,
					'message'  => "未找到该访客"
				];
				Push_data($return);
			}
			
		}elseif($type == 2){

			//我看过谁
			$where['touid'] = $uid;
			$where['uid'] = $touid;

			$res = M('user_see')->where($where)->select();

			if(!empty($res)){

				$sql = " update t_user_see set se2 = 2 where id = ".$res[0]['id'];

				M()->execute($sql);

			}else{
				$return = [
					'code' => 4002,
					'message'  => "未找到该访客"
				];
				Push_data($return);
			}

		}

		$wheres['se2'] = array('EQ',2);;
		$wheres['se1'] = array('EQ',2);;

		$UserSee = new UserSeeModel();
        
		$kk = $UserSee->getList($wheres);

		if(!empty($kk)){
			foreach ($kk as $key => $value) {
				$UserSee->delOne(array('id'=>$value['id']));
			}
		}
		$return = [
			'code' => 200,
			'message'  => "删除访客成功"
		];
				

		Push_data($return);


	}



	/**
	 * 获取用户信息
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request string touid 用户id
	 * @return data
	 */
	public function userinfo(){
		$data = $this->Api_recive_date;
        $platforminfo = $this->platforminfo;
		$myid = $this->uid;
		$Userinfo=$this->UserInfo;
		$otherId = $data['touid'];

		$Adddata = array();
		$Adddata['touid'] = $otherId;
		$Adddata['uid'] = $myid;
        //检测该用户是否被封号
        if($Userinfo['user_type']==8){
            $token = $data["token"];
            $redis = $this->redisconn();
            $redisStr = "token_" . $token;
            $redis->delete($redisStr);
            Push_data(array('message'=>$this->L("已经被封号处理"),'code'=>ERRORCODE_204));
        }

		//更新登录时间
        if($myid==$otherId){

            //用户登录时间记录
            $logintime = time();
            $this->set_user_field($myid,"logintime",$logintime);
            $User_baseM = new UserBaseModel();
            $User_baseM->updateOne(array("uid"=>$myid),array('logintime'=>$logintime));
            //登录保存uid,将用户存进在线redis中
            $OnlineCon = new OnlineController();
            $OnlineCon->setUserOnline($this->UserInfo);
        }

		$User_seeM = new UserSeeModel();
		$return = $User_seeM->getOne($Adddata);
		if($return){
			$ret = $User_seeM->updateOne($Adddata,array('count'=>$return['count']+1,'time'=>time()));
		}else{
			$Adddata['time'] = time();
			$ret = $User_seeM->addOne($Adddata);
		}

		$TouserInfo = $this->get_diy_user_field($otherId,"*");
		if($TouserInfo['uid']){
		    $TouserInfo['issayhello'] = $this->isSayHello($otherId);//是否打过招呼，1->已打招呼, 0->未打招呼,
		    $TouserInfo['isfollow'] = $this->isFollow($otherId);//是否关注 isFollow，1->已, 0->未
            $TouserInfo['isblack'] = $this->isBlack($otherId);//是否关注 isFollow，1->已, 0->未

			if($TouserInfo['gender'] == 2){//获取女生通话收取的金币
				//检查是否后台添加主播
				$compereinfo = new CompereInfoModel();
				$gold_conf = $compereinfo->getOne(array("uid"=>$TouserInfo['uid']));

				$TouserInfo['videoprice'] = $gold_conf['gold_conf']?intval($gold_conf['gold_conf']):10;
				$TouserInfo['audioprice'] = $gold_conf['gold_conf']?intval($gold_conf['gold_conf'])/2:5;
					//获得通话时长和当前可设置等级
					$time_grade = $gold_conf['talk_time']+$gold_conf['video_time'];
					if($time_grade == 0){
						$grade = 0;
					}elseif ($time_grade >0 && $time_grade<=30){
						$grade = 1;
					}elseif ($time_grade >30 && $time_grade<=90){
						$grade = 2;
					}elseif ($time_grade >90 && $time_grade<=180){
						$grade = 3;
					}elseif ($time_grade >180 && $time_grade<=270){
						$grade = 4;
					}elseif ($time_grade >270 && $time_grade<=360){
						$grade = 5;
					}elseif ($time_grade >360 && $time_grade<=450){
						$grade = 6;
					}elseif ($time_grade >450 && $time_grade<=540){
						$grade = 7;
					}elseif ($time_grade >540 && $time_grade<=630){
						$grade = 8;
					}elseif ($time_grade >630 && $time_grade<=720){
						$grade = 9;
					}elseif($time_grade>720){
						$grade = 10;
					}

				$TouserInfo['time_grade'] = $grade;

			}else{
				$TouserInfo['audioprice'] = 0;
				$TouserInfo['videoprice'] = 0;
				$TouserInfo['time_grade'] = 0;
			}
			//获取是否在线 随机 并保持一致存入redis
			$redis = $this->redisconn();
			$keyexitone=$redis->exists('onlinestatus_25100_'.$TouserInfo['uid']);
			if(!empty($keyexitone)){
				$TouserInfo['isonline_vod'] = $redis->get('onlinestatus_25100_'.$TouserInfo['uid']);
			}else{
				$ran = mt_rand(1,3);
				$TouserInfo['isonline_vod'] = $ran;
				$redis->set('onlinestatus_25100_'.$TouserInfo['uid'],$ran,0,0,60*60*24);
			}
            //设置用户在线状态start
            $cachename= "get_onlinestatus_".$TouserInfo['uid'];
            $cache=S($cachename);//设置缓存标识
            if(!$cache) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
                $rand=array('1','2');
                $rand_keys=array_rand($rand,1);
                $TouserInfo["isonline"]=$rand[$rand_keys];
                $cache= $rand[$rand_keys];
                S($cachename,$cache,60*60*2);//设置缓存的生存时间
            }
            $TouserInfo["isonline"]=$cache;
            //设置用户在线状态end
            if($TouserInfo["user_type"]==2){
                //检测用户是否设置了不让对方查看
                if($TouserInfo['giftgetcanbysee']==0){
                    $TouserInfo['receivedgifts'] ='';
                }else{
                    $TouserInfo['receivedgifts'] = $this->getliaopubgetlist($otherId,$product);//获取收到的礼物
                }
                $countryiso=$this->country;
            }else{
			    $GiftC = new GiftController();
                if($TouserInfo['giftgetcanbysee']==0){
                    $TouserInfo['receivedgifts'] ='';
                }else {
                    $TouserInfo['receivedgifts'] = $GiftC->pubgetlist($otherId,$product);//获取收到的礼物
                }
                $countryiso=$TouserInfo['country'];
			}

            //获取用户的国家和地区
            $product=$this->platforminfo['product'];
            $countryids=M('products')->where(array('product'=>$product))->getField('countryids');
            //44为中国id
            if($countryids==44){
                if($product==22210){
                    if($TouserInfo['user_type']==2){
                        $countryiso=M('region')->where(array('id'=>$TouserInfo['area'],'level'=>1,'country_codeiso'=>'TW'))->getField('code');
                    }else{
                        $countryiso=M('region')->where(array('id'=>$countryiso,'level'=>1,'country_codeiso'=>'TW'))->getField('code');
                        $regioncode=M('region')->where(array('id'=>$TouserInfo['area'],'level'=>2,'upper_region'=>$countryiso))->getField('code');
                    }
                }else{
                    if($TouserInfo['user_type']==2){
                        $countryiso=M('region')->where(array('id'=>$Userinfo['country'],'level'=>1,'country_codeiso'=>'CN'))->getField('code');
                        $regioncode=M('region')->where(array('id'=>$TouserInfo['area'],'level'=>2,'upper_region'=>$countryiso))->getField('code');
                    }else{
                        $countryiso=M('region')->where(array('id'=>$countryiso,'level'=>1,'country_codeiso'=>'CN'))->getField('code');
                        $regioncode=M('region')->where(array('id'=>$TouserInfo['area'],'level'=>2,'upper_region'=>$countryiso))->getField('code');

                    }
                }
            }else{
                $regionid=$TouserInfo['area'];
                $reg=$this->get_regionlist($countryiso);
                foreach($reg as $k=>$v){
                    if($v['id']==$regionid){
                        $regioncode=$v['name'];
                        break;
                    }
                }
            }

            //获取用户的魅力值start
//            $redis=$this->redisconn();
//            if($redis->exists('withdrawal_'.$myid)) {
//                $red = $redis->get('withdrawal_' . $myid);
//                $withd= json_decode($red, true);
//                $withdrawalmoney=$withd['totalmoney'];
//            }else{
                $res = M('m_money')->where(array('uid' => $myid))->find();
                if ($res) {
                    $ret = array();
                    $ret['totalmoney'] = intval($res['totalmoney']);
                    $ret['leftmoney'] = intval($res['leftmoney']);
                    $ret['realmoney'] = intval($res['leftmoney'] *0.03* 0.2);
                    $ret['username']=$res['commonpay'];
                    $ret['phonenum']=$res['otherpay'];
                    $ret['zhifubao']=$res['zhifubao'];
                    $ret['weixin']=$res['weixin'];
                    $ret['paypal']=$res['paypal'];
                }else {
                    $ret = array();
                    $ret['totalmoney'] = 0;
                    $ret['leftmoney'] = 0;
                    $ret['realmoney'] = 0;
                }
                //界面提示信息
                $ret['message']='提示：魅力值≥500才可进行提现。';
                //界面魅力值大小限制信息
                $ret['count_mes']=500;
//                $redisStr = "withdrawal_" . $otherId;
//                $withd = json_encode($ret);
//                $redis->set($redisStr, $withd, 0, 0, C("SESSION_TIMEOUT"));
                $withdrawalmoney=$ret['totalmoney'];
//            }
            //获取用户的魅力值end
            //获取用户昵称
            if($TouserInfo['isnickname']==2) {
                //昵称没有通过审核
                if ($TouserInfo['gender'] == 2) {
                    $TouserInfo['nickname'] = '女生';
                }
            }

            //获取用户的内心独白end

            //获取用户的内心独白start
            if($TouserInfo['mood_status_first']==3){
                //内心独白没有通过审核
                if($myid==$otherId){
                    if($TouserInfo['gender']==1){
                        //查看自己内心独白如果是男用户则覆盖mood,女用户则显示上传的mood
                        $TouserInfo['mood']=$TouserInfo['mood'];
                    }
                }else{
                    //查看别人的mood没有通过审核的情况
                    if($product==23110){
                        $TouserInfo['mood']='';
                    }else{
                        $TouserInfo['mood']='';
                    }
                }
            }
            elseif($TouserInfo['mood_status_first']==2){
                //内心独白正在审核中
                if($TouserInfo['gender']==2){
                    $TouserInfo['mood']='';
                }
            }

            //获取用户的内心独白end
            //获取用户的昵称
            //检查是否设置过备注
            $cachename1 = "remark-".$myid.'-'.$otherId;
            $cache1 = S($cachename1);//设置缓存标示
            if(!$cache1){
                $remark=M('remark')->where(array('uid'=>$myid,'touid'=>$otherId))->getField('value');
                if($remark){
                    S($cachename1, $remark, 60 * 60*24);//设置缓存的生存时间
                    $TouserInfo['remark']=$remark;
                }else{
                    $TouserInfo['remark']='';
                }
            }else{
                $remark=$cache1;
                $TouserInfo['remark']=$remark;
            }










			//用户获取自己的信息
			if($myid==$otherId){
			    $this->update_platforminfo($myid, $this->platforminfo);//更新用户版本号
			    
			    $TouserInfo["photos"] = $this->get_user_photo_all($otherId);
			    $TouserInfo["head"] = $this->get_user_ico_all($otherId);
                $privilege["askgift"]=0;
                $privilege["sendlocation"]=0;
			    $privilege["sendaudio"]=0;
			    $privilege["sendvideo"]=0;
			    $privilege["sendimage"]=0;
			    $privilege["diyposition"]=0;
			    $privilege["guanliyonghu"]=0;
			    $privilege["tuijian"]=0;
                $privilege["recall"]=0;//撤回
                $privilege["copy"]=0;//复制
                $privilege["checkurl"]=0;//查看网址
                $privilege["customcolour"]=0;//聊天框字体自定义颜色和字号
                $privilege["checkonline"]=0;//查看是否在线
                $privilege["checkcontact"]=0;//查看联系方式
			    switch ($TouserInfo["vipgrade"]){
			        case 1:
			            //黄金会员
                        $privilege["askgift"]=1;
                        $privilege["sendlocation"]=1;
			            break;
			        case 2:
			            //铂金会员
                        $privilege["askgift"]=1;
                        $privilege["sendlocation"]=1;
			            $privilege["sendaudio"]=1;
			            $privilege["sendimage"]=1;
                        $privilege["recall"]=1;//撤回
                        $privilege["copy"]=1;//复制
                        $privilege["checkurl"]=1;//查看网址
                        $privilege["customcolour"]=1;//聊天框字体自定义颜色和字号

			            break;
			        case 3:
			            //钻石会员
                        $privilege["askgift"]=1;
                        $privilege["sendlocation"]=1;
			            $privilege["sendaudio"]=1;
			            $privilege["sendvideo"]=1;
			            $privilege["sendimage"]=1;
                        $privilege["recall"]=1;//撤回
                        $privilege["copy"]=1;//复制
                        $privilege["checkurl"]=1;//查看网址
                        $privilege["customcolour"]=1;//聊天框字体自定义颜色和字号
                        $privilege["checkonline"]=1;//查看是否在线
                        $privilege["checkcontact"]=1;//查看联系方式
			            break;
			        default:
			            
			            break;
			    }
			    if($TouserInfo["user_type"]==3||$TouserInfo["user_type"]==9||$TouserInfo["user_type"]==10||$TouserInfo["user_type"]==11){
			        $TouserInfo["vipgrade"] = 3;
			        $privilege["diyposition"]=1;
			        $privilege["guanliyonghu"]=1;
			        $privilege["tuijian"]=1;
			        $privilege["tuijianurl"]="Sver/boyen/index?uid=";
			    }
			    $returnret["data"]['privilege'] = $privilege;
			}
			$returnret["data"]['user'] = $TouserInfo;
            $returnret["data"]['user']['country']=$this->L($countryiso);
            $returnret["data"]['user']['area']=$this->L($regioncode);
            if($returnret["data"]['user']['area']==''){
                $regioncode=M('region')->where(array('id'=>1,'level'=>2,'upper_region'=>$countryiso))->getField('code');
                $returnret["data"]['user']['area']=$this->L($regioncode);
            }
            $returnret["data"]['user']['totalmoney']=$withdrawalmoney;

		}else{
			$returnret=array();
			$returnret ['code'] = ERRORCODE_201;
			$returnret ['message'] = $this->L("YONGHUBUCUNZAI");
		}

		Push_data($returnret);
	}
	protected function get_rundarr_key($array){
	    $count = count($array);
	    $start = 3;
	    $end = $count-10;
	    $numrand = rand($start, $end);
	    $ids =array();
	    for($num=1;$num<=$numrand;$num++){
	        $key = array_rand($array);
	        $ids[]=$array[$key];
	        unset($array[$key]);
	        $array = array_merge($array);
	    }
	    return $ids;
	}
	//根据uid获取收到礼物列表
	public  function getliaopubgetlist($uid,$product){
	    
	    
	    $res=array();
	    $res['touid']=$uid;
	    $cachename = "GetGiftList"."to".$uid;
	    $cache=S($cachename);
	    if(!$cache){
	        //$product=10008;
	       // $path = CHCHEPATH_GIFTLIST;
	        //$cache_name = 'giftlist'.$product;
	        
	        //if(F($cache_name,'',$path)){
	            
	        //    $gids = F($cache_name,'',$path);
	        //}else{
	            
	            $getM = new GiftModel();
	            $Lists = $getM->getList(array("product"=>$product,"status"=>1));
	            $gids = array();
	            foreach ($Lists as $k=>$v){
	                $gids[] = $v["id"];
	            }
	           // F($cache_name,$gids,$path);
	        //}
	        $gids=$this->get_rundarr_key($gids);
	        $GiftListM = new GiftListModel();
            $PubC=new PublicController();
	        foreach($gids as $k=>$v){
	            $giftextend=$PubC->giftextend($v,$product,$this->platforminfo["language"]);
	            $result[$k]['url']= C("IMAGEURL").$giftextend['url'];
	            $result[$k]['price']=$giftextend['price'];
	            $result[$k]['title']=$giftextend['title'];
	            $result[$k]['id']=$v;
	            $result[$k]['count']=rand(2, 300);
	        }
	        $cache = $result;
	        S($cachename,$cache,60*60*24*3); //设置缓存的生存时间
	    }
	    $result=$cache;
	    return $result;
	}
	/*
	 * 获取我的信息 /space/myInfo
	 */
	public function wodexinxi(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$pushData = array();
		$pushData['user'] = $this->get_user($uid);
		$pushData['isSucceed'] = '1';
		$pushData ['msg'] = 'Succeed';
		//write_logs(json_encode($pushData));
		Push_data($pushData);
	}
	/*
	 *是否打招呼
	 */
	private function isSayHello($touid, $uid = 0)
	{
	    $uid = $uid ? $uid : $this->uid;
	    $redis = $this->redisconn ();
	    $redisStr = "sayhello_" . $uid . "_" . $touid;
	    if ($redis->exists ($redisStr)) {
	        return 1;
	    } else {
	        return 0;
	    }
	}
		/*
	 *是否关注 isFollow
	*/
	public function isFollow($touid,$uid=0,$reset=0){
		$uid = $uid ? $uid : $this->uid;
		if($touid&&$uid){
		    //$path = $this->get_userpath($uid, 'cache/follow/');
		    $cache_name = 'follow_'.$uid.$touid;
		    if($reset==1){
		        return S($cache_name,null);
		    }
		    
		    if(S($cache_name)){
		        $cacheValue = S($cache_name);
		    }else{
		        $UserFollowM = new UserFollowModel();
		        $return = $UserFollowM->getOne(array('touid'=>$touid,'uid'=>$uid));
		        
		        if($return){
		            $cacheValue = 1;
		        }else{
		            $cacheValue = 0;
		        }
                S($cache_name,$cacheValue,60*60*24);
		    }
		    
		    return $cacheValue;
		}else{
		  return 0;
		}
	}

	public function isBlack($touid){
	    $uid=$this->uid;
        $touids=M('blacklist')->where(array('uid'=>$uid))->getField('touids');
        if($touids){
            $touidlist=explode('|',$touids);
            if(in_array($touid,$touidlist)){
                return 1;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
	/**
	 * 关注
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int touid 要关注人的用户id
	 * @return data
	 */
	public function follow(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$Adddata = array();
		$Adddata['touid'] = $data['touid'];
		$Adddata['uid'] = $myid;
		
	
		/***
		 * 返回信息定义
		*/
		$RetArray = array();

		$RetArray['message'] = $this->L("YIGUANZHU");
	
		$UserFollowM = new UserFollowModel();
	
		$return = $UserFollowM->getOne($Adddata);
	
		if($return){
			$RetArray['code'] = ERRORCODE_201;
			$RetArray['message'] = $this->L("YIJINGGUANZHUGUOLE");
	
		}else{
			$Adddata['time'] = time();
			$ret = $UserFollowM->addOne($Adddata);
			$this->isFollow($Adddata['touid'],$Adddata['uid'],1);
		}
		Push_data($RetArray);
	}
	
	/**
	 * 取消关注
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int touid 要取消关注人的用户id
	 * @return data
	 */
	public function canfollow(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$Deldata = array();
		$Deldata['touid'] = $data['touid'];
		$Deldata['uid'] = $myid;
	
		/***
		 * 返回信息定义
		*/
		$RetArray = array();
		$RetArray['message'] = $this->L("CHENGGONG");
	
		$UserFollowM = new UserFollowModel();
	
	
		$return = $UserFollowM->delOne($Deldata);

		    $this->isFollow($Deldata['touid'],$Deldata['uid'],1);
	    
		Push_data($RetArray);
	}
    /**
	 * 我关注的人
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int page 当前页面
	 * @request string userfield 需要的用户字段默认 uid|head|nickname
	 * @return data
	 */
	public function getfollowlist(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";
		
		$pageNum = $data['page'] ? $data['page'] : 1;
		$pageSize = 15;
		
		$Wdata = array();
		$Wdata['uid'] = $uid;
		
		$UserFollowM = new UserFollowModel();
		
		
		$ret = $UserFollowM->getListPage($Wdata,$pageNum,$pageSize);
		rsort($ret['listUser']);
		foreach($ret['listUser'] as $k=>$v){

		    $tempUserInfo = $this->get_user($v['touid']);
		    if($tempUserInfo){
    		    $tempArray = $this->get_user_field($tempUserInfo,$userfield);
                //随机获取用户在线状态
                $field_arr = explode("|", $userfield);
                if(in_array('isonline',$field_arr)) {
                    //设置用户在线状态start
                    $cachename = "get_onlinestatus_" . $v['touid'];
                    $cache = S($cachename);//设置缓存标示
                    if (!$cache) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
                        $rand = array('1', '2');
                        $rand_keys = array_rand($rand, 1);
                        $TouserInfo["isonline"] = $rand[$rand_keys];
                        $cache = $rand[$rand_keys];
                        S($cachename, $cache, 60 * 60 * 2);//设置缓存的生存时间
                    }
                    $tempArray["isonline"] = $cache;
                    //设置用户在线状态end
                }
    			$ret['listUser'][$k] = array();
    			$ret['listUser'][$k] = $tempArray;
				$touids=M('blacklist')->where(array('uid'=>$uid))->getField('touids');
				$touidlist=array();
				if(!empty($touids)){
					$touidlist=explode('|',$touids);
					if(!empty($touidlist)){
						if (in_array($v['touid'], $touidlist)){
							unset($ret['listUser'][$k]);
						}
					}

				}

			}
			
		}
		sort($ret['listUser']);
		$return["data"]['userlist'] = $ret['listUser'];
		$return['message'] = $this->L("CHENGGONG");
		$page["pagesize"] = $pageSize;
		$page["page"] = $pageNum;
		$page["totalcount"] = $ret["totalCount"];
		$return["data"]["page"] = $page;
		Push_data($return);

	}

	/**
	 * 关注我的人
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int page 当前页面
	 * @request string userfield 需要的用户字段默认 uid|head|nickname
	 * @return data
	 */
	public function getfollowmelist(){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";
		
		$pageNum = $data['page'] ? $data['page'] : 1;
		$pageSize = 15;
		
		$Wdata = array();
		$Wdata['touid'] = $uid;
		
		$UserFollowM = new UserFollowModel();
		
		
		$ret = $UserFollowM->getListPage($Wdata,$pageNum,$pageSize);
	
		foreach($ret['listUser'] as $k=>$v){
			$tempUserInfo = $this->get_user($v['uid']);
			if($tempUserInfo){
			    $tempArray = $this->get_user_field($tempUserInfo,$userfield);
			    $ret['listUser'][$k] = array();
			    $ret['listUser'][$k] = $tempArray;
			}
			
		}
		$return["data"]['userlist'] = $ret['listUser'];
		$return['message'] = $this->L("CHENGGONG");
		$page["pagesize"] = $pageSize;
		$page["page"] = $pageNum;
		$page["totalcount"] = $ret["totalCount"];
		$return["data"]["page"] = $page;
		Push_data($return);

	}
	
	/**
	 * 拉黑
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int touid 要拉黑的用户id
	 * @return data
	 */
	public function dragblack(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$Adddata = array();
		$Adddata['touid'] = $data['touid'];
		$Adddata['uid'] = $myid;
		
	
		/***
		 * 返回信息定义
		*/
		$RetArray = array();
		$RetArray['message'] = $this->L("CHENGGONG");
	
		$UserDragblacklistM = new UserDragblacklistModel();
	
	
		$return = $UserDragblacklistM->getOne($Adddata);
	
		if($return){
			//$RetArray['isSucceed'] = 0;
			//$RetArray['msg'] = "已经关注过了";

		}else{
			$Adddata['time'] = time();
			$ret = $UserDragblacklistM->addOne($Adddata);
		}
		Push_data($RetArray);
	}
	
	/**
	 * 取消拉黑
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int touid 取消拉黑的用户id
	 * @return data
	 */
	public function candragblack(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$Deldata = array();
		$Deldata['touid'] = $data['touid'];
		$Deldata['uid'] = $myid;
	
		/***
		 * 返回信息定义
		*/
		$RetArray = array();
		$RetArray['message'] = $this->L("CHENGGONG");
	
		$UserDragblacklistM = new UserDragblacklistModel();
	
	
		$return = $UserDragblacklistM->delOne($Deldata);
	
		Push_data($RetArray);
	}
	
	/**
	 * 举报
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int touid 要举报的用户id
	 * @request string content 当举报选项为其他时必填当类型为敏感词检测时，填敏感词内容
	 * @request string id 举报内容选项id当类型为敏感词时，可不填）
	 * @request string status 1:正常举报2：敏感词检测
	 * @return data
	 */
	public function report(){
		$data = $this->Api_recive_date;
		$myid = $this->uid;
		$Adddata = array();
		$Adddata['touid'] = $data['touid'];
		$Adddata['uid'] = $myid;
		$Adddata['contentid'] = $data['id'];
		$Adddata['content'] = $data['content']?$data['content']:'';
        $Adddata['status'] = $data['status']?$data['status']:1;
		$Adddata['time'] = time();
        $UserReportModelM = new UserReportModel();
        $return = array();
	    if($data['status']==2){//敏感字检测
            $Adddata['touid'] = $this->uid;
            $ret = $UserReportModelM->addOne($Adddata);
        }elseif($data['status']==1){//举报
            $filetype="image";
            $reportid = $UserReportModelM->data($Adddata)->add();
            if (!empty($_FILES)) {
                $saveUrl = $this->msgfileurl . $filetype . "/";
                $savePath = WR . $saveUrl;
                $uploadList = $this->_upload($savePath);
                if (!empty($uploadList)) {
                        $obj = array();
                        $PutOssarr = array();
                        $ids = array();
                        $PhotoreportM = M('photo_report');
                        foreach ($uploadList as $k => $v) {
                            $Indata = array();
                            $Indata['reportid'] = $reportid;
                            $Indata['uid'] = $myid;
                            //$Indata['type'] = 1;//1截图
                            $Indata['url'] = $saveUrl . $v['savename'];
                            $ret = $PhotoreportM->data($Indata)->add();
                            $ids[] = $ret;
                            $PutOssarr[] = $Indata['url'];
                            $obj[] = array(
                                "id" => $ret,
                                "url" => C("IMAGEURL") . $Indata['url'],
                                "thumbnaillarge" => C("IMAGEURL") . $Indata['url'],
                                "thumbnailsmall" => C("IMAGEURL") . $Indata['url'],
                                "status" => 1,
                                "seetype" => 1
                            );
                        }
                        $ids = implode("|", $ids);

                    $UserReportModelM->where(array("id" => $reportid))->setField("imgurlids", $ids);
                        PutOss($PutOssarr);
                        //更改缓存
                        $return = array();
                    $report = array(
                        "id" => $reportid,
                        "content" => $Adddata['content'],
                        "status" => $data['status'],
                        "time" => $Adddata["time"],
                        "obj" => $obj,
                    );
                    $return['data']["report"]=$report;
                    }else{
                    $return['code'] = ERRORCODE_201;
                    $return['message'] = $this->L("SHANGCHUANSHIBAI");
                }
                }else{
                $report = array(
                    "id" => $reportid,
                    "content" => $Adddata['content'],
                    "status" => $data['status'],
                    "time" => $Adddata["time"],
                );
                $return['data']["report"]=$report;
            }
                }


		/***
		 * 返回信息定义
		*/

        $return['message'] = $this->L("CHENGGONG");
		Push_data($return);
	}
	/**
	 * 投诉、建议、给客服留言
	 *
	 * @request json platforminfo 客户端信息对象
	 * @request string token 用户token
	 * @request int type 类型1：投诉；2：建议3：给客服留言
	 * @request string content 内容
	 * @return data
	 */
	public function service(){
	    $data = $this->Api_recive_date;
	    $myid = $this->uid;
	    $type = $data['type'] ? $data['type'] :1;
	    $Adddata = array();
	    $Adddata['content'] = $data['content'];
	    $Adddata['type'] = $data['type'];
	    $Adddata['uid'] = $myid;
	    $Adddata['time'] = time();
	    
	    /***
	     * 返回信息定义
	     */
	    $RetArray = array();
	    $RetArray['message'] = $this->L("CHENGGONG");
	    $LetterServiceM = new LetterServiceModel();
	     $ret = $LetterServiceM->addOne($Adddata);
	    Push_data($RetArray);
	}
    //从缓存中取出地区信息
    protected function get_regionlist($id,$reset=0){
        $redis=$this->redisconn();
        //缓存中储存总数据
        $lang=$redis->listSize('regionlist_'.$id);
        //如果缓存中不存在，则取数据库最新数据存入
        if($lang==0){
            $where = array("country_codeiso"=>$id,"level"=>1);
            $RegionM = new RegionModel();
            $res = $RegionM->getList($where);
            foreach($res as $k=>$v){
                $res[$k] = array(
                    "id"=>$v["id"],
                    "name"=>$v["code"]
                );

                $value=json_encode($res[$k],true);
                $ret[]=$redis->listPush('regionlist'.$id,$value);
            }
            $result1=$res;
            //F($cache_name,$res,$path);
        }else{
            $result=$redis->listLrange('regionlist'.$id,0,-1);
            if ($result) {
                foreach ($result as $k1 => $v1) {
                    if($result[$k1]==null){
                        continue;
                    }
                    $result1[$k1]=json_decode($v1,true);
                }
            }
        }
        return $result1;
    }

    //上传图片
    // 文件上传
    protected function _upload($savePath) {
        import("Api.lib.Behavior.fileDirUtil");
        $fileutil = new fileDirUtil();
        $fileutil->createDir($savePath);
        import("Api.lib.Behavior.UploadFile");
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize = 5242880;
        //$upload->maxSize = 6291456;
        //设置上传文件类型
        $upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
        //设置附件上传目录
        $upload->savePath = $savePath;
        //设置需要生成缩略图，仅对图像文件有效
        $upload->thumb = false;
        // 设置引用图片类库包路径
        $upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix = 'm_';  //生产2张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth = '400';
        //设置缩略图最大高度
        $upload->thumbMaxHeight = '400';
        //设置上传文件规则
        $upload->saveRule = "md5content";
        //删除原图
        $upload->thumbRemoveOrigin = false;
        if (!$upload->upload()) {
            $return = array();
            $return['code'] = ERRORCODE_501;
            $return['message'] = $upload->getErrorMsg();
            Push_data($return);
            //捕获上传异常
            //$this->error($upload->getErrorMsg());
        } else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo();
            return $uploadList;;
        }

    }

    //所推荐人员成功注册人员显示
    public function recommend(){
        $data = $this->Api_recive_date;
        $userfile=$data['userfield']?$data['userfield']:'uid|head|nickname';
	    $userinfo=$this->UserInfo;
	    $recommenduids=$userinfo['shareuid'];
	    $restcount=$userinfo['restsharecount'];
	    $userlist=array();
	    $totalcount=0;
        $pageNum = $data['page'] ? $data['page'] : 1;
        $pageSize =$data['pagesize'] ? $data['pagesize'] : 15;
	    if($recommenduids!=''||$recommenduids!=null){
            $shareuids= explode(',', $recommenduids);
            $totalcount=count($shareuids);
            $shareuidsa= array_slice($shareuids, ($pageNum - 1) * $pageSize, $pageSize);
            foreach($shareuids as $k=>$v){
                $shareuserinfo=$this->get_user($v);
                $userlist[$k]=$this->get_user_field($shareuserinfo,$userfile);
            }
        }
        //获取随机推荐语
        $randomword=array();
        $allrecommendword=$this->get_recommendword();
	    foreach($allrecommendword as $k=>$v){
            $randomword[$k]=$v['randomword'];
        }
	    //添加会员天数规则
        $rulelist=array();
        $rulelist[0]=array(
            'type'=>1,
            'usernumber'=>'30',
            'day'=>'3'
        );
        $rulelist[1]=array(
            'type'=>2,
            'usernumber'=>'50',
            'day'=>'10'
        );
        $rulelist[2]=array(
            'type'=>3,
            'usernumber'=>'80',
            'day'=>'20'
        );
        $rulelist[3]=array(
            'type'=>4,
            'usernumber'=>'100',
            'day'=>'30'
        );

        $return["data"]['userlist'] = $userlist;
        $return["data"]['rulelist'] = $rulelist;
        $return["data"]['count'] = $totalcount;
        $return["data"]['restcount'] = $restcount;
        $return["data"]['randomword'] = $randomword;

        $return['message'] = $this->L("CHENGGONG");
        $page["pagesize"] = $pageSize;
        $page["page"] = $pageNum;
        $page["totalcount"] = $totalcount;
        $return["data"]["page"] = $page;
        Push_data($return);

    }

    public function get_recommendword(){
        $redis=$this->redisconn();
        //缓存中储存总数据
        $lang=$redis->listSize('recommend-word');
        //如果缓存中不存在，则取数据库最新数据存入
        $res=array();
        if($lang==0){
            $recommendword=M('recommend_word')->where(array('isopen'=>1))->select();
            foreach($recommendword as $k=>$v){
                $res[$k] = array(
                    "id"=>$v["id"],
                    "randomword"=>$v["randomword"],
                );
                $value=json_encode($res[$k],true);
                $ret[]=$redis->listPush('recommend-word',$value);
            }
            $result1=$res;
        }else{
            $result=$redis->listLrange('recommend-word',0,-1);
            if ($result) {
                foreach ($result as $k1 => $v1) {
                    $result1[$k1]=json_decode($v1,true);
                }
            }
        }
        return $result1;

    }

    public function recommendcheck(){
        $data = $this->Api_recive_date;
        $userinfo=$this->UserInfo;
        $numberday=$data['numberday'];
        $count=$data['count'];
        $restcount=$userinfo['restsharecount']-$count;
        if($restcount>=0){
            if($userinfo['viptime']<time()){
                $realviptime=time()+$numberday*24*60*60;
            }else{
                $realviptime=$userinfo['viptime']+$numberday*24*60*60;
            }

            $uid=$this->uid;
            $this->set_user_field($uid,'restsharecount',$restcount);
            M('user_extend')->where(array('uid'=>$uid))->setField('restsharecount',$restcount);
            $this->set_user_field($uid,'viptime',$realviptime);
            $res=M('user_base')->where(array('uid'=>$uid))->setField('viptime',$realviptime);
            if($res){
                $return['message'] = $this->L("CHENGGONG");
                $return["data"]["restcount"] =$restcount ;
                Push_data($return);
            }else{
                $return['code'] = ERRORCODE_201;
                $return['message'] = "参数错误";
                Push_data($return);
            }
        }else{
            $return['code'] = ERRORCODE_201;
            $return['message'] = "参数错误";
            Push_data($return);
        }



    }
    //用户申请成为推广者
    public function generalize(){
	    $uid=$this->uid;
	    //更改用户类型为5
        $data=M('user_base')->where(array('uid'=>$uid))->setField('user_type',5);
        //更新缓存中的用户类型为5
        if($data){
            $this->set_user_field($uid,'user_type',5);
            $return['message'] = $this->L("CHENGGONG");
            Push_data($return);
        }else{
            $return['code'] = ERRORCODE_201;
            $return['message'] = "参数错误";
            Push_data($return);
        }
    }

    //设置备注
    public function setremark(){
        $data = $this->Api_recive_date;
        $uid=$this->uid;
        $touid=$data['touid'];
        $value=$data['value'];
        //检查是否设置过备注
        $remark=M('remark')->where(array('uid'=>$uid,'touid'=>$touid))->getField('value');
        $cachename1 = "remark-".$uid.'-'.$touid;
        if($remark){
            //更新操作
            $result=M('remark')->where(array('uid'=>$uid,'touid'=>$touid))->setField('value',$value);
            //删除缓存
            S($cachename1,null);
            S($cachename1,$value,60*60*24);
        }else{
            //增加操作
            $result=M('remark')->data(array('uid'=>$uid,'touid'=>$touid,'value'=>$value))->add();
            S($cachename1,$value,60*60*24);
        }
        if($result){
            $return['message'] = $this->L("CHENGGONG");
            Push_data($return);
        }else{
            $return['code'] = ERRORCODE_201;
            $return['message'] = "参数错误";
            Push_data($return);
        }
    }
    //取消备注
    public function delremark(){
        $data = $this->Api_recive_date;
        $uid=$this->uid;
        $touid=$data['touid'];
        $remark=M('remark')->where(array('uid'=>$uid,'touid'=>$touid))->getField('value');
        $result=0;
        if($remark){
            $cachename1 = "remark-".$uid.'-'.$touid;
            //删除备注
            $result=M('remark')->where(array('uid'=>$uid,'touid'=>$touid))->delete();
            //删除缓存
            S($cachename1,null);
        }
        if($result){
            $return['message'] = $this->L("CHENGGONG");
            Push_data($return);
        }else{
            $return['code'] = ERRORCODE_201;
            $return['message'] = "参数错误";
            Push_data($return);
        }
    }

    public function addblacklist(){
        $data = $this->Api_recive_date;
        $uid=$this->uid;
        $touid=$data['touid'];
        $touids=M('blacklist')->where(array('uid'=>$uid))->getField('touids');
        if($touids){
            $touidlist=array();
            $touidlist=explode('|',$touids);
            if(in_array($touid,$touidlist)){
                $return['message'] = $this->L("已经添加过了");
                Push_data($return);
            }else{
                array_push($touidlist,$touid);
                $touids=implode("|",$touidlist);
               $data=M('blacklist')->where(array('uid'=>$uid))->setField('touids',$touids);
            }
        }else{
            $datalist=array();
            $datalist['uid']=$uid;
            $datalist['touids']=$touid;
            $data=M('blacklist')->data($datalist)->add();
        }
        if($data){
            $return['message'] = $this->L("CHENGGONG");
            Push_data($return);
        }else{
            $return['code'] = ERRORCODE_201;
            $return['message'] = "参数错误";
            Push_data($return);
        }
    }
    //黑名单列表
    public function  blacklist(){
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";
        $pageNum = $data['page'] ? $data['page'] : 1;
        $pageSize = 15;
        $Wdata = array();
        $Wdata['uid'] = $uid;
        $touids=M('blacklist')->where(array('uid'=>$uid))->getField('touids');
        $touidlist=array();
        if($touids){
            $touidlist=explode('|',$touids);
            $touidlist = array_slice($touidlist, ($pageNum - 1) * $pageSize, $pageSize);
            foreach($touidlist as $k=>$v){
                $tempUserInfo = $this->get_user($v);
                if($tempUserInfo){
                    $tempArray = $this->get_user_field($tempUserInfo,$userfield);
                    $ret['listUser'][$k] = array();
                    $ret['listUser'][$k] = $tempArray;
                }
            }
        }
		rsort($ret['listUser']);
        $return["data"]['userlist'] = $ret['listUser'];
        $return['message'] = $this->L("CHENGGONG");
        $page["pagesize"] = $pageSize;
        $page["page"] = $pageNum;
        $page["totalcount"] = $ret["totalCount"];
        $return["data"]["page"] = $page;
        Push_data($return);
    }

    //移除黑名单
    public function removeblacklist(){
        $data = $this->Api_recive_date;
        $uid=$this->uid;
        $touid=$data['touid'];
        $touids=M('blacklist')->where(array('uid'=>$uid))->getField('touids');
        if($touids){
            $touidlist=array();
            $touidlist=explode('|',$touids);
            if(count($touidlist)==1){
                M('blacklist')->where(array('uid'=>$uid))->delete();
                $return['message'] = $this->L("CHENGGONG");
                Push_data($return);
            }else{
                if(in_array($touid,$touidlist)){
                    $key = array_search($touid, $touidlist);
                    if ($key !== false){
                        array_splice($touidlist, $key, 1);
                        $touids=implode("|",$touidlist);
                        $data=M('blacklist')->where(array('uid'=>$uid))->setField('touids',$touids);
                        $return['message'] = $this->L("CHENGGONG");
                        Push_data($return);
                    }
                }else{
                    array_push($touidlist,$touid);
                    $touids=implode("|",$touidlist);
                    $data=M('blacklist')->where(array('uid'=>$uid))->setField('touids',$touids);
                }
            }
        }

        $return['message'] = $this->L("未添加黑名单");
            Push_data($return);

    }


}

?>

