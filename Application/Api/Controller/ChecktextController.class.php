<?php
include_once   WR.'/lib/aliyuncs/aliyun-php-sdk-core/Config.php';
use Green\Request\V20180509 as Green;

class ChecktextController extends BaseController
{

    public function checktext($text, $scenes)
    {
        //请替换成您的accessKeyId、accessKeySecret
        $iClientProfile = DefaultProfile::getProfile("cn-shanghai", "LTAIR8tjvGtclDfZ", "gMJGibkaW2t44UnflpauLK7rADmdWB");
        DefaultProfile::addEndpoint("cn-shanghai", "cn-shanghai", "Green", "green.cn-shanghai.aliyuncs.com");
        $client = new DefaultAcsClient($iClientProfile);
        $request = new Green\TextScanRequest();
        $request->setMethod("POST");
        $request->setAcceptFormat("JSON");
        $task1 = array('dataId' => uniqid(),
            'content' => $text
        );

        /**
         * 文本垃圾检测： antispam
         **/
        $request->setContent(json_encode(array("tasks" => array($task1),
            "scenes" => $scenes)));
        try {
            $response = $client->getAcsResponse($request);
            if (200 == $response->code) {
                $taskResults = $response->data;
                foreach ($taskResults as $taskResult) {
                    $data=array();
                    if (200 == $taskResult->code) {
                        $sceneResults = $taskResult->results;
                        foreach ($sceneResults as $sceneResult) {
                            $scene = $sceneResult->scene;
                            $suggestion = $sceneResult->suggestion;
                            //根据scene和suggetion做相关处理
                            //do something
                            if($scene=='antispam'){
                                $data['antispam']=$suggestion;
                            }
                        }
                        return $data;
                    } else {
                        print_r("task process fail:" + $response->code);
                    }
                }
            } else {
                print_r("detect not success. code:" + $response->code);
            }
        } catch (Exception $e) {
            print_r($e);
        }
    }
}

