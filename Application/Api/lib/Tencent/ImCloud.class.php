<?php
    class ImCloud {
        private $private_key = false;
        private $public_key = false;
        private $appid = 1400128183;
        public function __construct($appid=0){
            if($appid){
                $this->setAppid($appid);
            }
        }
        /**
         * 设置Appid
         * @param type $appid
         */
        public function setAppid($appid) {
            $this->appid = $appid;
        }
        /*
         * 生成key
         */
       public function signature($uid)
        {
            $sdkappid = $this->appid;
            $private_key_path=WR."/Public/key/ImCloud_private_key";
            # 这里需要写绝对路径，开发者根据自己的路径进行调整
            $command = '/var/www/signature'
                . ' ' . escapeshellarg($private_key_path)
                . ' ' . escapeshellarg($sdkappid)
                . ' ' . escapeshellarg($uid);
                //echo $command;
                $ret = exec($command, $out, $status);
                if ($status == -1)
                {
                    return null;
                }
                return $out;
        }
}