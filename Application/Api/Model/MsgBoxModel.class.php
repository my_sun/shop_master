<?php
use Think\Model\AdvModel;
use Think\Model;
class MsgBoxModel extends AdvModel {
	Protected $autoCheckFields = false;  //一定要关闭字段缓存，不然会报找不到表的错误
	protected $partition = array(
			'field' => 'id',// 要分表的字段 通常数据会根据某个字段的值按照规则进行分表,我们这里按照用户的id进行分表
			'type' => 'mod',// 分表的规则 包括id year mod md5 函数 和首字母，此处选择mod（求余）的方式
			'expr' => '',// 分表辅助表达式 可选 配合不同的分表规则，这个参数没有深入研究
			'num' => '20',// 分表的数目 可选 实际分表的数量，在建表阶段就要确定好数量，后期不能增减表的数量
	);
	
	public function __construct(){
		parent::__construct();
		//$this->copy_table('t_msg_box',20);
		//$this->alter_table('t_msg_box',20);
		
	}
     /**
     * 计算在哪张表
     * @param array $data
     * @return \Think\Model
     */
   public function computeTable($data){
      $data = empty($data) ? $_POST : $data;
      $table = $this->getPartitionTableName($data);
      return $this->table($table);
   }
    /**
     * 添加一条数据
     * @param array $data
     * @return bool|int
     */
    public function addOne1($data){
        if($this->create($data)){
            $id = $this->add();
            if($id === false){
                $this->error = '插入数据错误';
                return false;
            }else{
                return $id;
            }
        }
        return false;
    }
   /**
    * 添加一条数据
    * @param array $data
    * @return bool|int
    */
   public function addOne($data){
   	if(empty($data[$this->partition['field']])){
   		//E('缺少' . $this->partition['field']);
   	}
   	
   	$data['id'] = intval($this->computeTable()->max('id')) + 1;
   	if($this->create($data)){
   		$id = $this->computeTable($data)->add();
   		if($id === false){
   			$this->error = '插入数据错误';
   			return false;
   		}else{
   			return $data['id'];
   		}
   	}
   	return false;
   }

   /**
    * 获取所有记录
    */
   public function getAll(){
   	return $this->computeTable()->select();
   }
   /**
    * 条件查询列表
    * @param $map
    * @return mixed
    */
   public function getList($where,$map=array()){
   	return $this->computeTable($map)->where($where)->select();
   }
   /**
    * 条件查询列表分页
    * @param $map
    * @return mixed
    */
   public function getListPage($where,$pageNum,$pageSize,$map=array()){
   	import("Extend.Library.ORG.Util.Page");       //导入分页类
   	$count  = $this->computeTable($map)->where($where)->count();    //计算总数
   	$Page   = new Page($count,$pageSize,$pageNum);
   //	Dump($Page);exit;
   	$data = $this->computeTable($map)->where($where)->order('time desc')->limit($Page->firstRow. ',' . $Page->listRows)->select();
	$retArray = array();
	$retArray['totalCount'] = $count;
	$retArray['pageSize'] = $pageSize;
	$retArray['pageNum'] = $pageNum;
	$retArray['listMsgBox'] = $data;
	return $retArray;
	
   }
    public function getListPage1($map,$pageNum,$pageSize){
        import("Extend.Library.ORG.Util.Page");       //导入分页类
        $count  = $this->where($map)->count();    //计算总数
        $Page = new Page($count,$pageSize,$pageNum);
        //	Dump($Page);exit;
        $data = $this->where($map)->order('id desc')->limit($Page->firstRow. ',' . $Page->listRows)->select();
        $retArray['totalCount'] = $count;
        $retArray['msgbox'] = $data;
        return $retArray;
    }
   /**
    * 搜索用户
    * @param $map
    * @return mixed
    */
   public function getlistSearch($where,$pageNum,$pageSize,$map=array()){
   	import("Extend.Library.ORG.Util.Page");       //导入分页类
   	$count  = $this->computeTable($map)->where($where)->count();    //计算总数
   	$Page   = new Page($count,$pageSize,$pageNum);
   	//	Dump($Page);exit;
   	$data = $this->computeTable($map)->where($where)->limit($Page->firstRow. ',' . $Page->listRows)->select();
   	$retArray = array();
   	$retArray['totalCount'] = $count;
   	$retArray['pageSize'] = $pageSize;
   	$retArray['pageNum'] = $pageNum;
   	$retArray['listSearch'] = $data;
   	return $retArray;
   
   }
   /**
    * 附近的人
    * @param $map
    * @return mixed
    */
   public function GetNearbyUser($where,$pageNum,$pageSize,$map=array()){
   	import("Extend.Library.ORG.Util.Page");       //导入分页类
   	$count  = $this->computeTable($map)->where($where)->count();    //计算总数
   	$Page   = new Page($count,$pageSize,$pageNum);
   	//	Dump($Page);exit;
   	$data = $this->computeTable($map)->where($where)->limit($Page->firstRow. ',' . $Page->listRows)->select();
   	$retArray = array();
   	$retArray['totalCount'] = $count;
   	$retArray['pageSize'] = $pageSize;
   	$retArray['pageNum'] = $pageNum;
   	$retArray['listUser'] = $data;
   	return $retArray;
   	 
   }
   /**
    * 根据查询条件获取一条记录
    * @param $map
    * @return mixed
    */
   public function getOne($map){
   	return $this->computeTable($map)->where($map)->find();
   }
    public function getOne1($map){
        $res = $this->where($map)->find();
        if($res === false){
            return $res;

        }else{
            return $res;   //更新的数据条数
        }
    }
   
   /**
    * 更新一条记录
    * @param $map
    * @param $data
    * @return bool
    */
   public function updateOne($where, $data){
   	if($this->create($data)){
   		$res = $this->computeTable($where)->where($where)->save();
   		if($res === false){
   			$this->error = '更新数据出错';
   		}else{
   			return $res;   //更新的数据条数
   		}
   	}
   	return false;
   }
    public function updateOne1($map, $data){
        if($this->create($data)){
            $res = $this->where($map)->save();
            if($res === false){
                $this->error = '更新数据出错';
            }else{
                return $res;   //更新的数据条数
            }
        }
        return false;
    }
   public function updateNotId($where, $data){
		$temp = $this->getOne($where);
		$newwhere = array('id'=>$temp['id']);
   	return $this->updateOne($newwhere, $data);
   }
   /**
    * 删除一条记录
    * @param $map
    * @return bool|mixed
    */
   public function delOne($map){
   	$res = $this->computeTable($map)->where($map)->delete();
   	if($res === false){
   		$this->error = '删除数据出错';
   		return false;
   	}else{
   		return $res;   //删除数据个数
   	}
   }
   
   /*
    *
   * 复制表
   */
   public function copy_table($table_name,$number){
   	for ($x=1; $x<=$number; $x++) {
   		$sql="create  table  ".$table_name."_".$x." like ".$table_name.";";
   		M()->execute($sql);
   	}
   	return true;
   }
   /*
    *
   * 给数据表添加字段
   */
   public function alter_table($table_name,$number){
   	for ($x=1; $x<=$number; $x++) {
   		//$sql = "ALTER TABLE ".$table_name."_".$x." ADD user_reply_time int(11) DEFAULT 0;";
   		//$sql = "ALTER TABLE ".$table_name."_".$x." ADD sys_reply_time int(11) DEFAULT 0;";
   		M()->execute($sql);
   	}
   	return true;
   }
}
?>