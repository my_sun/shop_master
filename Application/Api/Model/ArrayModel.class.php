<?php
class ArrayModel extends BaseModel {
	protected $partition = array(
		/**血型**/
		"blood" => array(
			array('id'=>'1' ,'code'=> "A"),
			array('id'=>'2' ,'code'=> "B"),
			array('id'=>'3' ,'code'=> "AB"),
			array('id'=>'4' ,'code'=> "O")
		),
		/**学历**/
		"education" => array(
				array('id'=>'1' , 'code'=> "Guozhongyixia"),
				array('id'=>'2' , 'code'=> "Gaozhong"),
				array('id'=>'3' , 'code'=> "Zhuanke"),
				array('id'=>'4' , 'code'=> "Daxue"),
				array('id'=>'5' , 'code'=> "Shuoshiyishang")
		),
		/**收入**/
		"income" => array(
				array('id'=>'1','code' => "<20000"),
				array('id'=>'2','code' => "20000-40000"),
				array('id'=>'3','code' => "40000-60000"),
				array('id'=>'4','code' => "60000-100000"),
				array('id'=>'5','code' => ">100000")
		),
		/**兴趣爱好**/
		"hobby" => array(
				array('id'=>'1','code' => "Shangwang"),
				array('id'=>'2','code' => "Yanjiuqiche"),
				array('id'=>'3','code' => "Yangxiaodongwu"),
				array('id'=>'4','code' => "Sheying"),
				array('id'=>'5','code' => "Kandianying"),
				array('id'=>'6','code' => "Tingyinyue"),
				array('id'=>'7','code' => "Xiezuo"),
				array('id'=>'8','code' => "Gouwu"),
				array('id'=>'9','code' => "Zuoshougongyi"),
				array('id'=>'10','code' => "Zuoyuanyi"),
				array('id'=>'11','code' => "Tiaowu"),
				array('id'=>'12','code' => "Kanzhanlan"),
				array('id'=>'13','code' => "Pengren"),
				array('id'=>'14','code' => "Dushu"),
				array('id'=>'15','code' => "Huihua"),
				array('id'=>'16','code' => "Yanjiujisuanji"),
				array('id'=>'17','code' => "Zuoyundong"),
				array('id'=>'18','code' => "Lvyou"),
				array('id'=>'19','code' => "Wanyouoxi"),
				array('id'=>'20','code' => "Qita")
		),
		/**婚姻状况**/
		"marriage" => array(
				array('id'=>'1','code' => "Weihun"),
				array('id'=>'2','code' => "Liyi"),
				array('id'=>'3','code' => "Sangou")
		),
		/**星座**/
		"constellation" => array(
				array('id'=>'1','code' => "Baiyangzuo"),
				array('id'=>'2','code' => "Jinniuzuo"),
				array('id'=>'3','code' => "Shuangzizuo"),
				array('id'=>'4','code' => "Juxiezuo"),
				array('id'=>'5','code' => "Shizizuo"),
				array('id'=>'6','code' => "Chunvzuo"),
				array('id'=>'7','code' => "Tianpingzuo"),
				array('id'=>'8','code' => "Tianxiezuo"),
				array('id'=>'9','code' => "Sheshouzuo"),
				array('id'=>'10','code' => "Mojiezuo"),
				array('id'=>'11','code' => "Shuipingzuo"),
				array('id'=>'12','code' => "Shuangyuzuo")
		),
		/**是否想要孩子**/
		"wantchild" => array(
				array('id'=>'1','code' => "Xiang"),
				array('id'=>'2','code' => "Buxiang"),
				array('id'=>'3','code' => "Haimeixianghao")
		),
		/**工作状况**/
		"work" => array(
				array('id'=>'1','code' => "Zaixuexiao"),
				array('id'=>'2','code' => "Falv"),
				array('id'=>'3','code' => "IT/Hulianwang"),
				array('id'=>'4','code' => "Yule"),
				array('id'=>'5','code' => "Jinrong"),
				array('id'=>'6','code' => "Zhengfujiguan"),
				array('id'=>'7','code' => "Xiezuo"),
				array('id'=>'8','code' => "Wenhua/Yishu"),
				array('id'=>'9','code' => "Yingshi"),
				array('id'=>'10','code' => "Fangdichan/Jianzhu"),
				array('id'=>'11','code' => "Jixie"),
				array('id'=>'12','code' => "Nengyuan/Huanbao"),
				array('id'=>'13','code' => "Yiliao/Jiankang"),
				array('id'=>'14','code' => "Jiaoyu/Kejian"),
				array('id'=>'15','code' => "Qita")
		),
		/**交友目的**/
		"friendsfor" => array(
				array('code' => "jiehun",'name'=>'结婚'),
				array('code' => "lvyou",'name'=>'旅游'),
				array('code' => "qita",'name'=>'其他'),
		),
		/**婚前同居**/
		"cohabitation" => array(
				array('code' => "bujieyi",'name'=>'不介意'),
				array('code' => "jujue",'name'=>'拒绝'),
				array('code' => "qita",'name'=>'其他'),
		),
		/**期望约会的地方**/
		"dateplace" => array(
				array('code' => "gongyuan",'name'=>'公园'),
				array('code' => "shatan",'name'=>'沙滩'),
				array('code' => "jiudian",'name'=>'酒店'),
				array('code' => "qita",'name'=>'其他'),
		),
		/**恋爱次数**/
		"lovetimes" => array(
				array('code' => "yici",'name'=>'1次'),
				array('code' => "liangci",'name'=>'2次'),
				array('code' => "sanci",'name'=>'3次'),
				array('code' => "sanciyishang",'name'=>'3次以上'),
		),
		/**性格类型(多选,用|分开)**/
		"charactertype" => array(
				array('code' => "wenrou",'name'=>'温柔'),
				array('code' => "gengzhi",'name'=>'耿直'),
				array('code' => "jujueduanli",'name'=>'具决断力'),
				array('code' => "wenzhong",'name'=>'稳重'),
				array('code' => "pingyijinren",'name'=>'平易近人'),
				array('code' => "huopokailang",'name'=>'活泼开朗'),
				array('code' => "xihuanshineihuodong",'name'=>'喜欢室内活动'),
				array('code' => "xihuanhuwaihuodong",'name'=>'喜欢户外活动'),
				array('code' => "renzhen",'name'=>'认真'),
				array('code' => "zhixing",'name'=>'知性'),
				array('code' => "chengshi",'name'=>'诚实'),
				array('code' => "yisibugou",'name'=>'一丝不苟'),
				array('code' => "leguan",'name'=>'乐观'),
				array('code' => "haixiu",'name'=>'害羞'),
				array('code' => "zongdaizheweixiao",'name'=>'总带着微笑'),
				array('code' => "gaoya",'name'=>'高雅'),
				array('code' => "chenwen",'name'=>'沉稳'),
				array('code' => "dafang",'name'=>'大方'),
		),
		/**是否有房**/
		"house" => array(
				array('code' => "you",'name'=>'有'),
				array('code' => "wu",'name'=>'无'),
		),
		/**是否有车**/
		"car" => array(
				array('code' => "you",'name'=>'有'),
				array('code' => "wu",'name'=>'无'),
		),
		/**非法字符替换**/
		"not_str" => array(
				"perdate",
				"找老外",
				"懶sangyv",
				"perdeat",
				"saymeet",
				"外緣",
				"外缘"
		),
		/**非法字符替换的内容**/
		"yes_str" => array(
				"“******”"
		)
	);
	
	public function __construct(){
		parent::__construct();
		
		//$this->copy_table('t_user_base',20);
	}
    public function get_var($key1="",$key2=""){
    	if($key1){
    		return $this->partition[$key1];
    	}elseif($key2){
    		return $this->partition[$key1]['$key2'];
    	}else{
    		return $this->partition;
    	}
    }
}
?>