<?php

class MemberController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//会员充值金额列表
	public function lists(){
		$this->name = '会员列表'; // 进行模板变量赋值

		$MemberM = new MemberModel();
//		$where = array();
//		$where['is_del'] = 1;
		$MemberInfo = $MemberM->getAll();

		$this->MemberInfo = $MemberInfo;
		$this->action = "/Adms/Member/lists.html";
		$this->display("lists");
	}

	//添加会员充值金额
	public function add(){
		$this->name = '添加会员充值金额';
		if($_POST){
				$MemberM = new MemberModel();
				$map = array();
				$map["money"] = $_POST["money"];
				$map["reward_money"] = $_POST["reward_money"];
				$map["addtime"] = time();
				$MemberM->addOne($map);
				header('Location:lists.html');
			}
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//修改会员充值金额
	public function edit(){
		$this->name = '修改会员充值金额';  // 进行模板变量赋值

		//获取id
		$id = $_GET['id'];
		if ($id){
			$MemberM = new MemberModel();
			$where['id'] = $id;
			//查数据
			$MemberInfo = $MemberM->getOne($where);
		}
		$this->MemberInfo = $MemberInfo;
		$this->id = $id;
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//处理修改
	public function update(){
		$this->name = '修改会员充值金额';  // 进行模板变量赋值

		$id = $_POST['id'];
		$where = array();
		$MemberM = new MemberModel();
		if ($id){
			$data['money'] = $_POST['money'];
			$data['reward_money'] = $_POST['reward_money'];
			$where["id"] = $_POST['id'];
			$res = $MemberM->updateOne($where,$data);
			if ($res){
				$this->tip = "修改成功";
			}
			header('Location:lists.html');
		}
	}

	//删除会员充值金额
	 public function del(){
	 	$this->name = '删除会员充值金额';

	 	if($_GET["id"]){
			$MemberM = new MemberModel();
	 		$ret = $MemberM->delOne(array("id"=>$_GET["id"]));
	 		header("Location:/Adms/Member/lists");
	 	}
	 }


}

?>
