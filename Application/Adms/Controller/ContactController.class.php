<?php

class ContactController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}

	//联系我们列表
	public function lists(){
		$this->name = '联系我们列表'; // 进行模板变量赋值

		$WhereArr = array();
		$WhereArr['is_del'] = 1;
        $ContactM = new ContactModel();
		$ContactInfo = $ContactM->getList($WhereArr);
		$this->ContactInfo = $ContactInfo;
		$this->get = $_GET;
		$this->action = "/Adms/Contact/lists.html";
		$this->display("lists");
	}

	//添加联系我们
	public function add(){
		$this->name = '添加联系我们';
		if($_POST){
				$ContactM = new ContactModel();
				$map = array();
				$map["phone"] = $_POST["phone"];
				$map["email"] = $_POST["email"];
				$map["addtime"] = time();
				$ContactM->addOne($map);
				header('Location:lists.html');
			}
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//修改联系我们
	public function edit(){
		$this->name = '修改联系我们';  // 进行模板变量赋值

		//获取id
		$id = $_GET['id'];
		if ($id){
			$ContactM = new ContactModel();
			$where['id'] = $id;
			//查数据
			$ContactInfo = $ContactM->getOne($where);
		}
		$this->ContactInfo = $ContactInfo;
		$this->id = $id;
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//处理修改
	public function update(){
		$this->name = '修改联系我们';  // 进行模板变量赋值

		$id = $_POST['id'];
		$where = array();
		$ContactM = new ContactModel();
		if ($id){
			$data['phone'] = $_POST['phone'];
			$data['email'] = $_POST['email'];
			$where["id"] = $_POST['id'];
			$res = $ContactM->updateOne($where,$data);
			if ($res){
				$this->tip = "修改成功";
			}
			header('Location:lists.html');
		}
	}

	//软删除联系我们
	public function del(){
		$this->name = '删除联系我们';
		$ContactM = new ContactModel();
		$where = array();
		$where['id'] = $_GET['id'];
		if($_GET["id"]){
			$data['is_del'] = 0;
			$ret = $ContactM->updateOne($where,$data);
			header("Location:/Adms/Contact/lists");
		}
	}

}

?>
