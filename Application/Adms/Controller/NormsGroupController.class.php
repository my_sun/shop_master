<?php

class NormsGroupController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//规格属性列表
	public function lists(){
		$this->name = '规格属性列表'; // 进行模板变量赋值

		$BannerM = new BannerModel();
		$bannerInfo = $BannerM->getAll();

		$this->BannerInfo = $bannerInfo;
		$this->msgurl=C ("IMAGEURL");
		$this->action = "/Adms/NormsGroup/lists.html";
		$this->display("lists");
	}

	//添加banner
	public function add(){
		$this->name = '添加banner';

		if($_POST){
			if(!empty($_FILES)) {
				$PutOssarr=$this->_imgupload();
			}
			if($_POST["link"]){
				$BannerM = new BannerModel();
				$map = array();
				$map["link"] = $_POST["link"];
				$map["picture"] =  $PutOssarr;
				$BannerM->addOne($map);
				header('Location:lists.html');
			}else{
				$this->tip = "不能为空！";
			}
		}
		$this->msgurl=C ("IMAGEURL");
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//修改banner
	public function edit(){
		$this->name = '修改banner';  // 进行模板变量赋值

		//获取id
		$id = $_GET['id'];
		if ($id){
			$BannerM = new BannerModel();
			$where['id'] = $id;
			//查数据
			$BannerInfo = $BannerM->getOne($where);
		}
		$this->msgurl=C ("IMAGEURL");
		$this->BannerInfo = $BannerInfo;
		$this->id = $id;
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//处理修改
	public function update(){
		$this->name = '修改banner';  // 进行模板变量赋值

		$id = $_POST['id'];
		$where = array();
		$BannerM = new BannerModel();
		if ($id){
			if(!empty($_FILES['picture']['name'])) {
				$bannerimg=$this->_imgupload();
				$data['picture'] = $bannerimg;
			}else{

				$where['id'] = $id;
				$BannerInfo = $BannerM->getOne($where);
				$data['picture'] = $BannerInfo['picture'];
			}
			$data['link'] = $_POST['link'];

			$where["id"] = $_POST['id'];
			$res = $BannerM->updateOne($where,$data);
			if ($res){
				$this->tip = "修改成功";
			}
			header('Location:lists.html');
		}
	}

	//删除banner
	// public function del(){
	// 	$this->name = '删除banner';
	//
	// 	if($_GET["id"]){
	// 		$BannerM = new BannerModel();
	// 		$ret = $BannerM->delOne(array("id"=>$_GET["id"]));
	// 		header("Location:/Adms/Banner/lists");
	// 	}
	// }

	//banner上架
	public function up(){
		$id = $_GET['id'];
		$BannerM = new BannerModel();
		$where = array();
		$where['id'] = $id;
		if ($id){
			$data['is_up'] = 1;
			$BannerM->updateOne($where,$data);
			$this->success('修改为上架成功');
		}
	}

	//banner下架
	public function down(){
		$id = $_GET['id'];
		$BannerM = new BannerModel();
		$where = array();
		$where['id'] = $id;
		if ($id){
			$data['is_up'] = 0;
			$BannerM->updateOne($where,$data);
			$this->success('修改为下架成功');
		}
	}

	//图片上传方法
	protected function _upload($savePath) {
		import("Api.lib.Behavior.fileDirUtil");
		$fileutil = new fileDirUtil();
		$fileutil->createDir($savePath);
		import("Api.lib.Behavior.UploadFile");
		//导入上传类
		$upload = new UploadFile();
		//设置上传文件大小
		$upload->maxSize =10485760;
		//设置上传文件类型
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
		//设置附件上传目录
		$upload->savePath = $savePath;
		//设置需要生成缩略图，仅对图像文件有效
		$upload->thumb = false;
		// 设置引用图片类库包路径
		$upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
		//设置需要生成缩略图的文件后缀
		$upload->thumbPrefix = 'm_';  //生产2张缩略图
		//设置缩略图最大宽度
		$upload->thumbMaxWidth = '400';
		//设置缩略图最大高度
		$upload->thumbMaxHeight = '400';
		//设置上传文件规则
		$upload->saveRule = "md5content";
		//删除原图
		$upload->thumbRemoveOrigin = false;
		if (!$upload->upload()) {
			$return = array();
			$return['code'] = ERRORCODE_501;
			$return['message'] = $upload->getErrorMsg();
			if($_REQUEST['ptesst']==1){
				dump($return);
			}
			//Push_data($return);
			//捕获上传异常
			//$this->error($upload->getErrorMsg());
		} else {
			//取得成功上传的文件信息
			$uploadList = $upload->getUploadFileInfo();
			//$uploadList = $uploadList[0];
			//$uploadList['imageUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . $upload->saveRule;
			//$uploadList['thumbnailUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . 'm_' . $upload->saveRule;

			return $uploadList;
			//import("Extend.Library.ORG.Util.Image");
			//给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
			//Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '/logo2.png');
			//$_POST['image'] = $uploadList[0]['savename'];
		}

	}
	//图片上传封装
	public function _imgupload(){
		$filetype = "images";
		$saveUrl = $this->msgfileurl . $filetype . "/";
		$savePath = WR . $saveUrl;
		//echo $savePath;exit;
		$uploadList = $this->_upload ($savePath);
		if (!empty($uploadList)) {
			// $UserPhotoM = new PhotoModel();
			$PutOssarr = array();
			$returnurlarr = array();
			foreach ($uploadList as $k => $v) {
				$PutOssarr[] = $saveUrl . $v['savename'];
				$returnurlarr[] = C ("IMAGEURL") . $saveUrl . $v['savename'];
			}
			PutOss ($PutOssarr);
			// print_r ($PutOssarr);exit;
			$return = array();
			$return["data"]["url"] = $PutOssarr[0];
			return $PutOssarr[0];
		}
	}

}

?>
