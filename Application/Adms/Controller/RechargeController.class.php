<?php

class RechargeController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}

	//充值列表
	public function lists(){
		$this->name = '充值列表'; // 进行模板变量赋值
		$Page = $_GET['Page'] ? $_GET['Page'] : 1;
		$PageSize = 20;

		$WhereArr = array();
		$WhereArr['is_del'] = 1;
		if(isset($_GET['keyword'])&&$_GET['keyword']!=""){
			$keyword=$_GET['keyword'];
			$WhereArr['order']=array('like',"%".$keyword."%");
		}
        $RechargeM = new RechargeModel();
		$RechargeInfo['list'] = $RechargeM->getList($WhereArr,$Page,$PageSize);
		$all_page = ceil($RechargeInfo['totalCount']/$PageSize);
		$RechargeInfo['all_page'] = $all_page;
		$this->RechargeInfo = 	$RechargeInfo['list'];
		$this->Pages = $this->GetPages($RechargeInfo);
		$this->get = $_GET;
		$this->action = "/Adms/Recharge/lists.html";
		$this->display("lists");
	}

	//添加充值金额
	public function add(){
		$this->name = '添加充值金额';
		if($_POST){
				$RechargeM = new RechargeModel();
				$map = array();
				$map["money"] = $_POST["money"];
				$map["reward_integral"] = $_POST["reward_integral"];
				$map["addtime"] = time();
				$RechargeM->addOne($map);
				header('Location:lists.html');
			}
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//修改充值金额
	public function edit(){
		$this->name = '修改充值金额';  // 进行模板变量赋值

		//获取id
		$id = $_GET['id'];
		if ($id){
			$RechargeM = new RechargeModel();
			$where['id'] = $id;
			//查数据
			$RechargeInfo = $RechargeM->getOne($where);
		}
		$this->RechargeInfo = $RechargeInfo;
		$this->id = $id;
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//处理修改
	public function update(){
		$this->name = '修改充值金额';  // 进行模板变量赋值

		$id = $_POST['id'];
		$where = array();
		$RechargeM = new RechargeModel();
		if ($id){
			$data['money'] = $_POST['money'];
			$data['reward_integral'] = $_POST['reward_integral'];
			$where["id"] = $_POST['id'];
			$res = $RechargeM->updateOne($where,$data);
			if ($res){
				$this->tip = "修改成功";
			}
			header('Location:lists.html');
		}
	}

	//软删除充值金额
	public function del(){
		$this->name = '删除充值金额';
		$RechargeM = new RechargeModel();
		$where = array();
		$where['id'] = $_GET['id'];
		if($_GET["id"]){
			$data['is_del'] = 0;
			$ret = $RechargeM->updateOne($where,$data);
			header("Location:/Adms/Recharge/lists");
		}
	}
}

?>
