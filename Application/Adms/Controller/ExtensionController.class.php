<?php

class ExtensionController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}

	//平台推广用户列表
	public function lists(){
		$this->name = '推广用户列表'; // 进行模板变量赋值
		$Page = $_GET['Page'] ? $_GET['Page'] : 1;
		$PageSize = 20;

		$WhereArr = array();
		$WhereArr['is_del']=1;
		if(isset($_GET['keyword'])&&$_GET['keyword']!=""){
			$keyword=$_GET['keyword'];
			$WhereArr['name']=array('like',"%".$keyword."%");
		}
		$ExtensionM = new ExtensionModel();
		$ExtensionInfo['list'] = $ExtensionM->getList($WhereArr,$Page,$PageSize);
//		dump($ExtensionInfo);die;
		$all_page = ceil($ExtensionInfo['totalCount']/$PageSize);
		$ExtensionInfo['all_page'] = $all_page;
		$this->ExtensionInfo = $ExtensionInfo['list'];
		$this->msgurl=C ("IMAGEURL");
		$this->Pages = $this->GetPages($ExtensionInfo);
		$this->get = $_GET;
		$this->action = "/Adms/Extension/lists.html";
		$this->display("lists");
	}

	//添加平台推广用户
	public function add(){
		$this->name = '添加平台推广用户';
		$ExtensionM = new ExtensionModel();
		$map = array();
		if($_POST){
			if($_POST["name"] && $_POST["phone"]){
				$map["name"] = $_POST["name"];
				$map["phone"] =  $_POST["phone"];
				$map["addtime"] =  time();
				$ExtensionM->addOne($map);
				header('Location:lists.html');
			}else{
				$this->tip = "不能为空！";
			}
		}
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//修改平台推广用户
	public function edit(){
		$this->name = '修改平台推广用户';  // 进行模板变量赋值
		$ExtensionM = new ExtensionModel();
		$where = array();
		//获取id
		$id = $_GET['id'];
		if ($id){
			$where['id'] = $id;
			//查数据
			$ExtensionInfo = $ExtensionM->getOne($where);
		}
		$this->ExtensionInfo = $ExtensionInfo;
		$this->id = $id;
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//处理修改
	public function update(){
		$this->name = '修改平台推广用户';  // 进行模板变量赋值

		$id = $_POST['id'];
		$where = array();
		$ExtensionM = new ExtensionModel();
		if ($id){
			$data['name'] = $_POST['name'];
			$data['phone'] = $_POST['phone'];
			$where["id"] = $_POST['id'];
			$res = $ExtensionM->updateOne($where,$data);
			if ($res){
				$this->tip = "修改成功";
			}
			header('Location:lists.html');
		}
	}

	//禁用平台推广用户
	public function del(){
		$this->name = '禁用平台推广用户';
		$ExtensionM = new ExtensionModel();
		$where = array();
		$where['id'] = $_GET['id'];
		if($_GET["id"]){
			$data['is_del'] = 0;
			$ret = $ExtensionM->updateOne($where,$data);
			header("Location:/Adms/Extension/lists");
		}
	}

}

?>
