<?php
class CheckauthtxmyController extends BaseAdmsController
{
    public function __construct(){
        parent::__construct();

    }
    public function  login(){
        if($_POST['username']){
            $where = array();
            $where['username'] = $_POST['username'];
            $where['password'] = $_POST['password'];

            $AdminUser = M('txmydl')->where($where)->find();
            if(empty($AdminUser)){
                $this->redirect("/Adms/Checkauthtxmy/login");
            }else{
                session('txmyUser',$AdminUser,time()+60);
                $this->redirect("/Adms/Checkauthtxmy/index");
            }
        }else{
            $this->display();
        }


    }

    public function index(){
        if (!session("txmyUser")) {
            header("Location:/Adms/Checkauthtxmy/login");
            //$this->error('操作失败','/Adms/Index2/Login');
            //IndexController::Index2();exit;
        }else{
            $this->name ='腾讯key列表';
            $ret=M('txmy')->select();
            $this->DataList= $ret;
            $this->action = __ACTION__ . ".html";
            $this->display ();
        }

    }
    public function edit(){
        if (!session("txmyUser")) {
            header("Location:/Adms/Checkauthtxmy/login");

        }else{
            $id = $_REQUEST['id'];
            if($id){
                if($_POST){
                    if($_POST["id"]){
                        $where=array();
                        $map = array();
                        $where['id']=$id;
                        $map["product"] =$_POST["product"];
                        $map["miyao"] =$_POST["miyao"];
                        M('txmy')->where($where)->save($map);
                        header('Location:index.html');
                    }
                }
                $where = array();
                $where["id"] = $id;
                $Info= M('txmy')->where($where)->find();
                $this->Info = $Info;
            }

            $this->id = $id;
            $this->action =  __ACTION__.".html";
            $this->display("add");

        }
    }

    public function add(){
        $this->name = $this->basename.'添加'; // 进行模板变量赋值
        if($_POST){
            $err=0;
            $msg="";
            if($this->checkproduct($_POST["product"])==false){
                $err = 1;
                $msg.="平台号格式错误<br />";
            }
            if(!$_POST["product"]){
                $err = 2;
                $msg.="平台号不能为空<br />";
            }
            if(!$_POST["miyao"]){
                $err = 3;
                $msg.="key不能为空<br />";
            }
            if($err==0){
                $Attr =M('txmy');
                $map = array();
                $map["product"] = $_POST["product"];

                $AttrValue = $Attr->where($map)->find();

                if($AttrValue){
                    $this->tip = "平台号已经存在！";
                }else{

                    $map["product"] = $_POST["product"];
                    $map["miyao"] = $_POST["miyao"];

                    $Attr->data($map)->add();

                    $this->tip = $_POST["product"]."已添加";
                    header('Location:index.html');
                }

            }else{
                $this->tip = $msg;
            }
        }
        $this->action =  __ACTION__.".html";
        $this->display();
    }


    /*
    * 删除
    */
    public function del(){
        $code =$_REQUEST["id"];
        if($code){
            $AttrValueM = M('txmy');
            $map = array();
            $map["id"] = $code;
            $AttrValue = $AttrValueM->where($map)->delete();
        }
        header('Location:'.$_SERVER['HTTP_REFERER']);
    }
    public function logout(){
        session('txmyUser',null);
        header("Location:/Adms/Checkauthtxmy/login");

    }

    private function checkproduct($product){
        $pingtai = substr($product, 2 , 1);
        if(!in_array($pingtai, array(0,1,2))){
            return false;
        }
        if(strlen($product)!=5){
            return false;
        }
        return true;
    }

}

