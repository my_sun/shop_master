<?php

class UserController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//用户列表
	public function lists(){
		$this->name = '用户列表'; // 进行模板变量赋值
		$Page = $_GET['Page'] ? $_GET['Page'] : 1;
		$PageSize = 20;

		$WhereArr = array();
//		$WhereArr['is_del']=1;
		if(isset($_GET['keyword'])&&$_GET['keyword']!=""){
			$keyword=$_GET['keyword'];
			$WhereArr['nickname|name']=array('like',"%".$keyword."%");
		}
		if (isset($_GET['phone'])&&$_GET['phone']!=""){
			$phone = $_GET['phone'];
			$WhereArr['phone']=array('like',"%".$phone."%");
		}
		$UserM = new UserModel();
		$UserInfo['list'] = $UserM->getListPage($WhereArr,$Page,$PageSize);
		$all_page = ceil($UserInfo['totalCount']/$PageSize);
		$UserInfo['all_page'] = $all_page;
		$this->UserInfo = $UserInfo['list'];
		$this->msgurl=C ("IMAGEURL");
		$this->Pages = $this->GetPages($UserInfo);
		$this->get = $_GET;
		$this->action = "/Adms/User/lists.html";
		$this->display("lists");
	}

	//添加平台推广用户
	public function add(){
		$this->name = '添加平台推广用户';
		$UserM = new UserModel();
		$map = array();
		if($_POST){
			if($_POST["name"] && $_POST["phone"]){
				$map["name"] = $_POST["name"];
				$map["phone"] =  $_POST["phone"];
				$map["is_extension"] =  1;
				$map["addtime"] =  time();
				$UserM->addOne($map);
				header('Location:lists.html');
			}else{
				$this->tip = "不能为空！";
			}
		}
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//修改平台推广用户
	public function edit(){
		$this->name = '修改平台推广用户';  // 进行模板变量赋值
		$UserM = new UserModel();
		$where = array();
		//获取id
		$id = $_GET['id'];
		if ($id){
			$where['uid'] = $id;
			//查数据
			$userInfo = $UserM->getOne($where);
		}
		$this->userInfo = $userInfo;
		$this->id = $id;
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//处理修改
	public function update(){
		$this->name = '修改平台推广用户';  // 进行模板变量赋值

		$id = $_POST['id'];
		$where = array();
		$UserM = new UserModel();
		if ($id){
			$data['name'] = $_POST['name'];
			$data['phone'] = $_POST['phone'];
			$where["id"] = $_POST['id'];
			$res = $UserM->updateOne($where,$data);
			if ($res){
				$this->tip = "修改成功";
			}
			header('Location:lists.html');
		}
	}

	//软删除用户
	public function del(){
		$this->name = '拉黑用户';
		$UserM = new UserModel();
		$where = array();
		$where['id'] = $_GET['id'];
		if($_GET["id"]){
			$data['is_del'] = 0;
			$ret = $UserM->updateOne($where,$data);
			header("Location:/Adms/User/lists");
		}
	}

	//设为会员
	public function vip(){
		$this->name = '设为会员';
		$UserM = new UserModel();
		$where = array();
		$where['id'] = $_GET['id'];
		if($_GET["id"]){
			$data['is_vip'] = 1;
			$ret = $UserM->updateOne($where,$data);
			header("Location:/Adms/User/lists");
		}
	}

	//图片上传方法
	protected function _upload($savePath) {
		import("Api.lib.Behavior.fileDirUtil");
		$fileutil = new fileDirUtil();
		$fileutil->createDir($savePath);
		import("Api.lib.Behavior.UploadFile");
		//导入上传类
		$upload = new UploadFile();
		//设置上传文件大小
		$upload->maxSize =10485760;
		//设置上传文件类型
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
		//设置附件上传目录
		$upload->savePath = $savePath;
		//设置需要生成缩略图，仅对图像文件有效
		$upload->thumb = false;
		// 设置引用图片类库包路径
		$upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
		//设置需要生成缩略图的文件后缀
		$upload->thumbPrefix = 'm_';  //生产2张缩略图
		//设置缩略图最大宽度
		$upload->thumbMaxWidth = '400';
		//设置缩略图最大高度
		$upload->thumbMaxHeight = '400';
		//设置上传文件规则
		$upload->saveRule = "md5content";
		//删除原图
		$upload->thumbRemoveOrigin = false;
		if (!$upload->upload()) {
			$return = array();
			$return['code'] = ERRORCODE_501;
			$return['message'] = $upload->getErrorMsg();
			if($_REQUEST['ptesst']==1){
				dump($return);
			}
			//Push_data($return);
			//捕获上传异常
			//$this->error($upload->getErrorMsg());
		} else {
			//取得成功上传的文件信息
			$uploadList = $upload->getUploadFileInfo();
			//$uploadList = $uploadList[0];
			//$uploadList['imageUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . $upload->saveRule;
			//$uploadList['thumbnailUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . 'm_' . $upload->saveRule;

			return $uploadList;
			//import("Extend.Library.ORG.Util.Image");
			//给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
			//Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '/logo2.png');
			//$_POST['image'] = $uploadList[0]['savename'];
		}

	}
	//图片上传封装
	public function _imgupload(){
		$filetype = "images";
		$saveUrl = $this->msgfileurl . $filetype . "/";
		$savePath = WR . $saveUrl;
		//echo $savePath;exit;
		$uploadList = $this->_upload ($savePath);
		if (!empty($uploadList)) {
			// $UserPhotoM = new PhotoModel();
			$PutOssarr = array();
			$returnurlarr = array();
			foreach ($uploadList as $k => $v) {
				$PutOssarr[] = $saveUrl . $v['savename'];
				$returnurlarr[] = C ("IMAGEURL") . $saveUrl . $v['savename'];
			}
			PutOss ($PutOssarr);
			// print_r ($PutOssarr);exit;
			$return = array();
			$return["data"]["url"] = $PutOssarr[0];
			return $PutOssarr[0];
		}
	}

}

?>
