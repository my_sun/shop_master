<?php

    use vod\Request\V20170321\UpdateWatermarkRequest;

    class GoodsController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/goods/';
		$this->msgfileurl = '/userdata/goodsimg/'.date('Ymd',time())."/";
	}

	//商品列表
	public function lists(){
		$this->name = '商品列表'; // 进行模板变量赋值
        //分页
		$Page = $_GET['Page'] ? $_GET['Page'] : 1;
		$PageSize = 20;
		//商品专题信息
		$specialInfo = M('special')->where(['is_up'=>1])->select();
		$WhereArr = array();
//		$WhereArr['is_up']=1;
		if (isset($_GET['special_id'])&&$_GET['special_id']!=""){
			$WhereArr['special_id'] = $_GET['special_id'];
		}
		//获取查询数据
		if(isset($_GET['keyword'])&&$_GET['keyword']!=""){
			$keyword=$_GET['keyword'];
			$WhereArr['name']=array('like',"%".$keyword."%");
		}
		$GoodsM = new GoodsModel();
		$GoodsTypeM = new GoodsTypeModel();
		$GoodsTypeInfo = $GoodsTypeM->getAll();
		$GoodsInfo['list'] = $GoodsM->getList($WhereArr,$Page,$PageSize);
		foreach ($GoodsInfo['list'] as $k=>$v){
			$GoodsInfo['list'][$k]['goodsimg'] = json_decode($v['goodsimg']);
			$GoodsInfo['list'][$k]['norms'] = json_decode($v['norms']);
		}
		$all_page = ceil($GoodsInfo['totalCount']/$PageSize);
		$GoodsInfo['all_page'] = $all_page;

		$this->GoodsInfo = $GoodsInfo['list'];
		$this->GoodsTypeInfo = $GoodsTypeInfo;
		$this->specialInfo = $specialInfo;
		$this->msgurl=C ("IMAGEURL");
		$this->Pages = $this->GetPages($GoodsInfo);
		$this->get = $_GET;
		$this->action = "/Adms/Goods/lists.html";
		$this->display("lists");
	}
	//添加商品 页面
	public function add(){
		$this->name = '添加商品';

        $GoodsTypeM = new GoodsTypeModel();
        $SpecialM = new SpecialModel();
        $where['is_del'] = 1;
        $where2['is_up'] = 1;
        $GoodsTypeInfo = $GoodsTypeM->getList($where);
        $SpecialInfo = $SpecialM->getAll($where2);
		$this->GoodsTypeInfo = $GoodsTypeInfo;
		$this->SpecialInfo = $SpecialInfo;
		$this->action =  __ACTION__.".html";
		$this->display();
	}
	//处理添加
	public function addshop(){
	    $map = array();
	    $data = array();
	    $normsdata = array();
        $GoodsM = new GoodsModel();
	    $RobM = new RobModel();
    	if($_POST){
        //获取商品图片
        if(!empty($_POST['goodsimg'])) {
        	if (is_array($_POST['goodsimg'])){
				foreach ($_POST['goodsimg'] as $k=>$v){
					$data[] = rtrim($v,'/');
				}
			}
			$goodsimg =  $data;
			$goodsimg = json_encode($goodsimg);
        }else{
            $this->tip="请上传商品图片";
        }
        //接收数据(添加商品表)
		$map["goods_type_id"] = $_POST["goods_type_id"];
		$map["special_id"] = $_POST["special_id"];
		//$map["is_new"] = $_POST["is_new"];
		$map["name"] = $_POST["name"];
		$map["goodsimg"] =  $goodsimg;
		$map["desc"] = $_POST["desc"];
		$map["price"] = $_POST["price"];
		$map["discount_price"] = $_POST["discount_price"];
		$map["stock"] = $_POST["stock"];
		$map["integral"] = $_POST["integral"];
		$map["reward_integral"] = $_POST["reward_integral"];
		$map["reward_up_integral"] = $_POST["reward_up_integral"];
		$map["reward_up_up_integral"] = $_POST["reward_up_up_integral"];
		$map["norms"] = json_encode($_POST["norms"]);//
		$map["details"] = $_POST["details"];
		$map["detailsimg"] = $_POST["detailsimg"];
		$map["addtime"] = time();
		$map["is_up"] = 1;
        $res = $GoodsM->addOne($map);
        $gid = $GoodsM->max(id);
        //接收数据（添加规格表）
			$norms = $_POST["norms"];
			foreach($norms as $k1=>$v1){
				$normskey = explode("：",$v1);//用：分割
				list($key,$val) = $normskey;
				$strsub = strpos($val,"|")?strpos($val,"|"):strlen($val);
				$normskwval = substr($val,0,$strsub);
				$shoponeo = substr($val,$strsub+1,-1);
				$shoponeoArr = explode("|",$shoponeo);
				$normArr = array();
				foreach($shoponeoArr as $k=>$item){
					$norstoc = explode(",",$item);
//					$normsdata['normskw'] = $key;
					$normsdata['normskwval'] = $key.':'.$normskwval;
					$normsdata['normsggval'] = '规格'.':'.$norstoc[0];
					$normsdata['stock'] = $norstoc[1];
					$normsdata['price'] = $norstoc[2];
					$normsdata['discount_price'] = $norstoc[3];
					$normsdata['uid'] = $_SESSION['userInfo']['uid'];
					$normsdata['goods_id'] = $gid;
					$normsdata['special_id'] = $_POST["special_id"];
					$normsdata['integral'] = $_POST["integral"];
					$normsdata['addtime'] = time();
					$res3 = M('norms')->add($normsdata);
				}
			}
		//接收数据（添加抢购表）
		if ($_POST["starttime"] && $_POST["endtime"] && $_POST["special_id"] == 1){
			$data["starttime"] = strtotime($_POST["starttime"]);
			$data["endtime"] = strtotime($_POST["endtime"]);
			$data["special_id"] = $_POST["special_id"];
			$data["goods_id"] = $gid;
			$res2 = $RobM->addOne($data);
		}
		if ($res || $res2 ||$res3){
			$this->ajaxReturn(["err"=>200,"msg"=>"添加成功"]);
		}
    }
}
		/**
		 * PHP上传视频（网上搜索）
		 */
		public function upload_file($files, $path = "./upload", $imagesExt = ['jpg', 'png', 'jpeg', 'gif', 'mp4'])
		{
			// 判断错误号
			if (@$files['error'] == 00) {
				// 判断文件类型
				$ext = strtolower(pathinfo(@$files['name'], PATHINFO_EXTENSION));
				if (!in_array($ext, $imagesExt)) {
					return "非法文件类型";
				}

				// 判断是否存在上传到的目录
				if (!is_dir($path)) {
					mkdir($path, 0777, true);
				}

				// 生成唯一的文件名
				$fileName = md5(uniqid(microtime(true), true)) . '.' . $ext;

				// 将文件名拼接到指定的目录下
				$destName = $path . "/" . $fileName;

				// 进行文件移动
				if (!move_uploaded_file($files['tmp_name'], $destName)) {
					return "文件上传失败！";
				}
				return "文件上传成功！";
			} else {
				// 根据错误号返回提示信息
				switch (@$files['error']) {
					case 1:
						echo "上传的文件超过了 php.ini 中 upload_max_filesize 选项限制的值";
						break;
					case 2:
						echo "上传文件的大小超过了 HTML 表单中 MAX_FILE_SIZE 选项指定的值";
						break;
					case 3:
						echo "文件只有部分被上传";
						break;
					case 4:
						echo "没有文件被上传";
						break;
					case 6:
					case 7:
						echo "系统错误";
						break;
				}
			}

		}

//	echo upload_file($upfile);

	/**
	 *
	 */
	public function upload_img(){

       	$PutOssarr=$this->_imgupload();
		echo $PutOssarr;
	}

	//修改商品
	public function edit(){
		$this->name = '修改商品';  // 进行模板变量赋值
        //获取id
        $id = $_GET['id'];

        //获取商品分类数据
        $where2 = array();
        $GoodsTypeM = new GoodsTypeModel();
        $where2['is_del'] = 1;
        $GoodsTypeInfo = $GoodsTypeM->getAll($where2);
        //获取商品专题
        $where4 = array();
        $where4['is_up'] = 1;
        $SpecialM = new SpecialModel();
        $SpecialInfo = $SpecialM->getList($where4);
        //获取抢购表
        $where3 = array();
        $where3['goods_id'] = $id;
        $RobM = new RobModel();
        $RobInfo = $RobM->getList($where3);
        foreach ($RobInfo as $k=>$v){
            $RobInfo[$k]['starttime'] = date('Y-m-d H:i:s',$v['starttime']);
            $RobInfo[$k]['endtime'] = date('Y-m-d H:i:s',$v['endtime']);
        }
        // dump($RobInfo[0]);die;
        //获取商品数据
        $where = array();
        $GoodsM = new GoodsModel();
		if ($id){
			$where['id'] = $id;
			//查数据
			$GoodsInfo = $GoodsM->getOne($where);
			$GoodsInfo['goodsimg'] = json_decode($GoodsInfo['goodsimg']);
			$GoodsInfo['norms'] = json_decode($GoodsInfo['norms']);
		}
//		dump($GoodsInfo);die;
		$this->msgurl=C ("IMAGEURL");
		$this->GoodsInfo = $GoodsInfo;
		$this->GoodsTypeInfo = $GoodsTypeInfo;
		$this->SpecialInfo = $SpecialInfo;
		$this->RobInfo = $RobInfo[0];
		$this->id = $id;
		$this->action =  __ACTION__.".html";
		$this->display();
	}
	//处理修改
	public function editgoods(){
		$this->name = '修改商品';  // 进行模板变量赋值

		$id = $_POST['id'];
		$where = array();
		$where2 = array();
		$where3 = array();
		$GoodsM = new GoodsModel();
        $RobM = new RobModel();
        //原商品里规格信息
		$goodsInfo = $GoodsM->where(['id'=>$id])->select();
		if ($id){
			//获取图片
			if(!empty($_POST['goodsimg'])) {
				if (is_array($_POST['goodsimg'])){
					foreach ($_POST['goodsimg'] as $k=>$v){
						$data[] = rtrim($v,'/');
					}
				}
				$goodsimg =  $data;
				$goodsimg = json_encode($goodsimg);
			}else{
				$where['id'] = $id;
				$GoodsInfo = $GoodsM->getOne($where);
				$goodsimg = $GoodsInfo['goodsimg'];
			}
			//修改商品表
			$data['goods_type_id'] = $_POST['goods_type_id'];
			$data['special_id'] = $_POST['special_id'];
			//$data['is_new'] = $_POST['is_new'];
            $data["name"] = $_POST["name"];
            $data["goodsimg"] =  $goodsimg;
            $data["desc"] = $_POST["desc"];
            $data["price"] = $_POST["price"];
            $data["discount_price"] = $_POST["discount_price"];
            $data["stock"] = $_POST["stock"];
            $data["integral"] = $_POST["integral"];
			$data["reward_integral"] = $_POST["reward_integral"];
			$data["reward_up_integral"] = $_POST["reward_up_integral"];
			$data["reward_up_up_integral"] = $_POST["reward_up_up_integral"];
            $data["norms"] = json_encode($_POST["norms"]);
            $data["details"] = $_POST["details"];
            $data["detailsimg"] = $_POST["detailsimg"];
			$where["id"] = $_POST['id'];
			$res = $GoodsM->updateOne($where,$data);
			//修改规格表
			if ($data["norms"] != $goodsInfo[0]['norms']){
				$where3['goods_id'] = $_POST['id'];
				M('norms')->where($where3)->delete();//删除原有的规格
				$norms = $_POST["norms"];
				foreach($norms as $k1=>$v1){
					$normskey = explode("：",$v1);//用：分割
					list($key,$val) = $normskey;
					$strsub = strpos($val,"|")?strpos($val,"|"):strlen($val);
					$normskwval = substr($val,0,$strsub);
					$shoponeo = substr($val,$strsub+1,-1);
					$shoponeoArr = explode("|",$shoponeo);
					$normArr = array();
					foreach($shoponeoArr as $k=>$item){
						$norstoc = explode(",",$item);
						$normsdata['normskwval'] = $key.':'.$normskwval;
						$normsdata['normsggval'] = '规格'.':'.$norstoc[0];
						$normsdata['stock'] = $norstoc[1];
						$normsdata['price'] = $norstoc[2];
						$normsdata['discount_price'] = $norstoc[3];
						//$normsdata['uid'] = $_SESSION['userInfo']['uid'];
						$normsdata['goods_id'] = $_POST['id'];
						$normsdata['special_id'] = $_POST["special_id"];
						$normsdata['integral'] = $_POST["integral"];
						$normsdata['addtime'] = time();//修改的时间
						$res3 = M('norms')->where($where3)->add($normsdata);
					}
				}
			}
			//修改抢购表
			$data2["special_id"] = $_POST["special_id"];
			if ($data2["special_id"] == 1){
				$robInfo = M('rob')->where(['goods_id'=>$id])->find();
				$data2["starttime"] = strtotime($_POST["starttime"]);
				$data2["endtime"] = strtotime($_POST["endtime"]);
				$data2["goods_id"] = $id;
				if ($robInfo['goods_id'] == $id){
					$res2 = M('rob')->where(['goods_id'=>$id])->save($data2);
				}else{
					$res2 = M('rob')->add($data2);
				}
			}
			if ($res || $res2 || $res3){
				$this->ajaxReturn(["err"=>200,"msg"=>"修改成功"]);
			}
		}
	}
	//软删除商品
	public function del(){
		$this->name = '删除商品';
		$GoodsM = new GoodsModel();
		$where = array();
		$where['id'] = $_GET['id'];
		if($_GET["id"]){
			$data['is_del'] = 0;
			$ret = $GoodsM->updateOne($where,$data);
			header("Location:/Adms/Register/lists");
		}
	}
	//商品上架
	public function upgoods(){
		$id = $_GET['id'];
		$GoodsM = new GoodsModel();
		$where = array();
		$where['id'] = $id;
		if ($id){
			$data['is_up'] = 1;
			$GoodsM->updateOne($where,$data);
			$this->success('修改为上架成功');
		}
	}
	//商品下架
	public function downgoods(){
		$id = $_GET['id'];
		$GoodsM = new GoodsModel();
		$where = array();
		$where['id'] = $id;
		if ($id){
			$data['is_up'] = 0;
			$GoodsM->updateOne($where,$data);
			$this->success('修改为下架成功');
		}
	}

	//图片上传方法
	protected function _upload($savePath) {
		import("Api.lib.Behavior.fileDirUtil");
		$fileutil = new fileDirUtil();
		$fileutil->createDir($savePath);
		import("Api.lib.Behavior.UploadFile");
		//导入上传类
		$upload = new UploadFile();
		//设置上传文件大小
		$upload->maxSize =10485760;
		//设置上传文件类型
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
		//设置附件上传目录
		$upload->savePath = $savePath;
		//设置需要生成缩略图，仅对图像文件有效
		$upload->thumb = false;
		// 设置引用图片类库包路径
		$upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
		//设置需要生成缩略图的文件后缀
		$upload->thumbPrefix = 'm_';  //生产2张缩略图
		//设置缩略图最大宽度
		$upload->thumbMaxWidth = '400';
		//设置缩略图最大高度
		$upload->thumbMaxHeight = '400';
		//设置上传文件规则
		$upload->saveRule = "md5content";
		//删除原图
		$upload->thumbRemoveOrigin = false;
		if (!$upload->upload()) {
			$return = array();
			$return['code'] = ERRORCODE_501;
			$return['message'] = $upload->getErrorMsg();
			if($_REQUEST['ptesst']==1){
				dump($return);
			}
			//Push_data($return);
			//捕获上传异常
			//$this->error($upload->getErrorMsg());
		} else {
			//取得成功上传的文件信息
			$uploadList = $upload->getUploadFileInfo();
			//$uploadList = $uploadList[0];
			//$uploadList['imageUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . $upload->saveRule;
			//$uploadList['thumbnailUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . 'm_' . $upload->saveRule;

			return $uploadList;
			//import("Extend.Library.ORG.Util.Image");
			//给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
			//Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '/logo2.png');
			//$_POST['image'] = $uploadList[0]['savename'];
		}

	}
	//图片上传封装
	public function _imgupload(){
		$filetype = "images";
		$saveUrl = $this->msgfileurl . $filetype . "/";
		$savePath = WR . $saveUrl;
//		echo $savePath;exit;
		$uploadList = $this->_upload ($savePath);
		if (!empty($uploadList)) {
			// $UserPhotoM = new PhotoModel();
			$PutOssarr = array();
			$returnurlarr = array();
			foreach ($uploadList as $k => $v) {
				$PutOssarr[] = $saveUrl . $v['savename'];
				$returnurlarr[] = C ("IMAGEURL") . $saveUrl . $v['savename'];
			}
			PutOss ($PutOssarr);
			// print_r ($PutOssarr);exit;
			$return = array();
			$return["data"]["url"] = $PutOssarr[0];
			return $PutOssarr[0];
		}
	}

}

?>
