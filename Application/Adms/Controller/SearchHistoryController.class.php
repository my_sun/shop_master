<?php

class SearchHistoryController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//搜索历史列表
	public function lists(){
		$this->name = '搜索历史列表'; // 进行模板变量赋值

		$SearchHistoryM = new SearchHistoryModel();
		$SearchHistoryInfo = $SearchHistoryM->getAll();

		$this->SearchHistoryInfo = $SearchHistoryInfo;
		$this->action = "/Adms/SearchHistory/lists.html";
		$this->display("lists");
	}

	//软删除搜索历史
	 public function del(){
	 	$this->name = '删除搜索历史';
		$where['id'] = $_GET['id'];
		 $SearchHistoryM = new SearchHistoryModel();
		 if($_GET["id"]){
			 $data['is_del'] = 0;
			 $ret = $SearchHistoryM->updateOne($where,$data);
			 header("Location:/Adms/SearchHistory/lists");
		 }
	 }


}

?>
