<?php

class GoodsTypeController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/goodstype/';
		$this->msgfileurl = '/userdata/goodstypeimg/'.date('Ymd',time())."/";
	}

	//商品分类列表
	public function lists(){
		$this->name = '商品分类列表'; // 进行模板变量赋值

		$GoodsTypeM = new GoodsTypeModel();
		//当前页数
		$Page = $_GET['Page'] ? $_GET['Page'] : 1;
		$PageSize = 20;
		//搜索数据
		$WhereArr = array();
		$WhereArr['is_del']='1';
		if(isset($_GET['kerword'])&&$_GET['kerword']!=""){
			$kerword=$_GET['kerword'];
			$WhereArr['name']=array('like',"%".$kerword."%");
		}
		$GoodsTypeInfo = $GoodsTypeM->getListPage($WhereArr,$Page,$PageSize);
		$all_page = ceil($GoodsTypeInfo['totalCount']/$PageSize);
		$ret["all_page"] = $all_page;

		$this->Pages = $this->GetPages($ret);
		$this->get = $_GET;
		$this->GoodsTypeInfo = $GoodsTypeInfo['list'];
		$this->action = "/Adms/GoodsType/lists.html";
		$this->display("lists");
	}

	//添加商品分类
	public function add(){
		$this->name = '添加商品分类';
		$GoodsTypeM = new GoodsTypeModel();

		if($_POST){
			if(!empty($_FILES)) {
				$PutOssarr=$this->_imgupload();
			}
			if($_POST["name"]){
				$map = array();
				$map["name"] = $_POST["name"];
				$map["img"] = $PutOssarr;
				$map["addtime"] = time();
				$GoodsTypeM->addOne($map);
				header('Location:lists.html');
			}else{
				$this->tip = "不能为空！";
			}
		}

		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//修改商品分类
	public function edit(){
		$this->name = '修改商品分类';  // 进行模板变量赋值
		$GoodsTypeM = new GoodsTypeModel();
		//获取id
		$id = $_GET['id'];
		if ($id){
			$where['id'] = $id;
			//查数据
			$GoodsTypeInfo = $GoodsTypeM->getOne($where);
		}
		$this->msgurl=C ("IMAGEURL");
		$this->GoodsTypeInfo = $GoodsTypeInfo;
		$this->id = $id;
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//处理修改
	public function update(){
		$this->name = '修改商品分类';  // 进行模板变量赋值

		$id = $_POST['id'];
		$where = array();
		$GoodsTypeM = new GoodsTypeModel();
		if ($id){
			if(!empty($_FILES['img']['name'])) {
				$img=$this->_imgupload();
				$data['img'] = $img;
			}else{
				$where['id'] = $id;
				$GoodsTypeInfo = $GoodsTypeM->getOne($where);
				$data['img'] = $GoodsTypeInfo['img'];
			}

			$data['name'] = $_POST['name'];
			$data['updatetime'] = time();
			$where["id"] = $_POST['id'];
			$res = $GoodsTypeM->updateOne($where,$data);
			if ($res){
				$this->tip = "修改成功";
			}
			header('Location:lists.html');
		}
	}

	//软删除商品分类
	public function del(){
		$this->name = '删除商品分类';
		$GoodsTypeM = new GoodsTypeModel();
		$where = array();
		if($_GET["id"]){
			$where['id'] = $_GET['id'];
			// $data = $GoodsTypeM->getOne($_GET["id"]);
			$datas['is_del'] = 0;
			$ret = $GoodsTypeM->updateOne($where,$datas);
			header("Location:/Adms/GoodsType/lists");
		}
	}

	//图片上传方法
	protected function _upload($savePath) {
		import("Api.lib.Behavior.fileDirUtil");
		$fileutil = new fileDirUtil();
		$fileutil->createDir($savePath);
		import("Api.lib.Behavior.UploadFile");
		//导入上传类
		$upload = new UploadFile();
		//设置上传文件大小
		$upload->maxSize =10485760;
		//设置上传文件类型
		$upload->allowExts = explode(',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
		//设置附件上传目录
		$upload->savePath = $savePath;
		//设置需要生成缩略图，仅对图像文件有效
		$upload->thumb = false;
		// 设置引用图片类库包路径
		$upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
		//设置需要生成缩略图的文件后缀
		$upload->thumbPrefix = 'm_';  //生产2张缩略图
		//设置缩略图最大宽度
		$upload->thumbMaxWidth = '400';
		//设置缩略图最大高度
		$upload->thumbMaxHeight = '400';
		//设置上传文件规则
		$upload->saveRule = "md5content";
		//删除原图
		$upload->thumbRemoveOrigin = false;
		if (!$upload->upload()) {
			$return = array();
			$return['code'] = ERRORCODE_501;
			$return['message'] = $upload->getErrorMsg();
			if($_REQUEST['ptesst']==1){
				dump($return);
			}
			//Push_data($return);
			//捕获上传异常
			//$this->error($upload->getErrorMsg());
		} else {
			//取得成功上传的文件信息
			$uploadList = $upload->getUploadFileInfo();
			//$uploadList = $uploadList[0];
			//$uploadList['imageUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . $upload->saveRule;
			//$uploadList['thumbnailUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . 'm_' . $upload->saveRule;

			return $uploadList;
			//import("Extend.Library.ORG.Util.Image");
			//给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
			//Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '/logo2.png');
			//$_POST['image'] = $uploadList[0]['savename'];
		}

	}
	//图片上传封装
	public function _imgupload(){
		$filetype = "images";
		$saveUrl = $this->msgfileurl . $filetype . "/";
		$savePath = WR . $saveUrl;
		//echo $savePath;exit;
		$uploadList = $this->_upload ($savePath);
		if (!empty($uploadList)) {
			// $UserPhotoM = new PhotoModel();
			$PutOssarr = array();
			$returnurlarr = array();
			foreach ($uploadList as $k => $v) {
				$PutOssarr[] = $saveUrl . $v['savename'];
				$returnurlarr[] = C ("IMAGEURL") . $saveUrl . $v['savename'];
			}
			PutOss ($PutOssarr);
			// print_r ($PutOssarr);exit;
			$return = array();
			$return["data"]["url"] = $PutOssarr[0];
			return $PutOssarr[0];
		}
	}

}

?>
