<?php
use Think\Controller;
class LoginController extends Controller {
	public function __construct(){
		parent::__construct();
		session_start();
	}
		
	public function LoginOut(){
		session('AdminUser',null);
		header("Location:/Adms/Login/login.html");
	}
	public function Login(){
		$this->name = '登录'; // 进行模板变量赋值
		if(session("AdminUser")){
			header("Location:/Adms/Index2/index.html");
		}
		$this->action="Logind.html";
		$this->display();
	}
	public function Logind(){
		$where = array();
		$where['name'] = $_POST['username'];
		$where['password'] = $_POST['password'];
		
		$AdminUserm = new AdminUserModel();
		$AdminUser = $AdminUserm->getOne($where);
		if(empty($AdminUser)){
			$this->redirect("/Adms/Login/login");
		}else{
			$InData = array();
			$InData['admin_id'] = $AdminUser['id'];
			$InData['admin_name'] = $AdminUser['name'];
			$InData['ip'] = get_client_ip();
			$InData['login_time'] = time();
			$AdminLoginIpM = new AdminLoginIpModel();
			$AdminLoginIpM->addOne($InData);
			session('AdminUser',$AdminUser,time()+3600*24);
			//session("AdminUser",$AdminUser);
            $_SESSION['user']=array(
                'id'=>$AdminUser['id'],
                'username'=>$AdminUser['name'],
                'expire'=>time()+3600*24
            );
			$this->redirect("/Adms/Index2/index");
		}
		
	
	}
}

?>
