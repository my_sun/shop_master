<?php

class OrderController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
	}

	//订单列表
	public function lists(){
		$this->name = '订单列表'; // 进行模板变量赋值
		$Page = $_GET['Page'] ? $_GET['Page'] : 1;
		$PageSize = 20;

		$WhereArr = array();
		if(isset($_GET['keyword'])&&$_GET['keyword']!=""){
			$keyword=$_GET['keyword'];
			$WhereArr['name']=array('like',"%".$keyword."%");
		}
        $OrderM = new OrderModel();

		$OrderInfo = $OrderM->getList($WhereArr,$Page,$PageSize);
		$UserInfo = M('user')->where("is_del",1)->select();
		$GoodsInfo = M('goods')->where("is_up",1)->select();
//         dump($UserInfo);die;
		$all_page = ceil($OrderInfo['totalCount']/$PageSize);
		$UserInfo['all_page'] = $all_page;
		$this->OrderInfo = $OrderInfo;
		$this->UserInfo = $UserInfo;
		$this->GoodsInfo = $GoodsInfo;
		$this->Pages = $this->GetPages($UserInfo);
		$this->get = $_GET;
		$this->action = "/Adms/Order/lists.html";
		$this->display("lists");
	}

	//添加订单
	public function add(){
		$this->name = '添加订单';
		if($_POST){
                $OrderM = new OrderModel();
				$map = array();
				$map["name"] = $_POST["name"];
				$map["is_up"] = 1;
				$map["addtime"] = time();
                $OrderM->addOne($map);
				header('Location:lists.html');
			}
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//修改订单
	public function edit(){
		$this->name = '修改订单';  // 进行模板变量赋值

		//获取id
		$id = $_GET['id'];
		if ($id){
            $OrderM = new OrderModel();
			$where['id'] = $id;
			//查数据
			$OrderInfo = $OrderM->getOne($where);
		}
		$this->OrderInfo = $OrderInfo;
		$this->id = $id;
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//处理修改
	public function update(){
		$this->name = '修改订单';  // 进行模板变量赋值

		$id = $_POST['id'];
		$where = array();
        $OrderM = new OrderModel();
		if ($id){
			$data['name'] = $_POST['name'];
			$where["id"] = $_POST['id'];
			$res = $OrderM->updateOne($where,$data);
			if ($res){
				$this->tip = "修改成功";
			}
			header('Location:lists.html');
		}
	}

	//软删除订单
	public function del(){
		$this->name = '删除订单';
        $OrderM = new OrderModel();
		$where = array();
		$where['id'] = $_GET['id'];
		if($_GET["id"]){
			$data['is_del'] = 0;
			$ret = $OrderM->updateOne($where,$data);
			header("Location:/Adms/Order/lists");
		}
	}

}

?>
