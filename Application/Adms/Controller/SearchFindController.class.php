<?php

class SearchFindController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//搜索发现列表
	public function lists(){
		$this->name = '搜索发现列表'; // 进行模板变量赋值

		$SearchFindM = new SearchFindModel();
		$SearchFindInfo = $SearchFindM->getAll();

		$this->SearchFindInfo = $SearchFindInfo;
		$this->action = "/Adms/SearchFind/lists.html";
		$this->display("lists");
	}

	//添加搜索发现
	public function add(){
		$this->name = '添加搜索发现';
		$SearchFindM = new SearchFindModel();
		if($_POST){
			if($_POST["name"]){
				$map = array();
				$map["name"] = $_POST["name"];
				$SearchFindM->addOne($map);
				header('Location:lists.html');
			}else{
				$this->tip = "不能为空！";
			}
		}
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//修改搜索发现
	public function edit(){
		$this->name = '修改搜索发现';  // 进行模板变量赋值

		//获取id
		$id = $_GET['id'];
		if ($id){
			$SearchFindM = new SearchFindModel();
			$where['id'] = $id;
			//查数据
			$SearchFindInfo = $SearchFindM->getOne($where);
		}
		$this->SearchFindInfo = $SearchFindInfo;
		$this->id = $id;
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//处理修改
	public function update(){
		$this->name = '修改搜索发现';  // 进行模板变量赋值

		$id = $_POST['id'];
		$where = array();
		$SearchFindM = new SearchFindModel();
		if ($id){
			$data['name'] = $_POST['name'];
			$where["id"] = $_POST['id'];
			$res = $SearchFindM->updateOne($where,$data);
			if ($res){
				$this->tip = "修改成功";
			}
			header('Location:lists.html');
		}
	}

	//软删除搜索发现
	 public function del(){
	 	$this->name = '删除搜索发现';
		$where['id'] = $_GET['id'];
		 $SearchFindM = new SearchFindModel();
		 if($_GET["id"]){
			 $data['is_del'] = 0;
			 $ret = $SearchFindM->updateOne($where,$data);
			 header("Location:/Adms/SearchFind/lists");
		 }
	 }


}

?>
