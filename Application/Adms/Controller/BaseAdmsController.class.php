<?php

use Think\Controller;

class BaseAdmsController extends Controller
{
    public $Api_recive_date = array();
    public $lang = 'simplified';
    public $uid = '0';


    public function _initialize()
    {
            $AdminUser = session("AdminUser");
            $this->is_login();
            $supermen=M('auth_group_access')->where(array('group_id'=>1))->getField('uid',true);

            if (!in_array($AdminUser['id'],$supermen)) {
                $auth = new \Think\Auth();
                $rule_name = MODULE_NAME . '/' . CONTROLLER_NAME . '/' . ACTION_NAME;
                $result = $auth->check($rule_name, $AdminUser['id']);
                if (!$result) {
                    $this->error('您没有权限访问');
                }
            }
    }

    public function __construct()
    {
        parent::__construct();
        //$this->is_login();
        $this->AdminUser = cookie("AdminUser");
        // 分配菜单数据
        $nav_data = D('AdminNav')->getTreeData('level', 'order_number,id');
        $assign = array(
            'nav_data' => $nav_data
        );
        //$this->menu = include WR."/userdata/admin/menu.php";
        $this->menu = $nav_data;

        //echo TranslateAnysay("hi");exit;
        //Dump($this->AdminUser);exit;
    }


    //连接redis
    public function redisconn()
    {
        $redis = new RedisModel();
        return $redis;
    }



    /*
     *
     * 验证用户是否登陆
     */
    public function is_login()
    {
        $Public_controller = array(
            'View',
            'Ajax',
            'Login',
            'Pay'
        );
        if (!in_array(CONTROLLER_NAME, $Public_controller)) {
            if (!session("AdminUser")) {
                header("Location:/Adms/Login/login.html");
                //$this->error('操作失败','/Adms/Index2/Login');
                //IndexController::Index2();exit;
            }
        }
    }

    //生成分页
    public function GetPages($arges)
    {

        $pre_page = $arges["pageNum"] - 1 ? $arges["pageNum"] - 1 : 1;
        $nex_page = $arges["pageNum"] + 1;
        $arges["all_page"] = $arges["all_page"] ? $arges["all_page"] : ceil($arges["totalCount"] / $arges["pageSize"]);
        if ($arges["all_page"] < 2) {
            return '';
        }
        $url = $_SERVER['PHP_SELF'];
        $url_arges = $_GET;
        if (empty($url_arges)) {
            $url .= "?a=1";
        } else {
            unset($url_arges["Page"]);
            $url .= "?" . http_build_query($url_arges);
        }

        $pages = '<ul class="pagination">';
        $pages .= '<li><a>共' . $arges["totalCount"] . '条数据</a></li>';
        if ($pre_page > 0) {
            $pages .= '<li><a href="' . $url . "&Page=" . $pre_page . '">上一页</a></li>';
        }
        $start = $arges["pageNum"] - 5;
        if ($start < 1) {
            $start = 1;
        }
        $end = $arges["pageNum"] + 5;
        if ($end > $arges["all_page"]) {
            $end = $arges["all_page"];
        }

        for ($x = $start; $x <= $end; $x++) {
            if ($x == $arges["pageNum"]) {
                $pages .= '<li class="active"><a href="' . $url . "&Page=" . $x . '">' . $x . '</a></li>';
            } else {
                $pages .= '<li><a href="' . $url . "&Page=" . $x . '">' . $x . '</a></li>';
            }
        }
        if ($nex_page <= $arges["all_page"]) {
            $pages .= '<li><a href="' . $url . "&Page=" . $nex_page . '">下一页</a></li>';
        }

        $pages .= '</ul> ';
        return $pages;
    }
    /**
     * 用用户id生成用户目录
     */
    public function get_userurl($uid, $type)
    {
        return $ret = '/userdata/' . $type . '/' . ($uid % 100) . '/' . ($uid % 200) . '/';
    }

    /**
     * 生成管理员操作日志
     */
    function addlog($id, $uid = 0, $status = 0)
    {
        $adminuser = session("AdminUser");//管理员id
        $data['admin_id'] = $adminuser['id'];
        $data['other_id'] = $id ? $id : '';
        $data['uid'] = $uid ? $uid : '';
        $data['status'] = $status ? $status : '';
        $data['ip'] = get_client_ip();//操作ip
        $data['create_time'] = time();//操作时间
        $data['name'] = strtolower(MODULE_NAME . "/" . CONTROLLER_NAME . "/" . ACTION_NAME);
        $url['name'] = strtolower(MODULE_NAME . "/" . CONTROLLER_NAME . "/" . ACTION_NAME);
        $authrule = M("auth_rule"); // 实例化authrule对象
        $data['auth_rule_title'] = $authrule->where($url)->getField('title');
        $admin_log = M("admin_log"); // 实例化admin_log对象
        $admin_log->add($data);
        return true;
    }


}


?>