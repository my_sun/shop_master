<?php

class ReturnIntegralController extends BaseAdmsController {
	public function __construct(){
		parent::__construct();
		$this->cachepath = WR . '/userdata/cache/face/';
		$this->msgfileurl = '/userdata/faceimg/'.date('Ymd',time())."/";
	}

	//注册成功返积分列表
	public function lists(){
		$this->name = '注册成功返积分列表'; // 进行模板变量赋值

		$ReturnIntegralM = new ReturnIntegralModel();
		$ReturnIntegralInfo = $ReturnIntegralM->getAll();

		$this->ReturnIntegralInfo = $ReturnIntegralInfo;
		$this->action = "/Adms/ReturnIntegral/lists.html";
		$this->display("lists");
	}

	//添加返积分
	public function add(){
		$this->name = '添加返积分';

		if ($_POST){
			if($_POST["return_up_integral"] && $_POST["return_up_up_integral"]){
				$ReturnIntegralM = new ReturnIntegralModel();
				$map = array();
				$map["return_up_integral"] = $_POST["return_up_integral"];
				$map["return_up_up_integral"] = $_POST["return_up_up_integral"];
				$map["addtime"] = time();
				$ReturnIntegralM->addOne($map);
				header('Location:lists.html');
			}else{
				$this->tip = "不能为空！";
			}
		}

		$this->msgurl=C ("IMAGEURL");
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//修改返积分列表
	public function edit(){
		$this->name = '修改返积分列表';  // 进行模板变量赋值

		//获取id
		$id = $_GET['id'];
		if ($id){
			$ReturnIntegralM = new ReturnIntegralModel();
			$where['id'] = $id;
			//查数据
			$ReturnIntegralInfo = $ReturnIntegralM->getOne($where);
		}
		$this->ReturnIntegralInfo = $ReturnIntegralInfo;
		$this->id = $id;
		$this->action =  __ACTION__.".html";
		$this->display();
	}

	//处理修改
	public function update(){
		$this->name = '修改返积分列表';  // 进行模板变量赋值

		$id = $_POST['id'];
		$where = array();
		$ReturnIntegralM = new ReturnIntegralModel();
		if ($id){
			$data['return_up_integral'] = $_POST['return_up_integral'];
			$data['return_up_up_integral'] = $_POST['return_up_up_integral'];
			$where["id"] = $_POST['id'];
			$res = $ReturnIntegralM->updateOne($where,$data);
			if ($res){
				$this->tip = "修改成功";
			}
			header('Location:lists.html');
		}
	}

	//删除返积分
	 public function del(){
	 	$this->name = '删除返积分';

	 	if($_GET["id"]){
			$ReturnIntegralM = new ReturnIntegralModel();
	 		$ret = $ReturnIntegralM->delOne(array("id"=>$_GET["id"]));
	 		header("Location:/Adms/ReturnIntegral/lists");
	 	}
	 }

}

?>
