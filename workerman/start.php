<?php
/**
 * run with command 
 * php start.php start
 */

use Workerman\Worker;
// composer 的 autoload 文件
require_once 'Workerman/Autoloader.php';

if(strpos(strtolower(PHP_OS), 'win') === 0)
{
    exit("start.php not support windows, please use start_for_win.bat\n");
}

foreach(glob(__DIR__.'/App/*/start.php') as $start_file)
{
    require_once $start_file;
}

// 运行所有服务
Worker::runAll();


/*

启动
以debug（调试）方式启动

php start.php start

以daemon（守护进程）方式启动

php start.php start -d

停止
php start.php stop

重启
php start.php restart

平滑重启
php start.php reload

查看状态
php start.php status

强行杀死所有workerman进程
（要求workerman版本>=3.2.2）

php start.php kill
*/