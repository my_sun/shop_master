<?php
use Workerman\Worker;
/* $redis = new Redis();
$redis->connect('10.27.44.139', 6379);
$redis->auth("990921"); //设置密码 */
// 全局数组保存uid在线数据
$uidConnectionMap = array();
// 监听一个http端口
$inner_http_worker = new Worker('http://0.0.0.0:212');
$inner_http_worker->count = 1;
$inner_http_worker->name = 'timer';
// 当http客户端发来数据时触发
$inner_http_worker->onMessage = function($http_connection, $data){
    // 计数
    $count = 1;
    // 要想$timer_id能正确传递到回调函数内部，$timer_id前面必须加地址符 &
    $timer_id = Timer::add(1, function()use(&$timer_id, &$count)
    {
        echo "Timer run $count\n";
        // 运行10次后销毁当前定时器
        if($count++ >= 10)
        {
            echo "Timer::del($timer_id)\n";
            Timer::del($timer_id);
        }
    });
    print_r($data);
};
// 执行监听
$inner_http_worker->listen();




