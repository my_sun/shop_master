<?php
define('WR',dirname(__FILE__)); 
/**
 * Created by PhpStorm.
 * User: yuyi
 * Date: 16/12/24
 * Time: 07:01
 */
include WR."/Docmake.php";
include WR."/WClass.php";
include WR."/WDoc.php";
include WR."/WFunction.php";
include WR."/WFile.php";
include WR."/wing-file-system/WDir.php";
include WR."/wing-file-system/WFile.php";
include WR."/wing-html/Html.php";
function get_millisecond()
{
    $time = explode(' ', microtime());
    return (float)sprintf('%.0f', (floatval($time[0]) + floatval($time[1])) * 1000);
}
$apiPath = "../../Application/Api/Controller";
$product = $_GET["product"]?$_GET["product"]:20110;
$md5str=preg_replace('|[0-9/]+|','',md5($product));
$productkey = substr($md5str, 0, 5);
try {
    $start_time = get_millisecond();

    $app = new \Wing\Doc\Doc(
        $apiPath,
        "doc"
    );
    $app->addExcludePath(array(
        "vendor/*", "Config/*", "config/*",
        "public/*", "database/*", "tests/*"
    ));
    $app->addExcludeFileName(
        array(
            "BaseController.class.php",
            "AdmController.class.php",
            "CaijiController.class.php",
            "OnlineController.class.php",
            "AutomsgController.class.php",
            "MoneyController.class.php",
            "PublicController.class.php",
            "index.html"
        )
    );
    $ret = $app->run();
    $htac = "";
    $phtac = "";
    $pstr = "";
    $wstr = "";
    foreach ($ret as $k=>$v){
        $newk = $v;
        foreach($v as $v1){
            $md5str = md5($product.$v1["key"].$v1["name"]);
            $md5str=preg_replace('|[0-9/]+|','',$md5str);
            $newv1 = $md5str;
            $ordaction = $k."/".$v1["key"];
            $ordactionres = encode($ordaction,$product);
            $productres = md5($ordaction.$productkey);
            $productres = preg_replace('|[0-9/]+|','',$productres);
            $newaction = $productres."/".$ordactionres;
            $pstr = $pstr.'<a class="list-group-item"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>'. $ordaction.'<span style="color:#5cb85c;margin:1rem;font-weight:bold">></span>'.$newaction.'<span class="label label-success">'.$v1["name"].'</span></a>';
        }
    }
    echo '<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
  <body>';
    echo '<style>
    body{background-color:#ddd}
    .main{width:80%;margin:0 auto;padding:20px;}
    .glyphicon{margin-right:1rem}
    .label{float:right}
    </style>';
    echo "<div class=\"main\">";
    echo '<div class="list-group">
  <a href="#" class="list-group-item active" style="font-size:2rem">
    <span class="glyphicon glyphicon-th-large" style="color:#FFFFFF;font-size:2rem" aria-hidden="true"></span>'.$product.'平台接口对照表
  </a>'.$pstr.'
</div>';
    echo "</div>
</body>
</html>";
    
    //echo "完成，耗时" . (get_millisecond() - $start_time) . "毫秒\r\n";
} catch(\Exception $e) {
    echo $e->getMessage();
}
function Dump($var){
    echo "<pre>";
    print_r($var);
    echo "</pre>";
}
function encode($tex, $key = null) {
    $key = $key ? $key : "20111";
    $md5str=preg_replace('|[0-9/]+|','',md5($key));
    $key = substr($md5str, 0, 2);
    $texlen = strlen($tex);
    $rand_key=md5($key);
    $reslutstr = "";
    for ($i = 0; $i < $texlen; $i++) {
        $reslutstr.=$tex{$i} ^ $rand_key{$i % 32};
    }
    $reslutstr = trim(base64_encode($reslutstr), "==");
    $reslutstr = $key.substr(md5($reslutstr), 0, 3) . $reslutstr;
    return $reslutstr;
}
function decode($tex) {
    $key = substr($tex, 0, 2);
    $tex = substr($tex, 2);
    $verity_str = substr($tex, 0, 3);
    $tex = substr($tex, 3);
    if ($verity_str != substr(md5($tex), 0, 3)) {
        //完整性验证失败
        return false;
    }
    $tex = base64_decode($tex);
    $texlen = strlen($tex);
    $reslutstr = "";
    $rand_key=md5($key);
    for($i = 0; $i < $texlen; $i++) {
        $reslutstr.=$tex{$i} ^ $rand_key{$i % 32};
    }
    return $reslutstr;
}
function generateRandomString($length = 10) {
    $characters = 'abcdefghijklmnopqrstuvwxyz';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}
function change($num){
    $arr=array(
        1=>'a',
        2=>'b',
        3=>'c',
        4=>'d',
        5=>'e',
        6=>'f',
        7=>'g',
        8=>'h',
        9=>'i',
        0=>'j'
    );
    $nums_arr=explode("\r\n",chunk_split($num,1));
    foreach($nums_arr as $n){
        if($n!=''){
            $str.=$arr[$n];
        }
    }
    return $str;
}