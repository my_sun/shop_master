<?php namespace Wing\Doc;

use Wing\FileSystem\WDir;
use Wing\FileSystem\WFile;
use Wing\Html\Html;

/**
 * 文档生成器
 * 与phpdoc不同的是，WingDoc使用的是简单的正则分析，运行时无上下文依赖
 * 因此，WingDoc支持所有的php系统
 *
 * @author yuyi
 * @version V1.0
 * @email 297341015@qq.com
 * @create-at 2016-12
 */
class Doc
{
    private $input_dir;
    private $out_dir;
    private $dirs  = array();
    private $files = array();

    /**
     * @var array 支持的文件后缀
     */
    private $support_file_ext = array(
        "php","","html","htm"
    );

    /**
     * @var array 排除的文件路径
     */
    private $exclude_path = array();

    /**
     * @var array 排除的文件名
     */
    private $exclude_filename = array();

    /**
     * @var array 排除的文件
     */
    private $exclude_file = array();

    /**
     * @请保证此文件的可写和可读
     */
    private $cache_path;// = __DIR__."/cache";

    /**
     * @构造函数
     *
     * @param string $input_dir 输入目录
     * @param string $output_dir 输出目录
     */
    public function __construct($input_dir, $output_dir)
    {
        $input_dir        = str_replace("\\","/",$input_dir);
        $this->input_dir  = rtrim($input_dir,"/");
        $this->out_dir    = $output_dir;
        $this->cache_path = __DIR__."/cache";
    }

    public function setCachePath($path)
    {
        $this->cache_path = $path;
    }

    /**
     * @添加支持的文件后缀
     *
     * @param string|array $ext
     */
    public function addSupportFileExtension($ext)
    {
        if (is_array($ext))
            $this->support_file_ext = array_merge($this->support_file_ext, $ext);
        else
            $this->support_file_ext[] = $ext;
    }

    /**
     * @添加排除目录
     *
     * @param string|array $path
     */
    public function addExcludePath($path)
    {
        if (is_array($path))
            $this->exclude_path = array_merge($this->exclude_path, $path);
        else
            $this->exclude_path[] = $path;
    }

    /**
     * @添加排除的文件名，可以包含扩展，也可以不含扩展
     */
    public function addExcludeFileName($file_name)
    {
        if (is_array($file_name)) {
            $this->exclude_filename = array_merge($this->exclude_filename, $file_name);
        }
        else {
            $this->exclude_filename[] = $file_name;
        }
    }


    /**
     * @添加排除的文件
     */
    public function addExcludeFile($file)
    {
        if (is_array($file)) {
            $this->exclude_file = array_merge($this->exclude_file, $file);
        }
        else {
            $this->exclude_file[] = $file;
        }
    }


    /**
     * @程序入口
     */
    public function run()
    {
        return $this->parse();
        //echo "输出目录：",$this->out_dir,"\r\n";
    }

    /**
     * @格式化模板文件 生成doc html
     */
    private function parse()
    {

        $this->helperScandir();


        


       
        $retarr= array();
        foreach($this->files as $file) {
            
            $wfile      = new \Wing\Doc\WFile($file);
            $classes    = $wfile->getClasses();
           
            
            foreach ($classes as $class) {
                if (!$class instanceof WClass)
                    continue;
                $namespace = $class->getNamespace();
                if ($namespace)
                    $namespace.="\\";
                $class_name = $namespace.$class->getClassName();
                $class_name = strtolower(str_replace("Controller","",$class_name));
                
               
                $functions = $class->getFunctions();
                $classIndex=0;
                foreach ($functions as $index => $function) {
                    if (!$function instanceof WFunction)
                        continue;
                    
                    $static = $function->getStatic();
                    if ($static)
                        $static.=" ";

                    $func_doc = $function->getDoc();
                    $requests = $function->getRequest();
                    $url = $func_doc->url;
                    if(!$requests){
                        continue;
                    }else{
                        $func_doc_str = trim($func_doc->doc);
                        $temp["key"]=$function->getFunctionName();
                        $temp["name"]= $func_doc_str;
                        $retarr[$class_name][]=$temp;
                        //$url = "http://47.75.79.115:81/".$class_name."/".$function->getFunctionName();
                        $classIndex++;
                    }
                   


                }
                unset($functions,$class_name);
                
            }
           

            unset($class_html, $wfile, $classes);

        }

       return $retarr;

    }


    /**
     * @目录遍历
     *
     * @return void
     */
    private function helperScandir()
    {
       // $this->files[] =
         //   "/Users/yuyi/Web/xiaoan/api/vendor/doctrine/annotations/lib/Doctrine/Common/Annotations/Reader.php";
            //"/Users/yuyi/Web/xiaoan/api/vendor/doctrine/annotations/lib/Doctrine/Common/Annotations/AnnotationException.php";
       //return;
        $path[] = $this->input_dir.'/*';
        while (count($path) != 0) {
            $v = array_shift($path);
            foreach (glob($v) as $item) {

                $is_match = false;
                foreach ($this->exclude_path as $c) {
                    $c     = str_replace("/", "\/", $c);
                    $c     = str_replace("*", ".*", $c);
                    $is_match = preg_match("/$c/", $item);
                    if ($is_match) {
                        break;
                    }
                }

                if ($is_match)
                    continue;

                if (is_dir($item)) {
                    $this->dirs[] = $item;
                    $path[] = $item . '/*';
                }
                elseif (is_file($item)) {
                    $info = pathinfo($item);

                    $is_pass = false;
                    foreach ($this->exclude_filename as $ex_file_name) {
                        if ($ex_file_name ==  $info["basename"] || $ex_file_name == $info["filename"]) {
                            $is_pass = true;
                            break;
                        }
                    }

                    foreach ($this->exclude_file as $ex_file) {
                        $ex_file = str_replace("\\","/",$ex_file);
                        if ($ex_file == str_replace("\\","/", $item)) {
                            $is_pass = true;
                            break;
                        }
                    }

                    if ($is_pass)
                        continue;

                    $ext  = "";
                    if (isset($info["extension"]))
                        $ext = $info["extension"];
                    if (in_array($ext,$this->support_file_ext))
                        $this->files[] = $item;
                }
            }
        }


    }

    /**
     * @树形结构html生成
     *
     * @return string
     */
    private function htmlFormat(array $datas)
    {
        $ul = new Html("ul");
        $ul->class = "file-list";
       
        foreach ($datas as $dir=>$data) {
            if (is_array($data)) {
                $li = new Html("li");
                $li->class = "is-dir h bg";

                $img = new Html("img");
                $img->src = "img/d.png";

                $li->append($img);

                $span = new Html("span");
                $span->html = $dir;

                $li->append($span);
                $ul->append($li);

                $li = new Html("li");
                $li->class = "is-dir";
                $li->html  = $this->htmlFormat($data);

                $ul->append($li);
            }
            else {
                list($name,$file) = explode("|",$data);
                
                $link = md5($file);
                
                $li = new Html("li");
                $li->setClass("is-file h li-".$link);
                $li->setAttr("data-tab",md5($file));
                $li->setAttr("data-file",$file);

                $span = new Html("span");
                $name = strtolower(str_replace("Controller.class.php","",$name));
                $span->html = $name;

                $li->append($span);

                $ul->append($li);
            }
        }
        return $ul->getTtml();
    }

    /**
     * @获取所有的目录
     *
     * @return array
     */
    public function getDirs()
    {
        return $this->dirs;
    }

    /**
     * @获取所有的文件
     *
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @文件路径树型结构生成算法
     *
     * @return array
     */
    private function filesDataFormat()
    {

        $pdir  = $this->input_dir;
        $files = $this->files;
        $datas = array();

        foreach ($files as $file) {
            $raw_file = $file;
            $file = ltrim(str_replace($pdir,"",$file),"/");
            $info = pathinfo($file);
            //dirname basename
            $sp = explode("/",$info["dirname"]);
            $tt = &$datas;
            $last = array_pop($sp);
            foreach ($sp as $d) {
                if (!isset($tt[$d]))
                    $tt[$d] = array();
                $tt = &$tt[$d];
            }
            $tt[$last][] = $info["basename"]."|".$raw_file;
            $tt = &$tt[$last];
        }

        return $datas;
    }
}