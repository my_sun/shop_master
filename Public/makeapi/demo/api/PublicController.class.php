<?php
class PublicController {
    public      $language ='';
	public function __construct(){


	}

    //获取礼物列表中相关数据
    public function giftextend ($id,$product="10008",$language)
    {
        $redis=$this->redisconn();
        //缓存中储存总数据
        $lang=$redis->exists('gift_' . $product.'_'.$id);
        //如果缓存中不存在，则取数据库最新数据存入
        if($lang){
            $Lists=$redis->get('gift_' . $product.'_'.$id);
            if ($Lists) {
                $Lists =json_decode($Lists,true);
            }
        } else {
            $getM = new GiftModel();
            $Lists = $getM->getOne(array("id" => $id));
            $Lists1 =json_encode($Lists,true);
            $redis->set('gift_' . $product.'_'.$id,$Lists1,0,0);
        }
        $Lists["title"] = $this->L($Lists["code"],null,$language);
        $Lists["content"] = $this->L($Lists["code"],null,$language);
        return $Lists;
    }

    public function redisconn(){
        $redis = new RedisModel();
        return $redis;
    }
    public function LangSet($langs){

        $languagecode = $this->get_languagecode();

        if(!in_array($langs, $languagecode)||$langs==""){
            $langs = "zh-tw";
        }
        $file   =  C("LANG_PATH").$langs.'.txt';

        $cachefile   =  C("LANG_PATH")."cache/".$langs.'.php';

        if(!file_exists($cachefile)||(filemtime($file)>filemtime($cachefile))){
            $TranslateM = new TranslateModel();
            $Wdata = array();
            $Wdata["lang"]=$langs;
            $ret = $TranslateM->getList($Wdata);
            $temp = array();
            foreach ($ret as $k=>$v){
                $temp[$v["code"]]=$v['content'];
            }

            $temp = "<?php return ".var_export($temp, true).";";
            mkdirs(dirname($cachefile));
            file_put_contents($cachefile, $temp);
        }

        import("Api.lib.Behavior.CheckLangBehavior");
        $lang = new CheckLangBehavior();

        $lang->run($langs);

        return $lang;
    }
    public function get_languagecode($reset=0){
        $path = CHCHEPATH_LANGUAGE;
        $cache_name = 'languagelistcode';
        if(F($cache_name,'',$path) && $reset == 0){
            $newarr = F($cache_name,'',$path);
        }else{
            $where = array();
            $LanguageM = new LanguageModel();
            $res = $LanguageM->getList($where);
            $newarr = array();
            foreach($res as $v){
                $newarr[] =$v["code"];
            }
            F($cache_name,$newarr,$path);
        }
        return $newarr;
    }
    public function L($name=null, $value=null,$language) {
        return $this->LangSet($language)->L($name,$value);
    }
}

?>
