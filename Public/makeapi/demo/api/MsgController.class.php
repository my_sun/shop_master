<?php
class MsgController extends BaseController
{
    public function __construct ()
    {
        parent::__construct ();
        //echo date("Y-m-d",time());exit;
        $this->msgfileurl = '/userdata/msgfile/' . date ('Ymd', time ()) . "/";
        $this->cachepath = WR . '/userdata/cache/msg/';
    }

    //打招呼sayhello
    public function sayhello ()
    {
        $data = $this->Api_recive_date;
        $touid = $data["touid"] ? $data["touid"] : 0;
        $uid = $this->uid;
        //测试回信 start
        //$AutomsgCon = new AutomsgController();
        //$AutomsgCon->testreply($touid,$uid,"打招呼");
        //测试回信 end
        $redis = $this->redisconn ();
        $redisStr = "sayhello_" . $uid . "_" . $touid;
        if($uid && $touid){
            if($redis->exists ($redisStr)){
                $return['code'] = ERRORCODE_201;
                $return['message'] = $this->L ("yijingdaguozhaohule");
            }else{
                $MyUserInfo = $this->UserInfo;
                $ToUserInfo = $this->get_user($touid);
                if($MyUserInfo["user_type"]==1&&$ToUserInfo["user_type"]==2){
                    
                    $recmsgcountkey = "recmsgcount_".$uid;
                    if($redis->exists($recmsgcountkey)){
                        $recmsgcount = $redis->get($recmsgcountkey);
                    }else{
                        $recmsgcount = 0;
                    }
                    if($recmsgcount<=1){
                        $jilv=10;
                    }elseif($recmsgcount<=5){
                        $jilv=6;
                    }else{
                        $jilv=3;
                    }
                    $rands = rand(1,100);
                    if($rands<=$jilv){
                        $AutomsgCon = new AutomsgController();
                        $AutomsgCon->rundmsg($uid,$touid);
                    }
                }
                $nowtime = time ();
                $endToday = mktime (0, 0, 0, date ('m'), date ('d') + 1, date ('Y')) - 1;
                $cachetime = $endToday - $nowtime;
                $redis->set ($redisStr, "1", 0, 0, $cachetime);
                $return['code'] = ERRORCODE_200;
                $return['message'] = $this->L ("CHENGGONG");
            }
        }else{
            $return['code'] = ERRORCODE_201;
            $return['message'] = $this->L ("CANSHUYICHANG");
        }
        Push_data ($return);
    }

    //打招呼群发
    public function sayhelloall ()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $touid_all = explode ("|", $data['touid']);
        $result = array();
        $return['code'] = ERRORCODE_200;
        $return['message'] = $this->L ("CHENGGONG");
        $result = $return;
        foreach ($touid_all as $k => $v) {
            $touid = $v;
            $redis = $this->redisconn ();
            $redisStr = "sayhello_" . $uid . "_" . $touid;
            if ($uid && $touid) {
                if ($redis->exists ($redisStr)) {
                    /* $return[$v]['code'] = ERRORCODE_201;
                    $return[$v]['message'] = $this->L ("yijingdaguozhaohule");
                    $result['data']=$return; */
                } else {
                    $nowtime = time ();
                    $endToday = mktime (0, 0, 0, date ('m'), date ('d') + 1, date ('Y')) - 1;
                    $cachetime = $endToday - $nowtime;
                    $redis->set ($redisStr, "1", 0, 0, $cachetime);

                }
            } else {
                $return['code'] = ERRORCODE_201;
                $return['message'] = $this->L ("CANSHUYICHANG");
                $result = $return;

            }
        }
        Push_data ($result);
    }

    //设为已读
    public function setread ()
    {
        $data = $this->Api_recive_date;
        //$platforminfo = $this->platforminfo;
        //$uid = $this->uid;
        if (isset($data['id'])) {
            $id = $data['id'];
            $map['id'] = $id;
            $map1['id'] = $id;
            $map1['isread'] = 1;
            $data1['isread'] = 1;
            $MsboxM = new MsgBoxModel();
            $result1 = $MsboxM->getOne1 ($map1);
            if ($result1) {
                Push_data (array('message' => $this->L ("已经设置为已读"), 'code' => ERRORCODE_501));
            }
            $result = $MsboxM->updateOne1 ($map, $data1);
            if ($result) {
                $this->get_msgboxlist (0, 0, 0, 1);
                $return = array();
                $return['message'] = $this->L ("CHENGGONG");
                Push_data ($return);
            }
        } else {
            Push_data (array('message' => $this->L ("id字段不能为空"), 'code' => ERRORCODE_501));
        }
    }

    /*
     *是否打招呼
     */
    public function isSayHello($touid, $uid = 0)
    {
        $uid = $uid ? $uid : $this->uid;
        $redis = $this->redisconn ();
        $redisStr = "sayhello_" . $uid . "_" . $touid;
        if ($redis->exists ($redisStr)) {
            return 1;
        } else {
            return 0;
        }
    }

    /*
         *发信
         */

    public function sendmsg ()
    {
        $data = $this->Api_recive_date;
        $platforminfo = $this->platforminfo;
        $uid = $this->uid;
        $MyUserInfo = $this->UserInfo;
        //参数完整性验证start
        if (!isset($data['touid'])) {
            Push_data (array('message' => $this->L ("touid字段不能为空"), 'code' => ERRORCODE_501));
        }
        /* if (!isset($data['content'])) {
            Push_data (array('message' => $this->L ("content字段不能为空"), 'code' => ERRORCODE_501));
        } */
        //参数完整性验证end

        $redis = $this->redisconn ();
        $redisStrj = "Smsg_" . $uid;
        $resultj1 = $this->judgeAuth ($MyUserInfo);
        $resultj = true;
        if ($resultj) {
            $msgtype = $data['msgtype'] ? $data['msgtype'] : "text";//发信类型:text文本消息，voice语音消息，video视频消息，location位置信息，gift礼物消息,image图片
            $content = $data['content'] ? $data['content'] : "";//发信内容//文本时
            $touid = $data['touid'] ? $data['touid'] : "";//收信人用户ID
            $ToUserInfo = $this->get_user ($touid);//收信人用户信息
            $contentint = $data['contentint'] ? $data['contentint'] : "0";//礼物数量、语音、红包金币数量或视频时长
            $contentbody = $data['contentbody'] ? $data['contentbody'] : "";//礼物id、语音url或视频url
            $contentsub = $data['contentsub'] ? $data['contentsub'] : "";//视频第一帧图片url
            $mybox_id = $uid . $touid;
            $tobox_id = $touid . $uid;
            
            
            $MyBoxinfo = array();
            $ToBoxinfo = array();
            $Msginfo = array();
            switch ($msgtype) {
                //voice语音消息
                case "voice":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    
                    $messageBody = array(
                        "content"=>$content,
                        "sendTime"=>time(),
                        "voice"=>array("url"=>$contentbody,"ltime"=>$contentint)
                    );
                    break;
                //video视频消息
                case "video":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    $Msginfo['contentsub'] = $contentsub;
                    $messageBody = array(
                        "content"=>$content,
                        "sendTime"=>time(),
                        "video"=>array("url"=>$contentbody,"ltime"=>$contentint,"imageurl"=>$contentsub)
                    );
                    break;
                //location位置信息
                case "location":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    $messageBody = array(
                        "content"=>$content,
                        "sendTime"=>time(),
                        "location"=>array("longitude"=>$contentint,"latitude"=>$contentbody)
                    );
                    break;
                //gift礼物消息
                case "gift":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    $result = $this->sendgift ($uid, $touid, $contentint, $contentbody);
                    $giftobj = $this->giftextend($contentbody);
                    $giftobj["count"] = $contentint;
                    $giftobj["demand"] = true;
                    $messageBody = array(
                        "content"=>$content,
                        "sendTime"=>time(),
                        "gift"=>$giftobj
                    );
                    if($result=='error'){
                        Push_data (array('message' => $this->L ("系统错误,插入数据库失败"), 'code' => ERRORCODE_501));
                    }elseif($result){
                        $leftgold=$result;
                        break;

                    }else{
                        Push_data (array('message' => $this->L ("金币不足请充值！"), 'code' => ERRORCODE_201));
                    }
                    
                    break;
                //askgift索要礼物消息    
                case "askgift":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    $giftobj = $this->giftextend($contentbody);
                    $giftobj["count"] = $contentint;
                    $giftobj["demand"] = false;
                    $messageBody = array(
                        "content"=>$content,
                        "sendTime"=>time(),
                        "gift"=>$giftobj
                    );
                    break;
                //,image图片
                case "image":
                    $Msginfo['contentint'] = $contentint;
                    $Msginfo['contentbody'] = $contentbody;
                    $imageobj = array(
                        "id"=>$contentint,
                        "url"=>$contentbody,
                        "status"=>1,
                        "seetype"=>1
                    );
                    $messageBody = array(
                        "content"=>$content,
                        "sendTime"=>time(),
                        "image"=>$imageobj
                    );
                    break;
                case "redpacket":
                    $Msginfo['contentint'] = $contentint;
                    $result = $this->sendredPacket ($uid, $touid, $contentint, $content);
                    $messageBody = array(
                        "content"=>$content,
                        "sendTime"=>time(),
                        "redPacket"=>array("diamondCount"=>$contentint,"describe"=>$content)
                    );
                    if($result=='error'){
                        Push_data (array('message' => $this->L ("系统错误,插入数据库失败"), 'code' => ERRORCODE_501));
                    }elseif($result=='true'){

                        break;

                    }else{
                        Push_data (array('message' => $this->L ("金币不足请充值！"), 'code' => ERRORCODE_201));
                    }
                    break;
                default:

                    break;
            }
            //auto and money start
            if($ToUserInfo["user_type"]==2){
                $AutomsgCon = new AutomsgController();
                $redisStr = "pmsg_".$uid."_".$touid;
                if($redis->exists($redisStr)){
                    $puid = $redis->get($redisStr);
                    //客户端消息对象封装
                    $clientMsgObj = array();
                    $clientMsgObj["touid"]=$puid;
                    $clientMsgObj["msgid"]=substr(md5($AutomsgCon->getMillisecond()), 8, 16);//substr(md5(time()), 8, 16)
                    $body = array();
                    $body["id"]=$clientMsgObj["msgid"];
                    $body["msgType"]=$msgtype;
                    $body["user"]=$this->get_diy_user_field($uid,"uid|gender|age|nickname|vipgrade|vip|gold|head");
                    $body["puser"]=$this->get_diy_user_field($touid,"uid|gender|age|nickname|vipgrade|vip|gold|head");
                    
                    $body["messageBody"]=$messageBody;
                    $sendmsg["msgbody"]=$body;
                    LM_sendmsg($sendmsg);
                }else{
                    $msglistkey = "automsglist_".$mybox_id;
                    if($redis->exists($msglistkey)){
                        $msgisreplykey = "msgisreply_".$mybox_id;
                        $redis->set($msgisreplykey,2,0,0,60*60);//1未回复2已回复
                        $AutomsgCon->rundmsg($uid,$touid);
                    }
                }
                
            }
            if ($MyUserInfo["user_type"]==3){
                $MoneyC = new MoneyController();
                $MoneyC->set_moneykey($uid, $touid);
            }
            //auto and money end
            $MyBoxinfo['type'] = $msgtype;
            //if($msgtype==1) {

            $MyBoxinfo['id'] = $mybox_id;
            $MyBoxinfo['content'] = $content;
            $MyBoxinfo['uid'] = $uid;
            $MyBoxinfo['touid'] = $touid;
            $MyBoxinfo['country'] = $platforminfo['country '] ? $platforminfo['country '] : 'TW';
            $MyBoxinfo['language'] = $platforminfo['language '] ? $platforminfo['language '] : 'zh-tw';
            $MyBoxinfo['sendtime'] = time ();
            //$where = array('uid'=>$uid);

            //信箱所需字段信息
            $Msginfo['type'] = $msgtype;
            $Msginfo['sendtime'] = $MyBoxinfo['sendtime'];
            $Msginfo['language'] = $MyBoxinfo['language'];
            $Msginfo['country'] = $MyBoxinfo['country'];
            $Msginfo['uid'] = $uid;
            $Msginfo['touid'] = $touid;
            $Msginfo['content'] = $content;
            $Msginfo['box_id'] = $mybox_id;
            $MsgM = new MsgModel();
            $MsgBoxm = new MsgBoxModel();
            $result = $MsgM->addOne ($Msginfo);
            //消息列表储存过后将消息储存在信箱列表中，运用redis查询信息列表中是否已有来往信息
            if ($result) {
                //统计用户发送次数start
                $val = $redis->get ($redisStrj);
                $redis->set ($redisStrj, $val + 1, 0, 0, 60 * 60 * 24 * 3);
                //统计用户发送次数end

                $redisStr = "Gmsgbox_" . $mybox_id;

                $cachetime = 60 * 60 * 24 * 3;
                $ab['id']=$mybox_id;
                if ($redis->exists ($redisStr)) {
                    $MsgBoxm->updateOne1 ($ab, $MyBoxinfo);
                } else {
                    $value = $MsgBoxm->getOne1 ($ab);
                    if ($value) {
                        $MsgBoxm->updateOne1 ($ab, $MyBoxinfo);
                        $redis->set ($redisStr, $mybox_id, 0, 0, $cachetime);
                    } else {
                        $MsgBoxm->addOne1 ($MyBoxinfo);
                        $redis->set ($redisStr, $mybox_id, 0, 0, $cachetime);
                    }
                }
                    $return = array();
                    $return['message'] = $this->L ("CHENGGONG");
                    $return['data']['leftgold']=$leftgold;
                    Push_data ($return);
            }
        }else {
            Push_data (array('message' => $this->L ("请充值！"), 'code' => ERRORCODE_201));
        }
    }


    /*
     * 是否可以回复
     */
    public function cansendmsg ()
    {
        $dataUser = $this->UserInfo;
        $uid = $this->uid;
        $resultj = $this->judgeAuth ($dataUser);
        $return = array();
        if ($resultj) {
            $return['data']["cansendmsg"] = "1";
        } else {
            $return['data']["cansendmsg"] = "0";
        }
        Push_data ($return);
    }

    /*
     * 判断发送权限
     */
    public function judgeAuth ($dataUser)
    {
        $redisStrj = "Smsg_" . $dataUser["uid"];
        $redis = $this->redisconn ();
        $cachetimej = 60*60*24*3;
        $vip = $dataUser['vip'];
        $regtime = $dataUser['regtime'];
        $time =time();
        if ($vip>0) {
            //return "vip";
            return true;
        } else {
            if ($time < $regtime + $cachetimej) {
                if ($time < $regtime + (60 * 5)) {
                    //return 3;
                    return true;
                }
                
                 if ($redis->exists ($redisStrj)) {
                   $val = $redis->get ($redisStrj);
                     if ($val < 4) {
                            //return 3;
                            return true;
                     }else{
                           // return $val;
                         return false;
                     }

                   }else {
                       //return 5;
                        return true;
                  }
                
            }else{
                return false;
            }
        }
        return false;
    }

    //获取礼物列表中相关数据
    public function giftextend ($id,$product="10008")
    {
        $redis=$this->redisconn();
        //缓存中储存总数据
        $lang=$redis->exists('gift_' . $product.'_'.$id);
        //如果缓存中不存在，则取数据库最新数据存入
        if($lang){
            $Lists=$redis->get('gift_' . $product.'_'.$id);
            if ($Lists) {
                $Lists =json_decode($Lists,true);
            }
        } else {
            $getM = new GiftModel();
            $Lists = $getM->getOne(array("id" => $id));
            $Lists1 =json_encode($Lists,true);
            $redis->set('gift_' . $product.'_'.$id,$Lists1,0,0);
        }
        $Lists["title"] = $this->L($Lists["code"]);
        $Lists["content"] = $this->L($Lists["code"]);
        return $Lists;
    }
    /*
     * 获取信箱
     */
    public function getmsgboxlist ()
    {
        $data = $this->Api_recive_date;
        $uid = $this->uid;
        $pageNum = $data['page'] ? $data['page'] : 1;
        $pageSize = $data['pagesize'] ? $data['pagesize'] : 10;
        $page = array();
        $page["pagesize"] = $pageSize;
        $page["pageNum"] = $pageNum;
        $Wget = array();
        $Wget['uid'] = $uid;
        $MsboxM = new MsgBoxModel();
        $msgbox= $MsboxM->getListPage1($Wget, $pageNum, $pageSize);
        //$msgbox = $this->get_msgboxlist($Wget, $pageNum, $pageSize);
        $page["totalcount"] = $msgbox["totalCount"];
        $msgbox1 = array();
        $touidmsg = array();
        foreach ($msgbox['msgbox'] as $k => $v) {

            $msgbox1[$k]['id'] = $msgbox['msgbox'][$k]['id'];
            $msgbox1[$k]['isread'] = $msgbox['msgbox'][$k]['isread'];
            $msgbox1[$k]['type'] = $msgbox['msgbox'][$k]['type'];
            $timtamp = $msgbox['msgbox'][$k]['sendtime'];
            $msgbox1[$k]['sendtime'] = date ("Y-m-d H:i", $timtamp);
            $msgbox1[$k]['content'] = $msgbox['msgbox'][$k]['content'];
            $touidUserInfo = $this->get_user ($v['touid']);
            if ($touidUserInfo) {
                //头像url、昵称、用户id、会员天数、金币数量、会员等级
                $touidmsg['url'] = $touidUserInfo['head']['url'];
                $touidmsg['nickname'] = $touidUserInfo['nickname'];
                $touidmsg['uid'] = $touidUserInfo['uid'];
                $touidmsg['vip'] = $touidUserInfo['vip'];
                $touidmsg['gold'] = $touidUserInfo['gold'];
                $touidmsg['vipgrade'] = $touidUserInfo['vipgrade'];
            }
            $msgbox1[$k]['touser'] = $touidmsg;
        }

        $result['msgboxlist'] = $msgbox1;
        $result['page'] = $page;
        $return['data']["msgboxlist"] = $result;
        //$return['data']["page"] = $pageNum;
        $return['message'] = $this->L ("CHENGGONG");
        Push_data ($return);

    }

    //获取缓存信箱
    protected function get_msgboxlist ($Wget, $pageNum, $pageSize, $reset = 0)
    {
        $path = $this->cachepath . "msgboxlist/";
        $cache_name = 'msgboxlist' . md5 (json_encode ($Wget) . $pageNum . $pageSize);
        if ($reset == 1) {
            return deldir ($path);
        }
        if (F ($cache_name, '', $path)) {
            $msgbox = F ($cache_name, '', $path);
        } else {
            $MsboxM = new MsgBoxModel();
            $msgbox = $MsboxM->getListPage1 ($Wget, $pageNum, $pageSize);
            F ($cache_name, $msgbox, $path);
        }
        return $msgbox;
    }

    /*
     * 获取消息列表
     */

    public function getmsglist ()
    {
        $data = $this->Api_recive_date;
        $selfuid = $this->uid;
        $uid=$data['uid'];
        $touid=$data['touid'];
        $pageNum = $data['page'] ? $data['page'] : 1;
        $pageSize = $data['pagesize'] ? $data['pagesize'] : 15;
        $page = array();
        $page["pagesize"] = $pageSize;
        $page["pageNum"] = $pageNum;
        $Wget = array();
        if(!$uid){
            $Wget['uid'] = $selfuid;
        }else{
            $Wget['uid'] = $uid;
        }
        $Wget['touid'] = $touid;
        $msg = $this->get_msglist ($Wget, $pageNum, $pageSize);
        $page["totalcount"]= $msg["totalcount"]?$msg["totalcount"]:0;
        $msg1 = array();
        $msg1=$msg;
        $msg1['uid']=$this->get_diy_user_field($Wget['uid']);
        $msg1['touid']=$this->get_diy_user_field($Wget['touid']);
        /*foreach ($msg as $k => $v) {
            $msg1[$k] =$msg[$k];
        }*/
        $result = $msg1;
        $result['page'] = $page;
        if($pageNum>ceil($page["totalcount"]/$pageSize)){
            $return['data']["msglist"]=array();
        }else{
            $return['data']["msglist"] = $result;
        }
        $return['data']["page"] = $pageNum;
        $return['message'] = $this->L ("CHENGGONG");
        Push_data ($return);

    }

    //获取消息缓存列表
    protected function get_msglist ($Wget, $pageNum, $pageSize, $reset = 0)
    {
        //$path = $this->cachepath . "msglist/";
      //  $cache_name = 'msglist' . md5 (json_encode ($Wget) . $pageNum . $pageSize);
      //  if ($reset == 1) {
      //      return deldir ($path);
      //  }
     //   if (F ($cache_name, '', $path)) {
     //       $msg = F ($cache_name, '', $path);
     //   } else {
        $msgM = new MsgModel();
        $msg = $msgM->getListPage1 ($Wget, $pageNum, $pageSize);
      //      F ($cache_name, $msg, $path);
      //  }
        return $msg;
    }
    /*
     * 发送信息上传文件
     */
    public function uploadfile ()
    {
        $data = $this->Api_recive_date;
        $type = $data["type"] ? $data["type"] : 1;
        $filetype = "image";
        if ($type == 2) {
            $filetype = "audio";
        } elseif ($type == 3) {
            $filetype = "video";
        }
        $uid = $this->uid;
        $return = array();
        $return['code'] = ERRORCODE_201;
        $return['message'] = $this->L ("SHANGCHUANSHIBAI");

        if (!empty($_FILES)) {
            //$_FILES['file']['type'] = 'image/jpeg';
            $saveUrl = $this->msgfileurl . $filetype . "/";
            $savePath = WR . $saveUrl;
            //echo $savePath;exit;
            $uploadList = $this->_upload ($savePath);
            if (!empty($uploadList)) {
                $UserPhotoM = new PhotoModel();
                $PutOssarr = array();
                $returnurlarr = array();
                foreach ($uploadList as $k => $v) {
                    $PutOssarr[] = $saveUrl . $v['savename'];
                    $returnurlarr[] = C ("IMAGEURL") . $saveUrl . $v['savename'];
                }
                PutOss ($PutOssarr);
                $return = array();
                $return["data"]["url"] = $returnurlarr;
            }
        }
        Push_data ($return);
    }

// 文件上传
    protected function _upload ($savePath)
    {
        import ("Api.lib.Behavior.fileDirUtil");
        $fileutil = new fileDirUtil();
        $fileutil->createDir ($savePath);
        import ("Api.lib.Behavior.UploadFile");
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize = 5242880;
        //设置上传文件类型
        $upload->allowExts = explode (',', 'jpg,gif,png,jpeg,3gp,mp4,avi,mp3,amr');
        //设置附件上传目录
        $upload->savePath = $savePath;
        //设置需要生成缩略图，仅对图像文件有效
        $upload->thumb = false;
        // 设置引用图片类库包路径
        $upload->imageClassPath = 'Extend.Library.ORG.Util.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix = 'm_';  //生产2张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth = '400';
        //设置缩略图最大高度
        $upload->thumbMaxHeight = '400';
        //设置上传文件规则
        $upload->saveRule = "md5content";
        //删除原图
        $upload->thumbRemoveOrigin = false;
        if (!$upload->upload ()) {
            $return = array();
            $return['code'] = ERRORCODE_501;
            $return['message'] = $upload->getErrorMsg ();
            Push_data ($return);
            //捕获上传异常
            //$this->error($upload->getErrorMsg());
        } else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo ();
            //$uploadList = $uploadList[0];
            //$uploadList['imageUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . $upload->saveRule;
            //$uploadList['thumbnailUrl'] = $this->get_userurl($uid, 'images').$uid.'/' . 'm_' . $upload->saveRule;

            return $uploadList;
            //import("Extend.Library.ORG.Util.Image");
            //给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
            //Image::water($uploadList[0]['savepath'] . 'm_' . $uploadList[0]['savename'], '/logo2.png');
            //$_POST['image'] = $uploadList[0]['savename'];
        }

    }


    /*
         * 发送礼物接口
         */
    public function sendgift($uid, $touid, $contentint, $contentbody)
    {
        $giftdata = array();
        $giftdata['uid'] = $uid;
        $giftdata['touid'] = $touid;
        $giftdata['giftcount'] = $contentint;
        $giftdata['giftid'] = $contentbody;

        $giftextend = $this->giftextend($contentbody);
        $giftdata['price'] = $giftextend['price'];
        $giftdata['title'] = $giftextend['title'];
        $giftdata['totalprice']=$giftextend['price']*$contentint;

        //判断用户是否用足够金币发送礼物
        $GoldCount=$this->UserInfo['gold'];
        $totalprice=$giftdata['totalprice'];

        if($GoldCount>=$totalprice) {
            $giftdata['time'] = time();
            $giftlistM = new GiftListModel();
            $type=1;
            $result = $giftlistM->addOne ($giftdata);
            if ($result) {
                S ('SendGiftList' . $uid, null);
                S ('GetGiftList' . 'to'.$uid, null);
                S ('SendGiftL' . $uid, null);
                S ('GetGiftL' . 'to'.$touid, null);

                //扣除用户账户所对应的金币
                $subgold=$this->gold_reduce($uid,$touid,$totalprice,$GoldCount,$type);
                if($subgold){
                    //add money start
                    $MoneyC = new MoneyController();
                    $MoneyC->sendgift($giftextend,$this->UserInfo,$touid,$contentint);
                    //add money end
                    $leftgold=$GoldCount-$totalprice;
                    return $leftgold;//返回1：插入数据库成功，扣除金额成功。
                }
                return 'error';//返回2：插入数据库成功，扣除金额失败。

            }
        }else{
            return false;//返回flase:余额不足，请充值
        }
    }
    //发送红包
    public  function sendredpacket($uid, $touid, $contentint,$content ){
        $packetdata = array();
        $packetdata['uid'] = $uid;
        $packetdata['touid'] = $touid;
        $packetdata['money'] = $contentint;
        $packetdata['content'] = $content;


        //判断用户是否用足够金币发送红包
        //发送方剩余金币数量
        $GoldCount=$this->UserInfo['gold'];
        //接收方剩余金币数量
        $touser=$this->get_user($touid);
                 $getgoldcount=$touser['gold'];
        if($GoldCount>=$contentint) {
            $packetdata['paytime'] = time();
                //扣除用户账户所对应的金币
            $UserBaseM = new UserBaseModel();
            $res=$UserBaseM->where('uid='.$uid)->setDec('gold',$contentint);
            $res1=$UserBaseM->where('uid='.$touid)->setInc('gold',$contentint);
            if($res&&$res1){
                $this->get_user($uid,1);
                $this->get_user($touid,1);
                $redpacketM =M("red_packet_consume");
                $packetdata['sendleftgold']=$GoldCount-$contentint;
               $packetdata['getleftgold']=$getgoldcount+$contentint;
                $dat=$redpacketM->add($packetdata);

                if($dat){
                    //add money start

                    return 'true';//插入数据库成功，扣除金额成功。
                }
                return 'error';//插入数据库成功，扣除金额失败。

            }
        }else{
            return false;//返回flase:余额不足，请充值
        }
    }



}

?>