<?php
class UserController extends BaseController {
	
	public function index(){
		//$this->redis->delete("useronlinelist_0_1");
		Push_data();
		
	}
	public function search(){

		$data = $this->Api_recive_date;
		$type = $data["type"] ? $data["type"] : 1;//1:推荐用户;2:附近的人;3:视频用户
		
		
		//基本查询条件
		$UserInfo = $this->UserInfo;
		//$UserInfo["country"]="AS";
		$baseWhere = array();
		$countryarr = array(
		    "TW"=>array("HK","TW","JP","MO","MY","SG","TH","VN","CN")
		);
		$country = "US";
		foreach($countryarr as $k=>$v){
		    if(in_array($UserInfo["country"], $v)){
		        $country=$k;
		        break;
		    }
		}
		$baseWhere['country'] = $country;
		$baseWhere['gender'] = array('neq',$UserInfo['gender']);//显示异性
		$baseWhere['user_type'] = 2;
		
		switch ($type){
			case 1://1:推荐用户
			    if($UserInfo['user_type']==3){
			        
			        $this->_tuijianonline($baseWhere);
			    }else{
			     $this->_tuijian($baseWhere);
			    }
			break;
			case 2://1:附近的人
			    $this->_fujin($baseWhere);
			break;
			case 3://1:视频用户
			    $this->_shipin($baseWhere);
			default://1:视频用户
			    $this->_fujin($baseWhere);
			break;
		}
		
	}
	private  function _tuijianonline($baseWhere){
	   
	    $data = $this->Api_recive_date;
	    $uid = $this->uid;
	    $pageNum = $data['page'] ? $data['page'] : 1;
	    $pageSize = $data['pagesize'] ? $data['pagesize'] : 30;
	    $userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";
	    $Wdata = $baseWhere;
	    $UserInfo = $this->UserInfo;
	    //登录保存uid
	    $OnlineCon = new OnlineController();
	    $uids = $OnlineCon->getUserOnline($UserInfo['product'],1,1,$pageNum);
	    $newlistuser = array();
	    $totalCount = $uids["totalCount"] ? $uids["totalCount"] : 0;
	    if($uids["uids"]){
	        
	        $onlinelist = $uids["uids"];
	        if($onlinelist){
	            foreach($onlinelist as $k=>$v){
	                $tempUserInfo = $this->get_user($v);
	                
	                if($tempUserInfo){
	                    $tempArray= array();
	                    $tempUserInfo["issayhello"] = MsgController::isSayHello($v);
	                    $tempUserInfo["isfollow"] = SpaceController::isFollow($v);
	                    
	                    $tempArray = $this->get_user_field($tempUserInfo,$userfield);
	                    $newlistuser[] = $tempArray;
	                }
	            }
	        }
	        //shuffle($newlistuser);
	    }
	    
	   
	    
	    
	    $return = array();
	    $return['data']["userlist"] = $newlistuser;
	    $page["pagesize"] = $pageSize;
	    $page["page"] = $pageNum;
	    $page["totalcount"] = $totalCount;
	    $return['data']["page"] = $page;
	    
	    Push_data($return);
	}
	private  function _tuijian($baseWhere){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$pageNum = $data['page'] ? $data['page'] : 1;
		$pageSize = $data['pagesize'] ? $data['pagesize'] : 30;
                                        $userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";
		$Wdata = $baseWhere;
		$UserInfo = $this->UserInfo;
		
		$cachename1 = "getsearchuser_".md5(json_encode($Wdata)).$pageNum.$pageSize.$userfield;
        //S($cachename1,null);
		$cache1=S($cachename1);//设置缓存标示
		//$cache1=0;
		if(!$cache1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
			$User_baseM = new UserBaseModel();
			$ret = $User_baseM->getListPage($Wdata,$pageNum,$pageSize);
			$cache1 = $ret;
			S($cachename1,$cache1,60*60*2);//设置缓存的生存时间
		}
		$ret=$cache1;
		$newlistuser = array();	
		foreach($ret['list'] as $k=>$v){
				$tempUserInfo = $this->get_user($v['uid']);
				if($tempUserInfo){
    				$tempArray= array();
    				$tempUserInfo["issayhello"] = MsgController::isSayHello($v['uid']);
    				$tempUserInfo["isfollow"] = SpaceController::isFollow($v['uid']);
                    //设置用户在线状态start
                    $cachename= "get_onlinestatus_".$v['uid'];
                    $cache=S($cachename);//设置缓存标示
                    if(!$cache) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
                        $rand=array('1','2');
                        $rand_keys=array_rand($rand,1);
                        $tempUserInfo["isonline"]=$rand[$rand_keys];
                        $cache= $rand[$rand_keys];
                        S($cachename,$cache,60*60*2);//设置缓存的生存时间
                    }else{
                        $tempUserInfo["isonline"]=$cache;
                    }
                    //设置用户在线状态end
                    $tempArray = $this->get_user_field($tempUserInfo,$userfield,$v['uid']);
    				//检测所传字段是否有国家地区字段
                    $field_arr = explode("|", $userfield);
                    if(in_array('area', $field_arr)){
                        //获取用户的国家和地区
                        $countryiso=$tempUserInfo['country'];
                        $regionid=$tempUserInfo['area'];
                        $regionlist=$this->get_regionlist($countryiso);
                        foreach($regionlist as $k1=>$v1){
                            if($v1['id']==$regionid){
                                $regioncode=$v1['name'];
                                break;
                            }
                        }
                        $tempArray['country']=$this->L($countryiso);
                        $tempArray['area']=$this->L($regioncode);
                    }
                    if(in_array('isonline', $field_arr)){
                        $tempArray['isonline']=$tempUserInfo["isonline"];
                    }
    				$newlistuser[] = $tempArray;
				}
		}
		ksort($newlistuser);
		//shuffle($newlistuser);

		$return = array();
        if($pageNum>ceil($ret["totalCount"]/$pageSize)){
            $return['data']["userlist"]=array();
        }else{
            $return['data']["userlist"] = $newlistuser;
        }
		$page["pagesize"] = $pageSize;
		$page["page"] = $pageNum;
		$page["totalcount"] = $ret["totalCount"];
		$return['data']["page"] = $page;
		
		Push_data($return);
	}
	private  function _fujin($baseWhere){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$pageNum = $data['page'] ? $data['page'] : 1;
		$pageSize = $data['pagesize'] ? $data['pagesize'] : 30;
		$userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";
		$Wdata = $baseWhere;
		$cachename1 = "getsearchuserfujin_".md5(json_encode($Wdata)).$pageNum.$pageSize.$userfield;
		$cache1=S($cachename1);//设置缓存标示
		//$cache1=0;
		if(!$cache1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
			$User_baseM = new UserBaseModel();
			$ret = $User_baseM->getListPage($Wdata,$pageNum,$pageSize);
			$cache1 = $ret;
			S($cachename1,$cache1,60*60*2); //设置缓存的生存时间
		}
		$ret=$cache1;	
		$newlistuser = array();	
		foreach($ret['list'] as $k=>$v){
		    
		    $tempUserInfo = $this->get_user($v['uid']);
		    if($tempUserInfo){
		        $tempArray= array();
		        $tempUserInfo["issayhello"] = MsgController::isSayHello($v['uid']);
		        //$UserInfo["isfollow"] = SpaceController::isFollow($v['uid']);
                //设置用户在线状态start
                $cachename= "get_onlinestatus_".$v['uid'];
                $cache=S($cachename);//设置缓存标示
                if(!$cache) {  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
                    $rand=array('1','2');
                    $rand_keys=array_rand($rand,1);
                    $tempUserInfo["isonline"]=$rand[$rand_keys];
                    $cache= $rand[$rand_keys];
                    S($cachename,$cache,60*60*2);//设置缓存的生存时间
                }else{
                    $tempUserInfo["isonline"]=$cache;
                }
                //设置用户在线状态end
		        $tempArray = $this->get_user_field($tempUserInfo,$userfield);
		        $newlistuser[] = $tempArray;
		    }
		}
		ksort($newlistuser);
		shuffle($newlistuser);

		$return = array();
        if($pageNum>ceil($ret["totalCount"]/$pageSize)){
            $return['data']["userlist"]=array();
        }else{
            $return['data']["userlist"] = $newlistuser;
        }
		$page["pagesize"] = $pageSize;
		$page["page"] = $pageNum;
		$page["totalcount"] = $ret["totalCount"];
		$return['data']["page"] = $page;

		Push_data($return);
	}
	private  function _shipin($baseWhere){
		$data = $this->Api_recive_date;
		$uid = $this->uid;
		$pageNum = $data['page'] ? $data['page'] : 1;
		$pageSize = $data['pagesize'] ? $data['pagesize'] : 30;
		$userfield = $data['userfield'] ? $data['userfield'] :"uid|head|nickname";
		$Wdata = array();
		$UserInfo =$baseWhere;
		$cachename1 = "getsearchuser_".md5(json_encode($Wdata)).$pageNum.$pageSize;
		$cache1=S($cachename1);//设置缓存标示
		$cache1=0;
		if(!$cache1){  //$cache 中是缓存的标示(每个查询都对应一个缓存 即 不同的查询有不同的缓存)
			$User_baseM = new UserBaseModel();
			$ret = $User_baseM->getListPage($Wdata,$pageNum,$pageSize);
			$cache1 = $ret;
			S($cachename1,$cache1,60*60*2); //设置缓存的生存时间 
		}
				$ret=$cache1;	
		$newlistuser = array();	
		foreach($ret['list'] as $k=>$v){
		    
		    $tempUserInfo = $this->get_user($v['uid']);
		    if($tempUserInfo){
		        $tempArray= array();
		        $tempUserInfo["issayhello"] = MsgController::isSayHello($v['uid']);
		        $UserInfo["isfollow"] = SpaceController::isFollow($v['uid']);
		        $tempArray = $this->get_user_field($tempUserInfo,$userfield);
		        
		        $newlistuser[] = $tempArray;
		    }
		}
		ksort($newlistuser);
		shuffle($newlistuser);

		
		
		$return = array();
        if($pageNum>ceil($ret["totalCount"]/$pageSize)){
            $return['data']["userlist"]=array();
        }else{
            $return['data']["userlist"] = $newlistuser;
        }
		$page["pagesize"] = $pageSize;
		$page["page"] = $pageNum;
		$page["totalcount"] = $ret["totalCount"];
		$return['data']["page"] = $page;
		
		Push_data($return);
	}
	public function logout(){
		$data = $this->Api_recive_date;
		$token = $data["token"];
    	$redis=$this->redisconn();
    	$redisStr = "token_".$token;
		$redis->delete($redisStr);
		Push_data();
	}

	public function modifypassword(){
		$platforminfo = $this->platforminfo;
    	$password = $this->Api_recive_date['password'];
    	$newpassword = $this->Api_recive_date['newpassword'];
    	$uid = $this->uid;
    	$return = array();
    	if($newpassword == ''){
    		$return['code'] = ERRORCODE_201;
    		$return['message'] = $this->L("XINMIMABUNENGWEIKONG");
    	}else{
	    	$User_baseM = new UserBaseModel();
	    	$ret = $User_baseM->updateOne(array('uid'=>$uid),array('password'=>$newpassword));
	    	$this->get_user($uid,1);
	    	if($ret){
				
	    	}else{
	    		$return['code'] = ERRORCODE_201;
	    		$return['message'] = $this->L("JIUMIMABUNENGWEIKONG");	    		
	    	}
    	}
    	Push_data($return);
	}

    //从缓存中取出地区信息
    protected function get_regionlist($id,$reset=0){
        $redis=$this->redisconn();
        //缓存中储存总数据
        $lang=$redis->listSize('regionlist_'.$id);
        //如果缓存中不存在，则取数据库最新数据存入
        if($lang==0){
            $where = array("country_codeiso"=>$id,"level"=>1);
            $RegionM = new RegionModel();
            $res = $RegionM->getList($where);
            foreach($res as $k=>$v){
                $res[$k] = array(
                    "id"=>$v["id"],
                    "name"=>$v["code"]
                );

                $value=json_encode($res[$k],true);
                $ret[]=$redis->listPush('regionlist'.$id,$value);
            }
            $result1=$res;
            //F($cache_name,$res,$path);
        }else{
            $result=$redis->listLrange('regionlist'.$id,0,-1);
            if ($result) {
                foreach ($result as $k1 => $v1) {
                    if($result[$k1]==null){
                        continue;
                    }
                    $result1[$k1]=json_decode($v1,true);
                }
            }
        }
        return $result1;
    }

}



?>