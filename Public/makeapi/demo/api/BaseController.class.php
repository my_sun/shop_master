<?php
use Think\Controller;
class BaseController extends Controller {
	public      $Api_recive_date =   array();
	public      $uid = '0';
	public      $UserInfo = array();
	public      $redis = array();
	public      $readredis = array();
	public      $platforminfo=array();
	/*
	 * 程序运行前执行
	 */
	public function __construct(){
		parent::__construct();
		$this->StartRunTime = time();
		$this->Api_recive_date = Recive_data($_POST);
		$this->platforminfo = $this->Api_recive_date["platforminfo"];
		$this->uid = $this->get_token_uid($this->Api_recive_date["token"]);
		$this->UserInfo = $this->get_user($this->uid);
		$this->Cheacklogin();
		if($this->uid){ 
		    $this->Lan = $this->LangSet($this->platforminfo["language"]);
			//$this->Lan = $this->LangSet($this->UserInfo["language"]);
			$this->country = $this->get_country($this->UserInfo["country"]);
		}else{
			$this->Lan = $this->LangSet($this->platforminfo["language"]);
			$this->country = $this->get_country($this->platforminfo["country"]);
		}
		//echo 1;
		//$QueryServerM = new QueryServerModel();
	}
	public function get_server_product(){
	    $localhost= $_SERVER["SERVER_ADDR"];
	    $path = CHCHEPATH_PRODUCTS;
	    $cache_name = 'product-'.$localhost;
	    if(F($cache_name,'',$path)){
	        $newarr = F($cache_name,'',$path);
	    }else{
	        $productsM = new ProductsModel();
	        $map["localhost"] = $localhost;
	        $newarr = $productsM->getOne($map);
	        if(empty($newarr)){
	            $newarr=1;
	        }
	        F($cache_name,$newarr,$path);
	    }
	    return $newarr;
	}
	/**
	 * 程序结束后执行
	 * @access public
	 */
	public function __destruct() {
	    parent::__destruct();
	    /* 统计程序执行时间start	     */
/* 	   $exetime = time()-$this->StartRunTime;
	   if($exetime>5){
	       $action = strtolower(CONTROLLER_NAME."/".ACTION_NAME);
	       $QueryServerM = new QueryServerModel();
	       $argc = base64_encode(json_encode($_POST));
	       $data = array(
	           "action"=>$action,
	           "time"=>$exetime,
	           "argc"=>$argc
	       );
	       $QueryServerM->addOne($data);
	   } */
	   /* 统计程序执行时间end  */
	}
	public function redisconn(){
		$redis = new RedisModel();
		return $redis;
	}

	public function LangSet($langs){
		
		$languagecode = $this->get_languagecode();
		
		if(!in_array($langs, $languagecode)||$langs==""){
			$langs = "zh-tw";
		}
		$file   =  C("LANG_PATH").$langs.'.txt';
		
		$cachefile   =  C("LANG_PATH")."cache/".$langs.'.php';
		
		if(!file_exists($cachefile)||(filemtime($file)>filemtime($cachefile))){
		    $TranslateM = new TranslateModel();
		    $Wdata = array();
		    $Wdata["lang"]=$langs;
		    $ret = $TranslateM->getList($Wdata);
		    $temp = array();
		    foreach ($ret as $k=>$v){
		        $temp[$v["code"]]=$v['content'];
		    }
		    
		    $temp = "<?php return ".var_export($temp, true).";";
		    mkdirs(dirname($cachefile));
		    file_put_contents($cachefile, $temp);
		}
		
		import("Api.lib.Behavior.CheckLangBehavior");
		$lang = new CheckLangBehavior();
		
		$lang->run($langs);
		
		return $lang;
	}
	protected function get_token_uid($token){
    	$redis=$this->redisconn();
    	$redisStr = "token_".$token;
		$uid=false;
		if($redis->exists($redisStr)){
			$uid = $redis->get($redisStr);
		}
		return $uid;
    }
	public function L($name=null, $value=null) {
		return $this->Lan->L($name,$value);
	}
  	public function get_languagecode($reset=0){
  	    $path = CHCHEPATH_LANGUAGE;
		$cache_name = 'languagelistcode';
		if(F($cache_name,'',$path) && $reset == 0){
			$newarr = F($cache_name,'',$path);
		}else{
			$where = array();
			$LanguageM = new LanguageModel();	
			$res = $LanguageM->getList($where);
		   $newarr = array();
			foreach($res as $v){
				$newarr[] =$v["code"];
			}
			F($cache_name,$newarr,$path);
		}
		return $newarr;
    }
    public function get_country($code){
    	$code = strtoupper($code);
    	$countrycode = $this->get_countrycode();
		if(!in_array($code, $countrycode)||$code==""){
			$code = "TW";
		}
		return $code;
    }
    public function get_countrycode($reset=0){
        $path = CHCHEPATH_COUNTRY;
		$cache_name = 'countrycode';
		if(F($cache_name,'',$path) && $reset == 0){
			$res = F($cache_name,'',$path);
		}else{
			$where = array();
			$CountryM = new CountryModel();	
			$res = $CountryM->getList($where);
			
			F($cache_name,$res,$path);
		}
    	$newarr = array();
		foreach($res as $v){
			$newarr[] = $v["iso"];
		}
		return $newarr;
    }
	/*
	 * 
	 * 验证用户是否登陆
	 */
	public function Cheacklogin(){
		$actionStr = strtolower(CONTROLLER_NAME."/".ACTION_NAME);
		$controllrStr = strtolower(CONTROLLER_NAME);
		/*
		 * 统计访问次数start
		 */
		//$QueryCountM = new QueryCountModel();
		//$qWhere = array("action"=>$actionStr);
		//$Qinfo = $QueryCountM->getOne($qWhere);
		//if($Qinfo){
		    //$QueryCountM->where($qWhere)->setInc('count',1);
		//}else{
		 //   $QueryCountM->addOne($qWhere);
		//}
		/*
		 * 统计访问次数end
		 */
		
		$Public_controller = C("NO_LOGIN");
		$Public_controller_name = C("NO_LOGIN_CONTROLLER");
		if(!in_array($actionStr,$Public_controller)&&!in_array($controllrStr,$Public_controller_name)&&MODULE_NAME=="Api"){
			$this->is_login();
		}
	}
	/*
	 * 
	 * 验证用户是否登陆
	 */
	public function is_login(){
	    $uid = $this->UserInfo["uid"];
	    $pid = $this->platforminfo["phoneid"];
	    $password = $this->UserInfo["password"];
	    $product = $this->UserInfo["product"];
	    $server_token = md5($uid.$pid.$password.$product);
	    if($this->Api_recive_date["token"]!=$server_token){
				$this->Lan = $this->LangSet($this->platforminfo["language"]);
				Push_data(array('code'=>ERRORCODE_202,'message'=>$this->L("DENGLUGUOQI")));
		}else{
		    //登录保存uid
		    $OnlineCon = new OnlineController();
		    $OnlineCon->setUserOnline($this->UserInfo);

		}
	}
	/*
	 * 
	 * platformInfo 对象
	 */
	public function get_platformInfo($token){
		
		$where = array('token'=>$token);

		$User_baseM = new UserBaseModel();
		$result = $User_baseM->getOne($where);
		return $this->format_platformInfo($result);
	}
	//格式化 语言
	public function format_lang($lang){
		if($lang=="en"){
			return 'english';
		}else{
			return 'traditional';
		}
	}
	/*
	 *
	* 格式化 platformInfo 对象
	*/
	public function format_platformInfo($platformInfo){
		$RetField = array(
				'phoneid'=>$platformInfo['phoneid'] ? $platformInfo['phoneid'] : '',
				'country'=>$platformInfo['country'] ? $platformInfo['country'] : '',
				'language'=>$platformInfo['language'] ? $platformInfo['language'] : '',
				'version'=>$platformInfo['version'] ? $platformInfo['version'] : '',
				'product'=>$platformInfo['fid'] ? $platformInfo['product'] : '',
				'platformnumber'=>$platformInfo['platform'] ? $platformInfo['platformnumber'] : '',
				'fid'=>$platformInfo['product'] ? $platformInfo['fid'] : '',
				'phonetype'=>$platformInfo['phonetype'] ? $platformInfo['phonetype'] : '',
				'systemversion'=>$platformInfo['systemversion'] ? $platformInfo['systemversion'] : ''
		);
		return $RetField;
	}
	/*
	 *
	 * setuserfield 设置用户单个字段
	 */
	public function set_user_field($uid,$field,$value){
	    if($uid){
	        $path = $this->get_userpath($uid, 'cache/userinfo/');
	        $cache_name = 'userinfo_'.$uid;
	        if(F($cache_name,'',$path)){
	            $UserInfo = F($cache_name,'',$path);
	            $UserInfo[$field] = $value;
	            F($cache_name,$UserInfo,$path);
	        }
	        return true;
	    }else{
	        return false;
	    }
	}
	/*
	 *
	* user 对象
	*/
	public function get_user($uid,$reset='0'){
	    if($uid){
    		$path = $this->get_userpath($uid, 'cache/userinfo/');
    		$cache_name = 'userinfo_'.$uid;
    		if($reset==1){
    		    return F($cache_name,NULL,$path);
    		}
    		if(F($cache_name,'',$path) && $reset == 0){
    			$UserInfo = F($cache_name,'',$path);
    		}else{
    			$where = array('uid'=>$uid);
    			$User_baseM = new UserBaseModel();
    			$UserExtendM = new UserExtendModel();
    			$UserInfo = $User_baseM->getOne($where);
    			$UserInfo1 = $UserExtendM->getOne($where);
    			if(!empty($UserInfo1)){
    			    $UserInfo = array_merge($UserInfo,$UserInfo1);
    			}
    			
    			$UserInfo = $this->format_user_info($UserInfo);
    			
    			F($cache_name,$UserInfo,$path);
    		}
    		//计算vip天数
    		if($UserInfo["viptime"]){
    		    $UserInfo["vip"] = ceil(($UserInfo["viptime"]-time())/60/60/24)+1;
    		}
    		return $UserInfo;
	    }else{
	        return array();
	    }
	}
	/*
	 *获取精简的用户信息
	 *
	 */
	public function get_diy_user_field($uid,$field="uid|head|nickname"){
	    $usertemp = $this->get_user($uid);
	    if($field=="*"){
	        $user = $usertemp;
	    }else{
	        $field_arr = explode("|", $field);
	        $user= array();
	        foreach ($usertemp as $k=>$v){
	            if(in_array($k, $field_arr)){
	                $user[$k] = $v;
	            }
	        }
	    }
	    //获取相册
	    if(isset($user["photos"])){
	        $user["photos"] = $this->get_user_photo($uid);
	    }
	    //获取语音认证对象
	    if(isset($user["audio"])){
	        $user["audio"] = $this->get_user_audio($uid);
	    }
	    //获取视频认证对象
	    if(isset($user["video"])){
	        $user["video"] = $this->get_user_video($uid);
	    }
	    return $user;
	}
	/*
	 *获取精简的用户信息
	 *
	 */
	public function get_user_field($usertemp,$field="uid|head|nickname",$uid){
	    $field_arr = explode("|", $field);
	    foreach ($usertemp as $k=>$v){
	        if(in_array($k, $field_arr)){
	            $user[$k] = $v;
	        }
	    }
        if(in_array('photo', $field_arr)){
            $user["photos"] = $this->get_user_photo($uid);
        }
	    
	    return $user;
	}
	/*
	 *获取精简的用户信息
	* 
	*/
	public function get_user_simple($uid){
		$usertemp = $this->get_user($uid);
		if(!empty($usertemp)){
			$user = array(
					"uid"=>$usertemp["uid"],
					"nickname"=>$usertemp["nickname"],
					"head"=>$usertemp["head"]["url"]
				);
		}
		return $user;
	}
	/*
	 *
	* user_ico 获取用户在线状态
	*/
	public function get_onlineState($uid,$arr=array()){
		$onlinestate=0;
		//$useronlinelistkey = "useronlinelist_10008_0_1";
		$useronlinelistkey = "useronlinelist_".$arr['product']."_".$arr['gender']."_".$arr['UserType'];
		if($this->readredis->exists($useronlinelistkey)){
			$onlinelist = $this->readredis->lRange($useronlinelistkey, 0, -1);
			if(in_array($uid, $onlinelist)){
				$onlinestate=1;
			}
		}
		return $onlinestate;
	}
	/*
	 *
	* user_ico 获取用户头像
	*/
	public function get_user_ico($uid){
		$UserPhotoM = new PhotoModel();
		$where = array();
		$where['uid'] = $uid;
		$where['type'] = "2";
		$where['status'] = "1";
		$ret = $UserPhotoM->getOne($where);
		if(!$ret){
			$photos = $this->get_user_photo($uid);
			if($photos[0]){
				$ret = $photos[0];
			}
		}
		if($ret)
		{
			$image = array();
			$image['id']=$ret["id"];
			$image['url']=C("IMAGEURL").$ret["url"];//原图url
			$image['thumbnaillarge']=C("IMAGEURL").$ret["url"];//大图url
			$image['thumbnailsmall']=C("IMAGEURL").$ret["url"];//小图url
			$image['status']=$ret["status"];//状态1审核通过;2正在审核3审核未通过已删除
			$image['seetype'] =$ret["seetype"]; //可见级别(1所有用户可见,2会员可见)
			
		}
		else
		{
			$image = array();
			$image['id']=1;
			$image['url']=C("IMAGEURL").'/1.png';//原图url
			$image['thumbnaillarge']=C("IMAGEURL").'/1.png';//大图url
			$image['thumbnailsmall']=C("IMAGEURL").'/1.png';//小图url
			$image['status']=1;//状态1审核通过;2正在审核3审核未通过已删除
			$image['seetype'] =1; //可见级别(1所有用户可见,2会员可见)		
		}
		return $image;
	}
	/*
	 *
	* user_photo 获取用户相册
	*/
	public function get_user_photo($uid,$reset=0){
	    $path = CHCHEPATH_USERPHOTOS;
	    $cache_name = 'user_photo_'.$uid;
	    if($reset==1){
	        return F($cache_name,NULL,$path);
	    }
	    if(F($cache_name,'',$path)){
	        $res = F($cache_name,'',$path);
	    }else{
	        $UserPhotoM = new PhotoModel();
	        $where = array();
	        $image = array(); 
	        $where['uid'] = $uid;
	        $where['type'] = "1";
	        $where['status'] = "1";
	        
	        $ret = $UserPhotoM->getList($where);
	        if($ret){
	            foreach($ret as $k=>$v){
	                $image[$k] = array();
	                $image[$k]['id']=$v["id"];
	                $image[$k]['url']=C("IMAGEURL").$v["url"];//原图url
	                $image[$k]['thumbnaillarge']=C("IMAGEURL").$v["url"];//大图url
	                $image[$k]['thumbnailsmall']=C("IMAGEURL").$v["url"];//小图url
	                $image[$k]['status']=$v["status"];//状态1审核通过;2正在审核3审核未通过已删除
	                $image[$k]['seetype'] =$v["seetype"]; //可见级别(1所有用户可见,2会员可见)
	            }
	        }
	        $res = $image;
	        F($cache_name,$res,$path);
	    }
	    return $res;
	    
		
	}
	/*
	 *
	 * user_ico 获取用户头像
	 */
	public function get_user_ico_all($uid){
	    $UserPhotoM = new PhotoModel();
	    $where = array();
	    $where['uid'] = $uid;
	    $where['type'] = "2";
	    $ret = $UserPhotoM->getOne($where);
	    if(!$ret){
	        $photos = $this->get_user_photo($uid);
	        if($photos[0]){
	            $ret = $photos[0];
	        }
	    }
	    if($ret)
	    {
	        $image = array();
	        $image['id']=$ret["id"];
	        $image['url']=C("IMAGEURL").$ret["url"];//原图url
	        $image['thumbnaillarge']=C("IMAGEURL").$ret["url"];//大图url
	        $image['thumbnailsmall']=C("IMAGEURL").$ret["url"];//小图url
	        $image['status']=$ret["status"];//状态1审核通过;2正在审核3审核未通过已删除
	        $image['seetype'] =$ret["seetype"]; //可见级别(1所有用户可见,2会员可见)
	        
	    }
	    else
	    {
	        $image = array();
	        $image['id']=1;
	        $image['url']=C("IMAGEURL").'/1.png';//原图url
	        $image['thumbnaillarge']=C("IMAGEURL").'/1.png';//大图url
	        $image['thumbnailsmall']=C("IMAGEURL").'/1.png';//小图url
	        $image['status']=1;//状态1审核通过;2正在审核3审核未通过已删除
	        $image['seetype'] =1; //可见级别(1所有用户可见,2会员可见)
	    }
	    return $image;
	}
	/*
	 *
	 * user_photo 获取用户相册
	 */
	public function get_user_photo_all($uid,$reset=0){
	    $path = CHCHEPATH_USERPHOTOS;
	    $cache_name = 'user_photo_all_'.$uid;
	    if($reset==1){
	        return F($cache_name,NULL,$path);
	    }
	    if(F($cache_name,'',$path)){
	        $res = F($cache_name,'',$path);
	    }else{
	        $UserPhotoM = new PhotoModel();
	        $where = array();
	        $image = array();
	        $where['uid'] = $uid;
	        $where['type'] = "1";
	        
	        $ret = $UserPhotoM->getList($where);
	        if($ret){
	            foreach($ret as $k=>$v){
	                $image[$k] = array();
	                $image[$k]['id']=$v["id"];
	                $image[$k]['url']=C("IMAGEURL").$v["url"];//原图url
	                $image[$k]['thumbnaillarge']=C("IMAGEURL").$v["url"];//大图url
	                $image[$k]['thumbnailsmall']=C("IMAGEURL").$v["url"];//小图url
	                $image[$k]['status']=$v["status"];//状态1审核通过;2正在审核3审核未通过已删除
	                $image[$k]['seetype'] =$v["seetype"]; //可见级别(1所有用户可见,2会员可见)
	            }
	        }
	        $res = $image;
	        F($cache_name,$res,$path);
	    }
	    return $res;
	    
	    
	}
	/*
	 *
	* 获取视频认证对象
	*/
	public function get_user_video($uid,$reset=0){
	    $path = CHCHEPATH_USERVIDEO;
	    $cache_name = 'get_user_video'.$uid;
	    if($reset==1){
	        return F($cache_name,NULL,$path);
	    }
	    if(F($cache_name,'',$path)){
	        $res = F($cache_name,'',$path);
	    }else{
	        $UserVideoM = new VideoModel();
	        $where = array();
	        $res = array();
	        $where['uid'] = $uid;
	        $ret = $UserVideoM->getOne($where);
	        
	        if($ret){
	            $res['id']=$ret['id'];
	            $res['ltime']=$ret['ltime'];
	            $res['url']=C("IMAGEURL").$ret["url"];
	            $res['imageurl']=C("IMAGEURL").$ret["imageurl"];
	            $res['status']=$ret["status"];
	        }
	        F($cache_name,$res,$path);
	    }
	    return $res;
	}
	/*
	 *
	 * 获取语音认证对象
	 */
	public function get_user_audio($uid,$reset=0){
	    
	    $path = CHCHEPATH_USERAUDIO;
	    $cache_name = 'get_user_audio'.$uid;
	    //F($cache_name,NULL,$path);
	    if($reset==1){
	        return F($cache_name,NULL,$path);
	    }
	    if(F($cache_name,'',$path)){
	        $res = F($cache_name,'',$path);
	    }else{
	        $AudioM = new AudioModel();
	        $where = array();
	        $res = array();
	        $where['uid'] = $uid;
	        $ret = $AudioM->getOne($where);
	       
	        if($ret){
	            $res['id']=$ret['id'];
	            $res['ltime']=$ret['ltime'];
	            $res['url']=C("IMAGEURL").$ret["url"];
	            $res['status']=$ret["status"];
	        }
	        F($cache_name,$res,$path);
	    }
	    
	    return $res;
	}
	/*
	 * 格式化用户对象
	 */
	public function format_user_info($user_info){
		if(!$user_info['uid']){
			return array();
		}
		$uid = $user_info['uid'];
		$res_date = array(
				'uid'=>$user_info['uid'] ? $user_info['uid'] : '',//用户id
				'password'=>$user_info['password'] ? $user_info['password'] : '',//用户密码
				'gender'=>$user_info['gender'] ? $user_info['gender'] : '1',//性别 1是男 2是女
		        'user_type'=>$user_info['user_type'] ? $user_info['user_type'] : '1',//
				'age'=>$user_info['age'] ? $user_info['age'] : '',//年龄
				'nickname'=>$user_info['nickname'] ? $user_info['nickname'] : '',//昵称
				'vipgrade'=>$user_info['vipgrade'] ? $user_info['vipgrade'] : '0',//会员等级 1是一级 2是二级 
				'vip'=>$user_info['vip'] ? $user_info['vip'] : '0',//会员天数
		        'viptime'=>$user_info['viptime'] ? $user_info['viptime'] : "0",//vip到期时间,
				'gold'=>$user_info['gold'] ? $user_info['gold'] : '0',//金币数量
				'mood'=>$user_info['mood'] ? $user_info['mood'] : '',//交友宣言
		        'blood'=>$user_info['blood'] ? $user_info['blood'] : '',//血型 
				'height'=>$user_info['height'] ? $user_info['height'] : "0",//身高
				'weight'=>$user_info['weight'] ? $user_info['weight'] : "0",//体重
				'head'=>$this->get_user_ico($uid),//头像
				'photos'=>array(),//{image对象列表
				'photosnumber'=>"0",//相片数量
				'area'=>$user_info['area'] ? $user_info['area'] : "0",//居住地
				'income'=>$user_info['income'] ? $user_info['income'] : "0",//收入
				'marriage'=>$user_info['marriage'] ? $user_info['marriage'] : "0",//婚姻状况
				'education'=>$user_info['education'] ? $user_info['education'] : "0",//学历
				'work'=>$user_info['work'] ? $user_info['work'] : "0",//工作
				'constellation'=>$user_info['constellation'] ? $user_info['constellation'] : "0",//星座
				'friendsfor'=>$user_info['friendsfor'] ? $user_info['friendsfor'] : "0",//交友目的
				'cohabitation'=>$user_info['cohabitation'] ? $user_info['cohabitation'] : "0",//婚前同居
				'dateplace'=>$user_info['dateplace'] ? $user_info['dateplace'] : "0",//期望约会的地方
				'lovetimes'=>$user_info['cohabitation'] ? $user_info['lovetimes'] : "0",//恋爱次数
				'charactertype'=>$user_info['charactertype'] ? $user_info['charactertype'] : "0",//性格类型(多选,用|分开)
				'hobby'=>$user_info['hobby'] ? $user_info['hobby'] : "0",//兴趣爱好(多选,用|分开)
				'wantchild'=>$user_info['wantchild'] ? $user_info['wantchild'] : "0",//是否要小孩
				'house'=>$user_info['house'] ? $user_info['house'] : "0",//是否有房
				'car'=>$user_info['car'] ? $user_info['car'] : "0",//是否有车
				'conditions'=>array(),//{ conditions对象}征友条件
				'line'=>$user_info['line'] ? $user_info['line'] : "",//line
				'tinder'=>$user_info['tinder'] ? $user_info['tinder'] : "",//tinder
				'wechat'=>$user_info['wechat'] ? $user_info['wechat'] : "",//wechat
				'facebook'=>$user_info['facebook'] ? $user_info['facebook'] : "",//facebook
				'email'=>$user_info['email'] ? $user_info['email'] : "",//email
            'twitter'=>$user_info['twitter'] ? $user_info['twitter'] : "",//email
				'isphonenumber'=>$user_info['isphonenumber'] ? $user_info['isphonenumber'] : "0",//手机号是否认证
				'phonenumber'=>$user_info['phonenumber'] ? $user_info['phonenumber'] : "",//手机号
				'isvideo'=>$user_info['isvideo'] ? $user_info['isvideo'] : "0",//是否视频认证
				'video'=>array(),//{video对象}视频认证信息
				'isaudio'=>$user_info['isaudio'] ? $user_info['isaudio'] : "0",//是否语音认证
				'audio'=>array(),//{audio对象}  语音认证信息
				'chatsetting'=>array(),//聊天设置对象
				'receivedgifts'=>array(),//{收到的礼物对象列表}收到的礼物
				'sendgifts'=>array(),//{送出的礼物对象列表}
				'usermood'=>array(),//用户动态{用户动态对象}
				'fansnumber'=>"0",//粉丝数量
				'follownumber'=>"0",//我关注的人数量
				'guarduser'=>array(),//守护人对象
				'regtime'=>$user_info['regtime'],//注册时间
		         'logintime'=>$user_info['logintime'],//注册时间
				'platformInfo'=>"",//客户端信息对象,
				'country'=>$user_info['country'] ? $user_info['country'] : "TW",//国家,
		         'language'=>$this->set_user_lang($user_info['language']),//语言,
		        'product'=>$user_info['product'] ? $user_info['product'] : "10508",//product,
		        'phoneid'=>$user_info['phoneid'] ? $user_info['phoneid'] : "",//phoneid,
		        
    		    'isopen'=>$user_info['isopen'] ? $user_info['isopen'] : "1",//1公开联系方式，2不公开,
    		    'exoticlove'=>$user_info['exoticlove'] ? $user_info['exoticlove'] : "",//是否接受异地恋 单选,
    		    'sexual'=>$user_info['sexual'] ? $user_info['sexual'] : "",//婚前性行为 单选,
    		    'livewithparents'=>$user_info['livewithparents'] ? $user_info['livewithparents'] : "",//愿意同父母居住 单选
    		    'personalitylabel'=>$user_info['personalitylabel'] ? $user_info['personalitylabel'] : "",//个性标签 多选
    		    'liketype'=>$user_info['liketype'] ? $user_info['liketype'] : "",//喜欢的类型 多选
    		    'glamour'=>$user_info['glamour'] ? $user_info['glamour'] : "",//魅力部位 多选
    		    'birth'=>$user_info['birth'] ? $user_info['birth'] : "",//生日

		);
		if($res_date["user_type"]==2){
		    $res_date["isvideo"] = rand(0, 1);
		    $res_date["isaudio"] = rand(0, 1);
		    $res_date["vip"] = rand(0, 1);
		    $res_date["isphonenumber"] = rand(0, 1);
		    $res_date["gold"] = rand(0, 1);
		}
		$res_date["photosnumber"] = count($res_date["photos"]);
		return 	$res_date;
	}
	public function set_user_lang($lang){
	    $languagecode = $this->get_languagecode();
	    
	    if(!in_array($lang, $languagecode)||$lang==""){
	        $lang = "zh-tw";
	    }
	    return $lang;
	}
	//增加金币
	public function gold_add($uid,$count){
	    $UserBaseM = new UserBaseModel();

	    $res = $UserBaseM->where('uid='.$uid)->setInc('gold',$count);
	    //echo $UserBaseM->getLastSql();
	    return $res;
		
	}
	//消费金币
	public function gold_reduce($uid,$touid,$count,$GoldCount,$type,$productid='1'){
	    $UserBaseM = new UserBaseModel();
	    $res=$UserBaseM->where('uid='.$uid)->setDec('gold',$count);
        if($res){
        $this->get_user($uid,1);
        $ConsumeM = new ConsumeModel();
        $con=array();
        $con['type']=$type;
        $con['leftgold']=$GoldCount-$count;
        $con['uid']=$uid;
        $con['productid'] = $productid;
        $con['paytime']=time();
        $con['money']=$count;
        $con['touid']=$touid;
            $dat=$ConsumeM->addOne($con);
            if($dat){
                return true;
            }
        }else{
            return flase;
        }

	}
	
	/**
	 * 用用户id生成用户目录
	 */
	public function get_userpath($uid,$type){
		return $ret = WR.'/userdata/'.$type.'/'.($uid%100).'/'.($uid%200).'/';
	}
	/**
	 * 用用户id生成用户目录
	 */
	public function get_userurl($uid,$type){
		return $ret = '/userdata/'.$type.'/'.($uid%100).'/'.($uid%200).'/';
	}
	
}


?>