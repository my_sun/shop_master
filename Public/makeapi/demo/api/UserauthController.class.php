<?php
class UserauthController extends BaseController {

	/**
	 * 用户登录接口
	 *
	 * @url http://api.local.com/user/login
	 * @request number(11,11) phone 手机号码
	 * @request string(6,64) password 密码
	 * @response json
	 */
	public function login(){
		$date = $this->Api_recive_date;
		$platforminfo = $this->platforminfo;
		if(($date['uid']!='') && $date['password']!=''){
		    //$this->get_user($date['uid'],1);
		    $UserInfo = $this->get_diy_user_field($date['uid'],"*");

			if($UserInfo['uid']==$date['uid'] && $UserInfo['password']==$date['password']){
				$result = $UserInfo;
			}

			if($result['uid'] != ''){
			    if($UserInfo["user_type"]==3){
			        $puserM = new PUserModel();
			        $Pinfo = array();
			        $Pinfo["uid"] = $date['uid'];
			        $puserM->updateOne($Pinfo,array("isyaoqing"=>1));
			    }else{
			        if($UserInfo["product"]!=$platforminfo['product']){
			            Push_data(array('message'=>$this->L("ZHANGHAOHUOMIMABUZHENGQUE")."1",'code'=>ERRORCODE_201));
			        }
			    }
			    //用户登录时间记录
			    $logintime = time();
			    $this->set_user_field($date['uid'],"logintime",$logintime);
			    $basedata = array();
			    $basedata['logintime'] =$logintime;
			    $User_baseM = new UserBaseModel();
			    $User_baseM->updateOne(array("uid"=>$date['uid']),$basedata);
			    //end
				$login_return = array();
				$UserInfo["token"] = $this->creat_token($UserInfo['uid'],$date['password']);

				$login_return['user'] = $UserInfo;

				Push_data(array('data'=>$login_return));
			}else{
				Push_data(array('message'=>$this->L("ZHANGHAOHUOMIMABUZHENGQUE"),'code'=>ERRORCODE_201));
			}
		}else{
			Push_data(array('message'=>$this->L("ZHANGHAOHUOMIMABUZHENGQUE"),'code'=>ERRORCODE_201));
		}
	}
	/**
	 * 发布悬赏
	 *
	 * @request string title 【nidshia你打】 悬赏标题
	 * @request string describe 问题描述
	 * @request json topics [{"id":0,"name":"${string(0,0)}"},{"id":0,"name":"${string(0,0)}"}] 悬赏话题
	 * @request datetime time ${Y-m-d H:i:s} 时间
	 * @request double money 悬赏金额
	 * @response json
	 * @return data
	 */
    public function regist(){
    	$date = $this->Api_recive_date;
    	$platforminfo= $this->platforminfo;
        $status= include WR."/userdata/publicvar/status.php";
    	if(isset($date['age']))
    	$basedata['age'] = $date['age'];

    	if(isset($date['gender'])){
    		$basedata['gender'] = $date['gender'];
    	}else{
    	    Push_data(array('message'=>$this->L("XINGBIEBUNENGWEIKONG"),'code'=>ERRORCODE_201));
    	}
    	if(isset($date['nickname'])){
			$basedata['nickname'] = $date['nickname'];
		}else{
			//注册时  男性用户默认昵称为“男士”女的默认为“女士”
		    if($basedata['gender']==1){
				$basedata['nickname']=$this->L("NANSHENG");
			}else{
			    $basedata['gender']=2;
				$basedata['nickname']=$this->L("NVSHENG");
			}
		}
		if(isset($date['country'])){
		  $basedata['country'] = $date['country'];
		}else{
		  $basedata['country'] =  $platforminfo['country'];
		}

    	$basedata['area']=$date['area'] ? $date['area'] : 1;//默认地区台北
      	if(isset($platforminfo['language']))
    	$basedata['language'] = $platforminfo['language'];

       	if(isset($platforminfo['phoneid']))
    	$basedata['phoneid'] = $platforminfo['phoneid'];
    	if(isset($platforminfo['product']))
    	$basedata['product'] = $platforminfo['product'];
    	if(isset($platforminfo['fid']))
    	$basedata['fid'] = $platforminfo['fid']? $platforminfo['fid'] :1;
    	if(isset($platforminfo['systemversion']))
    	$basedata['systemversion'] = $platforminfo['systemversion'];
    	if(isset($platforminfo['version']))
    	$basedata['version'] = $platforminfo['version'];


    	if(isset($platforminfo['phonetype']))
    	$extdata['phonetype'] = $platforminfo['phonetype'];
    	if(isset($platforminfo['channelid']))
		$extdata['channelid'] = $date['channelid'];

		if(empty($basedata)){
    		Push_data(array('message'=>$this->L("CANSHUYICHANG"),'code'=>ERRORCODE_201));
    	}else{
    	    $User_baseM = new UserBaseModel();
    	    if($basedata['phoneid']){
    	        /*$pidno = array(
    	            "1234567890"
    	        );*/
    	        $pidno=$status["phoneid"];
    	        if(!in_array($basedata['phoneid'],$pidno)){
    	            $where = array('phoneid'=>$basedata['phoneid'],"product"=>$basedata['product']);
    	            $result = $User_baseM->getOne($where);
    	        }
    	        
    	    }
    	    if($result["uid"]){
    	        $reg_return = array();
    	        $userInfo = $this->get_user($result["uid"]);
    	        $userInfo["token"] = $this->creat_token($userInfo["uid"],$userInfo['password']);
    	        $reg_return['data']['user'] = $userInfo;
    	        $reg_return["message"] = "ok";
    	        Push_data($reg_return);
    	    }else{
    			$timenow = time();
    			
    			$basedata['password'] = rand(20000,99999);
    			$basedata['regtime'] = $timenow;
				$basedata['logintime'] =$timenow;
				$retuid = $User_baseM->addOne($basedata);
				if($retuid){
				    
					$extdata['uid'] = $retuid;
					$User_extendM = new UserExtendModel();
					$User_extendM->addOne($extdata);
		    		$reg_return = array();
		    		$userInfo = $this->creat_userinfo($retuid,$basedata,$extdata);
		    		$userInfo["token"] = $this->creat_token($retuid,$basedata['password']);
		    		$reg_return['data']['user'] = $userInfo;
		    		$AutomsgCon = new AutomsgController();
		    		$AutomsgCon->rundmsg($retuid);
		    		$reg_return["message"] = "ok";
		    		Push_data($reg_return);
				}else{

				    Push_data(array('message'=>$this->L("CANSHUYICHANG"),'code'=>ERRORCODE_201));
				}
    	    }
    	}
    }
    protected function creat_userinfo($retuid,$user_info,$extdata){
        $res_date = array(
            'uid'=>$retuid,//用户id
            'password'=>$user_info['password'] ? $user_info['password'] : '',//用户密码
            'gender'=>$user_info['gender'] ? $user_info['gender'] : '1',//性别 1是男 2是女
            'user_type'=>$user_info['user_type'] ? $user_info['user_type'] : '1',//
            'age'=>$user_info['age'] ? $user_info['age'] : '',//年龄
            'nickname'=>$user_info['nickname'] ? $user_info['nickname'] : '',//昵称
            'vipgrade'=>$user_info['vipgrade'] ? $user_info['vipgrade'] : '0',//会员等级 1是一级 2是二级
            'vip'=>$user_info['vip'] ? $user_info['vip'] : '0',//会员天数
            'viptime'=>$user_info['viptime'] ? $user_info['viptime'] : "0",//vip到期时间,
            'gold'=>$user_info['gold'] ? $user_info['gold'] : '0',//金币数量
            'mood'=>$user_info['mood'] ? $user_info['mood'] : '',//交友宣言
            'blood'=>$user_info['blood'] ? $user_info['blood'] : '',//血型
            'height'=>$user_info['height'] ? $user_info['height'] : "0",//身高
            'weight'=>$user_info['weight'] ? $user_info['weight'] : "0",//体重
            'head'=>$this->get_user_ico($retuid),//头像
            'photos'=>array(),//{image对象列表
            'photosnumber'=>"0",//相片数量
            'area'=>$user_info['area'] ? $user_info['area'] : "1",//居住地
            'income'=>$user_info['income'] ? $user_info['income'] : "0",//收入
            'marriage'=>$user_info['marriage'] ? $user_info['marriage'] : "0",//婚姻状况
            'education'=>$user_info['education'] ? $user_info['education'] : "0",//学历
            'work'=>$user_info['work'] ? $user_info['work'] : "0",//工作
            'constellation'=>$user_info['constellation'] ? $user_info['constellation'] : "0",//星座
            'friendsfor'=>$user_info['friendsfor'] ? $user_info['friendsfor'] : "0",//交友目的
            'cohabitation'=>$user_info['cohabitation'] ? $user_info['cohabitation'] : "0",//婚前同居
            'dateplace'=>$user_info['dateplace'] ? $user_info['dateplace'] : "0",//期望约会的地方
            'lovetimes'=>$user_info['cohabitation'] ? $user_info['lovetimes'] : "0",//恋爱次数
            'charactertype'=>$user_info['charactertype'] ? $user_info['charactertype'] : "0",//性格类型(多选,用|分开)
            'hobby'=>$user_info['hobby'] ? $user_info['hobby'] : "0",//兴趣爱好(多选,用|分开)
            'wantchild'=>$user_info['wantchild'] ? $user_info['wantchild'] : "0",//是否要小孩
            'house'=>$user_info['house'] ? $user_info['house'] : "0",//是否有房
            'car'=>$user_info['car'] ? $user_info['car'] : "0",//是否有车
            'conditions'=>array(),//{ conditions对象}征友条件
            'line'=>$user_info['line'] ? $user_info['line'] : "",//line
            'tinder'=>$user_info['tinder'] ? $user_info['tinder'] : "",//tinder
            'wechat'=>$user_info['wechat'] ? $user_info['wechat'] : "",//wechat
            'facebook'=>$user_info['facebook'] ? $user_info['facebook'] : "",//facebook
            'email'=>$user_info['email'] ? $user_info['email'] : "",//email
            'twitter'=>$user_info['twitter'] ? $user_info['twitter'] : "",//email
            'isphonenumber'=>$user_info['isphonenumber'] ? $user_info['isphonenumber'] : "0",//手机号是否认证
            'phonenumber'=>$user_info['phonenumber'] ? $user_info['phonenumber'] : "",//手机号
            'isvideo'=>$user_info['isvideo'] ? $user_info['isvideo'] : "0",//是否视频认证
            'video'=>array(),//{video对象}视频认证信息
            'isaudio'=>$user_info['isaudio'] ? $user_info['isaudio'] : "0",//是否语音认证
            'audio'=>array(),//{audio对象}  语音认证信息
            'chatsetting'=>array(),//聊天设置对象
            'receivedgifts'=>array(),//{收到的礼物对象列表}收到的礼物
            'sendgifts'=>array(),//{送出的礼物对象列表}
            'usermood'=>array(),//用户动态{用户动态对象}
            'fansnumber'=>"0",//粉丝数量
            'follownumber'=>"0",//我关注的人数量
            'guarduser'=>array(),//守护人对象
            'regtime'=>$user_info['regtime'],//注册时间
            'logintime'=>$user_info['logintime'],//注册时间
            'platformInfo'=>"",//客户端信息对象,
            'country'=>$user_info['country'] ? $user_info['country'] : "TW",//国家,
            'language'=>$this->set_user_lang($user_info['language']),//语言,
            'product'=>$user_info['product'] ? $user_info['product'] : "10508",//product,
            'phoneid'=>$user_info['phoneid'] ? $user_info['phoneid'] : "",//phoneid,
            
            'isopen'=>$user_info['isopen'] ? $user_info['isopen'] : "1",//1公开联系方式，2不公开,
            'exoticlove'=>$user_info['exoticlove'] ? $user_info['exoticlove'] : "",//是否接受异地恋 单选,
            'sexual'=>$user_info['sexual'] ? $user_info['sexual'] : "",//婚前性行为 单选,
            'livewithparents'=>$user_info['livewithparents'] ? $user_info['livewithparents'] : "",//愿意同父母居住 单选
            'personalitylabel'=>$user_info['personalitylabel'] ? $user_info['personalitylabel'] : "",//个性标签 多选
            'liketype'=>$user_info['liketype'] ? $user_info['liketype'] : "",//喜欢的类型 多选
            'glamour'=>$user_info['glamour'] ? $user_info['glamour'] : "",//魅力部位 多选
            
        );
        return $res_date;
    }
    protected function creat_token($uid,$password){

    	$pid = $this->platforminfo["phoneid"];
    	$product = $this->platforminfo["product"];
    	$token = md5($uid.$pid.$password.$product);
    	$redis=$this->redisconn();
    	$redisStr = "token_".$token;
		$redis->set($redisStr,$uid,0,0,C("SESSION_TIMEOUT"));
		return $token;
    }
	public function getpassword(){
	    $filedArr = array("line","tinder","wechat","facebook","phonenumber","mailbox","twitter");
  		$data = $this->Api_recive_date;
  		$filed = $data["field"];
  		if(in_array($filed, $filedArr)){
  		    $where[$filed] = $data["value"];
  		}
  		if(empty($where)){
  		    $return['code'] = ERRORCODE_201;
  		    $return['message'] = $this->L("MEIYOUZHAODAOYONG");
  		}else{
  		    $userextM = new UserExtendModel();
  		   $ret = $userextM->getOne($where);
  		   if($ret["uid"]){
  		       $return['message'] = $this->L("CHENGGONG");
  		       $return['data']= $this->get_user($ret['uid']);
  		   }else{
  		       $return['code'] = ERRORCODE_201;
  		       $return['message'] = $this->L("MEIYOUZHAODAOYONG");
  		   }
  		}

		Push_data($return);
	}
	public function getviptime(){
        if($_POST['uid']){
            $uid=$_POST['uid'];
            $type = $_POST['type'];
            $UserBase = $this->get_diy_user_field($uid,'uid|viptime|gold|nickname');
            if(!$UserBase['uid']){
                $reuslt = array(
                    'status' =>'2',
                   'message'=>'不存在该用户'
                );

                exit(json_encode ($reuslt));
            }
            if($UserBase['viptime']==0||$UserBase['viptime']<time()){
                $days=0;
            }else{
                $days=ceil(($UserBase['viptime']-time())/(60*60*24));
            }

            $gold=$UserBase['gold']?$UserBase['gold']:0;

                $reuslt = array(
                    'status' =>'1',
                    'days' => $days,
                    'gold' => $gold,
                );

                exit(json_encode ($reuslt));



        }
    }
}

?>